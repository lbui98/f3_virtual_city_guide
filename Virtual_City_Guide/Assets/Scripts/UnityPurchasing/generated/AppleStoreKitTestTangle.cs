// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleStoreKitTestTangle
    {
        private static byte[] data = System.Convert.FromBase64String("lZuJiXeMjbiLiYl3uIaOi92Vh4m5mbiHjovdjIOEgNv85/rtw+H8ue+tMSPku+0lUEoS8MCHYURJr4VO6P6/2mR3JJHVhJ2nAod9AUBfo5XADn+FiYGJnoDb/Of67cPh/LgKiYKEgNv85/rtw+H8uZm4h46L3YyDhTOwGOOOTHSFbR3YRRlAMq180EGK3du5n7idjoGiDsAOf4WJgYmegKT9bsQ5mlt1a5fWhfUw0F8o9Gz3SnNu15pJiouJiIkrs7ixuIeOi93DGzOCcCmptkDpUaxA1pf9tlIbU6IOwA5/hYmJg42IuNe5mbiHjovdvbq7vdKfhby4uLu6v7m/vbq7vdJH8CTVMrbX2KPUoQRVSl+in1eqPPy5mbiHjovdjIKEgNv85/rtw+H8TCGDO0fqh3M8NH9TDNagY8+FeWVfbPAgJyDlL0lS4KXuREehg9ika9v85/rtw+H8uJafhbq4uLy4ubm/AyR9FziBsCvm80Ou2IKSaQFZ+v7rIx8So0uiUFg6aV3ShbGT5++gIYyLhIDb/Of67cPh/LmZuIeOi92MyS32gQUytbVF+N9QZWddUv9AG4yIuAqJgooKiYmIU/cYtKT7h1Mfrm/naJNuheBFreZL/oB9woMm8vy0BGn5U8LNe6WRHoveCS7MZv3/09u417mZuIeOi92Mi4SA2/zn+u3D4RFzOwLdF2eVoDDXoln7F73PvzPOuIKOgKOOiY2Nj4uLuIWOgaIOwA5/hYmJg42IiwqJiYg6iGi0eWBh6pRngQDw7UYJegX6b4+S5q6af7o5hIDb/Of67cPh/LmDuIGOi92Mjptb/ph6eOFkReoR+XHoAgzAsEw1A4O4gY6L3YyOm4rd27mfuJ2OgaIONUhelcgW3zGwGP/+2logw/jmMO8N8MBGSE2ameKHhCanjULn8uz3qLmSGQgxN22EQ7QzhszqrnKMoT7BuAqL/LgKitQoi4qJioqJibiFjoGquIWOgaIOwA5/hYmJiY2IiwqJh4OxxEPH5kg71z8dMjEg1cL8I2fnYz/ICbUPtfjZWUzVBS1U9+LQklZIz6DDY9JJo4HepC0igYvzurlUYEfstOUI0bsEQr6OopmTBcFGgHfumgteD/DlETXWO4JoEEZ+YksG5eGJd4yMi4qKDLiejovdla2JiXeMhJZkoxmNL7XH");
        private static int[] order = new int[] { 28,10,35,43,12,43,23,43,39,27,15,15,43,25,27,18,26,23,19,40,35,21,27,23,24,39,33,30,31,30,43,34,40,40,38,40,38,39,40,42,42,42,42,43,44 };
        private static int key = 136;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
