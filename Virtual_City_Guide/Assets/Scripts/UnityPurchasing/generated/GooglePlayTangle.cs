// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("0m0kyxfxzFDgR5XqIFbTH7JujiA+NnLwQiNw9syZQ+5SJagqUQu5mAGzMBMBPDc4G7d5t8Y8MDAwNDEynX1VcifwlREFITL4bli0jW0yOss75RG1zW4++5JtQ6pBN08g0TBzuO2jCaZKoyRmH+tkbateRvuD1zCB+ZhJgd7VKw8LQGXX2baDj/xm6AUqoc3uxsneAVeM/nm5RyVVX/ucE7MwPjEBszA7M7MwMDGWsjyaSPIROipKQLbHv6L22mjMjEYWWPU+/fNqZGvs2YTurbtIQq8qKGSbpI9ITYrZ0/42DepnnHXbOV7R17UG5MTVvehmv/dStLQcfBrFKOhA8aHIg71ZlOa1mileZUgIKg306T1/BQnOyq1RZX3A+K2zCDMyMDEw");
        private static int[] order = new int[] { 2,9,2,5,10,12,8,10,9,12,13,11,13,13,14 };
        private static int key = 49;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
