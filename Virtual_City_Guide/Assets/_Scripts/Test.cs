using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using bunLace.Database;
using System;
using UnityEngine.Purchasing;
using Unity.Services.Core;
using Unity.Services.Core.Environments;
using Proyecto26;
using UnityEngine.Networking;
using UnityEngine.UI;
using DG.Tweening;

public class Test : MonoBehaviour {

    public float duration;
    public int vibrato = 10;
    public float elasticity = 1;

    public Transform t;
    Tweener s;

    public void Jump() {
        StartCoroutine(JumpRoutine());
    }

    private IEnumerator JumpRoutine() {
        if (s != null) yield break;
        Debug.Log("JUMP");
        s = t.DOPunchPosition(Vector3.forward, duration, vibrato, elasticity);
        yield return new WaitForSeconds(duration);
        s = null;
    }
}