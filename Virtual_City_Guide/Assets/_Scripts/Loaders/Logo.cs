using bunLace.Database;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Logo : Loader {
    private Image img;

    private async void LoadHeader() {
        Region region = DatabaseManager.SelectEntry<Region>(GameManager.CurrentRegionID);
        if (!GameManager.debugging) {
            if (region == null || region.IsActive == 0) {
                img.enabled = false;
                return;
            }
        }

        string imgReference = region.LogoImgReference;
        string atlasReference = region.AtlasReference;

        if (string.IsNullOrEmpty(imgReference)) {
            img.enabled = false;
        }
        else {
            img.sprite = await AddressablesManager.Instance.LoadSprite(atlasReference, imgReference);
            img.preserveAspect = true;
            img.enabled = true;
        }

    }

    public override void Load() {
        if (img == null)
            img = GetComponent<Image>();
        LoadHeader();
    }
}
