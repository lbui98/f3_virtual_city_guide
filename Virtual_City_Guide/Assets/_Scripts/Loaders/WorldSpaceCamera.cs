using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldSpaceCamera : MonoBehaviour {
    void Start() {
        GetComponent<Canvas>().worldCamera = Camera.main;
        Destroy(this);
    }
}
