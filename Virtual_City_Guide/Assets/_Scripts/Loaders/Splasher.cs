using Proyecto26;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Splasher : MonoBehaviour {
    private const string splasher_API = "https://stempelpass.de/APP/API/splasher.php";
    [SerializeField] private GameObject helper;

    private void Start() {
        if (InternetReachabilityVerifier.Instance.status != InternetReachabilityVerifier.Status.NetVerified) {
            helper.SetActive(false);
            return;
        }

        var request = new RequestHelper {
            Uri = splasher_API,
            Method = "GET",
            Timeout = 60,
            Retries = 5,
            RetrySecondsDelay = 2,
            RetryCallback = (err, retries) => {
                Debug.Log("Repeat error Splasher: " + err.Message);
                helper.SetActive(false);
            }, //See the error before retrying the request
            EnableDebug = true,
        };

        RestClient.Request(request).Then(response => {
            SplasherImg img = JsonUtility.FromJson<SplasherImg>(response.Text);

            Debug.Log(img.reference);
            if (img == null || string.IsNullOrEmpty(img.reference)) {
                helper.SetActive(false);
                return;
            }

            RectTransform rectTransform = GetComponent<RectTransform>();
            rectTransform.pivot = new Vector2(img.pivotX, img.pivotY);

            AspectRatioFitter aspectRatioFitter = GetComponent<AspectRatioFitter>();
            aspectRatioFitter.aspectMode = AspectRatioFitter.AspectMode.EnvelopeParent;
            aspectRatioFitter.aspectRatio = (float)img.width / img.height;

            LoadSprite(img.reference);
        }).Catch(err => {
            Debug.Log("Splasher error: " + err);
            helper.SetActive(false);
        }).Done();
    }

    private async void LoadSprite(string reference) {
        Image i = GetComponent<Image>();
        Sprite sprite = await AddressablesManager.Instance.LoadSprite(reference);
        i.sprite = sprite;
        helper.SetActive(false);
    }

    [System.Serializable]
    private class SplasherImg {
        public string reference;
        public float pivotX;
        public float pivotY;
        public int width;
        public int height;
    }
}
