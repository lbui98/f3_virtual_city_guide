using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Localization;
using bunLace.Database;
using bunLace.Tools;
using bunLace.Localization;

public class NearestPOI : MonoBehaviour {
    [SerializeField] private Image preview;
    [SerializeField] private TextMeshProUGUI poi;

    private int currentTargetPoiId = -1;
    [SerializeField]
    private bool locationInitiated;

    List<POIRegion> pois;
    Tour currentTour;
    [SerializeField] bool chooseNearestPOI;
    [SerializeField] LocalizedString noGPS;

    private bool warningShown = false;

    private void Start() {
        OnlineMapsLocationService.instance.OnLocationInited += OnLocationServiceInitiated;
    }

    private void OnEnable() {
        if (locationInitiated) {
            OnlineMapsLocationService.instance.OnLocationChanged += CheckPOIs;
            OnTourChange(GameManager.SaveData.tourData.tourID);
        }
        MainManager.OnCityChanged += OnCityChange;
        MainManager.OnTourChanged += OnTourChange;
        MainManager.OnMarkerVisited += OnMarkerVisited;
    }

    private void OnDisable() {
        OnlineMapsLocationService.instance.OnLocationChanged -= CheckPOIs;
        MainManager.OnCityChanged -= OnCityChange;
        MainManager.OnTourChanged -= OnTourChange;
        MainManager.OnMarkerVisited -= OnMarkerVisited;
    }

    private void OnMarkerVisited(int id) {
        OnCityChange();
    }

    private void OnTourChange(int tourId) {
        Debug.Log("BUNNY Tour changed");
        currentTour = DatabaseManager.SelectEntry<Tour>(tourId);
        chooseNearestPOI = (TourTypes)currentTour.TourType == TourTypes.FREE_EXPLORATION;
        CheckPOIs(OnlineMapsLocationService.instance.position);
    }

    private void OnLocationServiceInitiated() {
        OnlineMapsLocationService.instance.OnLocationInited -= OnLocationServiceInitiated;
        OnlineMapsLocationService.instance.OnLocationChanged += CheckPOIs;
        locationInitiated = true;
        OnTourChange(GameManager.SaveData.tourData.tourID);
    }

    private async void CheckPOIs(Vector2 userPos) {
        if (userPos == null || GameManager.CurrentRegionID == -1) return;

        POI targetPOI = null;
        float closestDistance = float.MaxValue;

        if (chooseNearestPOI) {
            pois = DatabaseManager.SelectMultipleEntries<POIRegion>("RegionID = " + GameManager.CurrentRegionID);
            if (pois == null) {
                ShowNoGpsWarning();
                return;
            }

            for (int i = 0; i < pois.Count; i++) {
                POI poi = DatabaseManager.SelectEntry<POI>(pois[i].POIID);
                float distance = MainManager.DistanceInMeters(userPos, new Vector2((float)poi.Longitude, (float)poi.Latitude));
                if (distance < closestDistance) {
                    closestDistance = distance;
                    targetPOI = poi;
                }
            }

        }
        else {
            Debug.Log(currentTour.POIIDs[0].poiID);
            targetPOI = DatabaseManager.SelectEntry<POI>(currentTour.POIIDs[GameManager.SaveData.tourData.currentTourPOIArrayIndex].poiID);
            closestDistance = MainManager.DistanceInMeters(userPos, new Vector2((float)targetPOI.Longitude, (float)targetPOI.Latitude));
        }

        currentTargetPoiId = targetPOI.ID;

        preview.sprite = await AddressablesManager.Instance.LoadSprite(targetPOI.AtlasReference, targetPOI.ImgReference);
        var ratio = preview.GetComponent<AspectRatioFitter>();
        ratio.aspectMode = AspectRatioFitter.AspectMode.EnvelopeParent;
        ratio.aspectRatio = (float)preview.sprite.rect.width / (float)preview.sprite.rect.height;

        string unit;
        if (closestDistance / 1000f >= 1f) {
            closestDistance = closestDistance / 1000f;
            unit = "km";
        }
        else {
            unit = "m";
        }
        poi.text = LocalizationManager.GetLocalizedDBString(targetPOI.NameLocKey) + " (" + closestDistance.ToString("0.00") + unit + ")";
    }


    private void ShowNoGpsWarning() {
        noGPS.GetLocalizedStringAsync().Completed += asyncOp => {
            EasyMessageBox.Show(asyncOp.Result);
        };
    }

    public delegate void BtnNearestPOI(int poiId);
    public static event BtnNearestPOI OnBtnNearestPOIClick;
    public void OnBtnNearestPOI() {
        if (OnlineMapsLocationService.instance.position == Vector2.zero) {
            ShowNoGpsWarning();
            return;
        }

        if (currentTargetPoiId == -1) {
            OnCityChange();
            return;
        }

        OnBtnNearestPOIClick?.Invoke(currentTargetPoiId);
    }

    private void OnCityChange() {
        if (locationInitiated) {
            OnTourChange(GameManager.SaveData.tourData.tourID);
            CheckPOIs(OnlineMapsLocationService.instance.position);
        }
    }
}
