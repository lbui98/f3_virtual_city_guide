using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Loader : MonoBehaviour {
    public void Start() {
        Load();
    }

    public void OnEnable() {
        MainManager.OnCityChanged += Load;
        Load();
    }

    public void OnDisable() {
        MainManager.OnCityChanged -= Load;
    }

    public abstract void Load();
}
