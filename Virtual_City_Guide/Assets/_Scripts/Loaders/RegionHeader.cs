using bunLace.Constants;
using bunLace.Database;
using bunLace.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RegionHeader : Loader {
    private Image headerImg;

    private async void LoadHeader() {
        Region region = DatabaseManager.SelectEntry<Region>(GameManager.CurrentRegionID);
        if (!GameManager.debugging) {
            if (region == null || region.IsActive == 0) return;
        }
        string atlasReference = region.AtlasReference;
        string imgReference = region.ImgReference;

        headerImg.sprite = await AddressablesManager.Instance.LoadSprite(atlasReference, imgReference);
        var ratio = headerImg.GetComponent<AspectRatioFitter>();
        ratio.aspectMode = AspectRatioFitter.AspectMode.EnvelopeParent;
        ratio.aspectRatio = (float)headerImg.sprite.rect.width / (float)headerImg.sprite.rect.height;
    }

    public override void Load() {
        if (headerImg == null)
            headerImg = GetComponent<Image>();
        LoadHeader();
    }
}
