using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetScroll : MonoBehaviour {
    private void OnEnable() {
        RectTransform rt = GetComponent<RectTransform>();
        rt.anchoredPosition = new Vector2(rt.anchoredPosition.x, 0);
    }
}
