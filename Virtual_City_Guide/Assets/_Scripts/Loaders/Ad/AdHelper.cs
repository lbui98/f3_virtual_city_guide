using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdHelper : MonoBehaviour {
    public delegate void CloseAd();
    public event CloseAd OnCloseAd;
    public void Close() {
        OnCloseAd?.Invoke();
    }
}
