using bunLace.Database;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using bunLace.Tools;
using bunLace.Constants;
using UnityEngine.UI;

public class RegionAd : Loader {
    private static int lastRegionAd = -1;
    [SerializeField] private GameObject currentAd;

    public override void Load() {
        if (lastRegionAd == GameManager.CurrentRegionID || currentAd != null) return; //don't load ad if already shown
        currentAd = gameObject; //safety in case load gets called twice
        lastRegionAd = GameManager.CurrentRegionID;
        Region region = DatabaseManager.SelectEntry<Region>(GameManager.CurrentRegionID);
        if (region.IsActive == 0 || string.IsNullOrEmpty(region.AdReference)) {
            currentAd = null;
            return; //no ad
        }

        Addressables.InstantiateAsync(region.AdReference, transform).Completed += asyncOp => {
            currentAd = asyncOp.Result;
            //currentAd.SetActive(false);
            //CanvasFader.Screengrab(currentAd, true);
            currentAd.GetComponent<AdHelper>().OnCloseAd += DestroyAd;
        };
    }

    private void DestroyAd() {
        Debug.Log("Close Ad");
        CanvasFader.Screengrab(currentAd, false);
        currentAd.GetComponent<AdHelper>().OnCloseAd -= DestroyAd;
        Destroy(currentAd, 0.3f);
        currentAd = null;
    }
}
