using bunLace.Database;
using bunLace.Tools;
using bunLace.Constants;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RegionTitle : Loader {
    public override void Load() {
        Region region = DatabaseManager.SelectEntry<Region>(GameManager.CurrentRegionID);

        TMPro.TextMeshProUGUI title = gameObject.FindComponentInChildWithTag<TMPro.TextMeshProUGUI>(Tags.TEXT_REPLACE);
        Image img = GetComponent<Image>();

        title.gameObject.SetActive(region != null);
        img.enabled = region != null;


        if (region != null) {
            if (region.IsActive != 0 || GameManager.debugging) {
                Debug.Log(region.Name);
                title.text = region.Name;
            }
        }
        else {
            Debug.Log("nothing");
            title.text = string.Empty;
        }
    }
}
