using bunLace.Database;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RegionLink : Loader {
    public override void Load() {
        Button btn = GetComponent<Button>();
        btn.onClick.RemoveAllListeners();
        string link;
        if (GameManager.CurrentRegionID == -1) {
            btn.onClick.AddListener(delegate { GameManager.Instance.OpenURL(bunLace.Constants.Values.GENERAL_REGION_URL); });
            link = bunLace.Constants.Values.GENERAL_REGION_URL;
        }
        else {
            Region region = DatabaseManager.SelectEntry<Region>(GameManager.CurrentRegionID);
            if (string.IsNullOrEmpty(region.Link)) {
                btn.onClick.AddListener(delegate { GameManager.Instance.OpenURL(bunLace.Constants.Values.GENERAL_REGION_URL); });
                link = bunLace.Constants.Values.GENERAL_REGION_URL;
            }
            else {
                link = region.Link;
                btn.onClick.AddListener(delegate { GameManager.Instance.OpenURL(region.Link); });
            }
        }
        link = link.Replace("https://", "").Replace("http://", "").TrimEnd('/');

        btn.GetComponentInChildren<TextMeshProUGUI>().text = link;
    }
}
