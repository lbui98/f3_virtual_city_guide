using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlexibleGridLayout : LayoutGroup {
    public enum FitType { UNIFORM, WIDTH, HEIGHT, FIXED_ROWS, FIXED_COLUMNS, SAME_WIDTH, SAME_HEIGHT }

    public FitType fitType;
    public Vector2 spacing;

    [SerializeField]
    private int rows, columns;
    [SerializeField]
    private Vector2 cellSize;
    private bool fitX, fitY;

    public override void CalculateLayoutInputVertical() {
        base.CalculateLayoutInputHorizontal();

        if (fitType == FitType.WIDTH || fitType == FitType.HEIGHT || fitType == FitType.UNIFORM || fitType == FitType.SAME_HEIGHT || fitType == FitType.SAME_WIDTH) {
            fitX = true;
            fitY = true;
            float sqrRt = Mathf.Sqrt(transform.childCount);
            rows = Mathf.CeilToInt(sqrRt);
            columns = Mathf.CeilToInt(sqrRt);
        }

        if (fitType == FitType.WIDTH || fitType == FitType.FIXED_COLUMNS || fitType == FitType.SAME_WIDTH) {
            rows = Mathf.CeilToInt(transform.childCount / (float)columns);
        }
        if (fitType == FitType.HEIGHT || fitType == FitType.FIXED_ROWS || fitType == FitType.SAME_HEIGHT) {
            columns = Mathf.CeilToInt(transform.childCount / (float)rows);
        }

        float parentWidth = rectTransform.rect.width;
        float parentHeight = rectTransform.rect.height;

        float cellWidth = parentWidth / (float)columns - ((spacing.x / (float)columns) * 2) - (padding.left / (float)columns) - (padding.right / (float)columns);
        float cellHeight = parentHeight / (float)rows - ((spacing.y / (float)rows) * 2) - (padding.top / (float)rows) - (padding.bottom / (float)rows);

        cellSize.x = fitX ? cellWidth : cellSize.x;
        cellSize.y = fitY ? cellHeight : cellSize.y;

        if(fitType == FitType.SAME_HEIGHT) {
            cellSize.x = cellSize.y;
        }
        if(fitType == FitType.SAME_WIDTH) {
            cellSize.y = cellSize.x;
        }

        int columnCount = 0;
        int rowCount = 0;

        for (int i = 0; i < rectChildren.Count; i++) {
            rowCount = i / columns;
            columnCount = i % columns;

            var item = rectChildren[i];

            var xPos = (cellSize.x * columnCount) + (spacing.x * columnCount) + padding.left;
            var yPos = (cellSize.y * rowCount) + (spacing.y * rowCount) + padding.top;

            SetChildAlongAxis(item, 0, xPos, cellSize.x);
            SetChildAlongAxis(item, 1, yPos, cellSize.y);
        }

        if (GetComponent<ContentSizeFitter>() != null && fitType == FitType.SAME_WIDTH) {
            var totalMinWidth = (columns * (cellSize.x + spacing.x)) + padding.left + padding.right;
            var totalMinHeight = (rows * (cellSize.y + spacing.y)) + padding.top + padding.bottom;

            SetLayoutInputForAxis(totalMinWidth, -1, -1, 0);
            SetLayoutInputForAxis(totalMinHeight, -1, -1, 1);
        }
    }

    public override void CalculateLayoutInputHorizontal() {

    }

    public override void SetLayoutHorizontal() {
    }

    public override void SetLayoutVertical() {
    }
}
