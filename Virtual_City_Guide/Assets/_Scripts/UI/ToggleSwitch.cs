using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using bunLace.Tools;
using bunLace.Constants;

[RequireComponent(typeof(Toggle))]
public class ToggleSwitch : MonoBehaviour {
    [SerializeField] private Sprite on, off;
    private Image img;

    private void Start() {
        img = gameObject.FindComponentInChildWithTag<Image>(Tags.TARGET);
        Toggle toggle = GetComponent<Toggle>();
        toggle.onValueChanged.AddListener(delegate { SwitchSprite(toggle); });
    }

    private void SwitchSprite(Toggle toggle) {
        Sprite target = toggle.isOn ? on : off;
        img.sprite = target;
    }
}
