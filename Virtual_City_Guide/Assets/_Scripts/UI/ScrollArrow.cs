using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using bunLace.Tools;

public class ScrollArrow : MonoBehaviour {
    public Direction direction;
    private Image img;

    private void Start() {
        img = GetComponent<Image>();
        ScrollRect scrollRect = GetComponentInParent<ScrollRect>();
        scrollRect.onValueChanged.AddListener(OnValueChange);
    }

    private void OnEnable() {
        ScrollRect scrollRect = GetComponentInParent<ScrollRect>();
        OnValueChange(scrollRect.content.anchoredPosition);
    }

    public void OnValueChange(Vector2 value) {
        if (direction == Direction.LEFT || direction == Direction.RIGHT) {
            float x = Mathf.Clamp(value.x, 0f, 1f);
            if (direction == Direction.LEFT) {
                img.DOFade(x <= 0.07f ? 0f : 1f, 0.25f);
            }
            else {
                img.DOFade(x >= 0.95f ? 0f : 1f, 0.25f);
            }
        }
        else {
            float y = Mathf.Clamp(value.y, 0f, 1f);
            if (direction == Direction.DOWN) {
                img.DOFade(y <= 0.07f ? 0f : 1f, 0.25f);
            }
            else {
                img.DOFade(y >= 0.95f ? 0f : 1f, 0.25f);
            }
        }


    }
}
