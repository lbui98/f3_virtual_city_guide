using UnityEngine;
using UnityEngine.Localization;
using UnityEngine.SceneManagement;
using bunLace.Localization;
using bunLace.Constants;
using Directus;
using Proyecto26;
using Cysharp.Threading.Tasks;

public class StartMenuManager : MonoBehaviour {
    public DirectusContent directusContent;
    private readonly string update_API = "https://stempelpass.de/APP/API/updateCheck.php";

#if UNITY_ANDROID
    private readonly string playstore_link = "https://play.google.com/store/apps/details?id=de.wdv.stempelpass&hl=de";
#elif UNITY_IOS
    private readonly string playstore_link = "https://apps.apple.com/de/app/stempelpass/id1255873088";
#else
    private readonly string playstore_link = "https://play.google.com/store/apps/details?id=de.wdv.stempelpass&hl=de";
#endif

    [SerializeField] private LocalizedString updateNotice, initalDownloadError;
    [SerializeField] private GameObject panelPrivacy;

    private void Start() {
        //StartDownload();

        if (PlayerPrefs.HasKey(Values.PRIVACY_PLAYER_PREF)) {
            CheckUpdate();
        }
        else {
            //display Privacy
            panelPrivacy.SetActive(true);
        }
    }

    public void AcceptPrivacy() {
        PlayerPrefs.SetInt(Values.PRIVACY_PLAYER_PREF, 1);
        CheckUpdate();
        PlayerPrefs.Save();
        panelPrivacy.SetActive(false);
    }

    public void OpenURL(string url) {
        GameManager.Instance.OpenURL(url);
    }

    private async UniTask CheckUpdate() {
        string retry, continueNoNet;
        if (Values.LOCALIZED_TERMS != null && Values.LOCALIZED_TERMS.Count > 0) {
            retry = Values.LOCALIZED_TERMS[Values.LOC_KEY_RETRY];
            continueNoNet = Values.LOCALIZED_TERMS[Values.LOC_KEY_CONTINUE];
        }
        else {
            retry = await LocalizationManager.GetLocalizedUIString(Values.LOC_KEY_RETRY);
            continueNoNet = await LocalizationManager.GetLocalizedUIString(Values.LOC_KEY_CONTINUE);
        }

        //check if no internet and initial download has been made
        if (InternetReachabilityVerifier.Instance.status != InternetReachabilityVerifier.Status.NetVerified) {
            if (PlayerPrefs.HasKey(Values.INITIAL_DOWNLOAD_PLAYER_PREF))
                StartDownload();
            else {
                //initial download hasn't been completed
                string err = await initalDownloadError.GetLocalizedStringAsync();
                EasyMessageBox.Show(err,
                    button1Text: retry,
                    button1Action: () => { CheckUpdate(); },
                    button2Text: continueNoNet,
                    button2Action: () => { StartDownload(); }
                );
            }
            return;
        }

        var request = new RequestHelper {
            Uri = update_API,
            Method = "GET",
            Timeout = 60,
            Retries = 5,
            RetrySecondsDelay = 2,
            RetryCallback = (err, retries) => { Debug.Log("Repeat error Check version: " + err.Message); }, //See the error before retrying the request
            EnableDebug = true,
        };

        string localizedUpdateNotice = await updateNotice.GetLocalizedStringAsync();


        RestClient.Request(request).Then(response => {
            System.Version appVersion = new System.Version(Application.version);
            System.Version lastSupportVersion = new System.Version(response.Text);

            Debug.Log($"BUNNY: This app version is {appVersion}, the last supported version is {lastSupportVersion}");

            if (string.IsNullOrEmpty(GameManager.SaveData.version)) {
                //first record
                //force db update
                PlayerPrefs.DeleteKey("directusDb");
            }
            else {
                System.Version lastSavedVersion = new System.Version(GameManager.SaveData.version);
                if (lastSavedVersion.CompareTo(appVersion) != 0) {
                    //the current installed version is different from what was last recorded
                    //force db update
                    PlayerPrefs.DeleteKey("directusDb");
                }
            }
            GameManager.SaveData.version = Application.version;


            if (appVersion.CompareTo(lastSupportVersion) >= 0) {
                directusContent.GetCMSToken();
                //StartDownload();

                PlayerPrefs.SetInt(Values.INITIAL_DOWNLOAD_PLAYER_PREF, 1);
            }
            else {
                //current version is not up to date
                //force update
                EasyMessageBox.Show(localizedUpdateNotice,
                    button1Text: retry,
                    button1Action: () => { Application.OpenURL(playstore_link); },
                    button2Text: continueNoNet,
                    button2Action: () => { Application.Quit(); }
                    );
            }

        }).Catch(err => Debug.Log("Update Check Error: " + err)).Done();
    }

    private void OnEnable() {
        AddressablesManager.OnDownloadProgress += OnDownloadProgress;
        Directus.DirectusDBUpdate.OnDBWritingDone += StartDownload;
        DirectusContent.OnNoUpdate += StartDownload;
    }

    private void OnDisable() {
        AddressablesManager.OnDownloadProgress -= OnDownloadProgress;
        Directus.DirectusDBUpdate.OnDBWritingDone -= StartDownload;
        DirectusContent.OnNoUpdate -= StartDownload;
    }

    private async UniTask StartDownload() {
        string[] dependencies;
        if (GameManager.SaveData.currentRegionID != -1) {
            //preload region
            dependencies = new string[] { "Preload", GameManager.SaveData.currentRegionID.ToString() };
        }
        else {
            dependencies = new string[] { "Preload" };
        }
        await AddressablesManager.DownloadDependencies(dependencies);
    }

    private void OnDownloadProgress(float progress) {
        if (progress == 1f) {
            SceneManager.LoadScene(Scenes.MAIN);
        }
    }
}
