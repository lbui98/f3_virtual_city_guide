using bunLace.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Localization;
using UnityEngine.UI;

public class Tab : MonoBehaviour {
    [System.Serializable]
    public class TabInfo {
        public LocalizedString titleKey;
        public int id;
        public Image btnImg;
        [HideInInspector] public int tabSortOrder;
    }

    public TabInfo info;
}
