using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using bunLace.Singleton;

public class DownloadProgress : Singleton<DownloadProgress> {
    [SerializeField] private TextMeshProUGUI progress, action;
    [SerializeField] private Slider progressBar;
    [SerializeField] private bool setInactiveAfterDone = true;

    private void OnEnable() {
        AddressablesManager.OnDownloadProgress += OnDownloadProgress;
    }

    private void OnDisable() {
        AddressablesManager.OnDownloadProgress -= OnDownloadProgress;
    }

    public void OnDownloadProgress(float progress) {
        progressBar.value = progress;
        this.progress.text = progress.ToString("p");

        if (progress == 1f && setInactiveAfterDone) {
            gameObject.SetActive(false);
        }
    }

    public void UpdateProgressText(string progress) {
        if (action == null) return;
        Instance.action.text = progress;
    }
}
