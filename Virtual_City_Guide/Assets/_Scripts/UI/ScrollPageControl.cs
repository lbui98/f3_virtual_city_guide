using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI.Extensions;

[RequireComponent(typeof(HorizontalScrollSnap))]
public class ScrollPageControl : MonoBehaviour {
    [System.Serializable]
    private class SnapItem {
        public int pageNumber;
        public UnityEngine.Events.UnityEvent
            OnPageSelected,
            OnPageUnselected;
    }

    [SerializeField] private SnapItem[] snapItems;
    private Dictionary<int, UnityEngine.Events.UnityEvent> control = new Dictionary<int, UnityEngine.Events.UnityEvent>();

    private void Start() {
        foreach (SnapItem snapItem in snapItems) {
            control.Add(snapItem.pageNumber, snapItem.OnPageSelected);
        }
    }

    private void OnEnable() {
        GetComponent<HorizontalScrollSnap>().OnSelectionPageChangedEvent.AddListener(OnPageChanged);
    }

    private void OnDisable() {
        GetComponent<HorizontalScrollSnap>().OnSelectionPageChangedEvent.RemoveAllListeners();
    }

    private void OnPageChanged(int page) {
        foreach (SnapItem snapItem in snapItems) {
            snapItem.OnPageUnselected?.Invoke();
        }

        if (control.ContainsKey(page)) {
            control[page]?.Invoke();
        }
    }
}
