using bunLace.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class TabGroup : MonoBehaviour {
    public Dictionary<int, Tab> tabsDict = new Dictionary<int, Tab>();
    [SerializeField] private bool animateFade = false;
    [SerializeField] private bool changeColor = false;
    private int currentTab;
    private void Awake() {
        Initialize();
    }

    private void OnEnable() {
        GameManager.OnPaletteUpdate += OnPaletteUpdated;
    }
    private void OnDisable() {
        GameManager.OnPaletteUpdate -= OnPaletteUpdated;
    }

    public void Initialize() {
        if (tabsDict.Count > 0) return;

        Tab[] tabs = GetComponentsInChildren<Tab>(true);
        foreach (Tab tab in tabs) {
            tabsDict.Add(tab.info.id, tab);

            if (tab.info.btnImg != null) {
                Canvas canvas = tab.info.btnImg.GetComponentInParent<Canvas>();
                if (canvas != null) {
                    tab.info.tabSortOrder = canvas.sortingOrder;
                }
                //Destroy(tab);
            }
        }
    }

    public void OnTabSelect(int tabTypeToActivate) {
        currentTab = tabTypeToActivate;
        foreach (Tab tab in tabsDict.Values) {

            if (animateFade) {
                StartCoroutine(FadeTab(tab.info.id == tabTypeToActivate, tab.gameObject));
            }
            else {
                tab.gameObject.SetActive(tab.info.id == tabTypeToActivate);
            }

            if (changeColor) {
                Color targetColor = tab.info.id == tabTypeToActivate ? GameManager.Instance.GetPaletteColor(Palette.ColorGroups.SELECTED) : GameManager.Instance.GetPaletteColor(Palette.ColorGroups.UNSELECTED);
                tab.info.btnImg.DOColor(targetColor, bunLace.Constants.Values.COLOR_FADE_DURATION);
            }
        }
    }

    private IEnumerator FadeTab(bool active, GameObject tab) {
        tab.SetActive(true);
        CanvasGroup canvasGroup = tab.GetComponent<CanvasGroup>();
        if (canvasGroup == null) {
            Debug.Log("No canvas group was attached to the tab; adding it now");
            canvasGroup = tab.AddComponent<CanvasGroup>();
        }
        float targetAlpha = active ? 1f : 0f;
        canvasGroup.DOFade(targetAlpha, bunLace.Constants.Values.FADE_PANEL_DURATION);
        yield return new WaitForSeconds(bunLace.Constants.Values.FADE_PANEL_DURATION);
        tab.SetActive(active);
    }

    private void OnPaletteUpdated() {
        foreach (Tab tab in tabsDict.Values) {
            //Color targetColor = tab.info.id == currentTab ? GameManager.Instance.GetPaletteColor(Palette.ColorGroups.SELECTED) : GameManager.Instance.GetPaletteColor(Palette.ColorGroups.UNSELECTED);
            tab.gameObject.SetActive(tab.info.id == currentTab);
            //tab.info.btnImg.DOColor(targetColor, bunLace.Constants.Values.COLOR_FADE_DURATION);
        }
    }
}
