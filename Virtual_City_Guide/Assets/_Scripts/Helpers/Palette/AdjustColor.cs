using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AdjustColor : MonoBehaviour {
    public Palette.ColorGroups colorGroup;
    public bool keepAlpha = true;

    void Start() {
        OnPaletteUpdate();
    }

    public void OnPaletteUpdate() {
        Color c = GameManager.Instance.GetPaletteColor(colorGroup);
        Image img = GetComponent<Image>();
        if (img != null) {
            if (keepAlpha) {
                c = new Color(c.r, c.g, c.b, img.color.a);
            }
            img.color = c;
            return;
        }

        TMPro.TextMeshProUGUI text = GetComponent<TMPro.TextMeshProUGUI>();
        if (text != null) {
            if (keepAlpha) {
                c = new Color(c.r, c.g, c.b, text.color.a);
            }
            text.color = c;
        }
        else {
            Debug.LogWarning("PALETTE CHANGE: No Image or Text detected");
        }
    }
}
