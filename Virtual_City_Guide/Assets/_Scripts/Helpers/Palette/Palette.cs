using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Palette {
    public enum ColorGroups { BACKGROUND, FOREGROUND, SELECTED, UNSELECTED, TEXT_DARK, TEXT_LIGHT }
    public ColorGroups colorGroup;
    public Color color;

    public Palette(ColorGroups colorGroup, Color color) {
        this.colorGroup = colorGroup;
        this.color = color;
    }
}
