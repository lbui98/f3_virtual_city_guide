using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaletteManager : MonoBehaviour {
    [SerializeField] AdjustColor[] palette;

    private void Start() {
        palette = GameObject.FindObjectsOfType<AdjustColor>(true);
    }

    private void OnEnable() {
        GameManager.OnPaletteUpdate += OnPaletteUpdate;
    }

    private void OnDisable() {
        GameManager.OnPaletteUpdate -= OnPaletteUpdate;
    }

    private void OnPaletteUpdate() {
        for (int i = 0; i < palette.Length; i++) {
            palette[i].OnPaletteUpdate();
        }
    }
}
