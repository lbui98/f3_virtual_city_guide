using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using bunLace.Database;
using bunLace.Constants;
using bunLace.Localization;
using bunLace.Tools;
using bunLace.SaveLoad;
using System.Linq;

public class Achievement : MonoBehaviour {
    [SerializeField] private TextMeshProUGUI title, progressText;
    [SerializeField] private Image img;
    [SerializeField] private GameObject particles;
    private string description;
    private int progress;

    private bool initialized = false;
    public void Initialize(Achievements achievement, Sprite sprite, bool all) {
        TextMeshProUGUI title = gameObject.FindComponentInChildWithTag<TextMeshProUGUI>(Tags.NAME_REPLACE);
        TextMeshProUGUI progressPercentage = gameObject.FindComponentInChildWithTag<TextMeshProUGUI>(Tags.TEXT_REPLACE);

        description = LocalizationManager.GetLocalizedDBString(achievement.DescriptionLocKey);

        if (achievement.IsRegional == 1 && all) {
            Region region = DatabaseManager.SelectEntry<Region>(achievement.RegionID);
            title.text = $"{LocalizationManager.GetLocalizedDBString(achievement.NameLocKey)}<size={Values.FONT_SUB_SIZE_TINY}>\n{region.Name}";
        }
        else {
            title.text = $"{LocalizationManager.GetLocalizedDBString(achievement.NameLocKey)}";
        }

        Image img = gameObject.FindComponentInChildWithTag<Image>(Tags.IMG_REPLACE);
        img.sprite = sprite;
        var ratio = img.GetComponent<AspectRatioFitter>();
        ratio.aspectMode = AspectRatioFitter.AspectMode.EnvelopeParent;
        ratio.aspectRatio = (float)img.sprite.rect.width / (float)img.sprite.rect.height;

        GetProgress(achievement);
        initialized = true;
    }

    private void GetProgress(Achievements achievement) {
        progress = GetAchievementProgress(achievement);
        if (progress == 100) {
            this.progressText.text = string.Empty;
            img.color = Color.white;
            img.material = null;
            particles.SetActive(true);
        }
        else {
            this.progressText.text = progress + "%";
            img.color = Color.gray;
        }
    }

    private int GetAchievementProgress(Achievements achievement) {
        string ConditionsJson = achievement.ConditionsJSON;
        AchievementCondition achievementConditions = JsonUtility.FromJson<AchievementCondition>(ConditionsJson);

        int totalConditions = 0;
        int clearedConditions = 0;

        List<POIRegion> poiRegions = DatabaseManager.SelectMultipleEntries<POIRegion>($"RegionID = {achievement.RegionID}");
        List<AdditionalContent> acs = new List<AdditionalContent>();
        for (int i = 0; i < poiRegions.Count; i++) {
            List<AdditionalContentPOI> additionalContentPOIs = DatabaseManager.SelectMultipleEntries<AdditionalContentPOI>($"POIID = {poiRegions[i].POIID}");
            if (additionalContentPOIs == null) continue;
            for (int j = 0; j < additionalContentPOIs.Count; j++) {
                AdditionalContent ac = DatabaseManager.SelectEntry<AdditionalContent>(additionalContentPOIs[j].AdditionalContentID);
                acs.Add(ac);
            }
        }


        if (achievement.IsRegional == 1) {
            //quiz conditions; certain quizzes cleared
            if (achievementConditions.quizzesToBeCleared != null || achievementConditions.quizzesToBeCleared.Length > 0) {
                totalConditions += achievementConditions.quizzesToBeCleared.Length;

                for (int i = 0; i < achievementConditions.quizzesToBeCleared.Length; i++) {
                    QuizData quizData = GameManager.SaveData.quizData.FirstOrDefault(data => data.id == achievementConditions.quizzesToBeCleared[i] && data.cleared);
                    if (quizData != null) clearedConditions++;
                }
            }

            //quiz conditions; certain number of quizzes cleared
            if (achievementConditions.numberOfQuizzesCleared > 0) {
                int cleared = 0;
                totalConditions += achievementConditions.numberOfQuizzesCleared;
                for (int i = 0; i < acs.Count; i++) {
                    if (acs[i].MediaType == (int)MediaTypes.QUIZ) {
                        QuizData quizData = GameManager.SaveData.quizData.FirstOrDefault(data => data.id == acs[i].ID && data.cleared);
                        if (quizData != null) cleared++;
                    }
                }
                clearedConditions += Mathf.Clamp(cleared, 0, achievementConditions.numberOfQuizzesCleared);
            }

            //quiz conditions; certain number of quizzes cleared
            if (achievementConditions.numberOfQuizzesFirstTimeCleared > 0) {
                int cleared = 0;
                totalConditions += achievementConditions.numberOfQuizzesFirstTimeCleared;
                for (int i = 0; i < acs.Count; i++) {
                    if (acs[i].MediaType == (int)MediaTypes.QUIZ) {
                        QuizData quizData = GameManager.SaveData.quizData.FirstOrDefault(data => data.id == acs[i].ID && data.firstTimeClear);
                        if (quizData != null) cleared++;
                    }
                }
                clearedConditions += Mathf.Clamp(cleared, 0, achievementConditions.numberOfQuizzesFirstTimeCleared);
            }

            //poi conditions; certain visited
            if (achievementConditions.poisToBeVisited != null || achievementConditions.poisToBeVisited.Length > 0) {
                totalConditions += achievementConditions.poisToBeVisited.Length;

                for (int i = 0; i < achievementConditions.poisToBeVisited.Length; i++) {
                    bool visited = GameManager.Instance.HasVisited(achievementConditions.poisToBeVisited[i]);
                    if (visited) clearedConditions++;
                }
            }

            //poi conditions; certain number visited    
            if (achievementConditions.numberOfpoisVisited > 0) {
                int cleared = 0;

                totalConditions += achievementConditions.numberOfpoisVisited;
                for (int i = 0; i < poiRegions.Count; i++) {
                    bool visited = GameManager.Instance.HasVisited(poiRegions[i].POIID);
                    if (visited) cleared++;
                }
                clearedConditions += Mathf.Clamp(cleared, 0, achievementConditions.numberOfpoisVisited);
            }

            //minigame conditions; certain cleared
            if (achievementConditions.minigamesToBeCleared != null || achievementConditions.minigamesToBeCleared.Length > 0) {
                totalConditions += achievementConditions.minigamesToBeCleared.Length;

                for (int i = 0; i < achievementConditions.minigamesToBeCleared.Length; i++) {
                    bool visited = GameManager.SaveData.minigameData.Contains(achievementConditions.minigamesToBeCleared[i]);
                    if (visited) clearedConditions++;
                }
            }

            //minigame conditions; certain number cleared
            if (achievementConditions.numberOfMinigamesCleared > 0) {
                int cleared = 0;

                totalConditions += achievementConditions.numberOfMinigamesCleared;
                for (int i = 0; i < acs.Count; i++) {
                    if (acs[i].MediaType == (int)MediaTypes.MINIGAME) {
                        if (GameManager.SaveData.minigameData.Contains(acs[i].ID)) cleared++;
                    }
                }

                clearedConditions += Mathf.Clamp(cleared, 0, achievementConditions.numberOfMinigamesCleared);
            }
        }
        else {
            Debug.Log("BUNNY Checking universal achievement...");

            //List<Achievements> achievements = DatabaseManager.SelectMultipleEntries<Achievements>($"IsRegional = 1");

            //bool certain = false;
            //bool number = false;
            //int clearedAchievements = 0;

            //if (achievementConditions.achievementsToCollect != null || achievementConditions.achievementsToCollect.Length > 0) {
            //    certain = true;
            //    totalConditions += achievementConditions.achievementsToCollect.Length;
            //    Debug.Log("BUNNY has achievements to collect " + achievementConditions.achievementsToCollect.Length);
            //}

            //if (achievementConditions.numberAchievementsToCollect > 0) {
            //    number = true;
            //    totalConditions += achievementConditions.numberAchievementsToCollect;
            //    Debug.Log("BUNNY has number of achievements to collect " + achievementConditions.numberAchievementsToCollect);
            //}

            //for (int i = 0; i < achievements.Count; i++) {
            //    float progress = GetAchievementProgress(achievements[i]);
            //    Debug.Log("BUNNY Progress is at " + progress);
            //    if (progress == 100) {
            //        if (number) clearedAchievements++;
            //        if (certain && achievementConditions.achievementsToCollect.Contains(achievements[i].ID)) {
            //            clearedConditions++;
            //        }
            //    }
            //}

            //clearedConditions += Mathf.Clamp(clearedConditions, 0, achievementConditions.numberAchievementsToCollect);

            //poi conditions; certain number of pois cleared
            if (achievementConditions.numberOfpoisVisited > 0) {
                int cleared = 0;

                totalConditions += achievementConditions.numberOfpoisVisited;
                if (GameManager.SaveData.poiData != null) {
                    cleared += GameManager.SaveData.poiData.Count;
                }
                clearedConditions += Mathf.Clamp(cleared, 0, achievementConditions.numberOfpoisVisited);
            }


            //quiz conditions; certain number of quizzes cleared
            if (achievementConditions.numberOfQuizzesCleared > 0) {
                int cleared = 0;
                totalConditions += achievementConditions.numberOfQuizzesCleared;
                if (GameManager.SaveData.quizData != null) {
                    for (int i = 0; i < GameManager.SaveData.quizData.Count; i++) {
                        if (GameManager.SaveData.quizData[i].cleared)
                            cleared++;
                    }
                }
                clearedConditions += Mathf.Clamp(cleared, 0, achievementConditions.numberOfQuizzesCleared);
            }

            //quiz conditions; certain number of quizzes first cleared
            if (achievementConditions.numberOfQuizzesFirstTimeCleared > 0) {
                int cleared = 0;
                totalConditions += achievementConditions.numberOfQuizzesFirstTimeCleared;
                if (GameManager.SaveData.quizData != null) {
                    for (int i = 0; i < GameManager.SaveData.quizData.Count; i++) {
                        if (GameManager.SaveData.quizData[i].firstTimeClear)
                            cleared++;
                    }
                }
                clearedConditions += Mathf.Clamp(cleared, 0, achievementConditions.numberOfQuizzesFirstTimeCleared);
            }

            //minigame conditions; certain number cleared
            if (achievementConditions.numberOfMinigamesCleared > 0) {
                int cleared = 0;

                totalConditions += achievementConditions.numberOfMinigamesCleared;
                if (GameManager.SaveData.minigameData != null) {
                    cleared = GameManager.SaveData.minigameData.Count;
                }

                clearedConditions += Mathf.Clamp(cleared, 0, achievementConditions.numberOfMinigamesCleared);
            }

        }

        return Mathf.Clamp(Mathf.FloorToInt((float)clearedConditions / (float)totalConditions * 100), 0, 100);
    }

    public delegate void BtnAchievement(string name, string description, int progress);
    public static event BtnAchievement OnBtnAchievement;

    public void OnBtnClick() {
        if (initialized)
            OnBtnAchievement?.Invoke(title.text, description, progress);
    }
}
