using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class InternetReachabilityIndicator : MonoBehaviour {
    [SerializeField] private Sprite reachable, notReachable;
    private Image image;

    private void Start() {
        image = GetComponent<Image>();
    }

    // Start is called before the first frame update
    void OnEnable() {
        if (image == null)
            image = GetComponent<Image>();

        if (InternetReachabilityVerifier.Instance.status == InternetReachabilityVerifier.Status.NetVerified) {
            image.sprite = reachable;
        }
        else {
            image.sprite = notReachable;
        }

        InternetReachabilityVerifier.Instance.statusChangedDelegate += OnStatusChanged;
    }

    private void OnDisable() {
        InternetReachabilityVerifier.Instance.statusChangedDelegate -= OnStatusChanged;

    }

    private void OnStatusChanged(InternetReachabilityVerifier.Status newStatus) {
        if (newStatus == InternetReachabilityVerifier.Status.NetVerified) {
            image.sprite = reachable;
        }
        else {
            image.sprite = notReachable;
        }
    }
}
