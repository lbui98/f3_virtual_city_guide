using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPS : MonoBehaviour {

    private TMPro.TextMeshProUGUI fps;

    // Start is called before the first frame update
    void Start() {
        fps = GetComponent<TMPro.TextMeshProUGUI>();
        InvokeRepeating(nameof(GetFPS), 1, 1);
    }

    private void GetFPS() {
        int fpsCount = (int)(1f / Time.unscaledDeltaTime);
        fps.text = fpsCount + " FPS";
    }
}
