using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using bunLace.Constants;
using Cysharp.Threading.Tasks;
using System;

namespace bunLace.Tools {
    public enum TourTypes { GUIDED_TOUR, SCAVENGER_HUNT, FREE_EXPLORATION }
    public enum MediaTypes { SINGLE_IMAGE, AUDIO_PLAYER, VIDEO_PLAYER, CUSTOM_AUDIO_VISUAL, AR_PLACEMENT, AR_SCAN, QUIZ, MINIGAME, SELFIE, AR_PLACEMENT_MULTI }
    public enum TabTypes { MEDIA_GUIDE, ADDITIONAL_CONTENT, AR, QUIZ, MINIGAMES }
    public enum HomeTabTypes { LOGBOOK, ACHIEVEMENTS, CITIES, HELP, SETTINGS, COMMUNITY }
    public enum MainMenuTabTypes { HOME, SIGHTS, BOOKMARKED, TOURS }
    public enum Direction { LEFT, RIGHT, UP, DOWN }
    public enum SynchronisationOptions { OVERWRITE_NEWER, FORCE_LOCAL_OVERWRITE, FORCE_DB_OVERWRITE, CLEAR_LOCAL }

    public enum POITypes { POI, WASHROOM, GASTRONOMY, SHOP, }

    public static class Utilities {
        public static bool blocked;

        public static Direction GetOppositeDirection(Direction direction) {
            if ((int)direction <= 1) {
                return direction == Direction.LEFT ? Direction.RIGHT : Direction.LEFT;
            }
            else {
                return direction == Direction.UP ? Direction.DOWN : Direction.UP;
            }
        }

        /// <summary>
        /// Moves two panels in the same direction, one out of screen, one into the screen
        /// </summary>
        /// <param name="rt1">Panel that slides out of screen</param>
        /// <param name="rt2">Panel that slides into screen</param>
        /// <param name="direction">the direction they slide into</param>
        public static void MovePanels(RectTransform rt1, RectTransform rt2, Direction direction) {
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
            MovePanelsInternal(rt1, rt2, direction);
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
        }

        private static async UniTask MovePanelsInternal(RectTransform rt1, RectTransform rt2, Direction direction) {
            blocked = true;
            MovePanelInstantly(GetOppositeDirection(direction), rt2);
            rt2.gameObject.SetActive(true);
            MovePanel(direction, rt1);
            MovePanel(direction, rt2);
            await UniTask.Delay(TimeSpan.FromSeconds(Values.ANIMATION_SLIDE_SPEED), ignoreTimeScale: false);
            rt1.gameObject.SetActive(false);
            MovePanelInstantly(GetOppositeDirection(direction), rt1);
            blocked = false;
        }

        public static void MovePanelInstantly(Direction direction, RectTransform rt) {
            switch (direction) {
                case Direction.LEFT:
                    rt.anchorMin = new Vector2(rt.anchorMin.x - 1f, rt.anchorMin.y);
                    rt.anchorMax = new Vector2(rt.anchorMax.x - 1f, rt.anchorMax.y);
                    break;

                case Direction.RIGHT:
                    rt.anchorMin = new Vector2(rt.anchorMin.x + 1f, rt.anchorMin.y);
                    rt.anchorMax = new Vector2(rt.anchorMax.x + 1f, rt.anchorMax.y);
                    break;

                case Direction.UP:
                    rt.anchorMin = new Vector2(rt.anchorMin.x, rt.anchorMin.y - 1f);
                    rt.anchorMax = new Vector2(rt.anchorMax.x, rt.anchorMax.y - 1f);
                    break;

                case Direction.DOWN:
                    rt.anchorMin = new Vector2(rt.anchorMin.x, rt.anchorMin.y + 1f);
                    rt.anchorMax = new Vector2(rt.anchorMax.x, rt.anchorMax.y + 1f);
                    break;
            }
        }

        /// <summary>
        /// Moves a panel in a certain direction, given their anchors are set properly
        /// </summary>
        /// <param name="direction"></param>
        /// <param name="rt"></param>
        /// <returns></returns>
        public static void MovePanel(Direction direction, RectTransform rt) {
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
            MovePanelInternal(direction, rt);
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
        }

        private static async UniTask MovePanelInternal(Direction direction, RectTransform rt) {
            blocked = true;
            switch (direction) {
                case Direction.LEFT:
                    rt.DOAnchorMin(new Vector2(rt.anchorMin.x - 1f, rt.anchorMin.y), Values.ANIMATION_SLIDE_SPEED);
                    rt.DOAnchorMax(new Vector2(rt.anchorMax.x - 1f, rt.anchorMax.y), Values.ANIMATION_SLIDE_SPEED);
                    break;

                case Direction.RIGHT:
                    rt.DOAnchorMin(new Vector2(rt.anchorMin.x + 1f, rt.anchorMin.y), Values.ANIMATION_SLIDE_SPEED);
                    rt.DOAnchorMax(new Vector2(rt.anchorMax.x + 1f, rt.anchorMax.y), Values.ANIMATION_SLIDE_SPEED);
                    break;

                case Direction.UP:
                    rt.DOAnchorMin(new Vector2(rt.anchorMin.x, rt.anchorMin.y - 1f), Values.ANIMATION_SLIDE_SPEED);
                    rt.DOAnchorMax(new Vector2(rt.anchorMax.x, rt.anchorMax.y - 1f), Values.ANIMATION_SLIDE_SPEED);
                    break;

                case Direction.DOWN:
                    rt.DOAnchorMin(new Vector2(rt.anchorMin.x, rt.anchorMin.y + 1f), Values.ANIMATION_SLIDE_SPEED);
                    rt.DOAnchorMax(new Vector2(rt.anchorMax.x, rt.anchorMax.y + 1f), Values.ANIMATION_SLIDE_SPEED);
                    break;
            }
            await UniTask.Delay(TimeSpan.FromSeconds(Values.ANIMATION_SLIDE_SPEED), ignoreTimeScale: false);
            blocked = false;
        }

        public static Tween FadePanel(GameObject target, bool fadeIn) {
            float targetAlpha = fadeIn ? 1f : 0f;
            CanvasGroup canvasGroup = target.GetComponent<CanvasGroup>();
            if (canvasGroup == null) target.AddComponent<CanvasGroup>();
            return canvasGroup.DOFade(targetAlpha, Values.FADE_PANEL_DURATION);
        }

        public static T FindComponentInChildWithTag<T>(this GameObject parent, string tag) where T : Component {
            Transform t = parent.transform;
            Transform[] allChildren = t.GetComponentsInChildren<Transform>(true);
            foreach (Transform tr in allChildren) {
                if (tr.tag == tag) {
                    return tr.GetComponent<T>();
                }
            }
            return null;
        }
    }
}