using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Billboard : MonoBehaviour {
    private Transform cam;

    void Start() {
        cam = Camera.main.transform;
    }

    // Update is called once per frame
    void Update() {
        // Rotate the camera every frame so it keeps looking at the target
        Vector3 target = new Vector3(cam.position.x, transform.position.y, cam.position.z);

        transform.LookAt(target);
    }

}