using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;
using bunLace.Singleton;
using DG.Tweening;

public class CanvasFader : Singleton<CanvasFader> {

    public RawImage img;

    private Texture2D destinationTexture;
    private static bool isPerformingScreenGrab;

    private GameObject target;
    private bool show;

    void Start() {
        // Create a new Texture2D with the width and height of the screen, and cache it for reuse
        destinationTexture = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);

        // Make screenGrabRenderer display the texture.
        img.texture = destinationTexture;

        // Add the onPostRender callback
        //Camera.onPostRender += OnPostRenderCallback;
        RenderPipelineManager.endCameraRendering += OnPostRenderCallback;
    }

    private void OnPostRenderCallback(ScriptableRenderContext arg1, Camera cam) {
        if (isPerformingScreenGrab) {
            // Check whether the Camera that has just finished rendering is the one you want to take a screen grab from
            if (cam == Camera.main) {
                // Define the parameters for the ReadPixels operation
                Rect regionToReadFrom = new Rect(0, 0, Screen.width, Screen.height);
                int xPosToWriteTo = 0;
                int yPosToWriteTo = 0;
                bool updateMipMapsAutomatically = false;

                // Copy the pixels from the Camera's render target to the texture
                destinationTexture.ReadPixels(regionToReadFrom, xPosToWriteTo, yPosToWriteTo, updateMipMapsAutomatically);

                // Upload texture data to the GPU, so the GPU renders the updated texture
                // Note: This method is costly, and you should call it only when you need to
                // If you do not intend to render the updated texture, there is no need to call this method at this point
                destinationTexture.Apply();


                img.color = Color.white;
                if (target != null)
                    target.SetActive(show);
                img.DOFade(0f, 0.3f);
                //img.DOFade(0f, 1f);

                // Reset the isPerformingScreenGrab state
                isPerformingScreenGrab = false;

            }
        }
    }

    public static void Screengrab(GameObject target, bool show) {
        if (!isPerformingScreenGrab) {
            Debug.Log("pressed");
            Instance.target = target;
            Instance.show = show;

            isPerformingScreenGrab = true;
        }
    }

    // Remove the onPostRender callback
    void OnDestroy() {
        RenderPipelineManager.endCameraRendering -= OnPostRenderCallback;
    }
}