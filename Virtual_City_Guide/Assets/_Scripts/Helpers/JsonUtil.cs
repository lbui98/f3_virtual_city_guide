using UnityEngine;

/// <summary>
///JSON conversion tool class
///< para > to solve the problem of jsonutility's array conversion failure < / para >
/// <para>ZhangYu 2018-05-09</para>
/// </summary>
public static class JsonUtil {

    ///< summary > Convert objects to JSON strings < / summary >
    ///< param name = "obj" > object < / param >
    public static string ToJson<T>(T obj) {
        if (obj == null) return "null";

        if (typeof(T).GetInterface("IList") != null) {
            Pack<T> pack = new Pack<T>();
            pack.data = obj;
            string json = JsonUtility.ToJson(pack);
            return json.Substring(8, json.Length - 9);
        }

        return JsonUtility.ToJson(obj);
    }

    ///< summary > parse JSON < / summary >
    ///< typeparam name = "t" > type < / typeparam >
    ///< param name = "JSON" > JSON string < / param >
    public static T FromJson<T>(string json) {
        if (json == "null" && typeof(T).IsClass) return default(T);

        if (typeof(T).GetInterface("IList") != null) {
            json = "{\"data\":{data}}".Replace("{data}", json);
            Pack<T> Pack = JsonUtility.FromJson<Pack<T>>(json);
            return Pack.data;
        }

        return JsonUtility.FromJson<T>(json);
    }

    ///< summary > inner wrapper class < / summary >
    private class Pack<T> {
        public T data;
    }

}