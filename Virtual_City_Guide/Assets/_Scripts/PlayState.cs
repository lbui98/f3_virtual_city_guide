﻿/*
 * Copyright (c) Giuseppe Graziano
 * http://www.giuseppegraziano.de
 */

using System;

[Serializable]
public class PlayState
{
    public int Id;
    public bool PoiDone;
    public bool QuizDone;
    public bool[] QuizDoneMulti;
    public bool BonusDone;
} 