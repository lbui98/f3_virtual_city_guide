using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.AddressableAssets;

public class FindTheMistakeManager : MinigameManager2D {
    private Collider2D[] mistakes;
    private int mistakesFound;

    [SerializeField] private TextMeshProUGUI mistakesProgress;
    [SerializeField] private AssetReference foundSound;

    public new void Start() {
        base.Start();
        mistakes = GetComponentsInChildren<Collider2D>();

        UpdateProgress();
    }

    public override void Update() {
        BasicRaycast();
    }

    public override void OnTargetDetected(Collider2D collider) {
        //disable collider so that it won't be detected again
        collider.enabled = false;

        SoundManager.PlaySFX(foundSound.RuntimeKey.ToString());
        mistakesFound++;

        //update UI
        UpdateProgress();

        if (mistakesFound >= mistakes.Length) {
            //game cleared
            OnGameClear();
        }
    }

    private void UpdateProgress() {
        mistakesProgress.text = string.Format("{0} / {1}", mistakesFound, mistakes.Length);
    }
}
