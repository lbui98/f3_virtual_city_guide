using bunLace.Singleton;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.AddressableAssets;

public abstract class MinigameManager2D : Singleton<MinigameManager2D>, IMinigameManager<Collider2D> {
    [HideInInspector]
    public Camera cam;
    public GameObject clearFeedback, failureFeedback;
    public AssetReference clearGameSound, failGameSound, bgm;

    public void Start() {
        SoundManager.PlayBGM(bgm.RuntimeKey.ToString(), true);
        cam = GetComponentInChildren<Camera>();
    }

    public abstract void Update();

    public void BasicRaycast() {
#if UNITY_EDITOR
        if (Input.GetMouseButtonDown(0)) {
            Vector2 pos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            RaycastHit2D hitInfo = Physics2D.Raycast(cam.ScreenToWorldPoint(pos), Vector2.zero);
            if (hitInfo.collider && hitInfo.collider.CompareTag(bunLace.Constants.Tags.TARGET)) {
                OnTargetDetected(hitInfo.collider);
            }
        }
#else
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Stationary) {
            RaycastHit2D hitInfo = Physics2D.Raycast(cam.ScreenToWorldPoint(Input.GetTouch(0).position), Vector2.zero);
            if (hitInfo.collider && hitInfo.collider.CompareTag(bunLace.Constants.Tags.TARGET)) {
                OnTargetDetected(hitInfo.collider);
            }
        }
#endif
    }

    public abstract void OnTargetDetected(Collider2D collider);

    public void OnGameClear() {
        SoundManager.PlaySFX(clearGameSound.RuntimeKey.ToString());
        clearFeedback.SetActive(true);
        MinigameMediaManager.OnMinigameClear();
    }

    public void OnGameFail() {
        SoundManager.PlaySFX(failGameSound.RuntimeKey.ToString());
        failureFeedback.SetActive(true);
        MinigameMediaManager.OnMinigameFail();
    }
}
