using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LetterHolder : MonoBehaviour {
    public SoupLetter currentLetter;

    private void OnTriggerEnter2D(Collider2D collider) {
        if (currentLetter == null && collider.CompareTag(bunLace.Constants.Tags.TARGET)) {
            currentLetter = collider.GetComponent<SoupLetter>();
            OnCheckLetters?.Invoke();
        }
    }

    public delegate void CheckLetters();
    public static event CheckLetters OnCheckLetters;

    private void OnTriggerExit2D(Collider2D collider) {

        if (currentLetter == collider.GetComponent<SoupLetter>()) {
            currentLetter = null;
        }
    }
}
