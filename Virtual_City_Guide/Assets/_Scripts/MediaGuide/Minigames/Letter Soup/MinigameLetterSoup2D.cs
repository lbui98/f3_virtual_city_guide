using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.EventSystems;

public class MinigameLetterSoup2D : MinigameManager2D {
    [SerializeField] private string word;
    [Tooltip("The order in the array determines which letter they should hold")]
    [SerializeField] LetterHolder[] letterHolders;

    public static DragAndDroppable2D currentDrag;

    private new void Start() {
        base.Start();

        //idiot proof, because there are no spaces
        word = word.Replace(" ", "");

        if (letterHolders.Length != word.Length) Debug.LogError("The number of letter holders does not match the word length!");
    }

    private void OnEnable() {
        LetterHolder.OnCheckLetters += OnCheckLetters;
    }

    private void OnDisable() {
        LetterHolder.OnCheckLetters -= OnCheckLetters;
    }

    private void OnCheckLetters() {
        bool correct = true;
        for (int i = 0; i < letterHolders.Length; i++) {
            if (letterHolders[i].currentLetter == null || char.ToLower(letterHolders[i].currentLetter.letter) != char.ToLower(word[i])) {
                correct = false;
                break;
            }
        }

        if (correct) {
            OnGameClear();
        }
    }

    public override void OnTargetDetected(Collider2D collider) {
    }

    public override void Update() {
    }
}
