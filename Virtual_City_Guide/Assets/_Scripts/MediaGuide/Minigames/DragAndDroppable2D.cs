using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class DragAndDroppable2D : MonoBehaviour {
    bool canMove;
    bool dragging;
    private Collider2D col;

    void Start() {
        col = GetComponent<Collider2D>();
        canMove = false;
        dragging = false;
    }

    // Update is called once per frame
    void Update() {
        Vector2 mousePos = MinigameManager2D.Instance.cam.ScreenToWorldPoint(Input.mousePosition);
        if (Input.GetMouseButtonDown(0) || Input.touchCount == 1) {
            //don't drag another letter when already dragging
            if (MinigameLetterSoup2D.currentDrag == null) {

                if (col == Physics2D.OverlapPoint(mousePos)) {
                    canMove = true;
                }
                else {
                    canMove = false;
                }
                if (canMove) {
                    dragging = true;
                    MinigameLetterSoup2D.currentDrag = this;
                }
            }
        }
        if (dragging) {
            transform.position = mousePos;
        }
        if (Input.GetMouseButtonUp(0) || Input.touchCount <= 0) {
            canMove = false;
            dragging = false;
            MinigameLetterSoup2D.currentDrag = null;
        }
    }

}
