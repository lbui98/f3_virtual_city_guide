using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using System.Linq;
using UnityEngine.UI;

public class Timer : MonoBehaviour {
    [SerializeField] private bool countOnStart = true;
    [SerializeField] private TextMeshProUGUI timer;

    private bool counting;
    [SerializeField] private string format = "ss:fff";

    public float timeInSeconds;
    private float timeLeft;

    [SerializeField] private Image slider;

    [SerializeField] private List<float> notificationTimes;

    private void Start() {
        timeLeft = timeInSeconds;
        counting = countOnStart;
        TimeSpan time = TimeSpan.FromSeconds(timeLeft);
        timer.text = time.ToString(format);

        notificationTimes = notificationTimes.OrderByDescending(o => o).ToList();
    }

    public delegate void TimerUp();
    public static event TimerUp OnTimerUp;

    public delegate void NotifyTime(float timeLeft);
    public static event NotifyTime OnNotifyTime;

    public void StartTimer() {
        counting = true;
    }

    private void Update() {
        if (counting) {
            timeLeft -= Time.deltaTime;

            if (notificationTimes.Count > 0 && timeLeft <= notificationTimes[0]) {
                OnNotifyTime?.Invoke(timeLeft);
                notificationTimes.RemoveAt(0);
            }

            TimeSpan time = TimeSpan.FromSeconds(timeLeft);

            timer.text = time.ToString(format);

            if (slider != null) {
                slider.fillAmount = timeLeft / timeInSeconds;
            }

            if (timeLeft <= 0f) {
                time = TimeSpan.Zero;
                timer.text = time.ToString(format);

                OnTimerUp?.Invoke();

                counting = false;

                if (slider != null) {
                    slider.fillAmount = 0f;
                }
            }
        }
    }
}
