using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.AddressableAssets;
using DG.Tweening;

public class MinigameQuickSortManager2D : MinigameManager2D {
    [SerializeField] private TextMeshProUGUI progress;
    [SerializeField] private int requiredPointsForClear;

    [SerializeField] private Transform startPos;
    [SerializeField] private SortObject targetObject;

    [SerializeField] private SortObject[] folders;
    [SerializeField] private SortObject[] sortObjects;

    [SerializeField] private AssetReference correctAnswerSound;
    [SerializeField] private AssetReference wrongAnswerSound;

    [SerializeField] private float flySpeed = 0.5f;

    private int points;
    private bool allowInput = true;

    private new void Start() {
        base.Start();
        GetNextObject();
        UpdateProgress();
    }

    public override void OnTargetDetected(Collider2D collider) {
        SortObject folder = collider.GetComponent<SortObject>();
        if (targetObject.index == folder.index) {
            SoundManager.PlaySFX(correctAnswerSound.RuntimeKey.ToString());
            points++;
            UpdateProgress();
        }
        else {
            SoundManager.PlaySFX(wrongAnswerSound.RuntimeKey.ToString());

        }
        StartCoroutine(GetNextObject(collider.transform.position));
    }

    private IEnumerator GetNextObject(Vector3 flyDestination) {
        allowInput = false;
        targetObject.transform.DOMove(flyDestination, flySpeed);
        yield return new WaitForSeconds(flySpeed);

        int index = UnityEngine.Random.Range(0, sortObjects.Length);
        targetObject = sortObjects[index];
        targetObject.transform.position = startPos.position;
        targetObject.gameObject.SetActive(true);

        allowInput = true;
    }

    private void GetNextObject() {
        int index = UnityEngine.Random.Range(0, sortObjects.Length);
        targetObject = sortObjects[index];
        targetObject.transform.position = startPos.position;
        targetObject.gameObject.SetActive(true);
    }

    public override void Update() {
        if (allowInput)
            BasicRaycast();
    }

    private void OnEnable() {
        Timer.OnTimerUp += OnTimerUp;
    }

    private void OnDisable() {
        Timer.OnTimerUp -= OnTimerUp;
    }

    private void OnTimerUp() {
        allowInput = false;
        if (points >= requiredPointsForClear) OnGameClear();
        else {
            OnGameFail();
        }
    }

    private void UpdateProgress() {
        progress.text = string.Format("{0} / {1}", points, requiredPointsForClear);
    }
}
