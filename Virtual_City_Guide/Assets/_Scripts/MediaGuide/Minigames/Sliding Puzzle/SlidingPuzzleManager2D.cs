using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class SlidingPuzzleManager2D : MinigameManager2D {
    [SerializeField] private PuzzlePiece emptyTile;
    [SerializeField] private Sprite lastTile;
    [SerializeField] private float tileSize = 1f;

    [SerializeField] private PuzzlePiece[] puzzlePieces;

    private int correctPositioned = 0;

    public new void Start() {
        base.Start();
        puzzlePieces = GetComponentsInChildren<PuzzlePiece>();
    }

    public void SetOriginalPositions() {
        puzzlePieces = GetComponentsInChildren<PuzzlePiece>();
        foreach (PuzzlePiece puzzlePiece in puzzlePieces) {
            puzzlePiece.originalPosition = puzzlePiece.transform.position;
        }
    }

    public void Shuffle() {
        List<Transform> adj = new List<Transform>();
        for (int i = 0; i < puzzlePieces.Length; i++) {
            //check for adjacent pieces
            if (Vector2.Distance(emptyTile.transform.position, puzzlePieces[i].transform.position) < Mathf.Sqrt(2 * tileSize)) {
                adj.Add(puzzlePieces[i].transform);
            }
        }

        //choose one of the adjacents by random
        int index = Random.Range(0, adj.Count);

        //swap positions with empty
        Vector3 tempPos = emptyTile.transform.position;
        emptyTile.transform.position = adj[index].position;
        adj[index].position = tempPos;
    }

    public override void OnTargetDetected(Collider2D collider) {
        if (Vector2.Distance(emptyTile.transform.position, collider.transform.position) < Mathf.Sqrt(2 * tileSize)) {
            Vector3 tempPos = emptyTile.transform.position;
            emptyTile.transform.position = collider.transform.position;
            //collider.transform.DOMove(tempPos, 0.5f);
            collider.transform.position = tempPos;
        }


        if (emptyTile.InOriginalPosition()) {
            //empty tile is in its original position,
            //check how many are correctly positioned
            correctPositioned = 0;
            for (int i = 0; i < puzzlePieces.Length; i++) {
                if (puzzlePieces[i].InOriginalPosition()) correctPositioned++;
            }
            if (correctPositioned == puzzlePieces.Length) {
                emptyTile.GetComponent<SpriteRenderer>().sprite = lastTile;
                OnGameClear();
            }
        }
    }

    public override void Update() {
        BasicRaycast();
    }
}
