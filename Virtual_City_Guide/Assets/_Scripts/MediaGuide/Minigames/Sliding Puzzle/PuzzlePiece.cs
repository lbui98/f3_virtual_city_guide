using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzlePiece : MonoBehaviour {
    public Vector2 originalPosition;

    private Vector3 originalPos { get { return new Vector3(originalPosition.x, originalPosition.y, 0); } }

    public bool InOriginalPosition() {
        return transform.position == originalPos;
    }
}
