using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMinigameManager<T> where T : Component {
    void Start();

    void Update();

    void BasicRaycast();

    public abstract void OnTargetDetected(T collider);

    void OnGameClear();

    void OnGameFail();
}
