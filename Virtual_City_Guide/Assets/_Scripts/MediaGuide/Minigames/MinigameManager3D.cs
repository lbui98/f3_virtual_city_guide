using bunLace.Singleton;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.AddressableAssets;

public abstract class MinigameManager3D : Singleton<MinigameManager3D>, IMinigameManager<Collider> {
    [HideInInspector]
    public Camera cam;
    public GameObject clearFeedback, failureFeedback;
    public AssetReference clearGameSound, failGameSound, bgm;

    public void Start() {
        SoundManager.PlayBGM(bgm.RuntimeKey.ToString(), true);
        cam = GetComponentInChildren<Camera>();
    }

    public abstract void Update();

    public void BasicRaycast() {
#if UNITY_EDITOR
        if (Input.GetMouseButtonDown(0)) {
            RaycastHit hitInfo;
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hitInfo, Mathf.Infinity)) {
                if (hitInfo.collider.CompareTag(bunLace.Constants.Tags.TARGET))
                    OnTargetDetected(hitInfo.collider);
            }
        }
#else
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Stationary) {
            RaycastHit hitInfo;
            Ray ray = cam.ScreenPointToRay(Input.GetTouch(0).position);
            if (Physics.Raycast(ray, out hitInfo, Mathf.Infinity)) {
                if (hitInfo.collider.CompareTag(bunLace.Constants.Tags.TARGET))
                    OnTargetDetected(hitInfo.collider);
            }
        }
#endif
    }

    public abstract void OnTargetDetected(Collider collider);

    public void OnGameClear() {
        SoundManager.PlaySFX(clearGameSound.RuntimeKey.ToString());
        clearFeedback.SetActive(true);
        MinigameMediaManager.OnMinigameClear();
    }

    public void OnGameFail() {
        SoundManager.PlaySFX(failGameSound.RuntimeKey.ToString());
        failureFeedback.SetActive(true);
    }
}
