using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mole : MonoBehaviour {
    [SerializeField]
    [Range(1f, 10f)]
    private float despawnAfterSeconds;

    public int pointWorth;
    public Vector3 offset;

    private void OnEnable() {
        Timer.OnTimerUp += OnTimerUp;
    }
    private void OnDisable() {
        StopAllCoroutines();
        Debug.Log("Mole disabled");
        Timer.OnTimerUp -= OnTimerUp;
    }

    private void OnTimerUp() {
        StopAllCoroutines();
    }


    public void Spawn(Vector3 target) {
        transform.position = target + offset;
        gameObject.SetActive(true);
        StopAllCoroutines();
        StartCoroutine(SpawnRoutine());
    }

    private IEnumerator SpawnRoutine() {
        yield return new WaitForSeconds(despawnAfterSeconds);
        gameObject.SetActive(false);
        OnDespawned?.Invoke(this);
    }

    public delegate void Despawned(Mole mole);
    public static event Despawned OnDespawned;
}
