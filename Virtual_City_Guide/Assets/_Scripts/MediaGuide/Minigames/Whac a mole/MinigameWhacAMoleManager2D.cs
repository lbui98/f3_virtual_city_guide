using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.AddressableAssets;

public class MinigameWhacAMoleManager2D : MinigameManager2D {
    [SerializeField] private TextMeshProUGUI progress;
    [SerializeField] private int requiredPointsForClear;

    [Tooltip("Make sure there are enough moles for each new spawn based on time")]
    [SerializeField] private Mole[] moles;
    [SerializeField] private Transform[] targetPositions;

    [SerializeField] private List<Vector3> occupiedPositions = new List<Vector3>();

    [SerializeField] private AssetReference whackSound, spawnSound;

    private int points;
    private bool allowInput = false;

    private new void Start() {
        base.Start();
        UpdateProgress();
        allowInput = true;
        SpawnMole();
    }

    private void OnEnable() {
        Timer.OnTimerUp += OnTimerUp;
        Timer.OnNotifyTime += OnNotifyTime;
        Mole.OnDespawned += OnMoleDespawned;
    }

    private void OnDisable() {
        Timer.OnTimerUp -= OnTimerUp;
        Timer.OnNotifyTime -= OnNotifyTime;
        Mole.OnDespawned -= OnMoleDespawned;
    }

    private void OnNotifyTime(float timeLeft) {
        SpawnMole();
    }

    private void OnMoleDespawned(Mole mole) {
        SpawnMole();
    }

    private void OnTimerUp() {
        allowInput = false;
        if (points >= requiredPointsForClear) OnGameClear();
        else {
            OnGameFail();
        }
    }

    private void SpawnMole() {
        if (!allowInput) return; //stop spawning when game is over

        Transform targetPos = null;
        while (targetPos == null) {
            targetPos = targetPositions[UnityEngine.Random.Range(0, targetPositions.Length)];

            //check if position is taken
            if (occupiedPositions.Contains(targetPos.position)) targetPos = null;
            else occupiedPositions.Add(targetPos.position);

        }


        Mole mole = null;
        while (mole == null) {
            mole = moles[UnityEngine.Random.Range(0, moles.Length)];
            if (mole.gameObject.activeSelf) mole = null; //only take moles that are not on field
        }
        SoundManager.PlaySFX(spawnSound.RuntimeKey.ToString());
        mole.Spawn(targetPos.position);
    }

    public override void OnTargetDetected(Collider2D collider) {
        SoundManager.PlaySFX(whackSound.RuntimeKey.ToString());
        points += collider.GetComponent<Mole>().pointWorth;
        collider.gameObject.SetActive(false);
        occupiedPositions.Remove(collider.GetComponent<Mole>().transform.position);
        UpdateProgress();
        SpawnMole();
    }

    public override void Update() {
        if (allowInput)
            BasicRaycast();
    }

    private void UpdateProgress() {
        progress.text = string.Format("{0} / {1}", points, requiredPointsForClear);
    }
}
