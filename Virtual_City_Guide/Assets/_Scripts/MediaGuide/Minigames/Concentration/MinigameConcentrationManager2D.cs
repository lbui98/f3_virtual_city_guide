using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MinigameConcentrationManager2D : MinigameManager2D {
    [System.Serializable]
    public class MatchingPair {
        public ConcentrationCard a;
        public ConcentrationCard b;
    }

    [SerializeField] private MatchingPair[] matchingPairs;
    [SerializeField] private Sprite backside;
    [SerializeField] private TextMeshProUGUI progress;

    private ConcentrationCard currentSelected;

    private bool flipping;
    private int points;

    private new void Start() {
        base.Start();

        //flip all cards
        for (int i = 0; i < matchingPairs.Length; i++) {
            matchingPairs[i].a.Initialize();
            matchingPairs[i].a.Flip(backside);

            matchingPairs[i].b.Initialize();
            matchingPairs[i].b.Flip(backside);
        }

        UpdateProgress();
    }

    public override void OnTargetDetected(Collider2D collider) {
        if (!flipping) StartCoroutine(CardFlipRoutine(collider));
    }

    private IEnumerator CardFlipRoutine(Collider2D collider) {
        flipping = true;
        ConcentrationCard selectedCard = collider.transform.GetComponent<ConcentrationCard>();
        if (currentSelected == selectedCard) {
            flipping = false;
            yield break;
        }

        selectedCard.Flip(backside);

        //yield return new WaitForSeconds(1f);

        if (currentSelected == null) {
            currentSelected = selectedCard;
        }
        else {

            for (int i = 0; i < matchingPairs.Length; i++) {
                if (currentSelected == matchingPairs[i].a || currentSelected == matchingPairs[i].b) {
                    yield return new WaitForSeconds(0.5f);
                    if (selectedCard == matchingPairs[i].a || selectedCard == matchingPairs[i].b) {
                        //Matching pair found
                        currentSelected.RemoveFromField();
                        selectedCard.RemoveFromField();

                        points++;
                        UpdateProgress();
                        if (points == matchingPairs.Length) {
                            OnGameClear();
                        }
                    }
                    else {
                        //wrong pair
                        selectedCard.Flip(backside);
                        currentSelected.Flip(backside);

                        yield return new WaitForSeconds(0.5f);
                    }

                    currentSelected = null;
                    break;
                }
            }
        }

        flipping = false;
    }

    public override void Update() {
        BasicRaycast();
    }

    private void UpdateProgress() {
        progress.text = string.Format("{0} / {1}", points, matchingPairs.Length);
    }
}
