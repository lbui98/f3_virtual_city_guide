using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConcentrationCard : MonoBehaviour {
    private Sprite originalSprite;
    private bool front = true;
    private SpriteRenderer sr;

    public void Initialize() {
        sr = GetComponent<SpriteRenderer>();
        originalSprite = sr.sprite;
    }

    public void Flip(Sprite backside) {
        if (front) sr.sprite = backside;
        else sr.sprite = originalSprite;

        front = !front;
    }

    public void RemoveFromField() {
        gameObject.SetActive(false);
    }
}
