using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {
    [SerializeField] private GameObject[] balloons;
    [SerializeField] private float maxDistance;
    [SerializeField] private float maxHeight;
    [SerializeField] private Transform centerPosition;

    [SerializeField] private bool spawnWithTime;
    private int id = 0;

    private void Start() {
        if (spawnWithTime) return;

        for (int i = 0; i < balloons.Length; i++) {
            //reposition all prefabs
            Reposition(balloons[i].transform);
        }
    }

    public void Reposition(Transform target) {
        target.position = RandomNavSphere(centerPosition.position, maxDistance, -1);
        target.position += new Vector3(0, Random.Range(0f, maxHeight), 0);
    }

    private void OnEnable() {
        if (spawnWithTime)
            Timer.OnNotifyTime += OnNotifyTime;

    }

    private void OnDisable() {
        if (spawnWithTime)
            Timer.OnNotifyTime -= OnNotifyTime;
    }

    private void OnNotifyTime(float timeLeft) {
        Reposition(balloons[id].transform);
        id++;
        if (id >= balloons.Length) id = 0;
    }

    private static Vector3 RandomNavSphere(Vector3 origin, float dist, int layermask) {
        var randDirection = Random.insideUnitSphere * dist;
        randDirection += origin;
        UnityEngine.AI.NavMesh.SamplePosition(randDirection, out var navHit, dist, layermask);
        return navHit.position;
    }
}
