using bunLace.Database;
using Cysharp.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;

public class MinigameMediaManager : MediaGuideSubManager {
    private static int currentContentID;

    public override void LoadContent(AdditionalContent content, bunLace.Tools.MediaTypes type) {
        Addressables.InstantiateAsync(content.AssetReference, this.content);
        currentContentID = content.ID;
    }

    public override void OnBtnReturn() {
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
        OnBtnReturnInternal();
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
    }

    private async UniTask OnBtnReturnInternal() {
        await LoadMainScene();
    }

    public delegate void MinigameCleared();
    public static event MinigameCleared OnMinigameCleared;

    public static void OnMinigameClear() {
        if (!GameManager.SaveData.minigameData.Contains(currentContentID)) {
            GameManager.SaveData.minigameData.Add(currentContentID);
            Debug.Log("Adding minigame data " + currentContentID);
            OnMinigameCleared?.Invoke();
        }

        Debug.Log(GameManager.SaveData.minigameData.Count);
        Instance.StartCoroutine(ReturnToScene());
    }

    public static void OnMinigameFail() {
        Instance.StartCoroutine(ReturnToScene());
    }

    private static IEnumerator ReturnToScene() {
        yield return new WaitForSeconds(3f);
        Instance.OnBtnReturn();
    }
}
