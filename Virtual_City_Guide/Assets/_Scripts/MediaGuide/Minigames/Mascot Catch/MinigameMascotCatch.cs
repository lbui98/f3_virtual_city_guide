using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.Localization;

#if UNITY_ANDROID
using UnityEngine.Android;
#elif UNITY_IOS
using UnityEngine.iOS;
#endif


public class MinigameMascotCatch : MinigameManager3D {
    [SerializeField] private TextMeshProUGUI progress;
    [SerializeField] private int requiredPointsForClear;
    [SerializeField] private AssetReference hitSound;
    [SerializeField] private LocalizedString rules, warning;
    [SerializeField] private GameObject simulatedWorld, camCanvas;

    private Timer timer;

    private int points;
    private bool allowInput = false;

    private new void Start() {
        base.Start();

        timer = GetComponent<Timer>();

        UpdateProgress();

#if UNITY_ANDROID
        if (!Permission.HasUserAuthorizedPermission(Permission.Camera)) {
            PermissionCallbacks_PermissionDenied(null);
        }
        else {
            PermissionCallbacks_PermissionGranted(null);
        }
#elif UNITY_IOS
        if (Application.HasUserAuthorization(UserAuthorization.WebCam)) {
            PermissionCallbacks_PermissionGranted(null);
        }
        else {
            PermissionCallbacks_PermissionDenied(null);
        }
#endif
    }
    private void PermissionCallbacks_PermissionDenied(string obj) {
        Debug.Log("No Camera permissions");

        //enable simulated world
        simulatedWorld.SetActive(true);
        camCanvas.SetActive(false);

        warning.GetLocalizedStringAsync().Completed += asyncOp => {
            string text = string.Format(asyncOp.Result, timer.timeInSeconds);
            EasyMessageBox.Show(text, button1Action: ShowRules);
        };
    }

    private void PermissionCallbacks_PermissionGranted(string obj) {
        Debug.Log("Camera permissions granted");

        CameraBackground cameraBackground = GetComponentInChildren<CameraBackground>(true);
        cameraBackground.transform.parent.gameObject.SetActive(false);
        cameraBackground.transform.parent.gameObject.SetActive(true);
        cameraBackground.Initialize();

        warning.GetLocalizedStringAsync().Completed += asyncOp => {
            string text = string.Format(asyncOp.Result, timer.timeInSeconds);
            EasyMessageBox.Show(text, button1Action: ShowRules);
        };
    }

    private void ShowRules() {
        rules.GetLocalizedStringAsync().Completed += asyncOp => {
            string text = string.Format(asyncOp.Result, timer.timeInSeconds);
            EasyMessageBox.Show(text, button1Action: StartGame);
        };
    }

    private void StartGame() {
        timer.StartTimer();
        allowInput = true;
    }

    private void OnEnable() {
        Timer.OnTimerUp += OnTimerUp;
    }

    private void OnDisable() {
        Timer.OnTimerUp -= OnTimerUp;
    }

    private void OnTimerUp() {
        allowInput = false;
        if (points >= requiredPointsForClear) OnGameClear();
        else {
            OnGameFail();
        }
    }

    public override void OnTargetDetected(Collider collider) {
        ARMoverController mascot = collider.GetComponent<ARMoverController>();
        if (mascot == null) return;

        if (mascot.OnHit()) {
            SoundManager.PlaySFX(hitSound.RuntimeKey.ToString());
            points++;
            if (points >= requiredPointsForClear) {
                OnGameClear();
            }
        }
        UpdateProgress();
    }

    public override void Update() {
        if (allowInput) BasicRaycast();
    }

    private void UpdateProgress() {
        progress.text = string.Format("{0} / {1}", points, requiredPointsForClear);
    }
}
