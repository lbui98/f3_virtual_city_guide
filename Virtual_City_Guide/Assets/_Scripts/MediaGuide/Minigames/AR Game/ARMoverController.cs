﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Serialization;

public class ARMoverController : MonoBehaviour {
    [SerializeField] private float wanderRadius;
    [SerializeField] private float wanderTimer;
    [SerializeField] private float rotationOffset;
    [SerializeField] private Renderer[] renderers;
    [SerializeField] private Camera cam;
    [SerializeField] private Transform centerPosition;
    [SerializeField] private float invulnerabilityTime = 0.75f;
    [SerializeField] private GameObject vulnerability;

    private float timer;
    private bool initialPositionSet;
    private NavMeshAgent agent;
    private bool invulnerable = false;

    private void Start() {
        agent = GetComponent<NavMeshAgent>();
        renderers = GetComponentsInChildren<Renderer>();
    }

    private void OnEnable() {
        timer = wanderTimer;
        initialPositionSet = false;
    }

    private void Update() {
        var began = true;
        foreach (var item in renderers) {
            item.enabled = began;
        }

        if (!initialPositionSet && began) {
            var screenCenter = new Vector3((cam.pixelWidth - 1) / 2f, (cam.pixelHeight - 1) / 2f, 0);
            var dir = cam.ScreenPointToRay(screenCenter).direction;

            dir.y = 0f;
            dir = dir.normalized * (wanderRadius / 2f);
            agent.Warp(dir);

            timer = wanderTimer;

            // Make the mascot look into the camera
            Debug.Assert(cam != null, nameof(cam) + " != null");
            transform.LookAt(cam.transform.position);
            var eulers = transform.rotation.eulerAngles;
            eulers.x = 0;
            transform.rotation = Quaternion.Euler(eulers);
            transform.Rotate(transform.up, rotationOffset);

            initialPositionSet = true;
        }

        timer += Time.deltaTime;

        if (!(timer >= wanderTimer)) return;

        var newPos = RandomNavSphere(centerPosition.position, wanderRadius, -1);
        agent.SetDestination(newPos);
        timer = 0;
    }

    private static Vector3 RandomNavSphere(Vector3 origin, float dist, int layermask) {
        var randDirection = Random.insideUnitSphere * dist;
        randDirection += origin;
        UnityEngine.AI.NavMesh.SamplePosition(randDirection, out var navHit, dist, layermask);
        return navHit.position;
    }

    public bool OnHit() {
        if (invulnerable) return false;
        StopAllCoroutines();
        StartCoroutine(StartInvulnerableTimer(invulnerabilityTime));
        //not invulnerable
        //change direction
        var newPos = RandomNavSphere(centerPosition.position, wanderRadius, -1);
        agent.SetDestination(newPos);
        timer = 0;

        return true;
    }

    private IEnumerator StartInvulnerableTimer(float hitEffectTime) {
        invulnerable = true;
        vulnerability.SetActive(true);
        float timer = hitEffectTime;
        bool visible = true;
        agent.speed += 20;
        agent.acceleration *= 2;
        while (timer > 0f) {
            timer -= Time.deltaTime;
            foreach (var item in renderers) {
                item.gameObject.SetActive(visible);
            }
            visible = !visible;
            yield return null;
        }

        foreach (var item in renderers) {
            item.gameObject.SetActive(true);
        }
        agent.speed -= 20;
        agent.acceleration /= 2;
        vulnerability.SetActive(false);

        invulnerable = false;
    }
}