using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class CameraBackground : MonoBehaviour {
    private bool camAvaible = false;
    private WebCamTexture backCam;
    private Texture defaultBackground;

    public RawImage background;
    public AspectRatioFitter fit;

    public void Initialize() {
        defaultBackground = background.texture;
        WebCamDevice[] devices = WebCamTexture.devices;
        if (devices.Length == 0) {
            print("no webcam ");
            camAvaible = false;
            return;
        }

        for (int i = 0; i < devices.Length; i++) {
            if (!devices[i].isFrontFacing) {
                backCam = new WebCamTexture(devices[i].name,
                Screen.width, Screen.height);
            }
        }

        if (backCam == null) {
            camAvaible = false;
            print("No backcam");
            return;
        }

        backCam.Play();
        background.texture = backCam;
        camAvaible = true;
    }



    // Update is called once per frame

    void Update() {
        if (!camAvaible)
            return;

        float ratio = (float)backCam.width / (float)backCam.height;

        float scaleY = backCam.videoVerticallyMirrored ? -1f : 1f;

        background.rectTransform.localScale = new Vector3(1f * ratio, scaleY * ratio, 1f * ratio);
        fit.aspectMode = AspectRatioFitter.AspectMode.FitInParent;
        fit.aspectRatio = ratio;

        int orient = -backCam.videoRotationAngle;
        background.rectTransform.localEulerAngles = new Vector3(0, 0, orient);
    }
}