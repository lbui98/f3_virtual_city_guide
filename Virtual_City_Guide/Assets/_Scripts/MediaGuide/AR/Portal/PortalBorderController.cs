﻿using DG.Tweening;
using UnityEngine;
using bunLace.Singleton;

public class PortalBorderController : MonoBehaviour {
#pragma warning disable 649
    [SerializeField] private CanvasGroup adCanvas;
#pragma warning restore 649
    public PortalController portalController;

    private bool isPlayerInArea = true;

    private bool IsPlayerInArea {
        get => isPlayerInArea;
        set {
            isPlayerInArea = value;
            UpdateAdVisibility();
        }
    }

    private void OnTriggerExit(Collider other) {
        if (!other.CompareTag("MainCamera")) return;

        IsPlayerInArea = false;
    }

    private void OnTriggerEnter(Collider other) {
        if (!other.CompareTag("MainCamera")) return;

        IsPlayerInArea = true;
    }

    public void UpdateAdVisibility() {
        if (adCanvas == null) return;

        adCanvas.DOFade(portalController.IsPlayerInPortal && !IsPlayerInArea ? 1f : 0f, 0.5f);
        adCanvas.interactable = portalController.IsPlayerInPortal && !IsPlayerInArea;
        adCanvas.blocksRaycasts = portalController.IsPlayerInPortal && !IsPlayerInArea;
    }
}