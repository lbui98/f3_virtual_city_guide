using UnityEngine;
using bunLace.Constants;

/// <summary>This is expected to be placed on the portal plane</summary>
public class PortalTeleporter : MonoBehaviour {
#pragma warning disable 649
    [SerializeField] private bool isEnterPortal;
    [SerializeField] private GameObject otherPortal;
#pragma warning restore 649

    public PortalController portalController;

    /// <summary>Marks if the player is near the portal. We should check if this is true before calculating if the player has passed the portal.</summary>
    private bool playerIsOverlapping = false;

    private Camera arCamera, portalCamera;

    private void Start() {
        arCamera = portalController.ARCamera;
        portalCamera = portalController.PortalCamera;
    }

    private void Update() {
        if (!playerIsOverlapping) return;

        portalController.IsPlayerInPortal = isEnterPortal;

        Vector3 portalToPlayer = arCamera.transform.position - transform.position;
        float dotProduct = Vector3.Dot(transform.forward, portalToPlayer);

        // If this is true: The player has moved across the portal
        if (dotProduct > 0f) {
            //do smth
            Debug.Log("Crossed over");

            gameObject.SetActive(false);
            otherPortal.SetActive(true);

            arCamera.depth = isEnterPortal ? -1 : 0;
            arCamera.targetTexture = isEnterPortal ? portalController.OutTexture : null;
            arCamera.cullingMask = isEnterPortal ? portalController.OnlyDefault : portalController.NoPortalContent;
            portalCamera.depth = isEnterPortal ? 0 : -1;
            portalCamera.targetTexture = isEnterPortal ? null : portalController.InTexture;
            portalCamera.cullingMask = isEnterPortal ? portalController.Everything : portalController.NoRenderTexture;

            playerIsOverlapping = false;
        }
    }

    void OnTriggerEnter(Collider other) {
        if (other.tag == Tags.MAIN_CAMERA) {
            playerIsOverlapping = true;
        }
    }

    void OnTriggerExit(Collider other) {
        if (other.tag == Tags.MAIN_CAMERA) {
            playerIsOverlapping = false;
        }
    }
}