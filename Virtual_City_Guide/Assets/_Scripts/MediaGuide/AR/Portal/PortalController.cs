﻿using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.Assertions;
using bunLace.Singleton;

/// <summary>This controlls the construction and destruction of the portal module</summary>
public class PortalController : MonoBehaviour {
#pragma warning disable 649
    [SerializeField] private GameObject portalCameraPrefab;
    [SerializeField] private Material portalInMaterial;
    [SerializeField] private Material portalOutMaterial;
    [SerializeField] private LayerMask noRenderTexture;
    [SerializeField] private LayerMask noPortalContent;
    [SerializeField] private LayerMask onlyDefault;
    [SerializeField] private LayerMask everything;
#pragma warning restore 649

    private PortalBorderController borderController;

    private RenderTexture inTexture;
    private RenderTexture outTexture;

    private Camera arCamera;
    private Camera portalCamera;
    private bool isPlayerInPortal;

    public RenderTexture OutTexture => outTexture;
    public RenderTexture InTexture => inTexture;

    public Camera ARCamera => arCamera;
    public Camera PortalCamera => portalCamera;

    public LayerMask NoRenderTexture => noRenderTexture;
    public LayerMask NoPortalContent => noPortalContent;
    public LayerMask OnlyDefault => onlyDefault;
    public LayerMask Everything => everything;

    public bool IsPlayerInPortal {
        get => isPlayerInPortal;
        set {
            isPlayerInPortal = value;
            borderController.UpdateAdVisibility();
        }
    }

    private void Start() {
        borderController = GetComponentInChildren<PortalBorderController>(true);
        borderController.portalController = this;

        arCamera = Camera.main;

        // Create Camera
        var cameraGameObject = Instantiate(portalCameraPrefab, transform);
        portalCamera = cameraGameObject.GetComponent<Camera>();
        Assert.IsNotNull(portalCamera);
        portalCamera.cullingMask = noRenderTexture;

        // Positional and rotational constraints
        var posConstraint = cameraGameObject.GetComponent<PositionConstraint>();
        Assert.IsNotNull(posConstraint);

        var rotConstraint = cameraGameObject.GetComponent<RotationConstraint>();
        Assert.IsNotNull(rotConstraint);

        var mainCamera = ARManager.Instance.MainCamera;
        Assert.IsNotNull(mainCamera);
        var arCameraTransform = mainCamera.transform;
        var constraint = new ConstraintSource() { sourceTransform = arCameraTransform, weight = 1f };
        posConstraint.AddSource(constraint);
        rotConstraint.AddSource(constraint);

        if (portalCamera.targetTexture != null) portalCamera.targetTexture.Release();

        //Create RenderTexture
        inTexture = new RenderTexture(Screen.width, Screen.height, 24);
        outTexture = new RenderTexture(Screen.width, Screen.height, 24);
        portalCamera.targetTexture = inTexture;

        //Let Camera render into RenderTexture
        portalInMaterial.mainTexture = inTexture;
        portalOutMaterial.mainTexture = outTexture;
    }

    private void OnDisable() {
        if (arCamera == null) return;

        arCamera.depth = 0;
        arCamera.targetTexture = null;
        arCamera.cullingMask = noPortalContent;
    }

    private void OnDestroy() {
        inTexture.Release();
        outTexture.Release();
    }
}