﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using System;
using UnityEngine.XR.ARSubsystems;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using Unity.XR.CoreUtils;

public class ARTapToPlaceObject : MonoBehaviour {
    public GameObject placementIndicator;

    [SerializeField] private XROrigin arOrigin;
    [SerializeField] private ARRaycastManager aRRaycastManager;
    private Pose placementPose;
    private bool placementPoseIsValid = false;
    private Camera cam;

    private Dictionary<GameObject, AsyncOperationHandle> asyncOperationHandlesDict = new Dictionary<GameObject, AsyncOperationHandle>();
    private List<GameObject> arObjects = new List<GameObject>();

    public delegate void ObjectPlaced(AsyncOperationHandle asyncOp);
    public static event ObjectPlaced OnObjectPlaced;

    private void Start() {
        cam = ARManager.Instance.MainCamera;
    }

    private void OnDestroy() {
        Debug.Log("BUNNY: Scene change");
        for (int i = 0; i < arObjects.Count; i++) {
            if (!asyncOperationHandlesDict.ContainsKey(arObjects[i])) {
                Destroy(arObjects[i]);
            }
            else {
                Addressables.Release(asyncOperationHandlesDict[arObjects[i]]);
            }
        }
        arObjects.Clear();
    }

    private void SceneChanged(Scene arg0) {
        Debug.Log("BUNNY: Scene change");

        for (int i = 0; i < arObjects.Count; i++) {
            if (!asyncOperationHandlesDict.ContainsKey(arObjects[i])) {
                Destroy(arObjects[i]);
            }
            else {
                Addressables.Release(asyncOperationHandlesDict[arObjects[i]]);
            }
        }
        arObjects.Clear();
    }


    void Update() {
        UpdatePlacementPose();
        UpdatePlacementIndicator();

        if (Input.touchCount > 0 && EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId)) return;


#if UNITY_EDITOR
        if (Input.GetMouseButtonDown(0)) {
            PlaceObject();
        }
#else

        if (placementPoseIsValid && Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began && !string.IsNullOrEmpty(ARCatalogueManager.Instance.assetReference)) {
            PlaceObject();
        }
#endif
    }

    private void PlaceObject() {
        Addressables.InstantiateAsync(ARCatalogueManager.Instance.assetReference, placementPose.position, placementPose.rotation).Completed += (asyncOperationHandle) => {
            GameObject result = asyncOperationHandle.Result;

            if (result == null) {
                Debug.LogWarning("Could not instantiate. Please check the address: " + ARCatalogueManager.Instance.assetReference);
                return;
            }
            asyncOperationHandlesDict.Add(result, asyncOperationHandle);
            arObjects.Add(result);
            ARCatalogueManager.Instance.assetReference = string.Empty;
            OnObjectPlaced?.Invoke(asyncOperationHandle);
        };
    }

    private void UpdatePlacementIndicator() {
        if (placementPoseIsValid) {
            placementIndicator.SetActive(true);
            placementIndicator.transform.SetPositionAndRotation(placementPose.position, placementPose.rotation);
        }
        else {
            placementIndicator.SetActive(false);
        }
    }

    private void UpdatePlacementPose() {
        var screenCenter = cam.ViewportToScreenPoint(new Vector3(0.5f, 0.5f));
        var hits = new List<ARRaycastHit>();
        aRRaycastManager.Raycast(screenCenter, hits, TrackableType.Planes);

        placementPoseIsValid = hits.Count > 0;
        if (placementPoseIsValid) {
            placementPose = hits[0].pose;

            var cameraForward = cam.transform.forward;
            var cameraBearing = new Vector3(cameraForward.x, 0, cameraForward.z).normalized;
            placementPose.rotation = Quaternion.LookRotation(cameraBearing);
        }
    }

    public void ReleaseObj(GameObject obj) {
        if (!asyncOperationHandlesDict.ContainsKey(obj)) {
            Debug.Log("BUNNY: Key does not exist. " + obj.name);
            Destroy(obj);
            return;
        }
        Addressables.Release(asyncOperationHandlesDict[obj]);
        asyncOperationHandlesDict.Remove(obj);
        arObjects.Remove(obj);
    }
}