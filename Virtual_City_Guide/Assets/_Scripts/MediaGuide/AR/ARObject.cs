﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ARObject : MonoBehaviour {
    public Transform objectToLift;
    public GameObject selectedIndicator;

    private bool selected;
    private float timeToUnselect = 2f;
    [SerializeField] private float timeLeft;
    [SerializeField] private bool canMove;
    [SerializeField] private float moveSpeed = 0.25f;
    [SerializeField] private float rotateSpeed = 0.25f;
    private Camera cam;
    [SerializeField] private LayerMask layerMask;

    private void Start() {
        cam = Camera.main;
    }

    public void Select(bool selected) {
        if (selected) {
            objectToLift.localPosition = new Vector3(0, 0.1f, 0);
            selectedIndicator.SetActive(true);
            timeLeft = timeToUnselect;
        }
        else {
            objectToLift.localPosition = new Vector3(0, 0, 0);
            selectedIndicator.SetActive(false);

        }
        this.selected = selected;
    }

    bool moving = false;

    private void Update() {
        if (!selected) return;

        if (Input.touchCount == 1) {
            // GET TOUCH 0
            Touch touch0 = Input.GetTouch(0);

            if (canMove && touch0.phase == TouchPhase.Began) {
                if (EventSystem.current.IsPointerOverGameObject(touch0.fingerId)) return;
                Debug.Log("BUNNY: Check if moving mode");

                Ray raycast = cam.ScreenPointToRay(Input.GetTouch(0).position);
                RaycastHit[] raycastHit = Physics.RaycastAll(raycast, Mathf.Infinity, layerMask);
                if (raycastHit != null) {
                    foreach (RaycastHit hit in raycastHit) {
                        ARObject obj = hit.transform.GetComponent<ARObject>();
                        if (obj != null || hit.transform.CompareTag(bunLace.Constants.Tags.AROBJECT)) {
                            //hit an AR object                    
                            Debug.Log("BUNNY: Move object hit");
                            moving = true;
                            break;
                        }
                    }
                }
            }

            // APPLY ROTATION
            if (touch0.phase == TouchPhase.Moved) {
                timeLeft = timeToUnselect;
                Debug.Log(moving);
                if (moving) {
                    Debug.Log("Moving......");
                    transform.position = new Vector3(transform.position.x + touch0.deltaPosition.x * moveSpeed, transform.position.y, transform.position.z + touch0.deltaPosition.y * moveSpeed);
                }
                else {
                    Debug.Log("Rotating......");
                    transform.Rotate(0f, touch0.deltaPosition.x * rotateSpeed, 0f);
                }
            }

            if (touch0.phase == TouchPhase.Ended) moving = false;
        }

        timeLeft -= Time.deltaTime;
        if (timeLeft <= 0) {
            Select(false);
            ARInteractionManager.Instance.UnselectCurrentObj();
        }
    }
}
