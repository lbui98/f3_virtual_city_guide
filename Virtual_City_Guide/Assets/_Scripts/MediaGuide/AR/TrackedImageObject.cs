﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using DG.Tweening;
using UnityEngine.XR.ARFoundation;

public class TrackedImageObject : MonoBehaviour {
    public string imageKey;
    [SerializeField] private Transform center;
    private Transform parent;
    public bool locked = false;

    /// <summary>
    /// An Event that will be called when the trackedImage has been found
    /// </summary>
    public UnityEvent OnTrackerFound;

    /// <summary>
    /// An Event that will be called whenever the trackedImage is updated. Be careful that this will be called several times when the tracker is active.
    /// </summary>
    public UnityEvent OnTrackerUpdated;

    /// <summary>
    /// An Event that will be called when the trackedImage has been lost. This will be called regardless of whether hideWhenTrackerLost is set to false.
    /// </summary>
    public UnityEvent OnTrackerLost;

    private Tweener tweener;

    private void Start() {
        parent = transform.parent;
    }

    public void TriggerOnTrackerFound(ARTrackedImage trackedImage) {
        OnTrackerFound?.Invoke();
    }

    public void TriggerOnTrackerUpdated(ARTrackedImage trackedImage) {
        OnTrackerUpdated?.Invoke();
    }

    public void TriggerOnTrackerLost(ARTrackedImage trackedImage) {
        OnTrackerLost?.Invoke();
    }

    /// <summary>
    /// updates the position and rotation in a certain interval
    /// </summary>
    public void UpdateTransform(Transform target) {
        transform.position = target.position;
        if (tweener != null && tweener.IsPlaying()) return;
        if (Quaternion.Angle(transform.rotation, target.rotation) < 3f) return; //do not rotate if change is minimal
        tweener = transform.DORotateQuaternion(target.rotation, 0.5f);
    }

    public void MoveTransformToHere(Transform target) {
        target.position = center.position;
        target.rotation = center.rotation;
    }

    public void LockPosition() {
        locked = !locked;

        //if (locked) {
        //    transform.parent = null;
        //}
        //else {
        //    transform.parent = parent;
        //}
    }
}
