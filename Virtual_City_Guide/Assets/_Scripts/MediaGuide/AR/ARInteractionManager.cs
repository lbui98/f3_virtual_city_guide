﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.AddressableAssets;
using bunLace.Singleton;

public class ARInteractionManager : Singleton<ARInteractionManager> {

    private Camera cam;
    [SerializeField] private ARObject selectedObj;
    [SerializeField] private LayerMask layerMask;

    //private Transform placementIndicator;
    //[SerializeField] private Slider rotationSlider;

    //// Preserve the original and current orientation
    //private float previousValue;

    private void Start() {
        cam = Camera.main;
        //placementIndicator = GetComponent<ARTapToPlaceObject>().placementIndicator.transform;
    }

    private void Update() {
        if (selectedObj != null) {
            if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved) {
                return;
            }
        }

#if UNITY_EDITOR
        if (Input.GetMouseButtonUp(0)) {
            if (EventSystem.current.IsPointerOverGameObject()) {
                Debug.Log("Click failed");
                return;
            }

            Debug.Log("Click");
            Ray raycast = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit[] raycastHit = Physics.RaycastAll(raycast, Mathf.Infinity, layerMask);
            if (raycastHit != null) {
                foreach (RaycastHit hit in raycastHit) {
                    ARObject obj = hit.transform.GetComponent<ARObject>();
                    if (obj != null || hit.transform.CompareTag(bunLace.Constants.Tags.AROBJECT)) {
                        //hit an AR object                    
                        if (obj == selectedObj) {
                            UnselectCurrentObj();
                        }
                        else {
                            SelectObj(obj);
                        }
                        break;
                    }
                }
            }
        }
#else
        if ((Input.touchCount > 0) && (Input.GetTouch(0).phase == TouchPhase.Ended)) {
            if (EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId)) return;

            Ray raycast = cam.ScreenPointToRay(Input.GetTouch(0).position);
            RaycastHit[] raycastHit = Physics.RaycastAll(raycast, Mathf.Infinity, layerMask);
            if (raycastHit != null) {
                foreach (RaycastHit hit in raycastHit) {
                    ARObject obj = hit.transform.GetComponent<ARObject>();
                    if (obj != null || hit.transform.CompareTag(bunLace.Constants.Tags.AROBJECT)) {
                        //hit an AR object                    
                        if (obj == selectedObj) {
                            UnselectCurrentObj();
                        }
                        else {
                            SelectObj(obj);
                        }
                        break;
                    }
                }
            }
        }
#endif
    }

    private void SelectObj(ARObject newSelectedObject) {
        if (selectedObj != null)
            selectedObj.Select(false); //unselected previous obj

        selectedObj = newSelectedObject;
        selectedObj.Select(true);
        Debug.Log("BUNNY: Select object");
    }

    public void UnselectCurrentObj() {

        if (selectedObj != null) {
            selectedObj.Select(false);
        }
        selectedObj = null;
        Debug.Log("BUNNY: Unselect object");
    }

    public void RemoveSelectedObj() {
        if (selectedObj != null) {
            GetComponent<ARTapToPlaceObject>().ReleaseObj(selectedObj.gameObject);
            UnselectCurrentObj();
        }
    }
}
