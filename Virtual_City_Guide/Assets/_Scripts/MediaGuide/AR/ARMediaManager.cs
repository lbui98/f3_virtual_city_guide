﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using bunLace.Singleton;
using UnityEngine.XR.ARFoundation;
using Cysharp.Threading.Tasks;

public class ARMediaManager : MediaGuideSubManager {
    public override void LoadContent(bunLace.Database.AdditionalContent content, bunLace.Tools.MediaTypes type) {       
        ARCatalogueManager.Instance.OnCatalogueElementClick(content.AssetReference, type);
    }

    public override void OnBtnReturn() {
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
        OnBtnReturnInternal();
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
    }

    private async UniTask OnBtnReturnInternal() {
        await LoadMainScene();
        //unselect current poi
        GameManager.CurrentPOIID = -1;
    }
}
