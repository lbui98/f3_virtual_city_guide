﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using UnityEngine.AddressableAssets;
using Cysharp.Threading.Tasks;

[RequireComponent(typeof(VideoPlayer))]
public class ModuleARVideo : MonoBehaviour {
    [SerializeField] private AssetReference videoKey;
    [SerializeField] private GameObject fullScreen;
    private Slider seekBar;
    private TMPro.TextMeshProUGUI currentTime;
    private double clipLength;
    private VideoPlayer videoPlayer;
    private bool dragging;

    public void Start() {
        StartupInternal((string)videoKey.RuntimeKey);
    }

    private async UniTask StartupInternal(string videoKey) {
        videoPlayer = GetComponent<VideoPlayer>();

        videoPlayer.renderMode = VideoRenderMode.RenderTexture;

        VideoClip clip = await AddressablesManager.Instance.LoadVideo(videoKey);
        videoPlayer.clip = clip;

        RectTransform fullscreenClip = fullScreen.GetComponentInChildren<RawImage>().GetComponent<RectTransform>();
        RenderTexture renderTexture = new RenderTexture((int)fullscreenClip.rect.width, (int)fullscreenClip.rect.height, 0);

        videoPlayer.targetTexture = renderTexture;
        videoPlayer.aspectRatio = VideoAspectRatio.FitHorizontally;

        RawImage[] rawImages = GetComponentsInChildren<RawImage>(true);
        foreach (RawImage raw in rawImages) {
            raw.texture = renderTexture;
        }

        if (videoPlayer.playOnAwake)
            videoPlayer.Play();

        seekBar = fullScreen.GetComponentInChildren<Slider>(true);
        seekBar.minValue = 0;
        seekBar.maxValue = (float)clip.length;
        clipLength = clip.length;
        seekBar.onValueChanged.AddListener(OnSeekBarChanged);
        currentTime = seekBar.GetComponentInChildren<TMPro.TextMeshProUGUI>();
        SetTime(videoPlayer.time);
    }

    private void Update() {
        if (videoPlayer != null && seekBar != null && videoPlayer.isPlaying && !dragging) {
            seekBar.value = (float)videoPlayer.time;
        }
    }

    public void OnSliderBeginDrag() {
        dragging = true;
    }

    public void OnSliderEndDrag() {
        dragging = false;
    }

    public void OnFullscreenPlay() {
        if (!videoPlayer.isPlaying || videoPlayer.isPaused) {
            videoPlayer.Play();
        }
        else {
            videoPlayer.Pause();
        }
    }

    private void OnSeekBarChanged(float value) {
        SetTime(value);
        if (dragging)
            videoPlayer.time = Mathf.Clamp(value, 0f, (float)videoPlayer.clip.length);
    }

    private void SetTime(double currentSecond) {
        System.TimeSpan currentT = new System.TimeSpan(0, 0, (int)currentSecond);
        System.TimeSpan totalT = new System.TimeSpan(0, 0, (int)clipLength);

        currentTime.text = $"{currentT.ToString(@"mm\:ss")} / {totalT.ToString(@"mm\:ss")}";
    }

}
