﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AddressableAssets;
using SimpleSQL;
using bunLace.Database;
using bunLace.Singleton;
using bunLace.Tools;
using DG.Tweening;
using System;
using UnityEngine.XR.ARFoundation;
using Cysharp.Threading.Tasks;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.Localization;

public class ARCatalogueManager : Singleton<ARCatalogueManager> {
    public string assetReference;
    private string previousRef;
    [HideInInspector] public int currentContentId;
    [SerializeField] private Transform catalogueContent;
    [SerializeField] private RectTransform catalogue;
    [SerializeField] private AssetReference catalogueEntryRef;
    [SerializeField] private ARPlaneManager arPlaneManager;
    [SerializeField] private ARPointCloudManager arPointCloudManager;
    [SerializeField] private LocalizedString warning, instruction;

    [SerializeField]
    private bool visible = true;
    [SerializeField]
    private Image btnImgVisibility;
    [SerializeField]
    private Sprite planeVisibilityOn, planeVisibilityOff;

    [SerializeField]
    private bool catalogueOpen = true;
    [SerializeField]
    private Image btnImgMenu;

    private MediaTypes currentMode;


    private List<AsyncOperationHandle> arObjectsInScene = new List<AsyncOperationHandle>();

    private async void Start() {
        GenerateCatalogueEntries(GameManager.CurrentPOIID);


        string warning = await this.warning.GetLocalizedStringAsync();
        string instruction = await this.instruction.GetLocalizedStringAsync();
        EasyMessageBox.Show(message: warning, button1Action: delegate { ShowInstruction(instruction); });
    }

    private void ShowInstruction(string instruction) {
        EasyMessageBox.Show(message: instruction);
    }

    private void OnEnable() {
        ARTapToPlaceObject.OnObjectPlaced += OnObjectPlaced;
    }

    private void OnDisable() {
        ARTapToPlaceObject.OnObjectPlaced -= OnObjectPlaced;
    }

    private async UniTask GenerateCatalogueEntries(int poi) {
        List<AdditionalContentPOI> acpList = DatabaseManager.SelectMultipleEntries<AdditionalContentPOI>($"POIID = {poi}");
        if (acpList == null) return;
        for (int i = 0; i < acpList.Count; i++) {
            AdditionalContent ac = DatabaseManager.SelectEntry<AdditionalContent>(acpList[i].AdditionalContentID);
            if (ac.MediaType == (int)MediaTypes.AR_PLACEMENT || ac.MediaType == (int)MediaTypes.AR_SCAN || ac.MediaType == (int)MediaTypes.AR_PLACEMENT_MULTI) {

                GameObject go = await Addressables.InstantiateAsync(catalogueEntryRef, catalogueContent);

                Button btn = go.GetComponent<Button>();
                string reference = ac.AssetReference;
                MediaTypes type = (MediaTypes)ac.MediaType;
                btn.onClick.AddListener(delegate { OnCatalogueElementClick(reference, type); });

                Image img = go.FindComponentInChildWithTag<Image>(bunLace.Constants.Tags.IMG_REPLACE);
                img.sprite = await AddressablesManager.Instance.LoadSprite(ac.AtlasReference, ac.PreviewImgReference);

                var ratioFitter = img.GetComponent<AspectRatioFitter>();
                ratioFitter.aspectMode = AspectRatioFitter.AspectMode.EnvelopeParent;
                ratioFitter.aspectRatio = (float)img.sprite.rect.width / (float)img.sprite.rect.height;

                TMPro.TextMeshProUGUI title = go.FindComponentInChildWithTag<TMPro.TextMeshProUGUI>(bunLace.Constants.Tags.NAME_REPLACE);
                if (title != null) title.text = bunLace.Localization.LocalizationManager.GetLocalizedDBString(ac.NameLocKey);
            }
        }
    }

    public void OnCatalogueElementClick(string assetReference, MediaTypes type) {
        OnPlaneVisibility(type != MediaTypes.AR_SCAN);
        ARInteractionManager.Instance.gameObject.SetActive(type == MediaTypes.AR_PLACEMENT || type == MediaTypes.AR_PLACEMENT_MULTI);
        ARManager.Instance.arTrackedImageManager.enabled = type == MediaTypes.AR_SCAN;

        if (type == MediaTypes.AR_SCAN) {
            //clear scene
            if (currentMode == MediaTypes.AR_PLACEMENT || currentMode == MediaTypes.AR_PLACEMENT_MULTI) ClearARObjects();

            //instantiate object
            Addressables.InstantiateAsync(assetReference).Completed += (asyncOperationHandle) => {
                GameObject result = asyncOperationHandle.Result;

                if (result == null) {
                    Debug.LogWarning("Could not instantiate. Please check the address: " + assetReference);
                    return;
                }
                assetReference = string.Empty;
                OnObjectPlaced(asyncOperationHandle);
            };
        }
        else if (type == MediaTypes.AR_PLACEMENT) {
            ClearARObjects();

            //single placement
            this.assetReference = assetReference;
            previousRef = assetReference;
            OnShowCatalogueBtn();
        }
        else if (type == MediaTypes.AR_PLACEMENT_MULTI) {
            if (currentMode == MediaTypes.AR_SCAN || currentMode == MediaTypes.AR_PLACEMENT) ClearARObjects();

            //multi placement
            this.assetReference = assetReference;
            previousRef = assetReference;
            OnShowCatalogueBtn();
        }

        currentMode = type;
    }

    public void OnShowCatalogueBtn() {
        catalogueOpen = !catalogueOpen;
        float endValue = catalogueOpen ? 0f : catalogue.rect.height;

        float targetScale = catalogueOpen ? -1f : 1f;
        btnImgMenu.rectTransform.DOScaleY(targetScale, 0.5f);

        catalogue.DOAnchorPosY(endValue, 1f);
    }

    /// <summary>
    /// this function is assigned to a button and serves as toggle
    /// </summary>
    public void OnPlaneVisibility() {
        OnPlaneVisibility(!visible);
    }

    public void OnPlaneVisibility(bool visible) {
        this.visible = visible;
        //Sprite s = visible ? planeVisibilityOn : planeVisibilityOff;
        //btnImgVisibility.sprite = s;

        ARInteractionManager arInteractionManager = ARInteractionManager.Instance;
        //arInteractionManager.enabled = visible;
        arInteractionManager.UnselectCurrentObj();
        ARTapToPlaceObject arTapToPlaceObject = arInteractionManager.GetComponent<ARTapToPlaceObject>();
        arTapToPlaceObject.enabled = visible;
        arTapToPlaceObject.placementIndicator.SetActive(visible);

        arPointCloudManager.enabled = visible;
        foreach (ARPointCloud pointCloud in arPointCloudManager.trackables) {
            pointCloud.gameObject.SetActive(visible);
        }

        arPlaneManager.enabled = visible;
        foreach (ARPlane plane in arPlaneManager.trackables) {
            plane.gameObject.SetActive(visible);
        }
        if (!visible && catalogueOpen) OnShowCatalogueBtn();
    }

    private void OnObjectPlaced(AsyncOperationHandle asyncOp) {
        arObjectsInScene.Add(asyncOp);
        OnPlaneVisibility(false);
    }

    public void ClearARObjects() {
        for (int i = arObjectsInScene.Count - 1; i >= 0; i--) {
            Addressables.Release(arObjectsInScene[i]);
        }
        arObjectsInScene.Clear();

        assetReference = previousRef;
    }
}
