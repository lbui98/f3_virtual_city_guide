﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;

public class ARCleaner : MonoBehaviour {
    // Start is called before the first frame update
    void Start() {
        GetComponent<ARSession>().Reset();
        //Destroy(this);
    }

    public void Clean() {
        GetComponent<ARSession>().Reset();
    }
}
