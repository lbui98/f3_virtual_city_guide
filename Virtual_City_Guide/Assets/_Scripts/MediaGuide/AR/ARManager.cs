﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using bunLace.Singleton;
using UnityEngine.XR.ARFoundation;

/// <summary>
/// Keeps a reference to ARTrackedImageManager
/// </summary>
[RequireComponent(typeof(ARTrackedImageManager))]
public class ARManager : Singleton<ARManager> {
    public ARTrackedImageManager arTrackedImageManager;
    public Camera MainCamera;

    private void Start() {
        if (arTrackedImageManager == null) arTrackedImageManager = GetComponent<ARTrackedImageManager>();
        if (MainCamera == null) MainCamera = Camera.main;
    }
}
