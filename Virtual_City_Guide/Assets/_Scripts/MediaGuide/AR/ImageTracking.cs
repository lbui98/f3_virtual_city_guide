﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using DG.Tweening;

public class ImageTracking : MonoBehaviour {
    [Serializable]
    public class ImageReference {
        public string key;
        public Texture2D texture;
        public Vector2 physicalSize;
    }

    [Tooltip("if stationary, they stay in place even when their marker tracker has been lost")]
    public bool isStationary;

    public ImageReference[] imageReferences;
    public int maxNumberOfMovingImages = 1;
    //public XRReferenceImageLibrary referenceImageLibrary;
    private TrackedImageObject[] placeablePrefabs;

    private Dictionary<string, TrackedImageObject> trackedImgObjDict = new Dictionary<string, TrackedImageObject>();

    private void Start() {
        //add objects to dictionary
        placeablePrefabs = GetComponentsInChildren<TrackedImageObject>(true);
        foreach (TrackedImageObject trackedImgObj in placeablePrefabs) {
            trackedImgObjDict.Add(trackedImgObj.imageKey, trackedImgObj);
            Debug.Log("BUNNY: Adding image to Dictionary - " + trackedImgObj.imageKey);
            trackedImgObj.gameObject.SetActive(false);
        }


        ARTrackedImageManager trackedImageManager = ARManager.Instance.arTrackedImageManager;
        trackedImageManager.enabled = false;
        trackedImageManager.trackedImagePrefab = null; //set init prefab to null since they are managed via this script
        trackedImageManager.requestedMaxNumberOfMovingImages = maxNumberOfMovingImages;

        ////create reference library
        var library = trackedImageManager.CreateRuntimeLibrary() as MutableRuntimeReferenceImageLibrary;
        if (library is MutableRuntimeReferenceImageLibrary mutableLibrary) {
            //add images
            foreach (ImageReference imageReference in imageReferences) {
                AddReferenceImageJobState job = library.ScheduleAddImageWithValidationJob(imageReference.texture, imageReference.key, imageReference.physicalSize.x);
                job.jobHandle.Complete();
                Debug.Log("BUNNY: Adding image " + imageReference.key);
            }

            trackedImageManager.referenceLibrary = library;
            trackedImageManager.enabled = true;
            ARManager.Instance.arTrackedImageManager.trackedImagesChanged += ImageChanged;
        }
        else {
            Debug.LogWarning("Mutable Library is not supported on this device");
        }

        //create reference library
        //var library = trackedImageManager.CreateRuntimeLibrary(imageReferenceLibrary);
        //trackedImageManager.referenceLibrary = library;
        //trackedImageManager.enabled = true;
        //ARManager.Instance.arTrackedImageManager.trackedImagesChanged += ImageChanged;
    }

    private void OnDisable() {
        ARManager.Instance.arTrackedImageManager.trackedImagesChanged -= ImageChanged;
    }

    private void ImageChanged(ARTrackedImagesChangedEventArgs eventArgs) {
        foreach (ARTrackedImage trackedImage in eventArgs.added) {
            if (!trackedImgObjDict.ContainsKey(trackedImage.referenceImage.name)) {
                Debug.LogWarning(trackedImage.referenceImage.name + " could not be found");
                continue;
            }

            if (trackedImgObjDict[trackedImage.referenceImage.name].locked) continue;

            trackedImgObjDict[trackedImage.referenceImage.name].TriggerOnTrackerFound(trackedImage);
            trackedImgObjDict[trackedImage.referenceImage.name].gameObject.SetActive(true);
            //UpdateImage(trackedImage);
        }

        foreach (ARTrackedImage trackedImage in eventArgs.updated) {
            if (trackedImgObjDict[trackedImage.referenceImage.name].locked) continue;

            if (trackedImage.trackingState == TrackingState.Tracking) {
                trackedImgObjDict[trackedImage.referenceImage.name].TriggerOnTrackerUpdated(trackedImage);
                UpdateImage(trackedImage);
            }
            else {
                if (!trackedImgObjDict.ContainsKey(trackedImage.referenceImage.name)) {
                    Debug.LogWarning(trackedImage.referenceImage.name + " could not be found");
                    continue;
                }
                if (!trackedImgObjDict[trackedImage.referenceImage.name].gameObject.activeSelf) continue; //don't deactivate and trigger event if already deactivated


                trackedImgObjDict[trackedImage.referenceImage.name].TriggerOnTrackerLost(trackedImage);

                trackedImgObjDict[trackedImage.referenceImage.name].gameObject.SetActive(false);
            }
        }


        foreach (ARTrackedImage trackedImage in eventArgs.removed) {
            if (trackedImgObjDict[trackedImage.referenceImage.name].locked) continue;

            if (!trackedImgObjDict.ContainsKey(trackedImage.referenceImage.name)) {
                Debug.LogWarning(trackedImage.referenceImage.name + " could not be found");
                continue;
            }
            if (!trackedImgObjDict[trackedImage.referenceImage.name].gameObject.activeSelf) continue; //don't deactivate and trigger event if already deactivated

            trackedImgObjDict[trackedImage.referenceImage.name].TriggerOnTrackerLost(trackedImage);
            trackedImgObjDict[trackedImage.referenceImage.name].gameObject.SetActive(false);
        }
    }

    private void UpdateImage(ARTrackedImage trackedImage) {
        string name = trackedImage.referenceImage.name;

        if (!trackedImgObjDict.ContainsKey(name)) return;

        TrackedImageObject prefab = trackedImgObjDict[name];
        prefab.UpdateTransform(trackedImage.transform);
        prefab.gameObject.SetActive(true);

        foreach (TrackedImageObject obj in trackedImgObjDict.Values) {
            if (obj.imageKey != name) {
                if (!obj.gameObject.activeSelf) continue; //don't deactivate and trigger event if already deactivated

                obj.TriggerOnTrackerLost(null);
                obj.gameObject.SetActive(false);
            }
        }
    }
}
