﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class AnimalDrag : MonoBehaviour, IDragHandler {

    [SerializeField] private Canvas canvas;

    public void OnDrag(PointerEventData eventData) {
        transform.localPosition += new Vector3(eventData.delta.x, eventData.delta.y) / canvas.scaleFactor;
    }
}
