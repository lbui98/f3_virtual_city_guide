using NatCam;
using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Linq;

public class MiniCam : MonoBehaviour {
    public static string MascotImageFilename { private get; set; }

    [SerializeField]
    private bool useFrontCamera;

    [SerializeField]
    private RawImage rawImage;

    [SerializeField]
    private GameObject prepareCameraInfo;

    [SerializeField]
    private AspectRatioFitter aspectFitter;

    [SerializeField]
    private Button switchCamButton;

    [SerializeField]
    private Image avatarImage;

    [SerializeField]
    private GameObject captureButton;

    [SerializeField]
    private GameObject[] allUIItems;

    private DeviceCamera deviceCamera;
    private Texture previewTexture;
    private Texture2D photo;

    public void Initialize(Sprite img) {
        avatarImage.sprite = img;

        // Start camera preview
        deviceCamera = useFrontCamera ? DeviceCamera.FrontCamera : DeviceCamera.RearCamera;
        if (!deviceCamera) {
            Debug.LogError("Camera is null. Consider using " + (useFrontCamera ? "rear" : "front") + " camera");
            return;
        }

        // Setup camera
        deviceCamera.PreviewResolution = new Vector2Int(1200, 690);
        deviceCamera.Framerate = 24;

        // Hidden Camera Info View with delay
        StartCoroutine(HiddenCameraInfo());

        deviceCamera.StartPreview(OnStart);
        switch (NativeGallery.CheckPermission(NativeGallery.PermissionType.Write, NativeGallery.MediaType.Image)) {
            case NativeGallery.Permission.ShouldAsk:
                NativeGallery.RequestPermission(NativeGallery.PermissionType.Write, NativeGallery.MediaType.Image);
                break;
            case NativeGallery.Permission.Denied:
            case NativeGallery.Permission.Granted:
            default:
                break;
        }
    }

    IEnumerator HiddenCameraInfo() {
        yield return new WaitForSeconds(2);
        prepareCameraInfo.SetActive(false);
    }

    /// <summary>
    /// Fix for memory issue and camera error 2 on Android. Stops preview when leaving scene.
    /// On Android it is possible for the camera to be connected or in use after you left the scene
    /// </summary>
    private void OnDestroy() {
        deviceCamera?.StopPreview();
        deviceCamera = null;
    }

    private void OnStart(Texture preview) {
        // Display the preview
        previewTexture = preview;
        rawImage.texture = preview;
        aspectFitter.aspectRatio = preview.width / (float)preview.height;
        // Set flash to auto
        deviceCamera.FlashMode = FlashMode.Auto;
    }

    public virtual void CapturePhoto() {
        captureButton.SetActive(false);

        foreach (GameObject go in allUIItems) {
            go.SetActive(false);
        }

        StartCoroutine(TakeScreenshotAndSave());
    }

    private IEnumerator TakeScreenshotAndSave() {
        yield return new WaitForEndOfFrame();

        var screenShot = ScreenCapture.CaptureScreenshotAsTexture();
        NativeGallery.SaveImageToGallery(screenShot, "Stempelpass", "Stempelpass {0}.png", PhotoSaved);
        Destroy(screenShot);
        foreach (GameObject go in allUIItems) {
            go.SetActive(true);
        }

        //TutorialManager.Instance.OnPictureCaptured();
    }

    private void PhotoSaved(bool success, string path) {
        if (success) {
            Debug.Log("Photo saved to " + path);
        }
        else {
            Debug.Log("Photo could not be saved. Please check the path: " + path);
        }
        captureButton.SetActive(true);
    }

    private void PhotoSaved(string error) {
        if (error != null) {
            Debug.LogError("Error while saving photo to gallery: " + error);
        }
        else {
            Debug.Log("Photo saved!");
        }

        captureButton.SetActive(true);
    }

    public void SwitchCamera() {
        deviceCamera.StopPreview();
        deviceCamera = deviceCamera.IsFrontFacing ? DeviceCamera.RearCamera : DeviceCamera.FrontCamera;
        deviceCamera.StartPreview(OnStart);
    }

    public void FocusCamera(BaseEventData e) {
        // Get the touch position in viewport coordinates
        var eventData = e as PointerEventData;
        RectTransform transform = eventData.pointerPress.GetComponent<RectTransform>();
        Vector3 worldPoint;
        if (!RectTransformUtility.ScreenPointToWorldPointInRectangle(transform, eventData.pressPosition, eventData.pressEventCamera, out worldPoint))
            return;
        var corners = new Vector3[4];
        transform.GetWorldCorners(corners);
        var point = worldPoint - corners[0];
        var size = new Vector2(corners[3].x, corners[1].y) - (Vector2)corners[0];
        Vector2 relativePoint = new Vector2(point.x / size.x, point.y / size.y);
        // Set the focus point
        deviceCamera.FocusPoint = relativePoint;
    }
}