using bunLace.Database;
using bunLace.Tools;
using Cysharp.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfieMediaManager : MediaGuideSubManager {
    public MiniCam miniCam;

    public override void LoadContent(AdditionalContent content, MediaTypes type) {
        LoadContentInternal(content, type);
    }

    private async UniTask LoadContentInternal(AdditionalContent content, MediaTypes type) {
        Sprite sp = await AddressablesManager.Instance.LoadSprite(content.AtlasReference, content.PreviewImgReference);
        miniCam.Initialize(sp);
    }

    public override void OnBtnReturn() {
        LoadMainScene();
    }
}
