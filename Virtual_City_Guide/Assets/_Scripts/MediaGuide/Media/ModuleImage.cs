using Cysharp.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ModuleImage : MonoBehaviour {
    [SerializeField] private Image img;

    public void StartUp(string atlasReference, string imgKey) {
        StartUpInternal(atlasReference, imgKey);
    }

    private async UniTask StartUpInternal(string atlasReference, string imgKey) {
        if (string.IsNullOrEmpty(atlasReference)) {
            img.sprite = await AddressablesManager.Instance.LoadSprite(imgKey);
        }
        else {
            img.sprite = await AddressablesManager.Instance.LoadSprite(atlasReference, imgKey);
        }
    }
}
