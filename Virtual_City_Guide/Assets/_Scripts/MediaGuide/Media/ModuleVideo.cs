﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using UnityEngine.AddressableAssets;
using Cysharp.Threading.Tasks;

[RequireComponent(typeof(VideoPlayer))]
public class ModuleVideo : MonoBehaviour {
    [SerializeField] private AssetReference videoKey;
    [SerializeField] private GameObject fullScreen;
    [SerializeField] private RotationDirection rotationDirection;
    private Slider seekBar;
    private TMPro.TextMeshProUGUI currentTime;
    private double clipLength;
    public enum RotationDirection { NONE, CLOCKWISE, COUNTER_CLOCKWISE }

    [SerializeField] private VideoPlayer videoPlayer;
    private bool dragging;

    private void Start() {
        StartUp(videoKey.RuntimeKey.ToString());
    }

    public void StartUp(string videoKey) {
        StartupInternal(videoKey);
    }

    private async UniTask StartupInternal(string videoKey) {
        videoPlayer = GetComponent<VideoPlayer>();

        videoPlayer.renderMode = VideoRenderMode.RenderTexture;

        VideoClip clip = await AddressablesManager.Instance.LoadVideo(videoKey);
        videoPlayer.clip = clip;

        RenderTexture renderTexture = new RenderTexture((int)Screen.width, (int)Screen.height, 0);
        videoPlayer.targetTexture = renderTexture;
        RawImage[] rawImages = GetComponentsInChildren<RawImage>(true);
        foreach (RawImage raw in rawImages) {
            raw.texture = renderTexture;
        }

        videoPlayer.Play();

        seekBar = fullScreen.GetComponentInChildren<Slider>();
        seekBar.minValue = 0;
        seekBar.maxValue = (float)clip.length;
        clipLength = clip.length;
        seekBar.onValueChanged.AddListener(OnSeekBarChanged);
        currentTime = seekBar.GetComponentInChildren<TMPro.TextMeshProUGUI>();
        SetTime(videoPlayer.time);
    }

    private void Update() {
        if (videoPlayer != null && videoPlayer.isPlaying && !dragging) {
            seekBar.value = (float)videoPlayer.time;
        }
    }

    public void OnSliderBeginDrag() {
        dragging = true;
    }

    public void OnSliderEndDrag() {
        dragging = false;
    }

    public void OnFullscreenPlay() {
        if (!videoPlayer.isPlaying || videoPlayer.isPaused) {
            videoPlayer.Play();
        }
        else {
            videoPlayer.Pause();
        }
    }

    private void OnSeekBarChanged(float value) {
        SetTime(value);
        if (dragging)
            videoPlayer.time = Mathf.Clamp(value, 0f, (float)videoPlayer.clip.length);
    }

    private void SetTime(double currentSecond) {
        System.TimeSpan currentT = new System.TimeSpan(0, 0, (int)currentSecond);
        System.TimeSpan totalT = new System.TimeSpan(0, 0, (int)clipLength);

        currentTime.text = $"{currentT.ToString(@"mm\:ss")} / {totalT.ToString(@"mm\:ss")}";
    }
}
