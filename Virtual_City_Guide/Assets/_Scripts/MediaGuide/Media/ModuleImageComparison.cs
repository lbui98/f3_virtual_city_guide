using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using bunLace.Tools;

public class ModuleImageComparison : MonoBehaviour {
    private Slider slider;
    [SerializeField] private Image target;

    private void Start() {
        slider = GetComponentInChildren<Slider>();
        //target = this.gameObject.FindComponentInChildWithTag<Image>(bunLace.Constants.Tags.IMG_REPLACE);

        slider.onValueChanged.AddListener(OnSliderValueChange);
    }

    private void OnSliderValueChange(float value) {
        target.fillAmount = value;
    }
}
