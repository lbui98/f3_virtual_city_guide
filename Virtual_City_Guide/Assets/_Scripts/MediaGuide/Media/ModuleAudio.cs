using Cysharp.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.UI;

public class ModuleAudio : MonoBehaviour {
    [SerializeField] private TextMeshProUGUI currentTime;
    private Slider seekbar;
    private AudioSource source;
    private bool dragging;
    [SerializeField] private Image playPause;
    [SerializeField] private Sprite play, pause;

    public void StartUp(string audioKey) {
        StartUpInternal(audioKey);
    }

    private async UniTask StartUpInternal(string audioKey) {
        seekbar = GetComponentInChildren<Slider>();
        seekbar.onValueChanged.AddListener(OnSliderValueChange);

        AudioClip audio = await SoundManager.GetAudioClip(audioKey);
        GameObject go = new GameObject();
        go.transform.parent = transform;
        source = go.AddComponent<AudioSource>();
        source.volume = GameManager.SaveData.soundSettings._maxAudioFileVolume;
        source.clip = audio;
        source.Play();
        playPause.sprite = pause;

        seekbar.minValue = 0f;
        seekbar.maxValue = audio.length;
    }

    private void OnSliderValueChange(float value) {
        SetTime(value);
        if (dragging && value < source.clip.length) {
            source.time = value;
            if (!source.isPlaying) source.Play();
        }
    }

    private void Update() {
        if (source != null && source.isPlaying && !dragging) {
            seekbar.value = source.time;
        }
    }

    public void PauseResumeAudio() {
        if (source != null) {
            if (source.isPlaying) {
                source.Pause();
                playPause.sprite = play;
            }
            else {
                source.Play();
                playPause.sprite = pause;
            }
        }
    }

    public void OnSliderBeginDrag() {
        dragging = true;
    }

    public void OnSliderEndDrag() {
        dragging = false;
    }

    private void SetTime(double currentSecond) {
        System.TimeSpan currentT = new System.TimeSpan(0, 0, (int)currentSecond);
        System.TimeSpan totalT = new System.TimeSpan(0, 0, (int)source.clip.length);

        currentTime.text = $"{currentT.ToString(@"mm\:ss")} / {totalT.ToString(@"mm\:ss")}";
    }
}
