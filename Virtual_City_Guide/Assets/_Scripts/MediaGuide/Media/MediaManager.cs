using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.SceneManagement;
using bunLace.Singleton;
using bunLace.Tools;
using UnityEngine.UI;

public class MediaManager : MediaGuideSubManager {
    [SerializeField] private ModuleImage ImageViewer;
    [SerializeField] private ModuleAudio AudioPlayer;
    [SerializeField] private ModuleVideo VideoPlayer;

    public override void LoadContent(bunLace.Database.AdditionalContent content, bunLace.Tools.MediaTypes type) {
        switch (type) {
            case MediaTypes.CUSTOM_AUDIO_VISUAL:
                Addressables.InstantiateAsync(content.AssetReference, this.content);
                break;
            case MediaTypes.SINGLE_IMAGE:
                ImageViewer.StartUp(content.AtlasReference, content.AssetReference);
                ImageViewer.gameObject.SetActive(true);
                break;
            case MediaTypes.AUDIO_PLAYER:
                AudioPlayer.StartUp(content.AssetReference);
                AudioPlayer.gameObject.SetActive(true);
                break;
            case MediaTypes.VIDEO_PLAYER:
                VideoPlayer.StartUp(content.AssetReference);
                VideoPlayer.gameObject.SetActive(true);
                break;

        }
    }

    public override void OnBtnReturn() {
        UnloadScene(bunLace.Constants.Scenes.MEDIA_VIEWER);
    }
}
