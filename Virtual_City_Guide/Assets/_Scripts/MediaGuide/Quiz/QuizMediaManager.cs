using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.AddressableAssets;
using UnityEngine.UI;
using Cysharp.Threading.Tasks;
using bunLace.Database;
using System.Linq;
using bunLace.SaveLoad;
using UnityEngine.EventSystems;
using System;
using bunLace.Localization;
using UnityEngine.Localization;

public class QuizMediaManager : MediaGuideSubManager {
    public class Quiz {
        public string question;
        public string[] answers; //by default, the first element is the correct answer
        public string feedback;
    }

    [SerializeField] private GameObject panelQuizWindow;
    [SerializeField] private TextMeshProUGUI title, question, feedback;
    [SerializeField] private AssetReference answerListElementReference;
    private List<GameObject> answerList = new List<GameObject>();
    [SerializeField] private Color correct, wrong;

    [SerializeField] private AssetReference correctAnswerSound;
    [SerializeField] private AssetReference wrongAnswerSound;

    //HP
    [SerializeField] private Sprite lostHeart;
    [SerializeField] private Image heart;
    [SerializeField] private LocalizedString pointsNotice;

    private int currentQuizID = -1;
    private bool clicked = false;

    public delegate void QuizCleared();
    public static event QuizCleared OnQuizCleared;

    public override void OnBtnReturn() {
        UnloadScene(bunLace.Constants.Scenes.QUIZ_VIEWER);
    }

    public override void LoadContent(AdditionalContent content, bunLace.Tools.MediaTypes type) {
        currentQuizID = content.ID;
        QuizData quizData = GameManager.SaveData.quizData.FirstOrDefault(data => data.id == currentQuizID);

        if (quizData == null) {
            pointsNotice.GetLocalizedStringAsync().Completed += asyncOp => {
                EasyMessageBox.Show(asyncOp.Result, button1Action: delegate { LoadContentInternal(content); });
            };
        }
        else {
            heart.gameObject.SetActive(!quizData.cleared);

            LoadContentInternal(content);
        }
    }

    private async UniTask LoadContentInternal(AdditionalContent content) {

        Quiz quiz = JsonUtility.FromJson<Quiz>(content.AssetReference);
        QuizData quizData = GameManager.SaveData.quizData.FirstOrDefault(data => data.id == currentQuizID);

        question.text = LocalizationManager.GetLocalizedDBString(quiz.question);
        for (int i = 0; i < quiz.answers.Length; i++) {
            GameObject go = await Addressables.InstantiateAsync(answerListElementReference, this.content);

            Button button = go.GetComponent<Button>();
            bool correct = i == 0;
            button.onClick.AddListener(delegate { OnBtnAnswerClick(correct); });
            string answer = LocalizationManager.GetLocalizedDBString(quiz.answers[i]);
            go.GetComponentInChildren<TextMeshProUGUI>().text = answer;
            answerList.Add(go);
        }

        if (!string.IsNullOrEmpty(quiz.feedback)) {
            feedback.text = LocalizationManager.GetLocalizedDBString(quiz.feedback);
        }

        //shuffle answer possiblities
        for (int i = 0; i < this.content.childCount; i++) {
            this.content.GetChild(i).SetSiblingIndex(UnityEngine.Random.Range(0, this.content.childCount));
        }

        //check clear and adjust hearts
        if (quizData != null) {
            if (!quizData.cleared || !quizData.firstTimeClear) {
                heart.sprite = lostHeart;
            }
        }

        //display quiz window once initialization is done
        panelQuizWindow.SetActive(true);
    }

    private async UniTask OnBtnAnswerClick(bool correct) {
        if (clicked) return;
        QuizData quizData = GameManager.SaveData.quizData.FirstOrDefault(data => data.id == currentQuizID);

        if (quizData == null) {
            //no quiz data recorded yet
            //adding now
            GameManager.SaveData.quizData.Add(new QuizData { id = currentQuizID, firstTimeClear = correct, cleared = correct });
        }
        else {
            if (!quizData.cleared) {
                GameManager.SaveData.quizData.FirstOrDefault(data => data.id == currentQuizID).cleared = correct;
                GameManager.Instance.Save();
            }
        }


        Image btnImg = EventSystem.current.currentSelectedGameObject.GetComponent<Image>();

        //feedback
        if (correct) {
            SoundManager.PlaySFX(correctAnswerSound.RuntimeKey.ToString());
            btnImg.color = this.correct;
            await UniTask.Delay(TimeSpan.FromSeconds(1));
            OnQuizCleared?.Invoke();

            if (string.IsNullOrEmpty(feedback.text)) {
                OnBtnReturn();
            }
            else {
                foreach (GameObject go in answerList) {
                    go.GetComponent<CanvasGroup>().alpha = 0f;
                    go.GetComponent<Button>().onClick.RemoveAllListeners();
                    go.GetComponent<Button>().onClick.AddListener(OnBtnReturn);
                }

                feedback.gameObject.SetActive(true);
            }
        }
        else {
            heart.sprite = lostHeart;

            SoundManager.PlaySFX(wrongAnswerSound.RuntimeKey.ToString());
            btnImg.color = wrong;
        }


        clicked = false;
    }
}
