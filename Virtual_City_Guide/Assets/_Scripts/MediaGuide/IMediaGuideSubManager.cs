using Cysharp.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using bunLace.Database;
using bunLace.Tools;

public interface IMediaGuideSubManager {
    void LoadContent(AdditionalContent content, MediaTypes type);

    void OnBtnReturn();

    /// <summary>
    /// use when scene was loaded additively
    /// </summary>
    /// <param name="scene"></param>
    void UnloadScene(string scene);

    /// <summary>
    /// use when scene was loaded single
    /// </summary>
    /// <param name="scene"></param>
    UniTask LoadMainScene();
}
