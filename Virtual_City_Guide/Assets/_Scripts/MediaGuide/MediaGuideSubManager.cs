using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.SceneManagement;
using bunLace.Singleton;
using Cysharp.Threading.Tasks;
using bunLace.Database;
using bunLace.Tools;

public abstract class MediaGuideSubManager : Singleton<MediaGuideSubManager>, IMediaGuideSubManager {
    public Transform content;

    public abstract void LoadContent(AdditionalContent content, MediaTypes type);

    public abstract void OnBtnReturn();

    public void UnloadScene(string scene) {
        SceneManager.UnloadSceneAsync(scene);
        SoundManager.StopAudio();
        GameManager.Instance.Save();
    }

    public async UniTask LoadMainScene() {
        SoundManager.StopAudio();
        GameManager.Instance.Save();
        await SceneManager.LoadSceneAsync(bunLace.Constants.Scenes.MAIN);
        POIMenuManager.Instance.OpenMenu(GameManager.CurrentPOIID);
    }
}
