//This class is auto-generated do not modify
namespace bunLace.Constants
{
	public static class Scenes
	{
		public const string START = "Start";
		public const string MAIN = "Main";
		public const string MEDIA_VIEWER = "MediaViewer";
		public const string _ARVIEWER = "_ARViewer";
		public const string ARVIEWER = "ARViewer";
		public const string QUIZ_VIEWER = "QuizViewer";
		public const string MINIGAME_VIEWER = "MinigameViewer";
		public const string SELFIE_VIEWER = "SelfieViewer";
		public const string NEW_SCENE = "New Scene";
		public const string COMMUNITY = "Community";

		public const int TOTAL_SCENES = 10;


		public static int nextSceneIndex()
		{
			if( UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex + 1 == TOTAL_SCENES )
				return 0;
			return UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex + 1;
		}
	}
}