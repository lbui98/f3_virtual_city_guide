//This class is auto-generated do not modify
namespace bunLace.Constants
{
	public static class Tags
	{
		public const string UNTAGGED = "Untagged";
		public const string RESPAWN = "Respawn";
		public const string FINISH = "Finish";
		public const string EDITOR_ONLY = "EditorOnly";
		public const string MAIN_CAMERA = "MainCamera";
		public const string PLAYER = "Player";
		public const string GAME_CONTROLLER = "GameController";
		public const string IMG_REPLACE = "ImgReplace";
		public const string AROBJECT = "ARObject";
		public const string NAME_REPLACE = "NameReplace";
		public const string TEXT_REPLACE = "TextReplace";
		public const string TARGET = "Target";
		public const string CHARACTER = "Character";
	}
}