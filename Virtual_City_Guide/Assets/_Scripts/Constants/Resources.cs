//This class is auto-generated do not modify
namespace bunLace.Constants
{
	public static class Resources
	{
		public const string BILLING_MODE = "BillingMode";
		public const string DOTWEEN_SETTINGS = "DOTweenSettings";
		public const string IAPPRODUCT_CATALOG = "IAPProductCatalog";
		public const string LINE_BREAKING_FOLLOWING_CHARACTERS = "LineBreaking Following Characters";
		public const string LINE_BREAKING_LEADING_CHARACTERS = "LineBreaking Leading Characters";
		public const string TMP_SETTINGS = "TMP Settings";
		public const string DEFAULT_STYLE_SHEET = "Style Sheets/Default Style Sheet";
		public const string XRSIMULATION_RUNTIME_SETTINGS = "XRSimulationRuntimeSettings";
		public const string ALL_IN1ONE_SHADER_FUNCTIONS = "AllIn1OneShaderFunctions";
		public const string ALL_IN1SPRITE_SHADER = "AllIn1SpriteShader";
		public const string ALL_IN1SPRITE_SHADER_SCALED_TIME = "AllIn1SpriteShaderScaledTime";
		public const string ALL_IN1SPRITE_SHADER_UI_MASK = "AllIn1SpriteShaderUiMask";
		public const string PROCEDURAL_UI_IMAGE_DEFAULT_SPRITE = "procedural_ui_image_default_sprite";
		public const string PROCEDURAL_IMAGE_RUNTIME = "Shaders/ProceduralImageRuntime";
		public const string XRSIMULATION_PREFERENCES = "XRSimulationPreferences";
		public const string ALL_IN1URP2D_RENDERER = "AllIn1Urp2dRenderer";
		public const string TRUE_SHADOW_PROJECT_SETTINGS = "True Shadow Project Settings";
		public const string IMPRINT_POST_PROCESS = "Shaders/ImprintPostProcess";
		public const string SHADOW_CUTOUT = "Shaders/ShadowCutout";
		public const string SHADOW_GENERATE = "Shaders/ShadowGenerate";
		public const string SHADOW_POST_PROCESS = "Shaders/ShadowPostProcess";
		public const string TRUE_SHADOW_ADDITIVE = "Shaders/TrueShadow-Additive";
		public const string TRUE_SHADOW_MULTIPLY = "Shaders/TrueShadow-Multiply";
		public const string TRUE_SHADOW_NORMAL = "Shaders/TrueShadow-Normal";
		public const string TRUE_SHADOW_SCREEN = "Shaders/TrueShadow-Screen";
		public const string TRUE_SHADOW = "Shaders/TrueShadow";
		public const string DEFAULT_MESSAGE_BOX = "Default Message Box";
		public const string GLOBAL_SETTINGS = "Global Settings";
		public const string LETTERBOX = "Letterbox";
	}
}