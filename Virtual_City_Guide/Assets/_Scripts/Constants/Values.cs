using System.Collections.Generic;
using UnityEngine;

namespace bunLace.Constants {
    public static class Values {
        public const float ANIMATION_SLIDE_SPEED = 0.5f;
        public const float FADE_PANEL_DURATION = 0.3f;
        public const float FADE_TEXT_DURATION = 0.5f;
        public const float COLOR_FADE_DURATION = 0.5f;
        public const float VOLUME_FADE_DURATION = 0.5f;
        public const string STAMP_ANIMATION_TRIGGER = "stamp";

        public const int FONT_SUB_SIZE = 42;
        public const int FONT_SUB_SIZE_SMALL = 36;
        public const int FONT_SUB_SIZE_TINY = 32;
        public const int QUIZ_FIRST_TIME_CLEAR_POINTS = 5;
        public const int QUIZ_CLEAR_POINTS = 2;
        public const int MINIGAME_CLEAR_POINTS = 5;

        public const int MAX_LOGBOOK_LOAD = 5;

        public const int MAX_UPLOAD_SIZE = 5242880; //in bytes

        public const string EXPLANATION_SUFFIX = "_EXPLANATION";
        public const string MAP_USER_MARKER = "USER";

        public const string LOC_KEY_CONFIRM = "CONFIRM";
        public const string LOC_KEY_CANCEL = "CANCEL";
        public const string LOC_KEY_RETRY = "RETRY";
        public const string LOC_KEY_CONTINUE = "CONTINUE";
        public const string LOC_KEY_NO_CAMERA = "NO_CAMERA_PERMISSION";
        public const string LOC_KEY_DO_NOT_ASK_AGAIN = "DO_NOT_ASK_AGAIN";
        public const string LOC_KEY_POI_NOT_VISITED = "POI_NOT_VISITED";
        public const string LOC_KEY_STRIKES = "STRIKES";
        public const string LOC_KEY_POINTS = "POINTS";


        public static string[] LOC_KEYS = { LOC_KEY_CONFIRM, LOC_KEY_CANCEL, LOC_KEY_RETRY, LOC_KEY_CONTINUE, LOC_KEY_NO_CAMERA, LOC_KEY_DO_NOT_ASK_AGAIN, LOC_KEY_POI_NOT_VISITED, LOC_KEY_STRIKES, LOC_KEY_POINTS };
        public static Dictionary<string, string> LOCALIZED_TERMS = new Dictionary<string, string>();

        public const string AUDIO_TEST_KEY = "Assets/Sounds/SFX/Win2.wav";
        public const string PLACEHOLDER_IMG_KEY = "Assets/Sprites/UI/placeholder.PNG";
        public const string AUDIO_PAGE_FLIP_KEY = "Assets/Sounds/SFX/window_close.wav";

        public const string GENERAL_HELP_URL = "https://stempelpass.de/APP/web/Help.html";
        public const string GENERAL_REGION_URL = "https://stempelpass.de";

        public const string PRIVACY_PLAYER_PREF = "Privacy";
        public const string DO_NOT_ASK_PLAYER_PREF = "DoNotAskAgain";
        public const string INITIAL_DOWNLOAD_PLAYER_PREF = "InitialDownload";

    }
}