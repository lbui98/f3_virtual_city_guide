using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Localization;
using UnityEngine.Localization.Settings;

namespace bunLace.Database {
    public class Achievements : TableBaseData {
        public int RegionID { get; set; }
        public int NameLocKey { get; set; }
        public int DescriptionLocKey { get; set; }
        public string AtlasReference { get; set; }
        public string ImgReference { get; set; }
        public string ConditionsJSON { get; set; }
        public int IsRegional { get; set; }
    }

    public class AchievementCondition {
        //quiz conditions
        public int[] quizzesToBeCleared;
        public int numberOfQuizzesCleared;
        public int numberOfQuizzesFirstTimeCleared;

        //poi conditions
        public int[] poisToBeVisited;
        public int numberOfpoisVisited;

        //minigame conditions
        public int[] minigamesToBeCleared;
        public int numberOfMinigamesCleared;

        //achievement conditions
        public int[] achievementsToCollect;
        public int numberAchievementsToCollect;
    }

    public class AdditionalContent : TableBaseData {
        public int NameLocKey { get; set; }
        public int DescriptionLocKey { get; set; }
        public string AtlasReference { get; set; }
        public string PreviewImgReference { get; set; }
        public string AssetReference { get; set; }
        public int MediaType { get; set; }
        public int RequiresPOIUnlocked { get; set; }
    }

    public class POI : TableBaseData {
        public int NameLocKey { get; set; }
        public int DescriptionLocKey { get; set; }
        public int MoreDescriptionLocKey { get; set; }
        public int ContactInfoLocKey { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public int Perimeter { get; set; }
        public string AtlasReference { get; set; }
        public string ImgReference { get; set; }
        public string StampAtlasReference { get; set; }
        public string StampReference { get; set; }
        public string StampColor { get; set; }
        public string WebsiteURL { get; set; }
        public string SponsorReference { get; set; }
        public string Telephone { get; set; }
        public int UnlockPoints { get; set; }
        public int Order { get; set; }
        public int TypeInt { get; set; }
        public Tools.POITypes Type { get { return (Tools.POITypes)TypeInt; } }

    }

    public class Region : TableBaseData {
        public string Name { get; set; }
        public int DescriptionLocKey { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }
        public string AtlasReference { get; set; }
        public string ImgReference { get; set; }
        public string LogoImgReference { get; set; }
        public int DefaultTour { get; set; }
        public string HelpURL { get; set; }
        public string ColorBackground { get; set; }
        public string ColorForeground { get; set; }
        public string ColorSelected { get; set; }
        public string ColorUnselected { get; set; }
        public string ColorTextDark { get; set; }
        public string ColorTextLight { get; set; }
        public int IsActive { get; set; }
        public string Link { get; set; }
        public int Order { get; set; }
        public string AdReference { get; set; }
        public string PreviewReference { get; set; }
        public int ParentID { get; set; }

    }

    public class Sounds : TableBaseData {
        public string Name { get; set; }
        public string SoundReference { get; set; }
    }

    public class Tour : TableBaseData {
        public int NameLocKey { get; set; }
        //public string DescriptionLocKey { get; set; }
        public int RegionID { get; set; }
        public int TourType { get; set; }
        public string AtlasReference { get; set; }
        public string ImgPreviewReference { get; set; }
        public string POIIDsJSON { get; set; }
        public TourPOI[] POIIDs {
            get {
                return JsonHelper.FromJson<TourPOI>(POIIDsJSON);
            }
        }
    }

    [System.Serializable]
    public class TourPOI {
        public int poiID;
        public int requiredPoints;
        public int nextDestinationMessage;
    }

    public class Localization : TableBaseData {
        public string Key { get; set; }
        public string LocalizedString {
            get {
                Locale locale = LocalizationSettings.SelectedLocale;
                string code = locale.Identifier.Code;


                switch (code) {
                    case "de":
                        if (!string.IsNullOrEmpty(DE) || !string.IsNullOrWhiteSpace(DE))
                            return DE;
                        else return EN;
                    case "en":
                        if (!string.IsNullOrEmpty(EN) || !string.IsNullOrWhiteSpace(EN))
                            return EN;
                        else return DE;
                    case "fr":
                        if (!string.IsNullOrEmpty(FR) || !string.IsNullOrWhiteSpace(FR))
                            return FR;
                        else if (!string.IsNullOrEmpty(EN) || !string.IsNullOrWhiteSpace(EN))
                            return EN;
                        else return DE;
                    case "es":
                        if (!string.IsNullOrEmpty(ES) || !string.IsNullOrWhiteSpace(ES))
                            return ES;
                        else if (!string.IsNullOrEmpty(EN) || !string.IsNullOrWhiteSpace(EN))
                            return EN;
                        else return DE;
                    case "zh":
                        if (!string.IsNullOrEmpty(CN) || !string.IsNullOrWhiteSpace(CN)) {
                            return CN;
                        }
                        else if (!string.IsNullOrEmpty(EN) || !string.IsNullOrWhiteSpace(EN))
                            return EN;
                        else return DE;
                    case "zh-CN":
                        if (!string.IsNullOrEmpty(CN) || !string.IsNullOrWhiteSpace(CN))
                            return CN;
                        else if (!string.IsNullOrEmpty(EN) || !string.IsNullOrWhiteSpace(EN))
                            return EN;
                        else return DE;
                    case "ja":
                        if (!string.IsNullOrEmpty(JP) || !string.IsNullOrWhiteSpace(JP))
                            return JP;
                        else if (!string.IsNullOrEmpty(EN) || !string.IsNullOrWhiteSpace(EN))
                            return EN;
                        else return DE;
                    default:
                        Debug.LogWarning($"Locale {code} not registered, using English instead");
                        if (!string.IsNullOrEmpty(EN) || !string.IsNullOrWhiteSpace(EN))
                            return EN;
                        else return DE;
                }

            }
        }
        public string DE { get; set; }
        public string EN { get; set; }
        public string FR { get; set; }
        public string ES { get; set; }
        public string CN { get; set; }
        public string JP { get; set; }
    }

    public class Reward : TableBaseData {
        public int RegionID { get; set; }
        public string Code { get; set; }
        public int RequiredPoints { get; set; }
        public string RewardMessage { get; set; }
    }

    public class VersionControl : TableBaseData {
        public string Version { get; set; }
    }

    public class AdditionalContentPOI : TableBaseData {
        public int AdditionalContentID { get; set; }
        public int POIID { get; set; }

    }

    public class POIRegion : TableBaseData {
        public int POIID { get; set; }
        public int RegionID { get; set; }

    }
}