﻿using UnityEngine;
using System;
using System.IO;
using System.Collections.Generic;
using bunLace.Tools;
using bunLace.Database;

namespace bunLace.SaveLoad {
    public static class SaveLoadManager {

        public static void Save(SaveData data, string fileName = "stempelpass_save") {
            string path = Application.persistentDataPath + "/" + fileName + ".json";
            string json = JsonUtility.ToJson(data);

            StreamWriter stream = File.CreateText(path);
            stream.Close();

            File.WriteAllText(path, json);
        }

        public static void SaveNavigation(NavigationSaveData data, string fileName = "navi") {
            string path = Application.persistentDataPath + "/" + fileName + ".json";
            string json = JsonUtility.ToJson(data);

            StreamWriter stream = File.CreateText(path);
            stream.Close();

            File.WriteAllText(path, json);
        }

        public static SaveData Load(string fileName = "stempelpass_save") {
            string path = Application.persistentDataPath + "/" + fileName + ".json";
            if (File.Exists(path)) {
                string json = File.ReadAllText(path);
                return JsonUtility.FromJson<SaveData>(json);
            }
            else {
                return null;
            }
        }

        public static NavigationSaveData LoadNavi(string fileName = "navi") {
            string path = Application.persistentDataPath + "/" + fileName + ".json";
            if (File.Exists(path)) {
                string json = File.ReadAllText(path);
                return JsonUtility.FromJson<NavigationSaveData>(json);
            }
            else {
                return null;
            }
        }

        public static void MigrateOldSaveFile() {
            string path = Application.persistentDataPath + "/save.json";
            if (File.Exists(path)) {
                //old save file exists
                //migrate
                string json = File.ReadAllText(path);
                json = json.Replace("{\"PoiDatas\":", "");
                json = json.TrimEnd('}');

                Debug.Log(json);

                POIData[] PoiDatas = JsonUtil.FromJson<POIData[]>(json);
                Debug.Log(PoiDatas.Length);
                if (PoiDatas != null) {
                    foreach (POIData data in PoiDatas) {
                        GameManager.SaveData.poiData.Add(new POIsData { id = data.id, dateOfVisit = DateTime.Now.ToString() });

                        foreach (bool done in data.quizDoneMulti) {
                            if (done) {
                                List<AdditionalContentPOI> additionalContentPOIs = DatabaseManager.SelectMultipleEntries<AdditionalContentPOI>($"POIID = {data.id}");
                                if (additionalContentPOIs != null) {
                                    foreach (AdditionalContentPOI acp in additionalContentPOIs) {
                                        AdditionalContent ac = DatabaseManager.SelectEntry<AdditionalContent>($"ID = {acp.AdditionalContentID} AND MediaType = {(int)MediaTypes.QUIZ}");
                                        GameManager.SaveData.quizData.Add(new QuizData { id = ac.ID, cleared = true, firstTimeClear = true });
                                    }
                                }
                            }
                            break;
                        }
                    }
                }
            }
        }
    }


    [Serializable]
    public class SaveData {
        public string SaveTime;
        public string version;
        public SoundManager.SoundSettings soundSettings;
        public int currentRegionID;
        public List<QuizData> quizData;
        public List<POIsData> poiData;
        public List<int> bookmarkData;
        public List<int> minigameData;
        public List<RedeemData> redeemedRewardData;
        public TourData tourData;
        public CommunityData communityData;

        public SaveData(int currentRegionID, List<QuizData> quizData, List<POIsData> poiData, List<int> minigameData, TourData tourData, List<int> bookmarkData, List<RedeemData> redeemedRewardData, SoundManager.SoundSettings soundSettings, CommunityData communityData) {
            this.soundSettings = soundSettings;
            this.currentRegionID = currentRegionID;
            this.quizData = quizData;
            this.poiData = poiData;
            this.minigameData = minigameData;
            this.tourData = tourData;
            this.bookmarkData = bookmarkData;
            this.redeemedRewardData = redeemedRewardData;
            this.communityData = communityData;
        }
    }

    [Serializable]
    public class NavigationSaveData {
        public List<TourNavigationPoints> tourNavigations;

        public NavigationSaveData(List<TourNavigationPoints> tourNavigationPoints) {
            tourNavigations = tourNavigationPoints;
        }

        public TourNavigationPoints GetTourNavigationPoints(int tourID) {
            TourNavigationPoints t = null;

            for (int i = tourNavigations.Count - 1; i >= 0; i--) {
                if (DateTime.Compare(DateTime.Now, tourNavigations[i].ExpirationDate) > 0) {
                    //expired
                    //remove
                    tourNavigations.RemoveAt(i);
                    continue;
                }

                if (tourNavigations[i].tourID == tourID) {
                    t = tourNavigations[i];
                }
            }

            return t;
        }
    }

    [Serializable]
    public class SubSaveData { }

    [Serializable]
    public class POIsData : SubSaveData {
        public int id;
        public string dateOfVisit;
    }

    [Serializable]
    public class POIData : SubSaveData {
        public int id;
        public bool poiDone;
        public bool[] quizDoneMulti;
        public bool bonusDone;

        public POIData(int id, bool poiDone, bool[] quizDoneMulti, bool bonusDone) {
            this.id = id;
            this.poiDone = poiDone;
            this.quizDoneMulti = quizDoneMulti;
            this.bonusDone = bonusDone;
        }
    }

    [Serializable]
    public class RedeemData : SubSaveData {
        public int id;
        public string date;
    }

    [Serializable]
    public class QuizData : SubSaveData {
        public int id;
        public bool cleared;
        public bool firstTimeClear;
    }

    [Serializable]
    public class TourData : SubSaveData {
        public int tourID;
        public int currentTourPOIArrayIndex;
    }

    [Serializable]
    public class TourNavigationPoints : SubSaveData {
        public int tourID;
        public string expirationDate = DateTime.Now.AddDays(14).ToString();
        public DateTime ExpirationDate { get { return DateTime.Parse(expirationDate); } }
        public List<OnlineMapsVector2d> points;
    }

    [Serializable]
    public class CommunityData : SubSaveData {
        public string username;
        public string password;
        public int profileImgId;
        public int profileColorId;
        public List<int> likedPosts;
    }
}