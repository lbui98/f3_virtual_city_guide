using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using bunLace.Singleton;
using UnityEngine.AddressableAssets;
using bunLace.Database;
using Cysharp.Threading;
using Cysharp.Threading.Tasks;
using UnityEngine.SceneManagement;
using System;
using DG.Tweening;

public class SoundManager : PersistentSingleton<SoundManager> {
    [System.Serializable]
    public class SoundSettings {
        [Range(0f, 1f)]
        public float _maxSFXVolume;
        [Range(0f, 1f)]
        public float _maxBGMVolume;
        [Range(0f, 1f)]
        public float _maxAudioFileVolume;
    }

    [SerializeField] private AssetReference _spatialAudioSource;
    [SerializeField] private AudioSource _bgmSource;

    SoundSettings _soundSettings { get { return GameManager.SaveData.soundSettings; } }

    private static Dictionary<Vector2, GameObject> _audioSources = new Dictionary<Vector2, GameObject>();
    private static Dictionary<string, AudioClip> _loadedAudio = new Dictionary<string, AudioClip>();

    private void OnEnable() {
        MainManager.OnAudioVolumeChanged += OnAudioVolumeChanged;
    }

    private void OnDisable() {
        MainManager.OnAudioVolumeChanged -= OnAudioVolumeChanged;
    }

    private void OnAudioVolumeChanged(float value) {
        Instance._soundSettings._maxAudioFileVolume = value;
    }

    /// <summary>
    /// Loads an audioclip of an addressable and returns it
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public static async UniTask<AudioClip> GetAudioClip(int id) {
        //check if audio had already been loaded

        //else get audio clip
        Sounds entry = DatabaseManager.SelectEntry<Sounds>(id);
        if (entry == null) Debug.LogError($@"Sound {id} does not exist");
        if (_loadedAudio.ContainsKey(entry.SoundReference)) return _loadedAudio[entry.SoundReference];

        AudioClip clip = await Addressables.LoadAssetAsync<AudioClip>(entry.SoundReference);

        _loadedAudio.Add(entry.SoundReference, clip);
        return clip;
    }

    public static async UniTask<AudioClip> GetAudioClip(string reference) {
        //check if audio had already been loaded
        if (_loadedAudio.ContainsKey(reference)) return _loadedAudio[reference];

        AudioClip clip = await Addressables.LoadAssetAsync<AudioClip>(reference);

        _loadedAudio.Add(reference, clip);

        return clip;
    }

    /// <summary>
    /// Plays an audio file using the universal audio source
    /// </summary>
    /// <param name="audio"></param>
    /// <param name="loop"></param>
    /// <param name="volume"></param>
    public static async UniTask<AudioClip> PlayAudioFile(string audioKey, bool loop, float volume = 1f) {
        return await Instance.PlayAudioInternal(audioKey, loop, Mathf.Clamp(volume, 0f, Instance._soundSettings._maxAudioFileVolume));
    }

    /// <summary>
    /// Plays a BGM using the universal audio source
    /// </summary>
    /// <param name="audio"></param>
    /// <param name="loop"></param>
    /// <param name="volume"></param>
    public static void PlayBGM(string audioKey, bool loop, float volume = 1f) {
        Instance.PlayAudioInternal(audioKey, loop, Mathf.Clamp(volume, 0, Instance._soundSettings._maxBGMVolume));
    }

    public static void StopAudio() {
        Instance.StartCoroutine(Instance.FadeOutAudio());
    }

    private IEnumerator FadeOutAudio() {
        _bgmSource.DOFade(0f, bunLace.Constants.Values.VOLUME_FADE_DURATION);
        yield return new WaitForSeconds(bunLace.Constants.Values.VOLUME_FADE_DURATION);
        _bgmSource.Stop();
    }

    public static void PlaySFX(string audioKey) {
        Instance.PlayAudioInternal(audioKey, Vector2.zero, false, Instance._soundSettings._maxSFXVolume, 0);
    }


    /// <summary>
    /// Plays an audio using the universal AudioSource
    /// </summary>
    /// <param name="audioKey"></param>
    /// <param name="loop"></param>
    /// <param name="volume"></param>
    private async UniTask<AudioClip> PlayAudioInternal(string audioKey, bool loop, float volume) {
        AudioClip clip = await GetAudioClip(audioKey);
        Instance._bgmSource.clip = clip;
        Instance._bgmSource.volume = volume;
        Instance._bgmSource.loop = loop;
        Instance._bgmSource.Play();
        return clip;
    }


    private async UniTask PlayAudioInternal(string audioKey, Vector2 position, bool loop, float volume, float spatialBlend = 1, float minDistance = 1, float maxDistance = 20) {
        GameObject go = new GameObject();
        go.transform.position = position;
        go.transform.parent = transform;

        AudioSource audioSource = go.AddComponent<AudioSource>();
        audioSource.clip = await GetAudioClip(audioKey);
        audioSource.loop = loop;
        audioSource.volume = volume;

        audioSource.spatialBlend = spatialBlend;
        audioSource.minDistance = minDistance;
        audioSource.maxDistance = maxDistance;

        audioSource.Play();

        if (position != Vector2.zero)
            _audioSources.Add(position, go);

        //register if looping
        if (!loop) {
            //destroy after clip end            
            Destroy(go, audioSource.clip.length);
        }
    }

}