using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Localization;
using UnityEngine.Events;
using bunLace.Tools;
using bunLace.Constants;
using bunLace.Database;
using bunLace.Localization;
using UnityEngine.AddressableAssets;
using System;

public class CityMenuManager : MonoBehaviour {
    //[SerializeField] private Image headerImg;
    [SerializeField]
    private TextMeshProUGUI
        regionName,
        description,
        poi,
        tour,
        achievements,
        downloadSize;
    [SerializeField] private LocalizedString locPoiKey, locTourKey, locAchievementKey, locSwitch, locBuyAndSwitch, locCannotSwitch;
    public Button btnSwitch;
    [SerializeField] private GameObject previewBtn;
    private GameObject loadedPreview;

    private string regionPreview;

    public enum SwitchText { SWITCH, PURCHASE, PURCHASE_ERROR }

    private void OnEnable() {
        IAPManager.OnPurchaseSuccess += OnPurchaseSuccess;
    }

    private void OnDisable() {
        IAPManager.OnPurchaseSuccess -= OnPurchaseSuccess;
    }

    private void OnPurchaseSuccess() {
        ChangeButtonText(SwitchText.SWITCH);
    }

    public void Initialize(Sprite cityImg, Region region, long size) {
        int poiCount = DatabaseManager.SelectMultipleEntries<POIRegion>($"RegionID = {region.ID}").Count;
        int tourCount = DatabaseManager.SelectMultipleEntries<Tour>($"RegionID = {region.ID}").Count;
        int achievementCount = DatabaseManager.SelectMultipleEntries<Achievements>($"RegionID = {region.ID}").Count;

        string locPoi, locTour, locAchievement;
        locPoi = locPoiKey.GetLocalizedString();
        locTour = locTourKey.GetLocalizedString();
        locAchievement = locAchievementKey.GetLocalizedString();

        regionName.text = region.Name;
        regionPreview = region.PreviewReference;

        if (size == 0) {
            downloadSize.text = "--";
        }
        else {
            downloadSize.text = AddressablesManager.SizeSuffix(size);
        }

        btnSwitch.gameObject.SetActive(InternetReachabilityVerifier.Instance.status == InternetReachabilityVerifier.Status.NetVerified);

        Image img = gameObject.FindComponentInChildWithTag<Image>(Tags.IMG_REPLACE);
        img.sprite = cityImg;
        var ratio = img.GetComponent<AspectRatioFitter>();
        ratio.aspectMode = AspectRatioFitter.AspectMode.EnvelopeParent;
        ratio.aspectRatio = (float)cityImg.rect.width / (float)cityImg.rect.height;

        string description = LocalizationManager.GetLocalizedDBString(region.DescriptionLocKey);
        this.description.text = string.Format(description, region.Name);
        poi.text = $"{poiCount} {locPoi}";
        tour.text = $"{tourCount} {locTour}";
        achievements.text = $"{achievementCount} {locAchievement}";

        previewBtn.SetActive(!string.IsNullOrEmpty(regionPreview));
    }

    public void ChangeButtonText(SwitchText switchText) {
        TextMeshProUGUI text = btnSwitch.GetComponentInChildren<TextMeshProUGUI>();

        if (switchText == SwitchText.PURCHASE) {
            locBuyAndSwitch.GetLocalizedStringAsync().Completed += asyncOp => {
                text.text = asyncOp.Result;
            };
        }
        else if (switchText == SwitchText.SWITCH) {
            locSwitch.GetLocalizedStringAsync().Completed += asyncOp => {
                text.text = asyncOp.Result;
            };
        }
        else if (switchText == SwitchText.PURCHASE_ERROR) {
            locCannotSwitch.GetLocalizedStringAsync().Completed += asyncOp => {
                text.text = asyncOp.Result;
            };
        }
    }

    public void OnBtnPreview() {
        if (string.IsNullOrEmpty(regionPreview)) {
            loadedPreview = null;
            return; //no ad
        }

        Addressables.InstantiateAsync(regionPreview, transform).Completed += asyncOp => {
            loadedPreview = asyncOp.Result;
            loadedPreview.SetActive(false);
            CanvasFader.Screengrab(loadedPreview, true);
            loadedPreview.GetComponent<AdHelper>().OnCloseAd += DestroyAd;
        };
    }

    private void DestroyAd() {
        Debug.Log("Close Ad");
        CanvasFader.Screengrab(loadedPreview, false);
        loadedPreview.GetComponent<AdHelper>().OnCloseAd -= DestroyAd;
        Destroy(loadedPreview, 0.3f);
        loadedPreview = null;
    }
}
