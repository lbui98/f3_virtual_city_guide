using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Linq;
using UnityEngine.UI;
using bunLace.Database;

public class CityRegion : MonoBehaviour {
    public int id;
    public int parentId;

    public List<GameObject> subregions = new List<GameObject>();
    public List<RectTransform> groupIndicators = new List<RectTransform>();
    public bool collapsed = false;

    public TextMeshProUGUI subRegionCount;
    public Image flag;

    private void OnEnable() {
        if (parentId == 0) {
            OnCollapseToggled += RecalculateGroupIndicators;
            MainManager.OnCitySorted += RecalculateGroupIndicators;
        }
    }

    private void OnDisable() {
        if (parentId == 0) {
            OnCollapseToggled -= RecalculateGroupIndicators;
            MainManager.OnCitySorted -= RecalculateGroupIndicators;
        }
    }

    public void ToggleSubRegions() {
        StartCoroutine(ToggleSubRegionsRoutine());
    }

    private IEnumerator ToggleSubRegionsRoutine() {
        collapsed = !collapsed;
        subRegionCount.text = collapsed ? "x" : subregions.Count.ToString();
        flag.gameObject.SetActive(!collapsed);

        for (int i = 0; i < subregions.Count; i++) {
            subregions[i].SetActive(collapsed);
        }

        yield return new WaitForEndOfFrame();
        for (int i = 0; i < groupIndicators.Count; i++) {
            groupIndicators[i].gameObject.SetActive(collapsed);
        }
        OnCollapseToggled?.Invoke();
    }

    public delegate void CollapseToggled();
    public static event CollapseToggled OnCollapseToggled;

    public void GenerateGroupIndicators() {
        //sort subregions first
        subregions = subregions.OrderBy(c => c.transform.GetSiblingIndex()).ToList();

        RectTransform r = transform as RectTransform;

        for (int i = 0; i < subregions.Count + 1; i++) {
            RectTransform rt = GenerateGroupIndicator(name + i);

            Region parentReg = DatabaseManager.SelectEntry<Region>(id);
            Color bgCol;
            ColorUtility.TryParseHtmlString(parentReg.ColorForeground, out bgCol);
            rt.gameObject.AddComponent<Image>().color = bgCol;

            rt.sizeDelta = new Vector2(0f, r.sizeDelta.y * 0.9f);

            groupIndicators.Add(rt);
            rt.gameObject.SetActive(false);
        }
    }

    private RectTransform GenerateGroupIndicator(string name = "") {
        GameObject go = new GameObject(name);
        go.transform.SetParent(transform.parent);
        go.transform.SetAsFirstSibling();
        go.AddComponent<LayoutElement>().ignoreLayout = true;
        go.tag = bunLace.Constants.Tags.RESPAWN;
        RectTransform rt = go.GetComponent<RectTransform>();
        rt.anchorMin = new Vector2(0, 1f);
        rt.anchorMax = new Vector2(1f, 1f);
        rt.localScale = Vector3.one;
        rt.localRotation = Quaternion.identity;
        rt.localPosition = Vector3.zero;

        return rt;
    }

    private void RecalculateGroupIndicators() {

        //check the parent
        RectTransform rectTransform = transform as RectTransform;
        int halfScreen = Screen.width / 2;
        if (rectTransform.anchoredPosition.x > halfScreen) {
            //is on the right side
            groupIndicators[groupIndicators.Count - 1].SetLeft(rectTransform.anchoredPosition.x);
            groupIndicators[groupIndicators.Count - 1].SetRight(0);
            groupIndicators[groupIndicators.Count - 1].anchoredPosition = new Vector3(groupIndicators[groupIndicators.Count - 1].anchoredPosition.x, rectTransform.localPosition.y, 0);
            groupIndicators[groupIndicators.Count - 1].gameObject.SetActive(collapsed);

        }
        else {
            //not needed since the right subregion will handle the indication
            groupIndicators[groupIndicators.Count - 1].gameObject.SetActive(false);
        }


        for (int i = 0; i < subregions.Count; i++) {
            Debug.Log(gameObject.name + " " + subregions[i].GetComponent<RectTransform>().localPosition.y);
            RectTransform rt = subregions[i].GetComponent<RectTransform>();
            if (rt.anchoredPosition.x > Screen.width / 2) {
                //is on the right half of the screen
                if (i == subregions.Count - 1) {
                    //is last region
                    // don't connect to the next one
                    if (i == 0) {
                        //last and only element
                        //to its left is the parent
                        groupIndicators[i].SetLeft(rectTransform.anchoredPosition.x);
                        groupIndicators[i].SetRight(rectTransform.anchoredPosition.x);
                    }
                    else {
                        groupIndicators[i].SetRight(rt.anchoredPosition.x);
                    }
                }
                else {
                    //not the last region
                    groupIndicators[i].SetRight(0);
                    if (i == 0) {
                        //first subregion
                        //don't connect to far left since parent is left to it
                        groupIndicators[i].SetLeft(rectTransform.anchoredPosition.x);
                    }
                    else {
                        //indication is handled by the left subregion
                        groupIndicators[i].gameObject.SetActive(false);
                    }
                }

            }
            else {
                //is on the left half of the screen
                groupIndicators[i].SetLeft(0);

                if (i == subregions.Count - 1) {
                    //is last region
                    // don't connect to the next one
                    groupIndicators[i].SetRight(halfScreen);
                }
                else {
                    if (i + 1 == subregions.Count - 1) {
                        //is the next region the last one?
                        //don't connect to the far end
                        groupIndicators[i].SetRight(rt.anchoredPosition.x);

                    }
                    else {
                        //connect to far right
                        groupIndicators[i].SetRight(0);
                    }
                }
            }
            groupIndicators[i].anchoredPosition = new Vector3(groupIndicators[i].anchoredPosition.x, rt.localPosition.y, 0);
        }
    }
}
