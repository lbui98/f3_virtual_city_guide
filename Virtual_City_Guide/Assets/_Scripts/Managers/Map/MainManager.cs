using System.Collections.Generic;
using UnityEngine;
using bunLace.Database;
using bunLace.Localization;
using bunLace.Tools;
using System;
using UnityEngine.AddressableAssets;
using UnityEngine.Localization;
using UnityEngine.UI;
using TMPro;
using Cysharp.Threading.Tasks;
using System.Linq;
using bunLace.SaveLoad;
using bunLace.Constants;
using Proyecto26;
using DG.Tweening;
using System.Collections;
using bunLace.Singleton;

public class MainManager : Singleton<MainManager> {
    [Header("Main Menu")]
    public AssetReference TabListBtnElement;
    [SerializeField] private Transform tabPoiContent;
    [SerializeField] private Transform tabTourContent;
    [SerializeField] private Transform tabBookmarkContent;
    [SerializeField] private TabGroup mainMenuTabGroup;
    [SerializeField] private RectTransform selectedTourIndicator, mainTopPanel;
    [SerializeField] private Button helpBtn;
    [SerializeField] private GameObject mainMenu, loadingProgress, btnToursTab, noBookmarkText, noSightsText, nearestPoi, tempHeader;
    private Dictionary<int, RectTransform> toursDict = new Dictionary<int, RectTransform>();
    private Dictionary<int, RectTransform> citiesDict = new Dictionary<int, RectTransform>();
    private Dictionary<int, RectTransform> poisDict = new Dictionary<int, RectTransform>();
    private bool mainMenuOpen = true;

    [Header("Map")]
    private OnlineMapsMarker3D userMarker;
    [SerializeField] private GameObject warningNoTourSelected, warningNoGPS, warningNoRegionSelected, homeBtn;
    [SerializeField] private LocalizedString tourInProgress, reachedPOI, quitWarning, ignoreGPSWarning, noRouteAvailable;
    [SerializeField] private GameObject userMarkerPrefab, visitedMarkerPrefab, specialMarkerPrefab;
    [SerializeField] private Texture2D userMarkerTexture, visitedPOIMarker;
    private bool markersInitialized = false;
    private OnlineMapsOpenRouteServiceDirections routeService;

    [Header("Main Tabs")]
    [SerializeField] private GameObject canvasMainTabs;
    [SerializeField] private TextMeshProUGUI tabHeaderName;
    [SerializeField] private TabGroup tabGroup;
    //[SerializeField] private Image headerImg;
    private int currentSelectedPoi;

    [Header("Cities")]
    [SerializeField] private AssetReference cityListElement;
    [SerializeField] private LocalizedString existingCityChangeWarning, cityChangeWarning, noCityDownloadWarning, internetRequired, noGPS;
    [SerializeField] private Transform citiesContent;
    [SerializeField] private DownloadProgress downloadProgress;
    [SerializeField] private GameObject btnReturnRegionView, premiumMapMarker;
    [SerializeField] private RectTransform selectedCityIndicator;
    public bool citiesInitialized = false;
    private CityMenuManager cityMenuManager;

    [Header("Logbook")]
    [SerializeField] private Transform logbookContent, loadMoreBtn;
    [SerializeField] private AssetReference logbookListElement;
    [SerializeField] private GameObject noLogbookEntriesText;

    [Header("Settings")]
    [SerializeField] private Slider sliderBGM;
    [SerializeField] private Slider sliderSFX;
    [SerializeField] private Slider sliderAudioFiles;
    [SerializeField] private TextMeshProUGUI redeemHistory;
    [SerializeField] private LocalizedString pointResetError, pointResetConfirm, noRedeemHistory;

    private async void Start() {
        cityMenuManager = GetComponentInChildren<CityMenuManager>(true);

        tabGroup.Initialize();
        //regionalAchievementsText = await regionalAchievements.GetLocalizedStringAsync();
        //redeemNotice = await localizedRedeemNotice.GetLocalizedStringAsync();
        //allAchievementsText = achievementToggleText.text;

        mainMenuTabGroup.OnTabSelect((int)MainMenuTabTypes.HOME);

        if (GameManager.CurrentRegionID != -1) {
            //a region has been selected before
            //recreate scene before app ended
            Region region = DatabaseManager.SelectEntry<Region>(GameManager.CurrentRegionID);

            if (region.IsActive == 0 && !GameManager.debugging) {
                //region is no longer active and we're not debugging
                GameManager.CurrentRegionID = -1;
                //OpenMainMenuInstant(false);
                //no region has been selected before
                //set up region selection
                GetCityMarkers();
                DisplayWarning(warningNoRegionSelected);
                SetHelpBtn(Values.GENERAL_HELP_URL);
                btnToursTab.SetActive(false);
                nearestPoi.SetActive(false);
            }
            else {
                //GameManager.Instance.UpdatePalette(region);
                SetHelpBtn(region.HelpURL);
                //await LoadHeaderImg(region.AtlasReference, region.ImgReference, region.LogoImgReference);

                await GetTours(GameManager.CurrentRegionID);
                if (GameManager.SaveData.tourData != null || GameManager.SaveData.tourData.tourID != -1) {
                    await GetMarkers(GameManager.SaveData.tourData);
                    SetTourIndicator(GameManager.SaveData.tourData.tourID);
                }
                else
                    DisplayWarning(warningNoTourSelected);
            }
        }
        else {
            //no region has been selected before
            //set up region selection
            //OpenMainMenuInstant(false);
            GetCityMarkers();
            DisplayWarning(warningNoRegionSelected);
            SetHelpBtn(Values.GENERAL_HELP_URL);
            btnToursTab.SetActive(false);
            nearestPoi.SetActive(false);
        }

        noSightsText.SetActive(GameManager.CurrentRegionID == -1);

        DisplayWarning(warningNoGPS);

        OnlineMapsLocationService.instance.OnLocationInited += CreateUsermarker;
        OnlineMapsLocationService.instance.OnLocationChanged += UpdateUserMarkerPosition;
        OnlineMapsLocationService.instance.OnPositionRestored += OnPositionRestored;

        InvokeRepeating(nameof(CheckGPS), 10f, 1);

        Debug.Log("END");
    }

    private void OnEnable() {
        AddressablesManager.OnDownloadProgress += OnDownloadProgress;
        NearestPOI.OnBtnNearestPOIClick += OnBtnMarkerClick;
        POIMenuManager.OnScavengerNextPOI += GetMarkers;
    }

    private void OnDisable() {
        OnlineMapsLocationService.instance.OnLocationInited -= CreateUsermarker;
        OnlineMapsLocationService.instance.OnLocationChanged -= UpdateUserMarkerPosition;
        OnlineMapsLocationService.instance.OnPositionRestored -= OnPositionRestored;
        AddressablesManager.OnDownloadProgress -= OnDownloadProgress;
        NearestPOI.OnBtnNearestPOIClick -= OnBtnMarkerClick;
        POIMenuManager.OnScavengerNextPOI -= GetMarkers;
    }

    public void OnBtnReturn(RectTransform targetPanel) {
        if (Utilities.blocked) return;
        AnimatePanel(false, targetPanel.gameObject);

        StartCoroutine(OnBtnReturnRoutine(targetPanel));
    }

    public void OnBtnReturnAndPalette(RectTransform targetPanel) {
        if (Utilities.blocked) return;
        OnBtnReturn(targetPanel);
        if (GameManager.Instance.currentRegionPalette != GameManager.CurrentRegionID) {
            Region region = DatabaseManager.SelectEntry<Region>(GameManager.CurrentRegionID);
            GameManager.Instance.UpdatePalette(region);
        }
    }

    private IEnumerator OnBtnReturnRoutine(RectTransform target) {
        yield return new WaitForSeconds(Values.ANIMATION_SLIDE_SPEED);
        target.gameObject.SetActive(false);
    }

    private void OnDownloadProgress(float progress) {
        if (progress >= 1f) {
            loadingProgress.SetActive(false);
        }
        else {
            loadingProgress.SetActive(true);
        }
    }

    public void OnBtnSubTab(int tabTypeToActivate, bool universal = false) {
        if (Utilities.blocked) return;
        //canvasMainTabs.SetActive(true);
        tabGroup.OnTabSelect(tabTypeToActivate);
        if (tabGroup.tabsDict.ContainsKey(tabTypeToActivate)) {
            tabGroup.tabsDict[tabTypeToActivate].info.titleKey.GetLocalizedStringAsync().Completed += asyncOp => {
                string tab = asyncOp.Result;
                if (universal || GameManager.CurrentRegionID == -1) {
                    tabHeaderName.text = tab;
                    tempHeader.SetActive(true);
                }
                else {
                    tabHeaderName.text = string.Format("{0} <size={1}>/ {2}</size>", DatabaseManager.SelectEntry<Region>(GameManager.CurrentRegionID).Name, Values.FONT_SUB_SIZE, tab);
                }

            };
        }
        //canvasMainTabs.SetActive(true);
        //AnimatePanel(true, canvasMainTabs.FindComponentInChildWithTag<RectTransform>(Tags.TARGET));
        AnimatePanel(true, canvasMainTabs);
    }

    public void OnBtnQuit() {
        quitWarning.GetLocalizedStringAsync().Completed += asyncOp => {
            string warning = asyncOp.Result;

            string confirm = Values.LOCALIZED_TERMS[Values.LOC_KEY_CONFIRM];
            string cancel = Values.LOCALIZED_TERMS[Values.LOC_KEY_CANCEL];

            EasyMessageBox.Show(warning,
                button1Text: confirm,
                button1Action: () => Application.Quit(),
                button2Text: cancel);
        };
    }

    private void DisplayWarning(GameObject warning) {
        warning.SetActive(true);
    }


    public void OpenTab(int tab) {
        if (Utilities.blocked) return;
        bool open = tab == ((int)MainMenuTabTypes.HOME) && GameManager.CurrentRegionID != -1;
        //OpenMainMenu(open);

        mainMenuTabGroup.OnTabSelect(tab);
    }

    public void OnBtnCommunityTab() {
        UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(bunLace.Constants.Scenes.COMMUNITY, UnityEngine.SceneManagement.LoadSceneMode.Single);
    }

    #region MAP
    private void GetCityMarkers() {
        ClearMapMarkers();
        List<Region> regions = DatabaseManager.SelectMultipleEntries<Region>(string.Format("IsActive = 1"));

        for (int i = 0; i < regions.Count; i++) {
            //OnlineMapsMarker marker;
            OnlineMapsMarker3D marker;
            //create markers
            marker = OnlineMapsMarker3DManager.CreateItem(new Vector2(regions[i].Longitude, regions[i].Latitude), OnlineMapsMarker3DManager.instance.defaultPrefab);
            //marker = OnlineMapsMarkerManager.instance.Create(new Vector2(regions[i].Longitude, regions[i].Latitude));

            marker["ID"] = regions[i].ID;

            marker.OnClick += OnBtnCityMarker;
        }

        //premium regions
        regions = DatabaseManager.SelectMultipleEntries<Region>(string.Format("IsActive = 2"));
        if (regions != null) {
            for (int i = 0; i < regions.Count; i++) {
                //OnlineMapsMarker marker;
                OnlineMapsMarker3D marker;
                //create markers
                marker = OnlineMapsMarker3DManager.CreateItem(new Vector2(regions[i].Longitude, regions[i].Latitude), premiumMapMarker);
                //marker = OnlineMapsMarkerManager.instance.Create(new Vector2(regions[i].Longitude, regions[i].Latitude));

                marker["ID"] = regions[i].ID;

                marker.OnClick += OnBtnCityMarker;
            }
        }
    }

    private void OnBtnCityMarker(OnlineMapsMarkerBase marker) {
        int id = (int)marker["ID"];

        OnBtnCity(id);
    }

    private async UniTask GetMarkers(TourData tourData) {
        await GetMarkers(tourData.tourID, tourData.currentTourPOIArrayIndex);
    }

    private async UniTask GetMarkers(int tourID, int progress = 0) {
        Tour tour = DatabaseManager.SelectEntry<Tour>(tourID);
        if (tour == null) {
            return;
        }


        loadingProgress.SetActive(true);
        markersInitialized = false;
        ClearMapMarkers();


        List<POI> pois;

        GameManager.currentTourMode = (TourTypes)tour.TourType;

        //if (tour.TourType == (int)TourTypes.FREE_EXPLORATION) {
        //    pois = DatabaseManager.SelectMultipleEntries<POI>(string.Format("RegionID = {0} ORDER BY \"Order\"", tour.RegionID));
        //}
        //else {
        pois = new List<POI>();
        for (int i = 0; i < tour.POIIDs.Length; i++) {
            POI poi = DatabaseManager.SelectEntry<POI>(tour.POIIDs[i].poiID);
            pois.Add(poi);
        }
        //}

        double[] coordinates = new double[pois.Count * 2];

        if (pois != null) {
            int number = 1;
            for (int i = 0; i < pois.Count; i++) {
                coordinates[i + i] = pois[i].Longitude;
                coordinates[i + i + 1] = pois[i].Latitude;

                POI poi = pois[i];
                OnlineMapsMarker3D marker;
                bool visited = false;
                //create markers

                if (poi.Type != POITypes.POI) {
                    marker = OnlineMapsMarker3DManager.CreateItem(new Vector2((float)poi.Longitude, (float)poi.Latitude), specialMarkerPrefab);
                }
                else {

                    if (tour.TourType == (int)TourTypes.SCAVENGER_HUNT) {
                        visited = i < progress;
                    }
                    else {
                        visited = GameManager.SaveData.poiData.FirstOrDefault(data => data.id == poi.ID) != null;
                    }

                    if (visited) {
                        marker = OnlineMapsMarker3DManager.CreateItem(new Vector2((float)poi.Longitude, (float)poi.Latitude), visitedMarkerPrefab);
                        //marker = OnlineMapsMarker3DManager.instance.Create(new Vector2((float)poi.Longitude, (float)poi.Latitude), visitedPOIMarker);
                    }
                    else {
                        marker = OnlineMapsMarker3DManager.CreateItem(new Vector2((float)poi.Longitude, (float)poi.Latitude), OnlineMapsMarker3DManager.instance.defaultPrefab);
                        //marker = OnlineMapsMarkerManager.instance.Create(new Vector2((float)poi.Longitude, (float)poi.Latitude));
                    }
                }

                marker.label = poi.ID.ToString();
                marker["ID"] = poi.ID;
                marker["visited"] = visited;
                marker["Perimeter"] = poi.Perimeter;
                //marker.label = poi.NameLocKey;
                marker.OnClick += OnBtnMarker;

                //create list elements
                GameObject tabElement = await Addressables.InstantiateAsync(TabListBtnElement, tabPoiContent);
                if (tabElement == null) {
                    Debug.LogError("Reference does not exist! " + TabListBtnElement);
                    return;
                }

                poisDict.Add(poi.ID, tabElement.GetComponent<RectTransform>());

                Image points = tabElement.FindComponentInChildWithTag<Image>(Tags.TARGET);

                if (poi.Type == POITypes.POI) {
                    Color pointsColor = visited ? GameManager.Instance.GetPaletteColor(Palette.ColorGroups.FOREGROUND) : GameManager.Instance.GetPaletteColor(Palette.ColorGroups.UNSELECTED);
                    points.color = pointsColor;
                    TextMeshProUGUI pointsText = points.GetComponentInChildren<TextMeshProUGUI>(true);
                    pointsText.text = poi.UnlockPoints.ToString();
                }
                else {
                    points.gameObject.SetActive(false);
                }

                tabElement.GetComponentInChildren<TextMeshProUGUI>().text = LocalizationManager.GetLocalizedDBString(poi.NameLocKey);
                Button btn = tabElement.GetComponent<Button>();
                btn.onClick.AddListener(delegate { OnBtnTabPoiClick(marker); });

                Image img = tabElement.FindComponentInChildWithTag<Image>(Tags.IMG_REPLACE);
                Sprite sprite = await AddressablesManager.Instance.LoadSprite(poi.AtlasReference, poi.ImgReference);
                img.sprite = sprite;
                var ratio = img.GetComponent<AspectRatioFitter>();
                ratio.aspectMode = AspectRatioFitter.AspectMode.EnvelopeParent;
                ratio.aspectRatio = (float)sprite.rect.width / (float)sprite.rect.height;

                if (tour.TourType == (int)TourTypes.SCAVENGER_HUNT && poi.Type == POITypes.POI) {
                    marker.enabled = i <= progress; //hide marker when not there yet
                                                    //btn.interactable = i <= progress; //disable btn interaction when it's not unlocked
                }

                //create list element in overview
                if (poi.Type == POITypes.POI) {
                    btn = await TrophiesAndOverviewManager.Instance.GenerateOverviewElement(poi, number, visited);
                    number++;
                    btn.onClick.AddListener(delegate { OnBtnMarkerClick(poi.ID); });
                }

            }
        }


        if (tour.TourType == (int)TourTypes.GUIDED_TOUR) {
            var naviPoints = SaveLoadManager.LoadNavi().GetTourNavigationPoints(tourID);
            if (naviPoints == null) {
                Debug.Log("Getting Navigation from API");
                if (InternetReachabilityVerifier.Instance.status == InternetReachabilityVerifier.Status.NetVerified) {
                    //draw line to next poi

                    routeService = OnlineMapsOpenRouteServiceDirections.Find(
                       coordinates,
                       new OnlineMapsOpenRouteServiceDirections.Params {
                           // Extra params
                           profile = OnlineMapsOpenRouteServiceDirections.Profile.footWalking
                       });
                    routeService.OnComplete += OnRouteRequestComplete;
                    routeService.OnFailed += OnRouteRequestFailed;

                }
            }
            else {
                OnlineMapsDrawingElementManager.AddItem(new OnlineMapsDrawingLine(naviPoints.points, GameManager.Instance.GetPaletteColor(Palette.ColorGroups.SELECTED), 3f));
            }
        }

        CreateUsermarker();
        markersInitialized = true;
        loadingProgress.SetActive(false);
    }

    private void OnRouteRequestComplete(string response) {
        Debug.Log(response);
        routeService.OnComplete -= OnRouteRequestComplete;
        OnlineMapsOpenRouteServiceDirectionResult result = OnlineMapsOpenRouteServiceDirections.GetResults(response);
        if (result == null || result.routes.Length == 0) {
            Debug.Log("Open Route Service Directions failed.");
            return;
        }

        // Get the points of the first route.
        List<OnlineMapsVector2d> points = result.routes[0].points;
        NavigationSaveData navi = SaveLoadManager.LoadNavi();
        navi.tourNavigations.Add(new TourNavigationPoints { tourID = GameManager.SaveData.tourData.tourID, points = points });
        SaveLoadManager.SaveNavigation(navi);

        // Draw the route.
        OnlineMapsDrawingElementManager.AddItem(new OnlineMapsDrawingLine(points, GameManager.Instance.GetPaletteColor(Palette.ColorGroups.SELECTED), 3f));
    }

    private void OnRouteRequestFailed(OnlineMapsTextWebService obj) {
        routeService.OnFailed -= OnRouteRequestFailed;
        //display warning
        noRouteAvailable.GetLocalizedStringAsync().Completed += asyncOp => {
            EasyMessageBox.Show(asyncOp.Result);
        };
    }


    public delegate void MarkerClick(int id, bool visited, bool stamp);
    public static event MarkerClick OnMarkerClick;

    private void OnBtnMarkerClick(int id) {
        bool visited = GameManager.Instance.HasVisited(id);

        if (GameManager.currentTourMode == TourTypes.SCAVENGER_HUNT) {
            Tour tour = DatabaseManager.SelectEntry<Tour>(GameManager.SaveData.tourData.tourID);
            if (tour.TourType == (int)TourTypes.SCAVENGER_HUNT && tour.POIIDs[GameManager.SaveData.tourData.currentTourPOIArrayIndex].poiID == id) {
                OnMarkerClick?.Invoke(id, visited, false);
                return;
            }

            if (!visited) {
                GameManager.Instance.DisplayPOINotUnlockedWarning();
            }
            else {
                OnMarkerClick?.Invoke(id, visited, false);
            }
        }
        else {
            OnMarkerClick?.Invoke(id, visited, false);
        }

    }

    private void OnBtnMarker(OnlineMapsMarkerBase marker) {
        bool visited = GameManager.Instance.HasVisited((int)marker["ID"]);
        bool stamp = false;
        if (!visited) {
            //not visited
            //but check again
            stamp = CheckPoiDistance((OnlineMapsMarker3D)marker);
        }
        OnMarkerClick?.Invoke((int)marker["ID"], visited, stamp);
    }

    private void OnBtnTabPoiClick(OnlineMapsMarker3D marker) {
        bool visited = false;
        bool stamp = false;
        int id = (int)marker["ID"];

        if (GameManager.currentTourMode == TourTypes.SCAVENGER_HUNT) {
            visited = marker.enabled;
            if (!visited)
                GameManager.Instance.DisplayPOINotUnlockedWarning();
            else {
                visited = GameManager.Instance.HasVisited((int)marker["ID"]);
                OnBtnTabPoiClick(id: id, lat: marker.position.y, lon: marker.position.x, visited: visited, stamp: false);
            }
        }
        else {
            visited = GameManager.Instance.HasVisited((int)marker["ID"]);

            if (!visited) {
                visited = CheckPoiDistance(marker);
                stamp = visited;
            }

            OnBtnTabPoiClick(id: id, lat: marker.position.y, lon: marker.position.x, visited: visited, stamp: stamp);
        }
    }

    private void OnBtnTabPoiClick(int id, double lat, double lon, bool visited, bool stamp) {
        if (currentSelectedPoi == id) {
            //already zoomed, open poi menu
            OnMarkerClick?.Invoke(id, visited, stamp);
        }
        else {
            if (mapLocked) return;

            OnlineMaps.instance.SetPositionAndZoom(lon, lat, 18);
            currentSelectedPoi = id;

            for (int i = 0; i < OnlineMapsMarker3DManager.instance.items.Count; i++) {
                Debug.Log("Checking poi no." + i);
                OnlineMapsMarker3D marker = OnlineMapsMarker3DManager.instance.items[i];
                //skip the user itself
                if (marker == userMarker || marker.label == Values.MAP_USER_MARKER) continue;
                if (marker.enabled == false) continue;

                int poiId = (int)marker["ID"];

                if (poiId == id) {
                    markerToHighlight = marker;
                    OnlineMaps.instance.OnMapUpdated += HighlightMarker;
                    break;
                }
            }
        }
    }

    private OnlineMapsMarker3D markerToHighlight;
    Tweener s;
    //Sequence s;
    private bool mapLocked;
    private void HighlightMarker() {
        if (mapLocked) return;
        Debug.Log("Highlight marker");
        //disable map movement
        OnlineMaps.instance.control.allowUserControl = false;
        mapLocked = true;
        HighlightMarkerInternal();
    }

    private async UniTask HighlightMarkerInternal() {
        await UniTask.Delay(50);
        //s = markerToHighlight.transform.DOJump(markerToHighlight.transform.position, 300f, 2, 0.5f);
        s = markerToHighlight.transform.DOPunchPosition(Vector3.back * 20, 0.5f, 3);
        await UniTask.Delay(550);
        s = null;
        OnlineMaps.instance.control.allowUserControl = true;
        mapLocked = false;
        OnlineMaps.instance.OnMapUpdated -= HighlightMarker;
    }
    private void CreateUsermarker() {
        if (userMarker == null) {
            Debug.Log("User maker created");
            userMarker = OnlineMapsMarker3DManager.CreateItem(OnlineMapsLocationService.instance.position, userMarkerPrefab);
            int LayerIgnoreRaycast = LayerMask.NameToLayer("Ignore Raycast");
            userMarker.instance.layer = LayerIgnoreRaycast;
            //userMarker = OnlineMapsMarkerManager.instance.Create(OnlineMapsLocationService.instance.position, userMarkerTexture, Values.MAP_USER_MARKER);
        }

        CheckGPS();
    }

    private void CheckGPS() {
        warningNoGPS.SetActive(OnlineMapsLocationService.instance.position == Vector2.zero && !GameManager.ignoreGPSWarning); //gps not found
    }

    public void IgnoreGPSWarning() {
        ignoreGPSWarning.GetLocalizedStringAsync().Completed += asyncOp => {
            string warning = asyncOp.Result;

            EasyMessageBox.Show(message: warning,
                button1Action: delegate { GameManager.ignoreGPSWarning = true; warningNoGPS.SetActive(false); },
                button1Text: Values.LOCALIZED_TERMS[Values.LOC_KEY_CONFIRM],
                button2Text: Values.LOCALIZED_TERMS[Values.LOC_KEY_CANCEL]
                );
        };
    }

    private void UpdateUserMarkerPosition(Vector2 position) {
        CreateUsermarker();
        userMarker.SetPosition(position.x, position.y);

        CheckPOIDistances();
    }

    public void UpdateUserMarkerPosition() {
#if UNITY_EDITOR
        UpdateUserMarkerPosition(new Vector2(8.613715f, 50.22423f));
        OnlineMaps.instance.SetPositionAndZoom(8.613715, 50.22423, 18);
#endif

        if (Input.location.isEnabledByUser) {
            UpdateUserMarkerPosition(new Vector2(Input.location.lastData.longitude, Input.location.lastData.latitude));
            OnlineMaps.instance.SetPositionAndZoom(Input.location.lastData.longitude, Input.location.lastData.latitude, 18);
        }
        else {
            noGPS.GetLocalizedStringAsync().Completed += asyncOp => {
                EasyMessageBox.Show(asyncOp.Result);
            };
        }
    }

    private void CheckPOIDistances() {
        if (!markersInitialized || userMarker == null) return;

        Debug.Log("Checking poi distance...");
        //check distance to pois
        for (int i = 0; i < OnlineMapsMarker3DManager.instance.items.Count; i++) {
            Debug.Log("Checking poi no." + i);
            OnlineMapsMarker3D marker = OnlineMapsMarker3DManager.instance.items[i];
            //skip the user itself
            if (marker == userMarker || marker.label == Values.MAP_USER_MARKER) continue;
            if (marker.enabled == false) continue;


            CheckPoiDistance(marker);
        }
    }

    private bool CheckPoiDistance(OnlineMapsMarker3D marker) {
        float distance = DistanceInMeters(userMarker.position, marker.position);
        int perimeter = (int)marker["Perimeter"];
        Debug.Log($"Distance: {distance} || Perimeter: {perimeter}");
        if (distance <= perimeter) {
            MarkPOIVisited(marker);
            return true;
        }
        return false;
    }

    public delegate void MarkerVisited(int id);
    public static event MarkerVisited OnMarkerVisited;

    private void MarkPOIVisited(OnlineMapsMarker3D marker) {
        int poiId = (int)marker["ID"];

        POI poi = DatabaseManager.SelectEntry<POI>(poiId);
        if (poi.Type != POITypes.POI) return; //don't do anything if it's a restpoint

        bool previouslyVisited = GameManager.Instance.HasVisited(poiId);
        if (!previouslyVisited) {
            GameManager.SaveData.poiData.Add(new POIsData { id = poiId, dateOfVisit = DateTime.Now.ToString() });
        }

        if (GameManager.currentTourMode == TourTypes.SCAVENGER_HUNT) {
            Tour tour = DatabaseManager.SelectEntry<Tour>(GameManager.SaveData.tourData.tourID);
            //if (tour.POIIDs[GameManager.SaveData.tourData.currentTourPOIArrayIndex].poiID == poiId && GameManager.Instance.CalculatePoints(tour) >= tour.POIIDs[GameManager.SaveData.tourData.currentTourPOIArrayIndex].requiredPoints) {
            //    GameManager.SaveData.tourData.currentTourPOIArrayIndex++;
            //    GetMarkers(GameManager.SaveData.tourData);
            //}
            if (tour.POIIDs[GameManager.SaveData.tourData.currentTourPOIArrayIndex].poiID == poiId && !previouslyVisited)
                DisplayNotePOIReached(poiId);
            OnMarkerVisited?.Invoke(poiId);
        }
        else {
            if (!previouslyVisited) {
                DisplayNotePOIReached(poiId);
                OnMarkerVisited?.Invoke(poiId);
            }
        }

        Image points = poisDict[poiId].gameObject.FindComponentInChildWithTag<Image>(Tags.TARGET);
        Color pointsColor = GameManager.Instance.GetPaletteColor(Palette.ColorGroups.FOREGROUND);
        points.color = pointsColor;

        TrophiesAndOverviewManager.Instance.MarkOverviewElementVisited(poiId);

        //change marker
        if (!(bool)marker["visited"]) {
            marker.prefab = visitedMarkerPrefab;
            marker.Update();
            //OnlineMapsMarker3D newMarker = OnlineMapsMarker3DManager.CreateItem(marker.position, visitedMarkerPrefab);
            //OnlineMapsMarker3DManager.RemoveItem(marker);
        }
    }

    /// <summary>
    /// displays a note on screen that the player reached a poi
    /// </summary>
    /// <param name="poiID"></param>
    /// <returns></returns>
    private async UniTask DisplayNotePOIReached(int poiID) {
        POI poi = DatabaseManager.SelectEntry<POI>(poiID);

        string visitedNote = await reachedPOI.GetLocalizedStringAsync();
        string name = LocalizationManager.GetLocalizedDBString(poi.NameLocKey);

        EasyMessageBox.Show(
            message: string.Format(visitedNote, name),
            button1Action: delegate { OnMarkerClick?.Invoke(poiID, true, true); }
            );
    }

    public static float DistanceInMeters(Vector2 a, Vector2 b) {
        float distance;
        double dx, dy, lat;
        lat = (a.y + b.y) / 2 * 0.01745;
        dx = 111.3 * Math.Cos(lat) * (a.x - b.x);
        dy = 111.3 * (a.y - b.y);
        distance = (float)Math.Sqrt(dx * dx + dy * dy); //in kilometers
        return distance * 1000;
    }

    private void ClearMapMarkers() {
        //clear all elements
        if (OnlineMapsMarker3DManager.instance.Count > 0) OnlineMapsMarker3DManager.instance.RemoveAll();
        if (tabPoiContent.childCount > 0) {
            foreach (Transform child in tabPoiContent) {
                Destroy(child.gameObject);
            }
        }

        if (TrophiesAndOverviewManager.Instance != null)
            TrophiesAndOverviewManager.Instance.ClearOverview();

        poisDict.Clear();
        OnlineMapsDrawingElementManager.RemoveAllItems();
        markersInitialized = false;
        userMarker = null;
    }

    private void OnPositionRestored() {
        currentSelectedPoi = -1;
        CheckPOIDistances();
    }

    #endregion

    //#region Bookmarked
    //public void OnBtnBookmarkedTab() {
    //    ClearBookmark();
    //    OnBtnBookmarkedTabInternal();
    //}

    //private async UniTask OnBtnBookmarkedTabInternal() {
    //    //generate bookmarks
    //    List<POI> pois = DatabaseManager.SelectMultipleEntries<POI>("RegionID = " + GameManager.CurrentRegionID);

    //    int bookmarked = 0;
    //    for (int i = 0; i < pois.Count; i++) {
    //        for (int j = 0; j < GameManager.SaveData.bookmarkData.Count; j++) {
    //            if (pois[i].ID == GameManager.SaveData.bookmarkData[j]) {
    //                bookmarked++;
    //                POI poi = pois[i];

    //                //create tab element
    //                GameObject tabElement = await Addressables.InstantiateAsync(TabListBtnElement, tabBookmarkContent);
    //                if (tabElement == null) {
    //                    Debug.LogError("Reference does not exist! " + TabListBtnElement);
    //                    return;
    //                }

    //                bool visited = GameManager.Instance.HasVisited(poi.ID);

    //                Image points = tabElement.FindComponentInChildWithTag<Image>(Tags.TARGET);
    //                Color pointsColor = visited ? GameManager.Instance.GetPaletteColor(Palette.ColorGroups.FOREGROUND) : GameManager.Instance.GetPaletteColor(Palette.ColorGroups.UNSELECTED);
    //                points.color = pointsColor;
    //                TextMeshProUGUI pointsText = points.GetComponentInChildren<TextMeshProUGUI>(true);
    //                pointsText.text = poi.UnlockPoints.ToString();

    //                tabElement.GetComponentInChildren<TextMeshProUGUI>().text = LocalizationManager.GetLocalizedDBString(poi.NameLocKey);
    //                Button btn = tabElement.GetComponent<Button>();
    //                btn.onClick.AddListener(delegate { OnBtnTabPoiClick(poi.ID, poi.Latitude, poi.Longitude, visited, false); });

    //                Image img = btn.gameObject.FindComponentInChildWithTag<Image>(Tags.IMG_REPLACE);
    //                Sprite sprite = await AddressablesManager.Instance.LoadSprite(poi.AtlasReference, poi.ImgReference);
    //                img.sprite = sprite;
    //                var ratio = img.GetComponent<AspectRatioFitter>();
    //                ratio.aspectMode = AspectRatioFitter.AspectMode.EnvelopeParent;
    //                ratio.aspectRatio = (float)sprite.rect.width / (float)sprite.rect.height;
    //            }
    //        }
    //    }

    //    if (bookmarked == 0) {
    //        noBookmarkText.SetActive(true);
    //    }
    //    else {
    //        noBookmarkText.SetActive(false);
    //    }
    //}

    //private void ClearBookmark() {
    //    if (tabBookmarkContent.childCount > 0) {
    //        foreach (Transform child in tabBookmarkContent) {
    //            Destroy(child.gameObject);
    //        }
    //    }
    //}

    //#endregion

    #region Tours
    private async UniTask GetTours(int region) {
        ClearTourList();

        List<Tour> tours = DatabaseManager.SelectMultipleEntries<Tour>("RegionID = " + region);
        btnToursTab.SetActive(tours.Count > 1);
        if (tours == null) return; //no tours available

        foreach (Tour tour in tours) {
            //create list elements
            GameObject tabElement = await Addressables.InstantiateAsync(TabListBtnElement, tabTourContent);
            if (tabElement == null) {
                Debug.LogError("Reference does not exist! " + TabListBtnElement);
                return;
            }

            tabElement.FindComponentInChildWithTag<RectTransform>(Tags.TARGET).gameObject.SetActive(false);

            tabElement.GetComponentInChildren<TextMeshProUGUI>().text = LocalizationManager.GetLocalizedDBString(tour.NameLocKey);
            tabElement.GetComponent<Button>().onClick.AddListener(delegate { OnBtnTourClick(tour.ID); });

            Image img = tabElement.gameObject.FindComponentInChildWithTag<Image>(Tags.IMG_REPLACE);
            Sprite sprite = await AddressablesManager.Instance.LoadSprite(tour.AtlasReference, tour.ImgPreviewReference);
            img.sprite = sprite;
            var ratio = img.GetComponent<AspectRatioFitter>();
            ratio.aspectMode = AspectRatioFitter.AspectMode.EnvelopeParent;
            ratio.aspectRatio = (float)sprite.rect.width / (float)sprite.rect.height;

            toursDict.Add(tour.ID, tabElement.GetComponent<RectTransform>());
        }
    }

    private void OnBtnTourClick(int tourId) {
        Debug.Log("Tour click");
        //check if player has an on-going tour
        if (GameManager.SaveData.tourData != null) {
            if (GameManager.SaveData.tourData.tourID != tourId) {
                Debug.Log("tour in progress");
                string warning;
                if (GameManager.currentTourMode != TourTypes.FREE_EXPLORATION) {

                    tourInProgress.GetLocalizedStringAsync().Completed += asyncOP => {
                        warning = asyncOP.Result;
                        string confirm = Values.LOCALIZED_TERMS[Values.LOC_KEY_CONFIRM];
                        string cancel = Values.LOCALIZED_TERMS[Values.LOC_KEY_CANCEL];

                        EasyMessageBox.Show(warning,
                            button1Text: confirm,
                            button1Action: () => { ConfirmTourChange(tourId); },
                            button2Text: cancel);
                    };


                }
                else {
                    ConfirmTourChange(tourId);
                }
            }
        }
        else {
            ConfirmTourChange(tourId);
        }
    }

    public delegate void TourChange(int tourId);
    public static event TourChange OnTourChanged;

    private void ConfirmTourChange(int tourId) {
        Debug.Log("tour selected");
        GetMarkers(tourId);
        GameManager.SaveData.tourData = new TourData { tourID = tourId, currentTourPOIArrayIndex = 0 };
        warningNoTourSelected.SetActive(false);
        OnTourChanged?.Invoke(tourId);
        SetTourIndicator(tourId);
        //OpenTab((int)MainMenuTabTypes.SIGHTS);
    }

    private void SetTourIndicator(int tourId) {
        selectedTourIndicator.gameObject.SetActive(true);
        selectedTourIndicator.SetParent(toursDict[tourId]);
        selectedTourIndicator.anchoredPosition = Vector2.zero;
        selectedTourIndicator.localPosition = new Vector3(selectedTourIndicator.localPosition.x, selectedTourIndicator.localPosition.y, 0);
    }

    private void ClearTourList() {
        selectedTourIndicator.gameObject.SetActive(false);
        selectedTourIndicator.SetParent(null);

        if (tabTourContent.childCount > 0) {
            foreach (Transform child in tabTourContent) {
                Destroy(child.gameObject);
            }
        }

        toursDict.Clear();
    }
    #endregion

    #region Cities
    public void OnBtnCitiesTab() {
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
        OnBtnCitiesTabInternal();
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
    }

    private async UniTask OnBtnCitiesTabInternal() {
        IAPManager.Instance.Initialize();

        if (!citiesInitialized) {
            selectedCityIndicator.SetParent(citiesContent);
            foreach (Transform child in citiesContent) {
                if (child == selectedCityIndicator) continue;
                Destroy(child.gameObject);
            }
            citiesDict.Clear();


            loadingProgress.SetActive(true);
            //List<Region> regions = DatabaseManager.SelectMultipleEntries<Region>(string.Format("IsActive = 1"));
            List<Region> regions;
            regions = DatabaseManager.SelectAll<Region>("\"Order\"");


            //if (GameManager.debugging) {
            //    regions = DatabaseManager.SelectAll<Region>("\"Order\"");
            //    Debug.Log("BUNNY: " + regions.Count);
            //}
            //else {
            //    regions = DatabaseManager.SelectMultipleEntries<Region>(string.Format("IsActive > 0 ORDER BY \"Order\""));
            //    //regions = regions.OrderBy(r => DistanceInMeters(new Vector2(r.Longitude, r.Latitude), userMarker.position)).ToList();
            //}

            List<CityRegion> subRegions = new List<CityRegion>();
            List<CityRegion> parentRegions = new List<CityRegion>();
            List<Transform> cities = new List<Transform>();

            for (int i = 0; i < regions.Count; i++) {
                if (regions[i].IsActive == 0) {
                    if (regions[i].ParentID != 0 && !GameManager.debugging)
                        continue; //inactive region that is not a parent region
                }
                int id = regions[i].ID;

                GameObject go = await Addressables.InstantiateAsync(cityListElement, citiesContent);
                cities.Add(go.transform);
                go.name = regions[i].Order + " " + regions[i].Name + " " + i;

                CityRegion cityRegion = go.GetComponent<CityRegion>();
                cityRegion.id = id;
                cityRegion.parentId = regions[i].ParentID;

                if (regions[i].ParentID >= 0) {
                    Region parentReg = DatabaseManager.SelectEntry<Region>(regions[i].ParentID);

                    Image border = go.FindComponentInChildWithTag<Image>(Tags.TARGET);
                    Color borderCol;
                    if (regions[i].ParentID == 0) {
                        //is parent
                        parentRegions.Add(cityRegion);
                        ColorUtility.TryParseHtmlString(regions[i].ColorSelected, out borderCol);
                        Color textCol;
                        ColorUtility.TryParseHtmlString(regions[i].ColorForeground, out textCol);
                        cityRegion.subRegionCount.color = textCol;
                        cityRegion.flag.color = textCol;

                    }
                    else {
                        ColorUtility.TryParseHtmlString(parentReg.ColorSelected, out borderCol);
                    }
                    border.color = borderCol;
                    border.gameObject.SetActive(true);

                    if (regions[i].ParentID > 0)
                        subRegions.Add(cityRegion);
                }

                Button btn = go.GetComponent<Button>();

                Image img = btn.gameObject.FindComponentInChildWithTag<Image>(Tags.IMG_REPLACE);
                Sprite sprite = await AddressablesManager.Instance.LoadSprite(regions[i].AtlasReference, regions[i].ImgReference);
                img.sprite = sprite;
                var ratio = img.GetComponent<AspectRatioFitter>();
                ratio.aspectMode = AspectRatioFitter.AspectMode.EnvelopeParent;
                ratio.aspectRatio = (float)sprite.rect.width / (float)sprite.rect.height;

                TextMeshProUGUI label = go.GetComponentInChildren<TextMeshProUGUI>();
                label.text = regions[i].Name;
                citiesDict.Add(regions[i].ID, go.GetComponent<RectTransform>());

                go.SetActive(regions[i].ParentID <= 0);

                if (regions[i].ParentID == 0) {
                    label.fontStyle = FontStyles.SmallCaps;
                    btn.onClick.AddListener(cityRegion.ToggleSubRegions);
                }
                else {
                    btn.onClick.AddListener(delegate { OnBtnCity(cityRegion.id); });
                }
            }

            //loop through subregions to re-assign their order
            for (int i = subRegions.Count - 1; i >= 0; i--) {
                CityRegion parent = citiesDict[subRegions[i].parentId].GetComponent<CityRegion>();
                parent.subregions.Add(subRegions[i].gameObject);
                //Debug.Log(i + " || " + subRegions[i].name + " || parent: " + citiesDict[parent.id].transform.name + "  - " + citiesDict[parent.id].transform.GetSiblingIndex());

                //subRegions[i].transform.SetSiblingIndex(citiesDict[subRegions[i].parentId].transform.GetSiblingIndex() + 1);
                parent.subRegionCount.text = parent.subregions.Count.ToString();
                parent.subRegionCount.transform.parent.gameObject.SetActive(true);
            }

            List<Transform> sorted = new List<Transform>();

            Debug.Log("SORT");
            sorted = cities.OrderBy(c => DatabaseManager.SelectEntry<Region>(c.GetComponent<CityRegion>().id).Order).ToList();

            for (int i = 0; i < sorted.Count; i++) {
                Debug.Log(i + " || " + sorted[i].name);
                sorted[i].SetSiblingIndex(i);
            }

            for (int i = sorted.Count - 1; i >= 0; i--) {
                CityRegion r = sorted[i].GetComponent<CityRegion>();
                if (r.parentId > 0) {
                    Debug.Log(i + " || " + sorted[i].name + " || parent: " + citiesDict[r.parentId].transform.name + "  - " + citiesDict[r.parentId].transform.GetSiblingIndex());
                    sorted[i].SetSiblingIndex(citiesDict[r.parentId].transform.GetSiblingIndex() + 1);
                }
            }

            for (int i = 0; i < parentRegions.Count; i++) {
                parentRegions[i].GenerateGroupIndicators();
            }

            loadingProgress.SetActive(false);
            citiesInitialized = true;
        }

        if (GameManager.CurrentRegionID != -1) {
            selectedCityIndicator.SetParent(citiesDict[GameManager.CurrentRegionID]);
            selectedCityIndicator.anchoredPosition = Vector2.zero;
            selectedCityIndicator.localScale = Vector3.one;
            selectedCityIndicator.gameObject.SetActive(true);
        }
        else {
            selectedCityIndicator.gameObject.SetActive(false);
        }

        OnBtnSubTab((int)HomeTabTypes.CITIES);
    }

    private void OnBtnCity(int id) {
        Debug.Log("blocked? " + Utilities.blocked);
        if (Utilities.blocked) return;
        //show menu                                                       
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
        Debug.Log("Open menu please");
        OpenCityMenu(id);
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
    }

    private async UniTask OpenCityMenu(int id) {
        Region region = DatabaseManager.SelectEntry<Region>(id);

        Sprite sprite = await AddressablesManager.Instance.LoadSprite(region.AtlasReference, region.ImgReference);

        long dlSize = 0;
        bool isValid = await AddressablesManager.ValidateKey(id.ToString());
        if (isValid) {
            dlSize = await Addressables.GetDownloadSizeAsync(id.ToString());
        }

        Debug.Log("Before palette");

        GameManager.Instance.UpdatePalette(region);

        Debug.Log("Before city initialization");


        cityMenuManager.Initialize(sprite, region, dlSize);

        if (!IAPManager.Instance.IsInitialized()) {
            Debug.Log("IAP not initialized");
            if (!IAPManager.Instance.IsPurchasable(id)) {
                cityMenuManager.ChangeButtonText(CityMenuManager.SwitchText.SWITCH);
            }
            else {
                cityMenuManager.ChangeButtonText(CityMenuManager.SwitchText.PURCHASE_ERROR);
            }
        }
        else {
            Debug.Log("IAP initialized");
            CityMenuManager.SwitchText switchText = IAPManager.Instance.IsPurchasable(id) && !IAPManager.Instance.HasPurchased(IAPManager.Instance.GetPurchaseID(id)) ? CityMenuManager.SwitchText.PURCHASE : CityMenuManager.SwitchText.SWITCH;
            cityMenuManager.ChangeButtonText(switchText);
        }

        Debug.Log("Before button set");


        //if already selected
        //hide switch button
        cityMenuManager.btnSwitch.gameObject.SetActive(GameManager.CurrentRegionID != id);

        cityMenuManager.btnSwitch.onClick.RemoveAllListeners();
        cityMenuManager.btnSwitch.onClick.AddListener(delegate { OnBtnCityInternal(id, region.Name, region.DefaultTour); });


        Debug.Log("Before animation");

        //AnimatePanel(true, cityMenuManager.gameObject.FindComponentInChildWithTag<RectTransform>(Tags.TARGET));
        AnimatePanel(true, cityMenuManager.gameObject);
        Debug.Log("Open menu please");
    }

    private async UniTask OnBtnCityInternal(int id, string regionName, int defaultTour) {
        Debug.Log("Change...");
        string warning;
        string confirm = Values.LOCALIZED_TERMS[Values.LOC_KEY_CONFIRM];
        string cancel = Values.LOCALIZED_TERMS[Values.LOC_KEY_CANCEL];

        //check if internet connected
        if (InternetReachabilityVerifier.Instance.status != InternetReachabilityVerifier.Status.NetVerified) {
            //no internet

            Region region = DatabaseManager.SelectEntry<Region>(id);
            //check if region is premium
            if (region.IsActive == 2) {
                //internet required to change
                warning = await internetRequired.GetLocalizedStringAsync();

                EasyMessageBox.Show(warning);
                return;
            }

            //check if city was downloaded
            bool available = await AddressablesManager.ValidateKey(id.ToString());
            if (!available) {
                //display warning
                warning = await noCityDownloadWarning.GetLocalizedStringAsync();

                EasyMessageBox.Show(warning,
                    button1Text: confirm,
                    button1Action: () => DisplayCityChangeWarning(id, regionName, defaultTour),
                    button2Text: cancel);
                return;
            }
        }

        if (IAPManager.Instance.IsPurchasable(id) && !IAPManager.Instance.HasPurchased(IAPManager.Instance.GetPurchaseID(id))) {
            Debug.Log("Purchase");
            IAPManager.Instance.BuyProductID(IAPManager.Instance.GetPurchaseID(id));
        }
        else {
            Debug.Log("Change");
            await DisplayCityChangeWarning(id, regionName, defaultTour);
        }
    }

    private async UniTask DisplayCityChangeWarning(int id, string regionName, int defaultTour) {
        string warning;
        string confirm = Values.LOCALIZED_TERMS[Values.LOC_KEY_CONFIRM];
        string cancel = Values.LOCALIZED_TERMS[Values.LOC_KEY_CANCEL];

        if (GameManager.CurrentRegionID == -1 || GameManager.currentTourMode == TourTypes.FREE_EXPLORATION) {
            //a city has not been selected yet
            string tempWarning = await cityChangeWarning.GetLocalizedStringAsync();
            warning = string.Format(tempWarning, regionName);
        }
        else {
            warning = await existingCityChangeWarning.GetLocalizedStringAsync();
        }


        EasyMessageBox.Show(warning,
            button1Text: confirm,
            button1Action: () => ConfirmCityChangeInternal(id, regionName, defaultTour),
            button2Text: cancel);
    }

    public delegate void CityChanged();
    public static event CityChanged OnCityChanged;


    private async UniTask ConfirmCityChangeInternal(int id, string regionName, int defaultTour) {
        noSightsText.SetActive(false);
        cityMenuManager.btnSwitch.gameObject.SetActive(false);
        downloadProgress.gameObject.SetActive(true);
        bool isNew = await AddressablesManager.DownloadDependencies(new string[] { id.ToString() });

        if (isNew) {
            string url = "https://stempelpass.de/APP/API/updateCount.php?regID=" + id;
            UnityEngine.Networking.UnityWebRequest request = new UnityEngine.Networking.UnityWebRequest(url);
            request.SendWebRequest();
        }

        GameManager.CurrentRegionID = id;
        Debug.Log("Region changed");

        Region region = DatabaseManager.SelectEntry<Region>(id);
        //await LoadHeaderImg(region.AtlasReference, region.ImgReference, region.LogoImgReference);
        SetHelpBtn(region.HelpURL);

        GameManager.Instance.UpdatePalette(region);

        GameManager.SaveData.tourData = null;
        await GetTours(id);

        OnBtnTourClick(defaultTour);
        tabGroup.tabsDict[(int)HomeTabTypes.CITIES].info.titleKey.GetLocalizedStringAsync().Completed += asyncOp => {
            string tab = asyncOp.Result;
            tabHeaderName.text = string.Format("{0} <size={1}>/ {2}</size>", regionName, Values.FONT_SUB_SIZE, tab);
        };



        //noTourSelected.GetLocalizedStringAsync().Completed += asyncOp => {
        //    mapWarning.text = asyncOp.Result;
        //    mapWarning.transform.parent.gameObject.SetActive(true);
        //};

        //RectTransform citymenu = cityMenuManager.gameObject.FindComponentInChildWithTag<RectTransform>(Tags.TARGET);
        //RectTransform menu = canvasMainTabs.FindComponentInChildWithTag<RectTransform>(Tags.TARGET);

        if (!btnReturnRegionView.activeSelf) {
            AnimatePanel(false, canvasMainTabs);
            //Utilities.MovePanelInstantly(Direction.LEFT, menu);
        }

        homeBtn.SetActive(true);
        nearestPoi.SetActive(true);
        mainMenu.SetActive(true);
        btnReturnRegionView.SetActive(false);
        mainMenuTabGroup.OnTabSelect((int)MainMenuTabTypes.HOME);
        warningNoRegionSelected.SetActive(false);
        //OpenMainMenuInstant(true);
        OnCityChanged?.Invoke();



        canvasMainTabs.SetActive(false);
        OnBtnReturn(cityMenuManager.GetComponent<RectTransform>());
    }

    public void OnBtnMapView() {
        if (Utilities.blocked) return;
        GetCityMarkers();
        CreateUsermarker();
        mainMenu.SetActive(false);
        btnReturnRegionView.SetActive(true);
        homeBtn.SetActive(false);
        //OnBtnReturn(canvasMainTabs.gameObject.FindComponentInChildWithTag<RectTransform>(Tags.TARGET));
        OnlineMaps.instance.SetPositionAndZoom(10.1213309783489, 52.3352926108221, 7.5f);
        OnBtnReturn(canvasMainTabs.GetComponent<RectTransform>());
    }

    public void OnBtnReturnToRegionView() {
        homeBtn.SetActive(true);
        mainMenu.SetActive(true);
        btnReturnRegionView.SetActive(false);
        if (GameManager.SaveData.currentRegionID != -1
            && DatabaseManager.SelectEntry<Region>(GameManager.SaveData.currentRegionID) != null
            && DatabaseManager.SelectEntry<Region>(GameManager.SaveData.currentRegionID).IsActive == 1) {
            GetMarkers(GameManager.SaveData.tourData);
        }
    }

    //default sorted by distance
    private bool sortAlphabet = true;

    public void SortCity(Toggle value) {
        Debug.Log("SORT");
        sortAlphabet = value.isOn;

        List<Transform> cities = new List<Transform>();
        List<Transform> sorted = new List<Transform>();
        foreach (Transform child in citiesContent) {
            if (child == selectedCityIndicator || child.CompareTag(Tags.RESPAWN)) continue;
            cities.Add(child);
        }

        if (cities.Count <= 0) return;
        Debug.Log("SORT");

        if (sortAlphabet) {
            sorted = cities.OrderBy(c => DatabaseManager.SelectEntry<Region>(c.GetComponent<CityRegion>().id).Order).ToList();
        }
        else {
            if (userMarker == null) {
                value.isOn = true; //force back to alphabet
                noGPS.GetLocalizedStringAsync().Completed += asyncOP => {
                    EasyMessageBox.Show(asyncOP.Result);
                };
                return;
            }
            sorted = cities.OrderBy(c => DistanceInMeters(userMarker.position, new Vector2(DatabaseManager.SelectEntry<Region>(c.GetComponent<CityRegion>().id).Longitude, DatabaseManager.SelectEntry<Region>(c.GetComponent<CityRegion>().id).Latitude))).ToList();
        }

        for (int i = 0; i < sorted.Count; i++) {
            Debug.Log(i + " || " + sorted[i].name);
            sorted[i].SetSiblingIndex(i);
        }

        for (int i = sorted.Count - 1; i >= 0; i--) {
            CityRegion r = sorted[i].GetComponent<CityRegion>();
            if (r.parentId > 0) {
                Debug.Log(i + " || " + sorted[i].name + " || parent: " + citiesDict[r.parentId].transform.name + "  - " + citiesDict[r.parentId].transform.GetSiblingIndex());
                sorted[i].SetSiblingIndex(citiesDict[r.parentId].transform.GetSiblingIndex() + 1);
            }
        }

        foreach (Transform child in citiesContent) {
            if (child.CompareTag(Tags.RESPAWN)) {
                child.SetAsFirstSibling();
            }
        }

        StartCoroutine(DelayOnCitySorted());
    }

    private IEnumerator DelayOnCitySorted() {
        yield return new WaitForEndOfFrame();
        OnCitySorted?.Invoke();
    }

    public delegate void CitySorted();
    public static event CitySorted OnCitySorted;

    #endregion

    #region Logbook
    private int currentLogbookCount = 0;

    public void OnBtnLogbookTab() {
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
        OnBtnLogbookTabInternal();
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
    }

    private async UniTask OnBtnLogbookTabInternal() {
        loadingProgress.SetActive(true);
        //clear list because it might have changed
        ClearLogbook();

        if (GameManager.SaveData.poiData.Count == 0) {
            //no entries yet
            noLogbookEntriesText.SetActive(true);
            loadMoreBtn.gameObject.SetActive(false);
        }
        else {
            noLogbookEntriesText.SetActive(false);

            //generate list
            //start from the last element since we want it to be displayed descending
            int count = 0;
            for (int i = GameManager.SaveData.poiData.Count - 1; i >= 0; i--) {
                if (count >= Values.MAX_LOGBOOK_LOAD) break; //stop loading to save performance
                count++;
                currentLogbookCount++;
                POI poi = DatabaseManager.SelectEntry<POI>(GameManager.SaveData.poiData[i].id);
                Region region = DatabaseManager.SelectEntry<Region>(DatabaseManager.SelectEntry<POIRegion>("POIID = " + poi.ID).RegionID);

                await GenerateLogbookEntry(poi, region, GameManager.SaveData.poiData[i].dateOfVisit);
            }

            loadMoreBtn.gameObject.SetActive(currentLogbookCount < GameManager.SaveData.poiData.Count);
            loadMoreBtn.SetAsLastSibling();
        }
        loadingProgress.SetActive(false);
        OnBtnSubTab((int)HomeTabTypes.LOGBOOK, true);
    }

    private async UniTask GenerateLogbookEntry(POI poi, Region region, string dateOfVisit) {
        GameObject go = await Addressables.InstantiateAsync(logbookListElement, logbookContent);

        Button btn = go.GetComponentInChildren<Button>();

        TextMeshProUGUI info = go.FindComponentInChildWithTag<TextMeshProUGUI>(Tags.NAME_REPLACE);
        TextMeshProUGUI date = go.FindComponentInChildWithTag<TextMeshProUGUI>(Tags.TEXT_REPLACE);

        string name = LocalizationManager.GetLocalizedDBString(poi.NameLocKey);
        string description = LocalizationManager.GetLocalizedDBString(poi.DescriptionLocKey);

        //description is too long to fit into the logbook entry, shorten it
        if (description.Length > 150) {
            description = description.Truncate(150) + "...";
        }

        string infoText = $"{name}<size={Values.FONT_SUB_SIZE}>\n{region.Name}<size={Values.FONT_SUB_SIZE_SMALL}>\n{description}";
        info.text = infoText;

        DateTime dateTime;
        DateTime.TryParse(dateOfVisit, out dateTime);
        if (dateTime != null) {
            date.text = dateTime.ToString("MMM\ndd\nyyyy");
        }

        Image timestamp = go.FindComponentInChildWithTag<Image>(Tags.TARGET);
        Color col;
        ColorUtility.TryParseHtmlString(region.ColorForeground, out col);
        timestamp.color = col;

        Image img = btn.gameObject.FindComponentInChildWithTag<Image>(Tags.IMG_REPLACE);
        Sprite sprite = await AddressablesManager.Instance.LoadSprite(poi.AtlasReference, poi.ImgReference);
        img.sprite = sprite;
        var ratio = img.GetComponent<AspectRatioFitter>();
        ratio.aspectMode = AspectRatioFitter.AspectMode.EnvelopeParent;
        ratio.aspectRatio = (float)sprite.rect.width / (float)sprite.rect.height;


        btn.onClick.AddListener(delegate { OnBtnLogbookClick(poi.ID); });
    }

    public void OnBtnLoadMoreLogbookEntries() {
        OnBtnLoadMoreLogbookEntriesInternal();
    }

    private async UniTask OnBtnLoadMoreLogbookEntriesInternal() {
        int count = 0;
        int start = GameManager.SaveData.poiData.Count - 1 - currentLogbookCount;
        if (start < 0) return;

        for (int i = start; i >= 0; i--) {
            if (count >= Values.MAX_LOGBOOK_LOAD) break; //stop loading to save performance
            count++;
            currentLogbookCount++;
            POI poi = DatabaseManager.SelectEntry<POI>(GameManager.SaveData.poiData[i].id);
            Region region = DatabaseManager.SelectEntry<Region>(DatabaseManager.SelectEntry<POIRegion>("POIID = " + poi.ID).RegionID);

            await GenerateLogbookEntry(poi, region, GameManager.SaveData.poiData[i].dateOfVisit);
        }

        loadMoreBtn.gameObject.SetActive(currentLogbookCount < GameManager.SaveData.poiData.Count);
        loadMoreBtn.SetAsLastSibling();
    }

    private void OnBtnLogbookClick(int poiId) {
        OnMarkerClick?.Invoke(poiId, true, false);
    }

    private void ClearLogbook() {
        foreach (Transform child in logbookContent) {
            if (child == loadMoreBtn) continue;
            Destroy(child.gameObject);
        }

        currentLogbookCount = 0;
    }

    #endregion

    #region Help
    private void SetHelpBtn(string helpURL) {
        helpBtn.onClick.RemoveAllListeners();
        helpBtn.onClick.AddListener(delegate { OnBtnHelpTab(helpURL); });
    }

    private void OnBtnHelpTab(string helpURL) {
        GameManager.Instance.OpenURL(helpURL);
    }
    #endregion

    #region Settings
    public void OnBtnSettingsTab() {
        OnBtnSubTab((int)HomeTabTypes.SETTINGS);
        sliderAudioFiles.value = GameManager.SaveData.soundSettings._maxAudioFileVolume;
        sliderBGM.value = GameManager.SaveData.soundSettings._maxBGMVolume;
        sliderSFX.value = GameManager.SaveData.soundSettings._maxSFXVolume;

        Debug.Log(GameManager.SaveData.redeemedRewardData.Count);


        //redeem history
        if (GameManager.SaveData.redeemedRewardData.Count == 0) {
            noRedeemHistory.GetLocalizedStringAsync().Completed += asyncOp => {
                redeemHistory.text = asyncOp.Result;
                return;
            };
        }
        else {
            Debug.Log("Checking redeem history");
            redeemHistory.text = string.Empty;

            for (int i = 0; i < GameManager.SaveData.redeemedRewardData.Count; i++) {
                Reward reward = DatabaseManager.SelectEntry<Reward>(GameManager.SaveData.redeemedRewardData[i].id);
                Region region = DatabaseManager.SelectEntry<Region>(reward.RegionID);

                DateTime dateTime = DateTime.Parse(GameManager.SaveData.redeemedRewardData[i].date);
                string text = dateTime.ToString("D") + "\n"
                    + $"\t{region.Name} - {reward.RequiredPoints} {bunLace.Constants.Values.LOCALIZED_TERMS[bunLace.Constants.Values.LOC_KEY_POINTS]}";

                if (!string.IsNullOrEmpty(reward.RewardMessage)) {
                    text += $"\n{reward.RewardMessage}";
                }

                text += "\n";

                redeemHistory.text += text;
            }
        }
    }

    public void OnBtnOpenLink(string link) {
        GameManager.Instance.OpenURL(link);
    }

    public delegate void AudioVolumeChanged(float value);
    public static event AudioVolumeChanged OnAudioVolumeChanged;

    public void OnSliderBGMChange(Single value) {
        GameManager.SaveData.soundSettings._maxBGMVolume = value;
        OnAudioVolumeChanged?.Invoke(value);
    }

    public void OnSliderSFXChange(Single value) {
        GameManager.SaveData.soundSettings._maxSFXVolume = value;
        OnAudioVolumeChanged?.Invoke(value);
    }

    public void OnSliderAudioFilesChange(Single value) {
        GameManager.SaveData.soundSettings._maxAudioFileVolume = value;
        OnAudioVolumeChanged?.Invoke(value);
    }

    public void OnBtnTestAudioFile() {
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
        SoundManager.PlayAudioFile(Values.AUDIO_TEST_KEY, false);
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
    }

    public void OnBtnTestBGM() {
        SoundManager.PlayBGM(Values.AUDIO_TEST_KEY, false);
    }

    public void OnBtnTestSFX() {
        SoundManager.PlaySFX(Values.AUDIO_TEST_KEY);
    }

    public void OnBtnReturn() {
        //stop all audio files
        SoundManager.StopAudio();
    }

    public async void OnBtnResetPoints() {
        if (GameManager.CurrentRegionID == -1) {
            //no region selected
            string error = await pointResetError.GetLocalizedStringAsync();
            EasyMessageBox.Show(error);
        }
        else {
            string confirmMessage = await pointResetConfirm.GetLocalizedStringAsync();
            string confirm = Values.LOCALIZED_TERMS[Values.LOC_KEY_CONFIRM];
            string cancel = Values.LOCALIZED_TERMS[Values.LOC_KEY_CANCEL];
            EasyMessageBox.Show(
                message: confirmMessage,
                button1Text: confirm,
                button1Action: ResetPoints,
                button2Text: cancel);
        }
    }

    private void ResetPoints() {
        GameManager.Instance.ResetPoints(GameManager.CurrentRegionID);
        GetMarkers(GameManager.SaveData.tourData.tourID, 0);
    }

    public void SyncSaveData() {
        GameManager.Instance.SyncSave(true);
    }

    #endregion

    #region Animation
    private void AnimatePanel(bool show, RectTransform target) {
        //Direction direction = show ? Direction.RIGHT : Direction.LEFT;
        //Utilities.MovePanel(direction, target);
        //Utilities.FadePanel(target.gameObject, show);
        //CanvasFader.Screengrab();
        target.gameObject.SetActive(show);
    }

    private void AnimatePanel(bool show, GameObject target) {
        SoundManager.PlaySFX(Values.AUDIO_PAGE_FLIP_KEY);
        //Direction direction = show ? Direction.RIGHT : Direction.LEFT;
        //Utilities.MovePanel(direction, target);
        //Utilities.FadePanel(target.gameObject, show);
        CanvasFader.Screengrab(target, show);
        //target.SetActive(show);
    }
    #endregion
}
