using UnityEngine;
using bunLace.Singleton;
using UnityEngine.Localization;
using UnityEngine.AddressableAssets;
using Cysharp.Threading.Tasks;

namespace bunLace.Localization {
    public class LocalizationManager : PersistentSingleton<LocalizationManager> {
        [SerializeField] private LocalizedStringTable m_ui;

        public LocalizedStringTable UI { get => m_ui; }

        public async static UniTask<string> GetLocalizedString(LocalizedStringTable table, string key) => await InternalGetLocalizedString(table, key);

        public async static UniTask<string> GetLocalizedString(LocalizedString localizedString) => await InternalGetLocalizedString(localizedString);

        public async static UniTask<string> GetLocalizedUIString(string key) => await GetLocalizedString(Instance.UI, key);

        private async static UniTask<string> InternalGetLocalizedString(LocalizedStringTable table, string key) {
            if (table == null) {
                Debug.LogWarning("LOCALIZATION: Table has not been set");
            }
            if (string.IsNullOrEmpty(key)) {
                Debug.LogWarning("LOCALIZATION: key has not been set");
            }

            var asyncOperationHandle = await table.GetTableAsync();

            var entry = asyncOperationHandle.GetEntry(key);
            if (entry == null) {
                Debug.LogWarning("LOCALIZATION: " + key + " could not be found");
                //Addressables.Release(asyncOperationHandle);
                return null;
            }

            string result = entry.LocalizedValue;
            //Addressables.Release(asyncOperationHandle);

            return result;
        }

        private async static UniTask<string> InternalGetLocalizedString(LocalizedString localiizedString) {
            var asyncOperationHandle = await localiizedString.GetLocalizedStringAsync();

            var result = asyncOperationHandle;
            //Addressables.Release(asyncOperationHandle);

            return result;
        }

        public static string GetLocalizedDBString(int id) {
            Database.Localization loc = Database.DatabaseManager.SelectEntry<Database.Localization>(id);
            if (loc != null)
                return loc.LocalizedString;
            else {
                Debug.LogWarning("No Localized String registered; ID = " + id);
                return string.Empty;
            }
        }

        public static string GetLocalizedDBString(string key) {
            Database.Localization loc = Database.DatabaseManager.SelectEntry<Database.Localization>(string.Format("Key = '{0}'", key));
            if (loc != null)
                return loc.LocalizedString;
            else {
                Debug.LogWarning("No Localized String registered; Key = " + key);
                return string.Empty;
            }
        }
    }
}