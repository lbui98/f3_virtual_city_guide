using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Localization;
using bunLace.Singleton;
using bunLace.Tools;
using SimpleSQL;
using bunLace.SaveLoad;
using bunLace.Database;
using System.Linq;
using System;
using Cysharp.Threading.Tasks;
using bunLace.Localization;
using bunLace.Constants;
using UnityEngine.Networking;

#if UNITY_ANDROID
using UnityEngine.Android;
#endif

public class GameManager : PersistentSingleton<GameManager> {
    public static int CurrentRegionID {
        get { return SaveData.currentRegionID; }
        set { SaveData.currentRegionID = value; }
    }
    public static int CurrentPOIID { get; set; }
    public static TourTypes currentTourMode;

    public SimpleSQLManager DatabaseManager { get; private set; }

    public static SaveData SaveData { get; private set; }

    public Palette[] palette;
    private Dictionary<Palette.ColorGroups, Color> paletteDict = new Dictionary<Palette.ColorGroups, Color>();
    public int currentRegionPalette;

    public static bool debugging, ignoreGPSWarning;
    public LocalizedString openURLwarning;

    private new void Awake() {
        base.Awake();
        UpdateLocalizedDictionary();
    }

    private void Start() {

#if UNITY_ANDROID
        if (!Permission.HasUserAuthorizedPermission(Permission.Camera)) {
            Permission.RequestUserPermission(Permission.Camera);
        }
#elif UNITY_IOS
        Application.RequestUserAuthorization(UserAuthorization.WebCam);
#endif

        //base.Awake();
        QualitySettings.vSyncCount = 0;
#if !UNITY_EDITOR
        Application.targetFrameRate = 60;
#else
        //debugging = true;
#endif
        Screen.sleepTimeout = SleepTimeout.NeverSleep;


        DatabaseManager = GetComponent<SimpleSQLManager>();


        Load();
        if (SaveData == null) {
            Save(new SaveData(-1, new List<QuizData>(), new List<POIsData>(), new List<int>(), new TourData { tourID = -1, currentTourPOIArrayIndex = 0 }, new List<int>(), new List<RedeemData>(), new SoundManager.SoundSettings { _maxAudioFileVolume = 1f, _maxBGMVolume = 0.5f, _maxSFXVolume = 0.5f }, new CommunityData()));
            Load();

            Migrate();
        }

        if (SaveLoadManager.LoadNavi() == null) {
            SaveLoadManager.SaveNavigation(new NavigationSaveData(new List<TourNavigationPoints>()));
        }

        CurrentPOIID = -1;

        if (CurrentRegionID != -1) {
            Region region = bunLace.Database.DatabaseManager.SelectEntry<Region>(CurrentRegionID);
            if (region.IsActive == 0) {
                if (debugging)
                    UpdatePalette(region);
                else
                    InitializePalette();
            }
            else {
                UpdatePalette(region);
            }
        }
        else {
            InitializePalette();

        }
        Debug.Log("AAAAAA");

        InvokeRepeating(nameof(Save), 60, 1);

    }

    #region Save Load
    private const string check_activity_API = "https://www.stempelpass.de/APP/API/checkActivity.php";
    private const string get_save_API = "https://www.stempelpass.de/APP/API/getSave.php";
    private const string update_save_API = "https://www.stempelpass.de/APP/API/updateSave.php";

    public void Load() {
        SaveData = SaveLoadManager.Load();
    }

    private void Save(SaveData saveData) {
        saveData.SaveTime = DateTime.Now.ToString();
        SaveLoadManager.Save(saveData);
    }

    public void Save() {
        SaveData.SaveTime = DateTime.Now.ToString();
        SaveLoadManager.Save(SaveData);
    }

    public void SyncSave(string username, string password, SynchronisationOptions sync = SynchronisationOptions.OVERWRITE_NEWER) {
        StartCoroutine(SyncSaveRoutine(username, password, sync));
    }

    public void SyncSave(bool showWhenDone = false) {
        //use username and password from existing savedata on mobile
        if (string.IsNullOrEmpty(SaveData.communityData.username)) {
            EasyMessageBox.Show("Register in the community first");
        }
        else {
            StartCoroutine(SyncSaveRoutine(SaveData.communityData.username, SaveData.communityData.password, showWhenDone: showWhenDone));
        }
    }

    public IEnumerator SyncSaveRoutine(string username, string password, SynchronisationOptions sync = SynchronisationOptions.OVERWRITE_NEWER, bool showWhenDone = false) {
        int currentRegionId = CurrentRegionID;
        TourData currentTour = SaveData.tourData;

        List<IMultipartFormSection> dbForm = new List<IMultipartFormSection>();

        dbForm.Add(new MultipartFormDataSection("username", username));
        dbForm.Add(new MultipartFormDataSection("password", password));
        UnityWebRequest request;

        switch (sync) {
            case SynchronisationOptions.OVERWRITE_NEWER:
                request = UnityWebRequest.Post(check_activity_API, dbForm);
                yield return request.SendWebRequest();

                if (request.result != UnityWebRequest.Result.Success) {
                    Debug.Log(request.error);
                }
                else {
                    Debug.Log("Success");
                    Debug.Log("Response: " + request.downloadHandler.text);
                    if (string.IsNullOrEmpty(request.downloadHandler.text)) {
                        Debug.Log("But error");
                        //no response, user not found or other error
                        yield break;
                    }

                    //resceived timestamp and savedata
                    DateTime dbTime = DateTime.Parse(request.downloadHandler.text);
                    DateTime localSaveTime = DateTime.Parse(SaveData.SaveTime);

                    if (DateTime.Compare(dbTime, localSaveTime) > 0) {
                        Debug.Log("replace local save");
                        //db save is newer
                        //replace local
                        request = UnityWebRequest.Post(get_save_API, dbForm);
                        yield return request.SendWebRequest();
                        Debug.Log(request.downloadHandler.text);

                        SaveData = JsonUtility.FromJson<SaveData>(request.downloadHandler.text);
                    }
                    else {
                        Debug.Log("replace db save");
                        //local is newer
                        //replace db
                        dbForm.Add(new MultipartFormDataSection("savedata", JsonUtility.ToJson(SaveData)));
                        dbForm.Add(new MultipartFormDataSection("points", CalculateTotalPoints().ToString()));
                        request = UnityWebRequest.Post(update_save_API, dbForm);
                        yield return request.SendWebRequest();
                    }
                }
                break;

            case SynchronisationOptions.FORCE_LOCAL_OVERWRITE:
                Debug.Log("FORCE replace local save");
                request = UnityWebRequest.Post(get_save_API, dbForm);
                yield return request.SendWebRequest();
                Debug.Log(request.downloadHandler.text);
                if (string.IsNullOrEmpty(request.downloadHandler.text)) {
                    Debug.Log("But error");
                    //no response, user not found or other error
                    yield break;
                }

                SaveData = JsonUtility.FromJson<SaveData>(request.downloadHandler.text);
                break;

            case SynchronisationOptions.FORCE_DB_OVERWRITE:
                dbForm.Add(new MultipartFormDataSection("savedata", JsonUtility.ToJson(SaveData)));
                dbForm.Add(new MultipartFormDataSection("points", CalculateTotalPoints().ToString()));
                request = UnityWebRequest.Post(update_save_API, dbForm);
                yield return request.SendWebRequest();
                break;

            case SynchronisationOptions.CLEAR_LOCAL:
                request = UnityWebRequest.Post(check_activity_API, dbForm);
                yield return request.SendWebRequest();

                if (request.result != UnityWebRequest.Result.Success) {
                    Debug.Log(request.error);
                }
                else {
                    Debug.Log("Success");
                    Debug.Log("Response: " + request.downloadHandler.text);
                    if (string.IsNullOrEmpty(request.downloadHandler.text)) {
                        Debug.Log("But error");
                        //no response, user not found or other error
                        yield break;
                    }

                    //resceived timestamp and savedata
                    DateTime dbTime = DateTime.Parse(request.downloadHandler.text);
                    DateTime localSaveTime = DateTime.Parse(SaveData.SaveTime);

                    if (DateTime.Compare(dbTime, localSaveTime) > 0) {
                        Debug.Log("replace local save");
                        //db save is newer
                        //replace local
                        request = UnityWebRequest.Post(get_save_API, dbForm);
                        yield return request.SendWebRequest();
                        Debug.Log(request.downloadHandler.text);

                        SaveData = JsonUtility.FromJson<SaveData>(request.downloadHandler.text);
                    }
                    else {
                        Debug.Log("replace db save");
                        //local is newer
                        //replace db
                        dbForm.Add(new MultipartFormDataSection("savedata", JsonUtility.ToJson(SaveData)));
                        dbForm.Add(new MultipartFormDataSection("points", CalculateTotalPoints().ToString()));
                        request = UnityWebRequest.Post(update_save_API, dbForm);
                        yield return request.SendWebRequest();
                    }
                }

                SaveData.communityData = new CommunityData();

                break;
        }

        if (CurrentRegionID != currentRegionId) {
            //was not the same region
            //stay in the same region
            //continue the ongoing tour
            SaveData.tourData = currentTour;
        }

        CurrentRegionID = currentRegionId; //don't sync the region the user has currently selected;
        UpdatePalette(CurrentRegionID);

        Save();


        if (showWhenDone) {
            EasyMessageBox.Show("Synchronisiert");
        }
    }
    #endregion

    #region Palette
    private void InitializePalette() {
        paletteDict.Clear();

        foreach (Palette p in palette) {
            paletteDict.Add(p.colorGroup, p.color);
        }
        //palette = null;
    }

    public Color GetPaletteColor(Palette.ColorGroups colorGroup) {
        if (paletteDict.Count == 0) InitializePalette();

        return paletteDict[colorGroup];
    }

    public delegate void PaletteUpdate();
    public static event PaletteUpdate OnPaletteUpdate;

    public void UpdatePalette(Region region) {
        if (region == null) return;
        Debug.Log("Update Palette " + region.ID);

        Color background, foreground, selected, unselected, textDark, textLight;
        ColorUtility.TryParseHtmlString(region.ColorBackground, out background);
        ColorUtility.TryParseHtmlString(region.ColorForeground, out foreground);
        ColorUtility.TryParseHtmlString(region.ColorSelected, out selected);
        ColorUtility.TryParseHtmlString(region.ColorUnselected, out unselected);
        ColorUtility.TryParseHtmlString(region.ColorTextDark, out textDark);
        ColorUtility.TryParseHtmlString(region.ColorTextLight, out textLight);

        palette = new Palette[] {
            new Palette(Palette.ColorGroups.BACKGROUND, background),
            new Palette(Palette.ColorGroups.FOREGROUND, foreground),
            new Palette(Palette.ColorGroups.SELECTED, selected),
            new Palette(Palette.ColorGroups.UNSELECTED, unselected),
            new Palette(Palette.ColorGroups.TEXT_DARK, textDark),
            new Palette(Palette.ColorGroups.TEXT_LIGHT, textLight),
        };

        InitializePalette();

        currentRegionPalette = region.ID;

        OnPaletteUpdate?.Invoke();
    }

    public void UpdatePalette(int regionId) {
        Region region = bunLace.Database.DatabaseManager.SelectEntry<Region>(regionId);
        UpdatePalette(region);
    }

    #endregion

    #region Migration
    void Migrate() {
        if (PlayerPrefs.HasKey("PlayerStateSetPrefs") || PlayerPrefs.HasKey("PlayerStatePrefs")) {
            Debug.Log("BUNNY: Mitigrating Playerprefs");
            MitigrateToNewDataHandling();
        }
        else {
            Debug.Log("BUNNY: Mitigrating Save file");
            SaveLoadManager.MigrateOldSaveFile();
        }
    }

    private void MitigrateToNewDataHandling() {
        string POINT_PREFS = "PointPrefs";
        string REDEEM_POINTS_PREFS = "RedeemPointsPrefs";
        string PLAYER_STATE_SET_PREFS = "PlayerStateSetPrefs";
        string PLAYER_STATE_PREFS = "PlayerStatePrefs";
        string[] playerPrefsToDelete = { POINT_PREFS, REDEEM_POINTS_PREFS, PLAYER_STATE_SET_PREFS, PLAYER_STATE_PREFS };

        List<POIsData> poiDataList = new List<POIsData>();
        //convert PlayerStatePrefs to region save data
        if (PlayerPrefs.HasKey(PLAYER_STATE_SET_PREFS) || PlayerPrefs.HasKey(PLAYER_STATE_PREFS)) {
            string json = PlayerPrefs.GetString(PLAYER_STATE_PREFS);

            json = json.TrimStart('{');
            json = json.Replace("\"Items\":", "");
            json = json.TrimEnd('}');

            PlayState[] playStateArray = JsonUtil.FromJson<PlayState[]>(json);

            Debug.Log(json);
            Debug.Log(playStateArray.Length);
            for (int i = 0; i < playStateArray.Length; i++) {
                //DebugMe.Log($"ID: {playStateArray[i].Id}\nPOI visited: {playStateArray[i].PoiDone}\nQuizDoneMulti: {playStateArray[i].QuizDoneMulti[0]}\nDeprecatedQuizDone: {playStateArray[i].QuizDone}\nBonus game done: {playStateArray[i].BonusDone}");

                //go through all entries and add only the regions that have been visited/altered
                if (playStateArray[i].PoiDone) {
                    SaveData.poiData.Add(new POIsData { id = playStateArray[i].Id, dateOfVisit = DateTime.Now.ToString() });
                }

                //check if any quiz has been done
                if (playStateArray[i].QuizDoneMulti != null) {
                    foreach (bool done in playStateArray[i].QuizDoneMulti) {
                        if (done) {
                            List<AdditionalContentPOI> additionalContentPOIs = bunLace.Database.DatabaseManager.SelectMultipleEntries<AdditionalContentPOI>($"POIID = {playStateArray[i].Id}");
                            if (additionalContentPOIs == null) continue;
                            for (int j = 0; j < additionalContentPOIs.Count; j++) {
                                AdditionalContent quiz = bunLace.Database.DatabaseManager.SelectEntry<AdditionalContent>($"ID = {playStateArray[j].Id} AND MediaType = {(int)MediaTypes.QUIZ}");
                                SaveData.quizData.Add(new QuizData { id = quiz.ID, cleared = true, firstTimeClear = true });
                            }
                        }
                        break;
                    }
                }

                //check if PlayState still uses the old QuizDone
                if (playStateArray[i].QuizDone) {
                    List<AdditionalContentPOI> additionalContentPOIs = bunLace.Database.DatabaseManager.SelectMultipleEntries<AdditionalContentPOI>($"POIID = {playStateArray[i].Id}");
                    if (additionalContentPOIs != null) {
                        for (int j = 0; j < additionalContentPOIs.Count; j++) {
                            AdditionalContent quiz = bunLace.Database.DatabaseManager.SelectEntry<AdditionalContent>($"ID = {playStateArray[j].Id} AND MediaType = {(int)MediaTypes.QUIZ}");
                            SaveData.quizData.Add(new QuizData { id = quiz.ID, cleared = true, firstTimeClear = true });
                        }
                    }
                }
            }
        }

        //delete playerprefs after mitigration
        foreach (string key in playerPrefsToDelete) {
            PlayerPrefs.DeleteKey(key);
        }
    }
    #endregion

    public int CalculateTotalPoints() {
        int points = 0;

        //poi points
        for (int i = 0; i < SaveData.poiData.Count; i++) {
            POI poi = bunLace.Database.DatabaseManager.SelectEntry<POI>(SaveData.poiData[i].id);
            points += poi.UnlockPoints;
        }

        for (int i = 0; i < SaveData.quizData.Count; i++) {
            if (SaveData.quizData[i].firstTimeClear) {
                points += Values.QUIZ_FIRST_TIME_CLEAR_POINTS;
            }
            else if (SaveData.quizData[i].cleared) {
                points += Values.QUIZ_CLEAR_POINTS;
            }
        }

        points += SaveData.minigameData.Count * Values.MINIGAME_CLEAR_POINTS;

        return points;
    }

    public int CalculatePoints(int regionID) {
        int points = 0;
        List<POIRegion> poiRegions = bunLace.Database.DatabaseManager.SelectMultipleEntries<POIRegion>($"RegionID = {regionID}");

        List<AdditionalContent> quizzes = new List<AdditionalContent>();
        List<AdditionalContent> minigames = new List<AdditionalContent>();

        for (int i = 0; i < poiRegions.Count; i++) {
            List<AdditionalContentPOI> additionalContentPOIs = bunLace.Database.DatabaseManager.SelectMultipleEntries<AdditionalContentPOI>($"POIID = {poiRegions[i].POIID}");
            if (additionalContentPOIs == null) continue;
            for (int j = 0; j < additionalContentPOIs.Count; j++) {
                AdditionalContent ac = bunLace.Database.DatabaseManager.SelectEntry<AdditionalContent>(additionalContentPOIs[j].AdditionalContentID);
                if (ac.MediaType == (int)MediaTypes.QUIZ) {
                    quizzes.Add(ac);
                }
                else if (ac.MediaType == (int)MediaTypes.MINIGAME) {
                    minigames.Add(ac);
                }
            }

            if (HasVisited(poiRegions[i].POIID)) {
                POI poi = bunLace.Database.DatabaseManager.SelectEntry<POI>(poiRegions[i].POIID);
                points += poi.UnlockPoints;
            }
        }


        if (quizzes != null) {
            for (int i = 0; i < quizzes.Count; i++) {
                for (int j = 0; j < SaveData.quizData.Count; j++) {
                    if (SaveData.quizData[j].id == quizzes[i].ID) {
                        if (SaveData.quizData[j].firstTimeClear) points += bunLace.Constants.Values.QUIZ_FIRST_TIME_CLEAR_POINTS;
                        else if (SaveData.quizData[j].cleared) points += bunLace.Constants.Values.QUIZ_CLEAR_POINTS;
                    }
                }
            }
        }

        if (minigames != null) {
            for (int i = 0; i < minigames.Count; i++) {
                for (int j = 0; j < SaveData.minigameData.Count; j++) {
                    if (SaveData.minigameData[j] == minigames[i].ID) {
                        points += bunLace.Constants.Values.MINIGAME_CLEAR_POINTS;
                    }
                }
            }
        }

        return points;
    }

    public int CalculatePoints(Tour tour) {
        int points = 0;
        //List<AdditionalContent> quizzes = bunLace.Database.DatabaseManager.SelectMultipleEntries<AdditionalContent>($"RegionID = {regionID} AND MediaType = {(int)MediaTypes.QUIZ}");
        //List<AdditionalContent> minigames = bunLace.Database.DatabaseManager.SelectMultipleEntries<AdditionalContent>($"RegionID = {regionID} AND MediaType = {(int)MediaTypes.MINIGAME}");

        List<AdditionalContent> quizzes = new List<AdditionalContent>();
        List<AdditionalContent> minigames = new List<AdditionalContent>();

        List<POI> tourPois = new List<POI>();
        for (int i = 0; i < tour.POIIDs.Length; i++) {
            POI poi = bunLace.Database.DatabaseManager.SelectEntry<POI>(tour.POIIDs[i].poiID);
            if (HasVisited(poi.ID)) {
                points += poi.UnlockPoints;
            }

            List<AdditionalContentPOI> additionalContentPOIs = bunLace.Database.DatabaseManager.SelectMultipleEntries<AdditionalContentPOI>($"POIID = {tour.POIIDs[i].poiID}");
            if (additionalContentPOIs != null) continue;
            for (int j = 0; j < additionalContentPOIs.Count; j++) {
                AdditionalContent ac = bunLace.Database.DatabaseManager.SelectEntry<AdditionalContent>(additionalContentPOIs[j].AdditionalContentID);
                if (ac.MediaType == (int)MediaTypes.QUIZ) {
                    quizzes.Add(ac);
                }
                else if (ac.MediaType == (int)MediaTypes.MINIGAME) {
                    minigames.Add(ac);
                }
            }
        }

        if (quizzes != null) {
            for (int i = 0; i < quizzes.Count; i++) {
                for (int j = 0; j < SaveData.quizData.Count; j++) {
                    if (SaveData.quizData[j].id == quizzes[i].ID) {
                        if (SaveData.quizData[j].firstTimeClear) points += bunLace.Constants.Values.QUIZ_FIRST_TIME_CLEAR_POINTS;
                        else if (SaveData.quizData[j].cleared) points += bunLace.Constants.Values.QUIZ_CLEAR_POINTS;
                    }
                }
            }
        }

        if (minigames != null) {
            for (int i = 0; i < minigames.Count; i++) {
                for (int j = 0; j < SaveData.minigameData.Count; j++) {
                    if (SaveData.minigameData[j] == minigames[i].ID) {
                        points += bunLace.Constants.Values.MINIGAME_CLEAR_POINTS;
                    }
                }
            }
        }

        return points;
    }

    public void ResetPoints(int regionID) {
        List<POIRegion> poiRegions = bunLace.Database.DatabaseManager.SelectMultipleEntries<POIRegion>($"RegionID = {regionID}");

        List<AdditionalContent> quizzes = new List<AdditionalContent>();
        List<AdditionalContent> minigames = new List<AdditionalContent>();

        for (int i = 0; i < poiRegions.Count; i++) {
            List<AdditionalContentPOI> additionalContentPOIs = bunLace.Database.DatabaseManager.SelectMultipleEntries<AdditionalContentPOI>($"POIID = {poiRegions[i].POIID}");
            if (additionalContentPOIs == null) continue;
            for (int j = 0; j < additionalContentPOIs.Count; j++) {
                AdditionalContent ac = bunLace.Database.DatabaseManager.SelectEntry<AdditionalContent>(additionalContentPOIs[j].AdditionalContentID);
                if (ac.MediaType == (int)MediaTypes.QUIZ) {
                    quizzes.Add(ac);
                }
                else if (ac.MediaType == (int)MediaTypes.MINIGAME) {
                    minigames.Add(ac);
                }
            }
        }

        List<Reward> rewards = bunLace.Database.DatabaseManager.SelectMultipleEntries<Reward>("RegionID = " + regionID);
        Tour tour = bunLace.Database.DatabaseManager.SelectEntry<Tour>(SaveData.tourData.tourID);

        if (tour.TourType != (int)TourTypes.FREE_EXPLORATION) {
            SaveData.tourData.currentTourPOIArrayIndex = 0;
        }

        for (int i = 0; i < poiRegions.Count; i++) {
            for (int j = SaveData.poiData.Count - 1; j >= 0; j--) {
                if (poiRegions[i].POIID == SaveData.poiData[j].id) {
                    SaveData.poiData.RemoveAt(j);
                    continue;
                }
            }
        }

        for (int k = 0; k < minigames.Count; k++) {
            for (int m = SaveData.minigameData.Count - 1; m >= 0; m--) {
                if (minigames[k].ID == SaveData.minigameData[m]) {
                    SaveData.minigameData.RemoveAt(m);
                    continue;
                }
            }
        }

        for (int k = 0; k < quizzes.Count; k++) {
            for (int n = SaveData.quizData.Count - 1; n >= 0; n--) {
                if (quizzes[k].ID == SaveData.quizData[n].id) {
                    SaveData.quizData.RemoveAt(n);
                    continue;
                }
            }
        }

        for (int o = 0; o < rewards.Count; o++) {
            for (int p = SaveData.redeemedRewardData.Count - 1; p >= 0; p--) {
                if (rewards[o].ID == SaveData.redeemedRewardData[p].id) {
                    SaveData.redeemedRewardData.RemoveAt(p);
                    continue;
                }
            }
        }

        Save();
    }

    void OnApplicationQuit() {
        Save();
        Debug.Log("Application ending after " + Time.time + " seconds");
        Screen.sleepTimeout = SleepTimeout.SystemSetting;
    }

    public bool HasVisited(int poiId) {
        return SaveData.poiData.FirstOrDefault(data => data.id == poiId) != null;
    }

    public void DisplayPOINotUnlockedWarning() {
        string warning = Values.LOCALIZED_TERMS[Values.LOC_KEY_POI_NOT_VISITED];
        EasyMessageBox.Show(warning);
    }

    private void OnApplicationFocus(bool focus) {
        if (!focus) Save();
    }

    //private void OnApplicationPause(bool pause) {
    //    if (pause) Save();
    //}

    private bool loading;

    public void OpenURL(string url) {
        if (loading) return;
        loading = true;
        if (PlayerPrefs.HasKey(Values.DO_NOT_ASK_PLAYER_PREF) && PlayerPrefs.GetInt(Values.DO_NOT_ASK_PLAYER_PREF) == 1) {
            Application.OpenURL(url);
            loading = false;
            return;
        }

        openURLwarning.GetLocalizedStringAsync().Completed += asyncOp => {
            string warning = asyncOp.Result;
            EasyMessageBox.Show(
                message: warning,
                button1Text: Values.LOCALIZED_TERMS[Values.LOC_KEY_CONFIRM],
                button1Action: delegate { Application.OpenURL(url); },
                button2Text: Values.LOCALIZED_TERMS[Values.LOC_KEY_DO_NOT_ASK_AGAIN],
                button2Action: delegate {
                    PlayerPrefs.SetInt(Values.DO_NOT_ASK_PLAYER_PREF, 1);
                    Application.OpenURL(url);
                },
                button3Text: Values.LOCALIZED_TERMS[Values.LOC_KEY_CANCEL]
            );

            loading = false;
        };
    }

    public async void UpdateLocalizedDictionary() {
        if (Values.LOCALIZED_TERMS != null) Values.LOCALIZED_TERMS.Clear();
        else {
            Values.LOCALIZED_TERMS = new Dictionary<string, string>();
        }

        for (int i = 0; i < Values.LOC_KEYS.Length; i++) {
            string l = await LocalizationManager.GetLocalizedUIString(Values.LOC_KEYS[i]);
            Values.LOCALIZED_TERMS.Add(Values.LOC_KEYS[i], l);
        }
    }

    public bool AdditionalContentIsViewable(AdditionalContent ac, int poiID) {
        bool hasVisited = HasVisited(poiID);
        bool inTour = false;
        switch (ac.RequiresPOIUnlocked) {
            case 0: //no tour restriction
                return true;
            case 1: //only guided tour
                inTour = currentTourMode == TourTypes.GUIDED_TOUR;
                break;
            case 2: //only scavenger hunt
                inTour = currentTourMode == TourTypes.SCAVENGER_HUNT;
                break;
            case 3: //guided tour and scavenger hunt
                inTour = currentTourMode == TourTypes.SCAVENGER_HUNT || currentTourMode == TourTypes.GUIDED_TOUR;
                break;
            case 4: //only free exploration
                inTour = currentTourMode == TourTypes.FREE_EXPLORATION;
                break;
            case 5: //guided and free exploration
                inTour = currentTourMode == TourTypes.GUIDED_TOUR || currentTourMode == TourTypes.FREE_EXPLORATION;
                break;
            case 6: //scavenger and free exploration
                inTour = currentTourMode == TourTypes.SCAVENGER_HUNT || currentTourMode == TourTypes.FREE_EXPLORATION;
                break;
            case 7: //all tours
                inTour = true;
                break;
        }

        if (!inTour) return true;
        else {
            return hasVisited;
        }
    }
}
