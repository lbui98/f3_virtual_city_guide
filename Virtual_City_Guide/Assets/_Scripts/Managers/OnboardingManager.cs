using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using bunLace.Singleton;
using TMPro;
using UnityEngine.Localization;
using UnityEngine.Events;

public class OnboardingManager : Singleton<OnboardingManager> {
    [SerializeField] private RectTransform focusPoint;
    [SerializeField] private GameObject screen;

    private int currentTipIndex;

    [System.Serializable]
    public class Tip {
        public Vector2 position;
        public RectTransform target;
        public LocalizedString tipString;
        public UnityEvent simulateClickEvent;
    }

    [SerializeField]
    private Tip[] tips;

    //private void Start() {
    //    StartOnboarding();
    //}

    public void StartOnboarding() {
        screen.SetActive(true);
        currentTipIndex = 0;
        ShowTip(currentTipIndex);
    }

    private void ShowTip(int index) {
        if (index > tips.Length - 1) {
            EndOnboarding();
            return; //outside of range
        }

        if (tips[index].target != null) {
            focusPoint.position = tips[index].target.position;
        }
        else {
            focusPoint.position = tips[index].position;
        }

        if (tips[index].tipString != null) {
            tips[index].tipString.GetLocalizedStringAsync().Completed += asyncOp => {
                string result = asyncOp.Result + string.Format(" ({0}/{1})", currentTipIndex + 1, tips.Length);
                EasyMessageBox.Show(message: result, button1Action: ShowNextTip);
            };
        }
    }

    private IEnumerator ShowNextTipRoutine() {
        tips[currentTipIndex].simulateClickEvent?.Invoke();

        yield return new WaitForSeconds(0.75f);
        currentTipIndex++;
        ShowTip(currentTipIndex);
    }

    private void ShowNextTip() {
        StartCoroutine(ShowNextTipRoutine());
    }

    public void EndOnboarding() {
        screen.SetActive(false);
    }
}
