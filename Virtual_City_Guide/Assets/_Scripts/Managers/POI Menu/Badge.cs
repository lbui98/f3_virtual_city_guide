using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Badge : MonoBehaviour {
    public enum BadgeType { VISITED, QUIZ_CLEARED, QUIZ_CLEARED_PERFECT, MINIGAME_CLEAR}
    public BadgeType badgeType;
}
