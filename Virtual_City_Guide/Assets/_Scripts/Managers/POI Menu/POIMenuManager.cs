using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using bunLace.Tools;
using bunLace.Database;
using bunLace.Localization;
using Cysharp.Threading.Tasks;
using UnityEngine.AddressableAssets;
using UnityEngine.Events;
using System.Linq;
using bunLace.SaveLoad;
using bunLace.Singleton;
using DG.Tweening;
using UnityEngine.Localization;
using bunLace.Constants;

#if UNITY_ANDROID
using UnityEngine.Android;
#elif UNITY_IOS
using UnityEngine.iOS;
#endif

public class POIMenuManager : Singleton<POIMenuManager> {
    [SerializeField] private GameObject poiMenu;
    [SerializeField] private Image headerImg, mediaHeaderImg, imgBookmark, imgStamp;
    [SerializeField] private TextMeshProUGUI mainPoiHeaderText, descriptionText, navigation;
    [SerializeField] private LocalizedString notVisitedWarning, notDownloadedWarning;
    private string notDownloadedWarningString;

    [Header("Media Guide")]
    [SerializeField] private GameObject mediaGuideMenu;
    [SerializeField] private Image headerPointsImg;
    [SerializeField] private TextMeshProUGUI poiHeaderName, collectedPoints, unlockPoints;
    [SerializeField] private TabGroup tabGroup;
    [SerializeField] private Button btnContact, btnWebsite, btnTelephone, btnMediaGuide, btnSelfie, btnSponsor;
    [SerializeField] private GameObject contactInfo, panelReadMore, btnReadMore;
    [SerializeField] private TextMeshProUGUI readMoreText;
    private string currentPoiName;
    private string currentMediaGuideReference;

    [Header("Additional Content")]
    [SerializeField] private GameObject btnAdditionalContent;
    [SerializeField] private Transform additionalContent;
    [SerializeField] private AssetReference additionalContentListElement;

    [Header("AR")]
    [SerializeField] private GameObject btnAR;
    [SerializeField] private Transform arContent;
    [SerializeField] private AssetReference arContentListElement;

    [Header("Quiz")]
    [SerializeField] private GameObject btnQuiz;
    [SerializeField] private Transform quizContent;
    [SerializeField] private AssetReference quizContentListElement;

    [Header("Minigames")]
    [SerializeField] private GameObject btnMinigames;
    [SerializeField] private Transform minigamesContent;
    [SerializeField] private AssetReference minigameContentListElement;

    [Header("Sponsor")]
    [SerializeField] private GameObject currentSponsor;
    [SerializeField] private Transform sponsorContent;
    private string currentSponsorReference;

    [SerializeField] private BadgeHandler badgeHandler;
    private bool bookmarked;

    private bool loading = false;
    private int currentPoiID = -1;

    private int[] pois;
    [SerializeField] private static int currentPoiArrayIndex = -1;
    [SerializeField] private GameObject navigationBar;

    /// <summary>
    /// DEBUG FUNCTION
    /// </summary>
    public void MarkPOIVisited() {
        if (GameManager.debugging && GameManager.SaveData.poiData.FirstOrDefault(data => data.id == GameManager.CurrentPOIID) == null) {
            GameManager.SaveData.poiData.Add(new bunLace.SaveLoad.POIsData { id = GameManager.CurrentPOIID, dateOfVisit = DateTime.Now.ToString() });
        }
    }

    private void Start() {
        //GameManager.CurrentPOIID = -1;
        badgeHandler.Initialize();
        tabGroup.Initialize();

        OnCityChanged();
    }

    private void OnEnable() {
        MainManager.OnMarkerClick += OnMarkerClick;
        MainManager.OnMarkerVisited += OnMarkerVisited;
        QuizMediaManager.OnQuizCleared += CheckBadges;
        MinigameMediaManager.OnMinigameCleared += CheckBadges;
        MainManager.OnCityChanged += OnCityChanged;
    }
    private void OnDisable() {
        MainManager.OnMarkerClick -= OnMarkerClick;
        MainManager.OnMarkerVisited -= OnMarkerVisited;
        QuizMediaManager.OnQuizCleared -= CheckBadges;
        MinigameMediaManager.OnMinigameCleared -= CheckBadges;
        MainManager.OnCityChanged -= OnCityChanged;
    }

    private void OnCityChanged() {
        if (GameManager.CurrentRegionID == -1) return;

        //initialize pois array
        List<POIRegion> poiRegionList = DatabaseManager.SelectMultipleEntries<POIRegion>("RegionID = " + GameManager.CurrentRegionID);
        List<POI> poisList = new List<POI>();
        pois = new int[poiRegionList.Count];
        for (int i = 0; i < poiRegionList.Count; i++) {
            POI poi = DatabaseManager.SelectEntry<POI>(poiRegionList[i].POIID);
            poisList.Add(poi);
        }

        poisList = poisList.OrderBy(o => o.Order).ToList();

        for (int i = 0; i < poisList.Count; i++) {
            pois[i] = poisList[i].ID;
        }
    }

    public void OnBtnReturn(RectTransform targetPanel) {
        if (Utilities.blocked) return;
        AnimatePanel(false, targetPanel.gameObject);
        StartCoroutine(OnBtnReturnRoutine(targetPanel));
    }

    public void ResetPagination() {
        currentPoiArrayIndex = -1;
    }

    public void OnBtnReturnAndPalette(RectTransform targetPanel) {
        if (Utilities.blocked) return;
        OnBtnReturn(targetPanel);
        if (GameManager.Instance.currentRegionPalette != GameManager.CurrentRegionID) {
            Region region = DatabaseManager.SelectEntry<Region>(GameManager.CurrentRegionID);
            GameManager.Instance.UpdatePalette(region);
        }
    }

    private IEnumerator OnBtnReturnRoutine(RectTransform target) {
        yield return new WaitForSeconds(Values.ANIMATION_SLIDE_SPEED);
        target.gameObject.SetActive(false);
    }

    private void ClearTab(Transform content) {
        foreach (Transform child in content) {
            Destroy(child.gameObject);
        }
    }

    private void OnBtnTabClick(int tabTypeToActivate, Sprite img) {
        if (Utilities.blocked) return;
        poiHeaderName.text = string.Format("{0} <size={1}>/ {2}</size>", currentPoiName, bunLace.Constants.Values.FONT_SUB_SIZE, tabGroup.tabsDict[tabTypeToActivate].info.titleKey.GetLocalizedString());
        //mediaHeaderImg.sprite = img;
        tabGroup.OnTabSelect(tabTypeToActivate);
        //mediaGuideMenu.SetActive(true);
        AnimatePanel(true, mediaGuideMenu);
        //AnimatePanel(true, mediaGuideMenu.FindComponentInChildWithTag<RectTransform>(Tags.TARGET));
    }

    public void OpenMenu(int id) {
        OnMarkerClick(id, GameManager.Instance.HasVisited(id), false);
        poiMenu.SetActive(true);
    }

    #region POI Menu
    public void OnBtnBookmark() {
        if (bookmarked) {
            //already bookmarked, remove
            GameManager.SaveData.bookmarkData.Remove(GameManager.CurrentPOIID);
        }
        else {
            //not bookmarked yet, add
            GameManager.SaveData.bookmarkData.Add(GameManager.CurrentPOIID);
        }

        MarkBookmark(!bookmarked);
    }

    public void CheckBadges() {
        CheckBadges(GameManager.CurrentPOIID);
    }

    private void CheckBadges(int poiId) {
        int points = 0;

        POI poi = DatabaseManager.SelectEntry<POI>(poiId);
        if (GameManager.Instance.HasVisited(poiId))
            points += poi.UnlockPoints;

        List<int> quizzes = new List<int>();
        List<int> minigames = new List<int>();
        List<AdditionalContentPOI> additionalContentPOIs = DatabaseManager.SelectMultipleEntries<AdditionalContentPOI>("POIID = " + poiId);

        if (additionalContentPOIs != null) {
            for (int i = 0; i < additionalContentPOIs.Count; i++) {
                AdditionalContent ac = DatabaseManager.SelectEntry<AdditionalContent>(additionalContentPOIs[i].AdditionalContentID);
                MediaTypes mediaType = (MediaTypes)ac.MediaType;

                if (mediaType == MediaTypes.QUIZ) {
                    quizzes.Add(ac.ID);
                }
                else if (mediaType == MediaTypes.MINIGAME) {
                    minigames.Add(ac.ID);
                }
            }
        }

        //check badges
        bool allQuizCleared = quizzes.Count > 0;
        bool allQuizClearedPerfect = quizzes.Count > 0;
        bool allMinigameCleared = minigames.Count > 0;

        for (int i = 0; i < quizzes.Count; i++) {
            var result = GameManager.SaveData.quizData.FirstOrDefault(data => data.id == quizzes[i]);
            if (result == null || !result.cleared) {
                allQuizCleared = false;
                allQuizClearedPerfect = false;
            }
            else {
                if (!result.firstTimeClear) {
                    allQuizClearedPerfect = false;
                    points += bunLace.Constants.Values.QUIZ_CLEAR_POINTS;
                }
                else {
                    points += bunLace.Constants.Values.QUIZ_FIRST_TIME_CLEAR_POINTS;
                }
            }
        }

        for (int i = 0; i < minigames.Count; i++) {
            if (!GameManager.SaveData.minigameData.Contains(minigames[i])) {
                allMinigameCleared = false;
            }
            else {
                points += bunLace.Constants.Values.MINIGAME_CLEAR_POINTS;
            }
        }

        if (!badgeHandler.initialized) badgeHandler.Initialize();

        //activate badges
        if (allQuizClearedPerfect) badgeHandler.ActivateBadge(Badge.BadgeType.QUIZ_CLEARED_PERFECT);
        else if (allQuizCleared) badgeHandler.ActivateBadge(Badge.BadgeType.QUIZ_CLEARED);
        if (allMinigameCleared) badgeHandler.ActivateBadge(Badge.BadgeType.MINIGAME_CLEAR);

        if (points > 0)
            collectedPoints.text = points.ToString();

        //scavenger hunt next poi check
        Tour tour = DatabaseManager.SelectEntry<Tour>(GameManager.SaveData.tourData.tourID);

        if (tour.TourType == (int)TourTypes.SCAVENGER_HUNT && tour.POIIDs[GameManager.SaveData.tourData.currentTourPOIArrayIndex].poiID == poiId && points >= tour.POIIDs[GameManager.SaveData.tourData.currentTourPOIArrayIndex].requiredPoints) {
            //fulfilled requirements
            string message = LocalizationManager.GetLocalizedDBString(tour.POIIDs[GameManager.SaveData.tourData.currentTourPOIArrayIndex].nextDestinationMessage);
            EasyMessageBox.Show(message);

            GameManager.SaveData.tourData.currentTourPOIArrayIndex++;
            POI nextPoi = DatabaseManager.SelectEntry<POI>(tour.POIIDs[GameManager.SaveData.tourData.currentTourPOIArrayIndex].poiID);
            Debug.Log("POI " + nextPoi.ID + " || Type is " + nextPoi.Type);
            if (nextPoi.Type != POITypes.POI) {
                while (nextPoi.Type != POITypes.POI) {
                    Debug.Log("POI iterating...");
                    GameManager.SaveData.tourData.currentTourPOIArrayIndex++;
                    nextPoi = DatabaseManager.SelectEntry<POI>(tour.POIIDs[GameManager.SaveData.tourData.currentTourPOIArrayIndex].poiID);
                }
            }

            //int nextPoiArrayIndex = 0;
            ////check if next destination is a restpoint
            //for (int i = currentPoiArrayIndex + 1; i < tour.POIIDs.Length; i++) {
            //    POI nextPoi = DatabaseManager.SelectEntry<POI>(tour.POIIDs[i].poiID);
            //    if (nextPoi.Type == POITypes.POI) {
            //        nextPoiArrayIndex = i;
            //        Debug.Log("CHECK " + poiId + " | " + nextPoi.ID + " | " + i);
            //        GameManager.SaveData.tourData.currentTourPOIArrayIndex = nextPoiArrayIndex - 1;
            //        break;
            //    }
            //}

            //Main manager delegate to create next marker
            OnScavengerNextPOI?.Invoke(GameManager.SaveData.tourData);
            Debug.Log("Points reached");
        }
    }

    public delegate UniTask ScavengerNextPOI(TourData tourData);
    public static event ScavengerNextPOI OnScavengerNextPOI;

    private void MarkBookmark(bool bookmarked) {
        this.bookmarked = bookmarked;
        Color target = bookmarked ? GameManager.Instance.GetPaletteColor(Palette.ColorGroups.SELECTED) : GameManager.Instance.GetPaletteColor(Palette.ColorGroups.UNSELECTED);
        imgBookmark.color = target;
    }

    private void OnMarkerVisited(int id) {
        if (poiMenu.activeSelf) {
            FillInformation(id, true, true);
            AnimateStamp();
        }
    }

    private async void OnMarkerClick(int id, bool visited, bool notify) {
        if (loading || poiMenu.activeSelf) return;
        await FillInformation(id, visited, notify);
    }

    private void AnimateStamp() {
        Debug.Log("Animation");
        imgStamp.gameObject.SetActive(true);
        Animator animator = imgStamp.GetComponent<Animator>();
        animator.SetTrigger(bunLace.Constants.Values.STAMP_ANIMATION_TRIGGER);
    }

    private async UniTask FillInformation(int id, bool visited, bool notify) {
        if (Utilities.blocked) return;


        POI poi = DatabaseManager.SelectEntry<POI>(id);

        List<POIRegion> poiRegionList = DatabaseManager.SelectMultipleEntries<POIRegion>("POIID = " + poi.ID);
        bool sameRegion = false;
        for (int i = 0; i < poiRegionList.Count; i++) {
            if (poiRegionList[i].RegionID == GameManager.CurrentRegionID) {
                sameRegion = true;
                break;
            }
        }


        if (!sameRegion) {
            //temporarily update palette
            Region region = DatabaseManager.SelectEntry<Region>(poiRegionList[0].RegionID);
            GameManager.Instance.UpdatePalette(region);
            navigationBar.SetActive(false);
        }
        else {
            if (currentPoiArrayIndex == -1) {
                //find out the current poi index
                if (pois == null) {
                    //initialize pois array
                    List<POI> poisList = new List<POI>();
                    for (int i = 0; i < poiRegionList.Count; i++) {
                        POI p = DatabaseManager.SelectEntry<POI>(poiRegionList[i].POIID);
                        poisList.Add(p);
                    }

                    poisList = poisList.OrderBy(o => o.Order).ToList();

                    pois = new int[poisList.Count];
                    for (int i = 0; i < poisList.Count; i++) {
                        pois[i] = poisList[i].ID;
                    }
                }

                currentPoiArrayIndex = Array.IndexOf(pois, id);
                navigation.text = $"{currentPoiArrayIndex + 1} / {pois.Length}";
                Debug.Log("Navigation set: " + navigation.text);
            }
            navigationBar.SetActive(true);
        }


        //do not fill information when it's already there
        loading = true;
        collectedPoints.text = "--";

        if (currentPoiID != id) {
            ClearTab(additionalContent);
            ClearTab(arContent);
            ClearTab(quizContent);
            ClearTab(minigamesContent);
            btnAdditionalContent.SetActive(false);
            btnAR.SetActive(false);
            btnQuiz.SetActive(false);
            btnMinigames.SetActive(false);
            btnSelfie.gameObject.SetActive(false);
            badgeHandler.DeactivateAll();


            string description = LocalizationManager.GetLocalizedDBString(poi.DescriptionLocKey);
            currentPoiName = LocalizationManager.GetLocalizedDBString(poi.NameLocKey);

            mainPoiHeaderText.text = currentPoiName;
            descriptionText.text = description;
            RectTransform scrollContent = descriptionText.transform.parent.GetComponent<RectTransform>();
            scrollContent.anchoredPosition = new Vector2(scrollContent.anchoredPosition.x, 0);
            ScrollRect scrollRect = descriptionText.GetComponentInParent<ScrollRect>(true);
            scrollRect.onValueChanged?.Invoke(scrollRect.content.anchoredPosition);


            poiHeaderName.text = currentPoiName;


            //check if adding media guide
            //if (!string.IsNullOrEmpty(poi.MediaGuideHtmlReference)) {
            //    btnMediaGuide.onClick.RemoveAllListeners();
            //    btnMediaGuide.onClick.AddListener(delegate { GameManager.Instance.OpenURL(poi.MediaGuideHtmlReference); });
            //    btnMediaGuide.gameObject.SetActive(true);
            //}
            //else {
            //    btnMediaGuide.gameObject.SetActive(false);
            //}

            if (!string.IsNullOrEmpty(poi.SponsorReference)) {
                btnSponsor.onClick.RemoveAllListeners();
                btnSponsor.onClick.AddListener(delegate { ShowSponsor(poi.SponsorReference); });
                btnSponsor.gameObject.SetActive(true);
            }
            else {
                btnSponsor.gameObject.SetActive(false);
            }

            //check if adding telephone btn
            if (!string.IsNullOrEmpty(poi.Telephone)) {
                btnTelephone.onClick.RemoveAllListeners();
                btnTelephone.onClick.AddListener(delegate { Application.OpenURL("tel:" + poi.Telephone); });
                btnTelephone.gameObject.SetActive(true);
            }
            else {
                btnTelephone.gameObject.SetActive(false);
            }

            //check if adding contact info
            if (poi.ContactInfoLocKey > 0) {
                btnContact.gameObject.SetActive(true);
                var info = contactInfo.FindComponentInChildWithTag<TextMeshProUGUI>(bunLace.Constants.Tags.TEXT_REPLACE);
                info.text = LocalizationManager.GetLocalizedDBString(poi.ContactInfoLocKey);
            }
            else {
                btnContact.gameObject.SetActive(false);
            }

            //check if adding website
            if (!string.IsNullOrEmpty(poi.WebsiteURL)) {
                btnWebsite.gameObject.SetActive(true);
                btnWebsite.onClick.RemoveAllListeners();
                btnWebsite.onClick.AddListener(delegate { GameManager.Instance.OpenURL(poi.WebsiteURL); });
            }
            else {
                btnWebsite.gameObject.SetActive(false);
            }


            if (poi.MoreDescriptionLocKey > 0) {
                btnReadMore.SetActive(true);
                readMoreText.text = LocalizationManager.GetLocalizedDBString(poi.MoreDescriptionLocKey);
            }
            else {
                btnReadMore.SetActive(false);
            }

            Sprite poiImage = await AddressablesManager.Instance.LoadSprite(poi.AtlasReference, poi.ImgReference);
            headerImg.sprite = poiImage;
            mediaHeaderImg.sprite = poiImage;

            Sprite stamp = await AddressablesManager.Instance.LoadSprite(poi.StampAtlasReference, poi.StampReference);
            Color stampColor;
            ColorUtility.TryParseHtmlString(poi.StampColor, out stampColor);
            imgStamp.sprite = stamp;
            imgStamp.color = stampColor;



            var ratioFitter = headerImg.GetComponent<AspectRatioFitter>();
            ratioFitter.aspectMode = AspectRatioFitter.AspectMode.EnvelopeParent;
            ratioFitter.aspectRatio = (float)poiImage.rect.width / (float)poiImage.rect.height;

            ratioFitter = mediaHeaderImg.GetComponent<AspectRatioFitter>();
            ratioFitter.aspectMode = AspectRatioFitter.AspectMode.EnvelopeParent;
            ratioFitter.aspectRatio = (float)poiImage.rect.width / (float)poiImage.rect.height;

            List<AdditionalContentPOI> additionalContentPOIs = DatabaseManager.SelectMultipleEntries<AdditionalContentPOI>("POIID = " + id);
            List<AdditionalContent> acList = new List<AdditionalContent>();
            if (additionalContentPOIs != null) {
                for (int i = 0; i < additionalContentPOIs.Count; i++) {
                    AdditionalContent ac = DatabaseManager.SelectEntry<AdditionalContent>(additionalContentPOIs[i].AdditionalContentID);
                    acList.Add(ac);
                }
                if (acList.Count > 0)
                    await FillAdditionalContent(acList);
            }
        }
        if (poi.Type != POITypes.POI) {
            headerPointsImg.gameObject.SetActive(false);
        }
        else {
            Color pointsColor = visited ? GameManager.Instance.GetPaletteColor(Palette.ColorGroups.FOREGROUND) : GameManager.Instance.GetPaletteColor(Palette.ColorGroups.UNSELECTED);
            headerPointsImg.color = pointsColor;
            unlockPoints.text = poi.UnlockPoints.ToString();
            Debug.Log("BUNNY: POI Click, visited is " + visited);
            headerPointsImg.gameObject.SetActive(true);
        }

        CheckBadges(id);
        contactInfo.SetActive(false);
        panelReadMore.SetActive(false);
        MarkBookmark(GameManager.SaveData.bookmarkData.Contains(id));
        if (!poiMenu.activeSelf)
            //AnimatePanel(true, poiMenu.FindComponentInChildWithTag<RectTransform>(Tags.TARGET));
            AnimatePanel(true, poiMenu);

        //poiMenu.SetActive(true);

        Debug.Log("stamp " + notify);
        if (visited && !notify)
            imgStamp.gameObject.SetActive(true);
        else if (notify) AnimateStamp();
        else imgStamp.gameObject.SetActive(false);

        GameManager.CurrentPOIID = id;
        currentPoiID = id;
        loading = false;
    }

    private async UniTask FillAdditionalContent(List<AdditionalContent> additionalContents) {
        for (int i = 0; i < additionalContents.Count; i++) {
            AdditionalContent content = additionalContents[i];
            AssetReference targetReference = null;
            Transform targetContent = null;
            UnityAction delegateAction = null;
            bool done = false;

            MediaTypes mediaType = (MediaTypes)additionalContents[i].MediaType;

            if (mediaType == MediaTypes.SINGLE_IMAGE || mediaType == MediaTypes.AUDIO_PLAYER || mediaType == MediaTypes.VIDEO_PLAYER || mediaType == MediaTypes.CUSTOM_AUDIO_VISUAL) {
                btnAdditionalContent.SetActive(true);
                targetReference = additionalContentListElement;
                targetContent = additionalContent;
                delegateAction = delegate { LoadAdditionalContentScene(content, mediaType); };
            }
            else if (mediaType == MediaTypes.AR_PLACEMENT || mediaType == MediaTypes.AR_SCAN || mediaType == MediaTypes.AR_PLACEMENT_MULTI) {
                btnAR.SetActive(true);
                targetReference = arContentListElement;
                targetContent = arContent;
                delegateAction = delegate { LoadARContentScene(content, mediaType); };
            }
            else if (mediaType == MediaTypes.QUIZ) {
                btnQuiz.SetActive(true);
                targetReference = quizContentListElement;
                targetContent = quizContent;
                delegateAction = delegate { LoadQuizContentScene(content, mediaType); };
                done = GameManager.SaveData.quizData.FirstOrDefault(data => data.id == content.ID && data.cleared) != null;
            }
            else if (mediaType == MediaTypes.MINIGAME) {
                btnMinigames.SetActive(true);
                targetReference = minigameContentListElement;
                targetContent = minigamesContent;
                delegateAction = delegate { LoadMinigameScene(content, mediaType); };
                done = GameManager.SaveData.minigameData.Contains(content.ID);
            }
            else if (mediaType == MediaTypes.SELFIE) {
                btnSelfie.gameObject.SetActive(true);
                btnSelfie.onClick.AddListener(delegate { LoadSelfieScene(content, mediaType); });
                continue;
            }

            Button btn = await GenerateAdditionalContent(targetReference, targetContent);
            btn.onClick.AddListener(delegateAction);

            Image img = btn.gameObject.FindComponentInChildWithTag<Image>(bunLace.Constants.Tags.IMG_REPLACE);
            if (string.IsNullOrEmpty(additionalContents[i].AtlasReference)) {
                img.sprite = await AddressablesManager.Instance.LoadSprite(additionalContents[i].PreviewImgReference);
            }
            else {
                img.sprite = await AddressablesManager.Instance.LoadSprite(additionalContents[i].AtlasReference, additionalContents[i].PreviewImgReference);
            }

            var ratioFitter = img.GetComponent<AspectRatioFitter>();
            if (ratioFitter != null) {
                ratioFitter.aspectMode = AspectRatioFitter.AspectMode.EnvelopeParent;
                ratioFitter.aspectRatio = (float)img.sprite.rect.width / (float)img.sprite.rect.height;
            }

            TextMeshProUGUI title = btn.gameObject.FindComponentInChildWithTag<TextMeshProUGUI>(bunLace.Constants.Tags.NAME_REPLACE);
            if (title != null) title.text = LocalizationManager.GetLocalizedDBString(additionalContents[i].NameLocKey);

            TextMeshProUGUI description = btn.gameObject.FindComponentInChildWithTag<TextMeshProUGUI>(bunLace.Constants.Tags.TEXT_REPLACE);
            if (description != null) description.text = LocalizationManager.GetLocalizedDBString(additionalContents[i].DescriptionLocKey);

            Image checkMark = btn.gameObject.FindComponentInChildWithTag<Image>(bunLace.Constants.Tags.TARGET);
            if (checkMark != null) checkMark.gameObject.SetActive(done);
        }
    }

    public void OnBtnNavigation(int step) {
        int previousIndex = currentPoiArrayIndex;

        currentPoiArrayIndex += step;
        if (currentPoiArrayIndex >= pois.Length) {
            currentPoiArrayIndex = 0;
        }
        else if (currentPoiArrayIndex < 0) {
            currentPoiArrayIndex = pois.Length - 1;
        }

        if (GameManager.currentTourMode == TourTypes.SCAVENGER_HUNT) {
            if (currentPoiArrayIndex > GameManager.SaveData.tourData.currentTourPOIArrayIndex) {
                GameManager.Instance.DisplayPOINotUnlockedWarning();
                currentPoiArrayIndex = previousIndex; //reset
                return;
            }
        }

        SoundManager.PlaySFX(Values.AUDIO_PAGE_FLIP_KEY);

        bool visited = GameManager.Instance.HasVisited(pois[currentPoiArrayIndex]);
        navigation.text = $"{currentPoiArrayIndex + 1} / {pois.Length}";
        FillInformation(pois[currentPoiArrayIndex], visited, false);
    }

    #endregion

    #region Additional Content
    public void OnBtnAdditionalContentTab() {
        OnBtnAdditionalContentTabInternal();
    }

    private async void OnBtnAdditionalContentTabInternal() {
        bool valid = await AddressablesManager.ValidateKey(GameManager.CurrentRegionID.ToString());
        if (valid)
            OnBtnTabClick((int)TabTypes.ADDITIONAL_CONTENT, btnAdditionalContent.FindComponentInChildWithTag<Image>(Tags.TARGET).sprite);
        else {
            notDownloadedWarning.GetLocalizedStringAsync().Completed += asnycOp => {
                string warning = asnycOp.Result;
                EasyMessageBox.Show(warning);
            };
        }
    }

    private async UniTask<Button> GenerateAdditionalContent(AssetReference element, Transform content) {
        GameObject go = await Addressables.InstantiateAsync(element, content);
        return go.GetComponent<Button>();
    }

    private void LoadAdditionalContentScene(AdditionalContent content, MediaTypes type) {
        if (!GameManager.Instance.AdditionalContentIsViewable(content, GameManager.CurrentPOIID)) {
            GameManager.Instance.DisplayPOINotUnlockedWarning();
            return;
        }

        if (loading) return;
        loading = true;
        LoadAdditionalContentSceneInternal(content, type);
    }

    private async UniTask LoadAdditionalContentSceneInternal(AdditionalContent content, MediaTypes type) {
        //load addititve
        await UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(bunLace.Constants.Scenes.MEDIA_VIEWER, UnityEngine.SceneManagement.LoadSceneMode.Additive);
        MediaManager.Instance.LoadContent(content, type);
        loading = false;
    }
    #endregion

    #region AR
    public void OnBtnARTab() {
        OnBtnARTabInternal();
    }

    private async void OnBtnARTabInternal() {
        bool valid = await AddressablesManager.ValidateKey(GameManager.CurrentRegionID.ToString());
        if (valid) {
            bool visited = GameManager.Instance.HasVisited(GameManager.CurrentPOIID);
            if (!visited) {
                GameManager.Instance.DisplayPOINotUnlockedWarning();
                return;
            }

            OnBtnTabClick((int)TabTypes.AR, btnAR.FindComponentInChildWithTag<Image>(Tags.TARGET).sprite);
        }
        else {
            string warning = await notDownloadedWarning.GetLocalizedStringAsync();
            EasyMessageBox.Show(warning);
        }
    }

    private void LoadARContentScene(AdditionalContent content, MediaTypes type) {
        if (!GameManager.Instance.AdditionalContentIsViewable(content, GameManager.CurrentPOIID)) {
            GameManager.Instance.DisplayPOINotUnlockedWarning();
            return;
        }

        if (loading) return;
        loading = true;
        LoadARContentSceneInternal(content, type);
    }

    private async UniTask LoadARContentSceneInternal(AdditionalContent content, MediaTypes type) {
#if UNITY_ANDROID
        if (!Permission.HasUserAuthorizedPermission(Permission.Camera)) {
            loading = false;
            //display message
            string warning = Values.LOCALIZED_TERMS[Values.LOC_KEY_NO_CAMERA];
            EasyMessageBox.Show(warning);
        }
        else {
            await UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(bunLace.Constants.Scenes.ARVIEWER, UnityEngine.SceneManagement.LoadSceneMode.Single);
            ARMediaManager.Instance.LoadContent(content, type);
            loading = false;
        }
#elif UNITY_IOS
        if (Application.HasUserAuthorization(UserAuthorization.WebCam)) {
            await UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(bunLace.Constants.Scenes.ARVIEWER, UnityEngine.SceneManagement.LoadSceneMode.Single);
            ARMediaManager.Instance.LoadContent(content, type);
            loading = false;
        }
        else {
            loading = false;
            //display message
            string warning = await LocalizationManager.GetLocalizedUIString(Values.LOC_KEY_NO_CAMERA);
            EasyMessageBox.Show(warning);
        }
#endif


    }
    #endregion

    #region Quiz
    public void OnBtnQuizTab() {
        if (quizContent.childCount == 1) {
            if (GameManager.currentTourMode == TourTypes.FREE_EXPLORATION) {
                quizContent.GetComponentInChildren<Button>().onClick.Invoke();
            }
            else {
                //if (!GameManager.Instance.AdditionalContentIsViewable(content, GameManager.CurrentPOIID)) {
                //    GameManager.Instance.DisplayPOINotUnlockedWarning();
                //    return;
                //}
                quizContent.GetComponentInChildren<Button>().onClick.Invoke();
            }
        }
        else {
            OnBtnTabClick((int)TabTypes.QUIZ, btnQuiz.FindComponentInChildWithTag<Image>(Tags.TARGET).sprite);
        }
    }

    private void LoadQuizContentScene(AdditionalContent content, MediaTypes type) {
        if (!GameManager.Instance.AdditionalContentIsViewable(content, GameManager.CurrentPOIID)) {
            GameManager.Instance.DisplayPOINotUnlockedWarning();
            return;
        }

        loading = true;
        LoadQuizContentSceneInternal(content, type);
    }
    private async UniTask LoadQuizContentSceneInternal(AdditionalContent content, MediaTypes type) {
        await UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(bunLace.Constants.Scenes.QUIZ_VIEWER, UnityEngine.SceneManagement.LoadSceneMode.Additive);
        QuizMediaManager.Instance.LoadContent(content, type);
        loading = false;
    }
    #endregion

    #region Minigames
    public void OnBtnMinigamesTab() {
        OnBtnMinigamesTabInternal();
    }

    private async void OnBtnMinigamesTabInternal() {
        bool valid = await AddressablesManager.ValidateKey(GameManager.CurrentRegionID.ToString());
        if (valid) {
            //bool visited = GameManager.Instance.HasVisited(GameManager.CurrentPOIID);
            //if (!visited) {
            //    GameManager.Instance.DisplayPOINotUnlockedWarning();
            //    return;
            //}

            if (minigamesContent.childCount == 1) {
                minigamesContent.GetComponentInChildren<Button>().onClick.Invoke();
            }
            else {
                OnBtnTabClick((int)TabTypes.MINIGAMES, btnMinigames.FindComponentInChildWithTag<Image>(Tags.TARGET).sprite);
            }
        }
        else
            notDownloadedWarning.GetLocalizedStringAsync().Completed += asnycOp => {
                string warning = asnycOp.Result;
                EasyMessageBox.Show(warning);
            };
    }

    private void LoadMinigameScene(AdditionalContent content, MediaTypes type) {
        if (!GameManager.Instance.AdditionalContentIsViewable(content, GameManager.CurrentPOIID)) {
            GameManager.Instance.DisplayPOINotUnlockedWarning();
            return;
        }

        if (loading) return;
        loading = true;
        LoadMinigameSceneInternal(content, type);
    }

    private async UniTask LoadMinigameSceneInternal(AdditionalContent content, MediaTypes type) {
        await UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(bunLace.Constants.Scenes.MINIGAME_VIEWER, UnityEngine.SceneManagement.LoadSceneMode.Single);
        MinigameMediaManager.Instance.LoadContent(content, type);
        loading = false;
    }
    #endregion

    #region Selfie

    private void LoadSelfieScene(AdditionalContent content, MediaTypes type) {
        if (!GameManager.Instance.AdditionalContentIsViewable(content, GameManager.CurrentPOIID)) {
            GameManager.Instance.DisplayPOINotUnlockedWarning();
            return;
        }

        if (loading) return;
        loading = true;
        LoadSelfieSceneInternal(content, MediaTypes.SELFIE);
    }


    private async UniTask LoadSelfieSceneInternal(AdditionalContent content, MediaTypes type) {
#if UNITY_ANDROID
        if (!Permission.HasUserAuthorizedPermission(Permission.Camera)) {
            string warning = Values.LOCALIZED_TERMS[Values.LOC_KEY_NO_CAMERA];
            EasyMessageBox.Show(warning);
            loading = false;
        }
        else {
            await UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(bunLace.Constants.Scenes.SELFIE_VIEWER, UnityEngine.SceneManagement.LoadSceneMode.Single);
            SelfieMediaManager.Instance.LoadContent(content, type);
            loading = false;
        }
#elif UNITY_IOS
        if (Application.HasUserAuthorization(UserAuthorization.WebCam)) {
            await UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(bunLace.Constants.Scenes.SELFIE_VIEWER, UnityEngine.SceneManagement.LoadSceneMode.Single);
            SelfieMediaManager.Instance.LoadContent(content, type);
            loading = false;
        }
        else {
            string warning = await LocalizationManager.GetLocalizedUIString(Values.LOC_KEY_NO_CAMERA);
            EasyMessageBox.Show(warning);
            loading = false;
        }
#endif


    }

    #endregion

    #region Animation
    private void AnimatePanel(bool show, RectTransform target) {
        //Direction direction = show ? Direction.RIGHT : Direction.LEFT;
        //Utilities.MovePanel(direction, target);
        //Utilities.FadePanel(target.gameObject, show);
        //CanvasFader.Screengrab();
    }

    private void AnimatePanel(bool show, GameObject target) {
        SoundManager.PlaySFX(Values.AUDIO_PAGE_FLIP_KEY);
        CanvasFader.Screengrab(target, show);
    }
    #endregion

    #region Sponsor
    private void ShowSponsor(string sponsorReference) {
        if (currentSponsor != null) {
            //a sponsor has been spawned already
            //check if it's identical
            if (currentSponsorReference == sponsorReference) {
                //reactivate
                currentSponsor.gameObject.SetActive(true);
            }
            else {
                //destroy current sponsor
                Destroy(currentSponsor);
                //replace
                Addressables.InstantiateAsync(sponsorReference, sponsorContent).Completed += asyncOp => {
                    currentSponsor = asyncOp.Result;
                    currentSponsorReference = sponsorReference;
                };
            }
        }
        else {
            Addressables.InstantiateAsync(sponsorReference, sponsorContent).Completed += asyncOp => {
                currentSponsor = asyncOp.Result;
                currentSponsorReference = sponsorReference;
            };
        }

    }
    #endregion
}
