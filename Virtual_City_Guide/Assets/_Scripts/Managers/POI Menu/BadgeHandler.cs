using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BadgeHandler : MonoBehaviour {
    public Dictionary<Badge.BadgeType, GameObject> badgeDict = new Dictionary<Badge.BadgeType, GameObject>();
    [HideInInspector]
    public bool initialized = false;
    private void Awake() {
        Initialize();
    }

    public void Initialize() {
        if (initialized) return;
        if (badgeDict.Count > 0) return;

        Badge[] badges = GetComponentsInChildren<Badge>(true);
        foreach (Badge badge in badges) {
            badgeDict.Add(badge.badgeType, badge.gameObject);
            badge.gameObject.SetActive(false);
            Destroy(badge);
        }
        initialized = true;
    }

    public void ActivateBadge(Badge.BadgeType badgeType) {
        badgeDict[badgeType].gameObject.SetActive(true);
    }

    public void DeactivateAll() {
        foreach (GameObject badge in badgeDict.Values) {
            badge.SetActive(false);
        }
    }
}
