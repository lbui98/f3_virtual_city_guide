using System.Collections.Generic;
using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;
using bunLace.Singleton;
using UnityEngine.AddressableAssets;
using System.Linq;
using System;
using System.Collections;
using Cysharp.Threading.Tasks;
using UnityEngine.Video;
using UnityEngine.Localization.Settings;
using UnityEngine.ResourceManagement.ResourceLocations;
using UnityEngine.U2D;
using UnityEngine.Localization;
using bunLace.Localization;
using bunLace.Constants;

public class AddressablesManager : PersistentSingleton<AddressablesManager> {
    public GameObject currentLoadedAddressable;

    private Dictionary<string, Sprite> LoadedSpritesDict = new Dictionary<string, Sprite>();
    private Dictionary<string, VideoClip> LoadedVideoDict = new Dictionary<string, VideoClip>();
    private Dictionary<string, SpriteAtlas> LoadedSpriteAtlasDict = new Dictionary<string, SpriteAtlas>();
    [SerializeField] private LocalizedString downloadError;

    string[] currentDependencies;

    public static async UniTask<bool> DownloadDependencies(string[] dependencies) {
        Instance.currentDependencies = dependencies;

        await LocalizationSettings.InitializationOperation;

        var op = await Addressables.InitializeAsync();

        //GameManager.Instance.DatabaseManager.databaseFile = await LoadDatabase();
        //float currentProgress = 0f;

        bool isValid = await Instance.ValidateKey(dependencies);

        if (!isValid) {
            //key is invalid
            OnDownloadProgress?.Invoke(1f);
            return false;
        }

        var downloadSize = await Addressables.GetDownloadSizeAsync(dependencies.AsEnumerable());
        if (downloadSize <= 0f) {
            OnDownloadProgress?.Invoke(1f);
            //DebugMe.Log("DOWNLOAD: No updates");
            return false;
        }

        var preload = Addressables.DownloadDependenciesAsync(dependencies.AsEnumerable(), Addressables.MergeMode.Union);
        int downloadCheck = 0;
        float previousProgress = 0f;
        while (!preload.IsDone && downloadCheck < 10f) {
            float progress = preload.GetDownloadStatus().Percent;
            OnDownloadProgress?.Invoke(progress);

            if (previousProgress == progress) {
                if (InternetReachabilityVerifier.Instance.status != InternetReachabilityVerifier.Status.NetVerified) {
                    downloadCheck++;
                    Debug.Log("DOWNLOAD: No download progress - current attempt: " + downloadCheck);
                    await UniTask.Delay(1000);
                }
            }
            else {
                previousProgress = progress;
            }

            await UniTask.DelayFrame(1);
        }

        if (downloadCheck >= 10) {
            Debug.Log("DOWNLOAD: Forcefully aborted/completed");
            string retry = Values.LOCALIZED_TERMS[Values.LOC_KEY_RETRY];
            string cancel = Values.LOCALIZED_TERMS[Values.LOC_KEY_CONTINUE];
            string error = await Instance.downloadError.GetLocalizedStringAsync();
            EasyMessageBox.Show(error,
                button1Text: retry,
                button1Action: () => DownloadDependencies(Instance.currentDependencies),
                button2Text: cancel,
                button2Action: () => Instance.ContinueWithoutDownload());
            return false;
        }
        else {
            OnDownloadProgress?.Invoke(1f);
            return true;
        }

    }

    public delegate void DownloadProgress(float progress);
    public static event DownloadProgress OnDownloadProgress;

    private void ContinueWithoutDownload() {
        OnDownloadProgress?.Invoke(1f);
    }

    public async UniTask<Sprite> LoadSprite(string key) {
        bool isValid = await ValidateKey(key);
        if (!isValid) {
            //load placeholder img instead
            Sprite placeholder = await LoadSprite(Values.PLACEHOLDER_IMG_KEY);
            return placeholder;
        }

        if (!LoadedSpritesDict.ContainsKey(key)) {
            var op = Addressables.LoadAssetAsync<Sprite>(key);
            await op;
            LoadedSpritesDict.Add(key, op.Result);
            //Addressables.Release(op);
        }

        return LoadedSpritesDict[key];
    }

    public async UniTask<Sprite> LoadSprite(string atlasKey, string imgKey) {
        bool isValid = false;
        if (!string.IsNullOrEmpty(atlasKey)) {
            isValid = await ValidateKey(atlasKey);
        }

        if (!isValid) {
            //load placeholder img instead
            Sprite placeholder = await LoadSprite(Values.PLACEHOLDER_IMG_KEY);
            return placeholder;
        }

        //if (!LoadedSpriteAtlasDict.ContainsKey(atlasKey)) {
        //    var op = Addressables.LoadAssetAsync<SpriteAtlas>(atlasKey);
        //    await op;
        //    LoadedSpriteAtlasDict.Add(atlasKey, op.Result);
        //    //Addressables.Release(op);
        //}


        var op = await Addressables.LoadAssetAsync<SpriteAtlas>(atlasKey);

        SpriteAtlas atlas = op;
        //SpriteAtlas atlas = LoadedSpriteAtlasDict[atlasKey];
        Sprite s = atlas.GetSprite(imgKey);
        if (s == null) {
            Debug.LogError("ImgKey " + imgKey + " is not included in Atlas" + atlasKey + "! Check the atas again!");

            Sprite placeholder = await LoadSprite(Values.PLACEHOLDER_IMG_KEY);
            return placeholder;
        }
        return atlas.GetSprite(imgKey);
    }

    public async UniTask<VideoClip> LoadVideo(string key) {
        if (!LoadedVideoDict.ContainsKey(key)) {
            var op = Addressables.LoadAssetAsync<VideoClip>(key);
            await op;
            LoadedVideoDict.Add(key, op.Result);
            //Addressables.Release(op);
        }

        return LoadedVideoDict[key];
    }

    public void ReleaseAddressable() {
        Debug.Log("BUNNY RELEASING ADDRESSABLE");
        if (currentLoadedAddressable != null) {
            Debug.Log("BUNNY RELEASING ADDRESSABLE " + currentLoadedAddressable.name);
            Addressables.Release(currentLoadedAddressable);
        }
        else {
            Debug.Log("BUNNY BUT THERE WAS NO ADDRESSABLE....");
        }

        currentLoadedAddressable = null;
    }

    public static async UniTask<bool> ValidateKey(string key) {
        AsyncOperationHandle<IList<IResourceLocation>> handle = Addressables.LoadResourceLocationsAsync(key);
        await handle;
        bool isValid = handle.Result.Count > 0;
        Addressables.Release(handle);

        if (!isValid)
            return false;

        //check if it needs to be downloaded/is in cache
        float downloadSize = await Addressables.GetDownloadSizeAsync(key);
        if (downloadSize == 0) {
            //is in cache
            return true;
        }

        if (InternetReachabilityVerifier.Instance.status != InternetReachabilityVerifier.Status.NetVerified) {
            //no download possible
            return false;
        }

        return isValid;
    }

    private async UniTask<bool> ValidateKey(string[] keys) {
        AsyncOperationHandle<IList<IResourceLocation>> handle = Addressables.LoadResourceLocationsAsync(keys.AsEnumerable(), Addressables.MergeMode.Union);
        await handle;
        bool isValid = handle.Result.Count > 0;
        Addressables.Release(handle);

        if (!isValid)
            return false;

        //check if it needs to be downloaded/is in cache
        for (int i = 0; i < keys.Length; i++) {
            float downloadSize = await Addressables.GetDownloadSizeAsync(keys[i]);
            if (downloadSize == 0) {
                //is in cache
                continue;
            }

            if (InternetReachabilityVerifier.Instance.status != InternetReachabilityVerifier.Status.NetVerified) {
                //no download possible
                return false;
            }
        }

        return isValid;
    }
    static readonly string[] SizeSuffixes = { "bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };

    public static string SizeSuffix(long value, int decimalPlaces = 1) {
        if (decimalPlaces < 0) { throw new ArgumentOutOfRangeException("decimalPlaces"); }
        if (value < 0) { return "-" + SizeSuffix(-value, decimalPlaces); }
        if (value == 0) { return string.Format("{0:n" + decimalPlaces + "} bytes", 0); }

        // mag is 0 for bytes, 1 for KB, 2, for MB, etc.
        int mag = (int)Math.Log(value, 1024);

        // 1L << (mag * 10) == 2 ^ (10 * mag) 
        // [i.e. the number of bytes in the unit corresponding to mag]
        decimal adjustedSize = (decimal)value / (1L << (mag * 10));

        // make adjustment when the value is large enough that
        // it would round up to 1000 or more
        if (Math.Round(adjustedSize, decimalPlaces) >= 1000) {
            mag += 1;
            adjustedSize /= 1024;
        }

        return string.Format("{0:n" + decimalPlaces + "} {1}",
            adjustedSize,
            SizeSuffixes[mag]);
    }

}
