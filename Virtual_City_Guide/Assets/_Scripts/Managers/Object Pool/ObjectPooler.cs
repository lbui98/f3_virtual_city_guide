using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using bunLace.Singleton;

public class ObjectPooler : Singleton<ObjectPooler> {

    [System.Serializable]
    public class Pool {
        public string tag;
        public GameObject prefab;
        public int size;
    }

    public List<Pool> pools;
    public Dictionary<string, Queue<GameObject>> poolDictionary = new Dictionary<string, Queue<GameObject>>();

    public delegate void ObjectsSpawned();
    public static event ObjectsSpawned OnObjectsSpawned;

    // Use this for initialization
    void Start() {
        foreach (Pool pool in pools) {
            Queue<GameObject> objectPool = new Queue<GameObject>();
            for (int i = 0; i < pool.size; i++) {
                GameObject obj = Instantiate(pool.prefab);
                obj.SetActive(false);
                obj.transform.SetParent(this.transform);
                objectPool.Enqueue(obj);
            }

            poolDictionary.Add(pool.tag, objectPool);
        }

        OnObjectsSpawned?.Invoke();
    }

    public GameObject SpawnFromPool(string tag, Vector3 position, Quaternion rotation) {

        if (!poolDictionary.ContainsKey(tag)) {
            Debug.Log("Dict does not contain tag: " + tag);
            return null;
        }

        GameObject objToSpawn = poolDictionary[tag].Dequeue();

        objToSpawn.SetActive(true);
        objToSpawn.transform.position = position;
        objToSpawn.transform.rotation = rotation;


        IPooledObject pooledObject = objToSpawn.GetComponent<IPooledObject>();
        if (pooledObject != null) {
            pooledObject.OnObjectSpawn();
        }

        poolDictionary[tag].Enqueue(objToSpawn);
        return objToSpawn;
    }
}