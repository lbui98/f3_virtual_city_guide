using bunLace.Database;
using bunLace.SaveLoad;
using Cysharp.Threading.Tasks;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.Localization;
using UnityEngine.UI;
using bunLace.Singleton;
using bunLace.Localization;
using bunLace.Constants;
using bunLace.Tools;

public class TrophiesAndOverviewManager : Singleton<TrophiesAndOverviewManager> {
    [Header("Stamp overview")]
    [SerializeField] private AssetReference overviewListElement;
    [SerializeField] private Transform overviewContent;
    [SerializeField] private LocalizedString overviewText;
    private List<GameObject> overviewList = new List<GameObject>();

    [Header("Achievements")]
    [SerializeField] private ScrollRect scrollView;
    [SerializeField] private AssetReference achievementListElement;
    [SerializeField] private Transform achievementsContent, allAchievementsTab, allCollectedAchievementsContent, allToCollectAchievementsContent;
    [SerializeField] private TextMeshProUGUI points, achievementTitle, achievementDescription, achievementToggleText, redeemNoticeText;
    [SerializeField] private LocalizedString localizedPointsRef, regionalAchievements, localizedRedeemNotice, localizedRedeemSuccess, localizedRedeemCodeFail, localizedRedeemNoPoints;
    [SerializeField] private GameObject panelAchievementsDescription, panelRedeemPoints, noAchievementsWarning, noRegionSelectedWarning, btnLoadMore;
    private string regionalAchievementsText, allAchievementsText, redeemNotice;
    private readonly string redeemPointAPI = "";
    private bool noAllAchievements = true;
    private Dictionary<int, GameObject> poisOverviewDict = new Dictionary<int, GameObject>();

    private int currentAllTrophiesPage;


    private async void Start() {
        regionalAchievementsText = await regionalAchievements.GetLocalizedStringAsync();
        redeemNotice = await localizedRedeemNotice.GetLocalizedStringAsync();
        allAchievementsText = achievementToggleText.text;
    }

    private void OnEnable() {
        Achievement.OnBtnAchievement += OnAchievementBtn;
    }

    private void OnDisable() {
        Achievement.OnBtnAchievement -= OnAchievementBtn;
    }

    public void OnBtnAchievementTab() {
        panelAchievementsDescription.SetActive(false);
        panelRedeemPoints.SetActive(false);
        OnBtnToggleAchievements(true);
        OnBtnAchievementTabInternal();
    }

    public void MarkOverviewElementVisited(int poiId) {
        if (poisOverviewDict.ContainsKey(poiId)) {
            poisOverviewDict[poiId].FindComponentInChildWithTag<Image>(Tags.TARGET).gameObject.SetActive(true);
            poisOverviewDict[poiId].FindComponentInChildWithTag<Image>(Tags.CHARACTER).gameObject.SetActive(false);
        }
    }

    private async UniTask OnBtnAchievementTabInternal() {
        //destroy all elements since achievements may change
        foreach (Transform child in achievementsContent) {
            Destroy(child.gameObject);
        }

        if (GameManager.CurrentRegionID == -1) {
            points.transform.parent.gameObject.SetActive(false);
            noRegionSelectedWarning.SetActive(true);
        }
        else {
            noRegionSelectedWarning.SetActive(false);
            points.transform.parent.gameObject.SetActive(true);

            int totalPoints = GameManager.Instance.CalculatePoints(GameManager.CurrentRegionID);
            int redeemedPoints = 0;
            //minus redeemed points
            for (int i = 0; i < GameManager.SaveData.redeemedRewardData.Count; i++) {
                Reward reward = DatabaseManager.SelectEntry<Reward>(GameManager.SaveData.redeemedRewardData[i].id);
                if (reward.RegionID == GameManager.CurrentRegionID) redeemedPoints += reward.RequiredPoints;
            }
            totalPoints -= redeemedPoints;


            points.text = Values.LOCALIZED_TERMS[Values.LOC_KEY_POINTS] + ": " + totalPoints.ToString();
            redeemNoticeText.text = string.Format(redeemNotice, redeemedPoints);

            //generate achievements
            List<Achievements> achievements = DatabaseManager.SelectMultipleEntries<Achievements>(string.Format("RegionID = {0} AND IsRegional = 1", GameManager.CurrentRegionID));
            if (achievements != null) {
                for (int i = 0; i < achievements.Count; i++) {
                    GameObject go = await Addressables.InstantiateAsync(achievementListElement, achievementsContent);
                    Sprite sprite = await AddressablesManager.Instance.LoadSprite(achievements[i].AtlasReference, achievements[i].ImgReference);

                    go.GetComponent<Achievement>().Initialize(achievements[i], sprite, false);
                }
            }
        }

        MainManager.Instance.OnBtnSubTab((int)HomeTabTypes.ACHIEVEMENTS);
    }

    public async UniTask<Button> GenerateOverviewElement(POI poi, int number, bool visited) {
        GameObject tabElement = await Addressables.InstantiateAsync(overviewListElement, overviewContent);
        if (tabElement == null) {
            Debug.LogError("Reference does not exist! " + MainManager.Instance.TabListBtnElement);
            return null;
        }
        string name = LocalizationManager.GetLocalizedDBString(poi.NameLocKey);
        tabElement.FindComponentInChildWithTag<TextMeshProUGUI>(Tags.NAME_REPLACE).text = number + ". " + name;
        Button btn = tabElement.GetComponent<Button>();
        tabElement.FindComponentInChildWithTag<TextMeshProUGUI>(Tags.TEXT_REPLACE).text = poi.UnlockPoints.ToString();

        tabElement.FindComponentInChildWithTag<RectTransform>(Tags.CHARACTER).gameObject.SetActive(!visited);

        Image img = tabElement.FindComponentInChildWithTag<Image>(Tags.IMG_REPLACE);
        Sprite sprite = await AddressablesManager.Instance.LoadSprite(poi.AtlasReference, poi.ImgReference);
        img.sprite = sprite;
        AspectRatioFitter ratio = img.GetComponent<AspectRatioFitter>();
        ratio.aspectMode = AspectRatioFitter.AspectMode.EnvelopeParent;
        ratio.aspectRatio = (float)sprite.rect.width / (float)sprite.rect.height;

        RectTransform stamp = tabElement.FindComponentInChildWithTag<RectTransform>(Tags.TARGET);
        stamp.gameObject.SetActive(visited);
        sprite = await AddressablesManager.Instance.LoadSprite(poi.StampAtlasReference, poi.StampReference);
        stamp.GetComponent<Image>().sprite = sprite;
        Color col;
        ColorUtility.TryParseHtmlString(poi.StampColor, out col);
        stamp.GetComponent<Image>().color = col;
        float angle = Mathf.Clamp((float)poi.UnlockPoints / (float)name.Length, -20, 5);
        if (name.Length % 2 == 0) {
            angle = -angle;
        }
        stamp.Rotate(Vector3.forward, angle);

        poisOverviewDict.Add(poi.ID, tabElement);
        overviewList.Add(tabElement);

        number++;
        return btn;
    }

    private async UniTask OnBtnAllAchievementsInternal() {
        //destroy all elements since achievements may change
        foreach (Transform child in allCollectedAchievementsContent) {
            Destroy(child.gameObject);
        }

        foreach (Transform child in allToCollectAchievementsContent) {
            Destroy(child.gameObject);
        }

        btnLoadMore.SetActive(true);

        currentAllTrophiesPage = 0;

        //generate achievements
        //regional
        OnAllTrophiesPage(0);

        //non regional
        List<Achievements> achievements = DatabaseManager.SelectMultipleEntries<Achievements>("IsRegional = 0");
        if (achievements != null && achievements.Count > 0) {
            noAllAchievements = false;
            noAchievementsWarning.SetActive(false);

            for (int i = 0; i < achievements.Count; i++) {
                Region region = DatabaseManager.SelectEntry<Region>(achievements[i].RegionID);

                GameObject go = await Addressables.InstantiateAsync(achievementListElement, allCollectedAchievementsContent);
                Sprite sprite = await AddressablesManager.Instance.LoadSprite(achievements[i].AtlasReference, achievements[i].ImgReference);
                go.GetComponent<Achievement>().Initialize(achievements[i], sprite, true);
            }

        }
    }



    private void OnAchievementBtn(string name, string description, int progress) {
        achievementTitle.text = string.Format("{0} - {1}%", name, progress);
        achievementDescription.text = description;
        panelAchievementsDescription.SetActive(true);
    }

    public void OnBtnToggleAchievements() {
        bool regional = !overviewContent.gameObject.activeSelf;
        OnBtnToggleAchievements(regional);
    }

    private void OnBtnToggleAchievements(bool regional) {
        overviewContent.gameObject.SetActive(regional);
        allAchievementsTab.gameObject.SetActive(!regional);
        if (regional) {
            noAchievementsWarning.SetActive(false);
            noRegionSelectedWarning.SetActive(GameManager.CurrentRegionID == -1);
        }
        else {
            OnBtnAllAchievementsInternal();
            noRegionSelectedWarning.SetActive(false);
            noAchievementsWarning.SetActive(noAllAchievements);
        }

        scrollView.content = regional ? overviewContent.GetComponent<RectTransform>() : allAchievementsTab.GetComponent<RectTransform>();

        achievementToggleText.text = regional ? allAchievementsText : regionalAchievementsText;
    }

    public void OnBtnRedeemPoints() {
        string code = panelRedeemPoints.GetComponentInChildren<TMP_InputField>().text;

        if (code == "f3pDebuggingModeON") {
            GameManager.debugging = true;
            EasyMessageBox.Show("Debug Mode ON");
            MainManager.Instance.citiesInitialized = false;
            return;
        }

        Reward reward = DatabaseManager.SelectEntry<Reward>($"RegionID = {GameManager.CurrentRegionID} AND Code = \"{code}\"");
        if (reward == null) {
            //reward does not exist
            //code error
            localizedRedeemCodeFail.GetLocalizedStringAsync().Completed += asyncOp => {
                string warning = asyncOp.Result;
                EasyMessageBox.Show(warning);
            };
            return;
        }

        int totalPoints = GameManager.Instance.CalculatePoints(GameManager.CurrentRegionID);
        int redeemedPoints = 0;
        //minus redeemed points
        for (int i = 0; i < GameManager.SaveData.redeemedRewardData.Count; i++) {
            Reward redeemedReward = DatabaseManager.SelectEntry<Reward>(GameManager.SaveData.redeemedRewardData[i].id);
            if (redeemedReward.RegionID == GameManager.CurrentRegionID) redeemedPoints += redeemedReward.RequiredPoints;
        }

        totalPoints -= redeemedPoints;

        //check if enough points
        if (totalPoints < reward.RequiredPoints) {
            localizedRedeemNoPoints.GetLocalizedStringAsync().Completed += asyncOp => {
                string warning = asyncOp.Result;
                EasyMessageBox.Show(warning);
            };
            return;
        }


        //redeem success
        GameManager.SaveData.redeemedRewardData.Add(new RedeemData { id = reward.ID, date = DateTime.Now.ToString() });

        redeemedPoints += reward.RequiredPoints;

        totalPoints -= reward.RequiredPoints;
        points.text = Values.LOCALIZED_TERMS[Values.LOC_KEY_POINTS] + ": " + totalPoints.ToString();
        redeemNoticeText.text = string.Format(redeemNotice, redeemedPoints);

        if (string.IsNullOrEmpty(reward.RewardMessage)) {
            localizedRedeemSuccess.GetLocalizedStringAsync().Completed += asyncOp => {
                EasyMessageBox.Show(asyncOp.Result);
            };
        }
        else {
            EasyMessageBox.Show(reward.RewardMessage);
        }
    }

    public void ClearOverview() {
        if (overviewList.Count > 0) {
            foreach (GameObject go in overviewList) {
                Destroy(go);
            }
            overviewList.Clear();
        }

        poisOverviewDict.Clear();
    }

    public async void OnAllTrophiesPage(int step) {
        currentAllTrophiesPage += step;
        List<Achievements> achievements = DatabaseManager.SelectMultipleEntries<Achievements>("IsRegional = 1 LIMIT 6 OFFSET " + currentAllTrophiesPage * 6);
        if (achievements != null && achievements.Count > 0) {
            noAllAchievements = false;
            noAchievementsWarning.SetActive(false);

            for (int i = 0; i < achievements.Count; i++) {
                Region region = DatabaseManager.SelectEntry<Region>(achievements[i].RegionID);
                if (region.IsActive == 0 && !GameManager.debugging) continue;

                GameObject go = await Addressables.InstantiateAsync(achievementListElement, allToCollectAchievementsContent);
                Sprite sprite = await AddressablesManager.Instance.LoadSprite(achievements[i].AtlasReference, achievements[i].ImgReference);
                go.GetComponent<Achievement>().Initialize(achievements[i], sprite, true);
            }

        }
        else {
            //no more entries
            btnLoadMore.SetActive(false);
        }
    }
}
