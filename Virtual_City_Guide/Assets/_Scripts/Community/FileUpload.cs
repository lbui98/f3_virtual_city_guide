using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections;
using System;
using Proyecto26;
using TMPro;
using System.Collections.Generic;
using UnityEngine.Localization;
using bunLace.Constants;

public class FileUpload : MonoBehaviour {
    private const string upload_API = "https://www.stempelpass.de/APP/API/upload.php";
    //private const string upload_API = "http://localhost:81/stempelpass/API/upload.php";

    //private const string db_upload_API = "http://localhost:81/stempelpass/API/addUpload.php";
    private const string db_upload_API = "https://stempelpass.de/APP/API/addUpload.php";

    private string filePath;
    private int category;

    [SerializeField] private LocalizedString uploadWarning, reviewNotice;

    [SerializeField] private TMP_InputField descriptionInput;
    [SerializeField] private TMP_Dropdown categoryDropdown;

    private void OnEnable() {
        Gallery.ImagePathChosen += OnImagePathChosen;
    }

    private void OnDisable() {
        Gallery.ImagePathChosen -= OnImagePathChosen;
    }

    private void OnImagePathChosen(string path) {
        filePath = path;
    }

    public void UploadFile() {
        uploadWarning.GetLocalizedStringAsync().Completed += asyncOp => {
            EasyMessageBox.Show(
                message: asyncOp.Result,
                button1Action: delegate { StartCoroutine(UploadCoroutine(filePath)); },
                button1Text: Values.LOCALIZED_TERMS[Values.LOC_KEY_CONFIRM],
                button2Text: Values.LOCALIZED_TERMS[Values.LOC_KEY_CANCEL]
                );
        };
    }

    IEnumerator UploadCoroutine(string filePath) {
        if (string.IsNullOrEmpty(filePath)) {
            //nothing selected
            yield break;
        }

        WWWForm form = new WWWForm();
        byte[] fileData = System.IO.File.ReadAllBytes(filePath);
        System.IO.FileInfo fi = new System.IO.FileInfo(filePath);


        string newFilename = System.DateTime.Now.ToString("yyyyMMdd-HHmmss") + "-" + CommunityManager.username;
        string ext = System.IO.Path.GetExtension(filePath);

        form.AddBinaryData("file", fileData, newFilename + ext, "image/" + ext.TrimStart('.'));

        using (UnityWebRequest www = UnityWebRequest.Post(upload_API, form)) {
            yield return www.SendWebRequest();

            if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError) {
                Debug.LogError(www.error);
            }
            else {
                Debug.Log("File uploaded successfully!");
                Debug.Log("Response: " + www.downloadHandler.text);

                List<IMultipartFormSection> dbForm = new List<IMultipartFormSection>();

                dbForm.Add(new MultipartFormDataSection("filename", newFilename + fi.Extension));
                dbForm.Add(new MultipartFormDataSection("username", CommunityManager.username));
                dbForm.Add(new MultipartFormDataSection("password", GameManager.SaveData.communityData.password));
                dbForm.Add(new MultipartFormDataSection("category", categoryDropdown.value.ToString()));
                if (!string.IsNullOrEmpty(descriptionInput.text)) dbForm.Add(new MultipartFormDataSection("description", descriptionInput.text));

                UnityWebRequest request = UnityWebRequest.Post(db_upload_API, dbForm);
                yield return request.SendWebRequest();

                if (request.result != UnityWebRequest.Result.Success) {
                    Debug.Log(request.error);
                }
                else {
                    Debug.Log("Form upload complete!");
                    Debug.Log("Response: " + request.downloadHandler.text);
                }


                //review notice
                var op = reviewNotice.GetLocalizedStringAsync();
                yield return op;
                EasyMessageBox.Show(op.Result);

                gameObject.SetActive(false);
            }
        }
    }
}
