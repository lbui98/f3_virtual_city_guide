using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NativeGalleryNamespace;
using System;
using System.IO;
using bunLace.Constants;
using UnityEngine.Localization;

public class Gallery : MonoBehaviour {
    public RawImage m_image;

    protected float m_aspectRatio = 1.0f;
    protected float m_rectAspectRatio = 1.0f;

    Texture2D texture;

    public delegate void ImagePathChoose(string path);
    public static event ImagePathChoose ImagePathChosen;

    [SerializeField] private LocalizedString fileTooBigWarning, fileUnsupported;

    public void OnBtnGallery() {
        //if (texture != null) Destroy(texture);

        NativeGallery.Permission permission = NativeGallery.GetImageFromGallery((path) => {
            Debug.Log("Image path: " + path);
            if (path != null) {
                //check image size
                long length = new FileInfo(path).Length;

                string ext = System.IO.Path.GetExtension(path);

                if (ext.ToLower() == ".png" || ext.ToLower() == ".jpg" || ext.ToLower() == ".jpeg") {
                    //is supported
                }
                else {
                    //error message
                    fileUnsupported.GetLocalizedStringAsync().Completed += asyncOp => {
                        EasyMessageBox.Show(asyncOp.Result);
                    };
                    return;

                }

                if (length > Values.MAX_UPLOAD_SIZE) {
                    //error - file too big
                    fileTooBigWarning.GetLocalizedStringAsync().Completed += asyncOp => {
                        EasyMessageBox.Show(asyncOp.Result);
                    };
                    return;
                }

                // Create Texture from selected image
                texture = NativeGallery.LoadImageAtPath(path);
                if (texture == null) {
                    Debug.Log("Couldn't load texture from " + path);
                    return;
                }


                m_image.texture = texture;
                AdjustAspect();
                // If a procedural texture is not destroyed manually, 
                // it will only be freed after a scene change
                //Destroy(texture, 5f);

                ImagePathChosen?.Invoke(path);
            }
        });

    }

    void SetupImage() {
        CalculateImageAspectRatio();
        CalculateTextureAspectRatio();
    }


    void CalculateImageAspectRatio() {
        RectTransform rt = m_image.transform as RectTransform;
        m_rectAspectRatio = rt.sizeDelta.x / rt.sizeDelta.y;
    }

    void CalculateTextureAspectRatio() {
        if (m_image == null) {
            Debug.Log("CalculateAspectRatio: m_image is null");
            return;
        }

        Texture2D texture = (Texture2D)m_image.texture;
        if (texture == null) {
            Debug.Log("CalculateAspectRatio: texture is null");
            return;
        }


        m_aspectRatio = (float)texture.width / texture.height;
        //Debug.Log("textW=" + texture.width + " h=" + texture.height + " ratio=" + m_aspectRatio);
    }


    public void AdjustAspect() {
        SetupImage();

        bool fitY = m_aspectRatio < m_rectAspectRatio;

        SetAspectFitToImage(m_image, fitY, m_aspectRatio);
    }


    protected virtual void SetAspectFitToImage(RawImage _image, bool yOverflow, float displayRatio) {
        if (_image == null) {
            return;
        }

        Rect rect = new Rect(0, 0, 1, 1);   // default
        if (yOverflow) {

            rect.height = m_aspectRatio / m_rectAspectRatio;
            rect.y = (1 - rect.height) * 0.5f;
        }
        else {
            rect.width = m_rectAspectRatio / m_aspectRatio;
            rect.x = (1 - rect.width) * 0.5f;

        }
        _image.uvRect = rect;
    }
}
