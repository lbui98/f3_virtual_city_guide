using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.Localization;
using bunLace.Singleton;
using Proyecto26;
using UnityEngine.Networking;
using UnityEngine.UI;
using bunLace.Tools;
using bunLace.Constants;
using TMPro;
using System;
using Coffee.UIExtensions;

public class CommunityManager : Singleton<CommunityManager> {

    [Serializable]
    public class CommunityPosts {
        public Post[] posts;
    }

    [Serializable]
    public class Post {
        public int ID;
        public string filename;
        public string userID;
        public string description;
        public int category;
        public string link;
        public int likes;
        public string username;
        public int profileImgId;
        public int profileColorId;
    }
    [Serializable]

    public class Strikes {
        public Strike[] strikes;
    }
    [Serializable]

    public class Strike {
        public int strikes;
        public string strikeDate;
    }

    public Sprite[] profilePictures;
    public Color[] profileColors;

    [SerializeField] private AssetReference newsPostReference, postReference;
    [SerializeField] private LocalizedString bannedNotice, strikeNotice, backToMainWarning;
    [SerializeField] private Transform newsContent, postContent, pagination, userPagination, userContent;
    [SerializeField] private TextMeshProUGUI page, userPage, strikeCount;
    [SerializeField] private GameObject pageBack, pageNext, postBtn;
    [SerializeField] private GameObject userPageBack, userPageNext;
    [SerializeField] private TMP_Dropdown categoryDropdown;
    [SerializeField] private UIParticle heartPs;

    public static string username;

    private const string get_Post_API = "https://www.stempelpass.de/APP/API/getPosts.php?pageCount=";
    private const string get_User_Post_API = "https://www.stempelpass.de/APP/API/getUserPosts.php?pageCount={0}&username={1}";
    private const string get_News_API = "https://www.stempelpass.de/APP/API/getNews.php";
    private const string get_Strikes_API = "https://www.stempelpass.de/APP/API/getStrikes.php";
    public int pageCount = 0;
    public int userpageCount = 0;


    private void Start() {
        if (GameManager.SaveData.communityData == null) {
            GameManager.SaveData.communityData = new bunLace.SaveLoad.CommunityData();
            GameManager.Instance.Save();
        }
        else {
            username = GameManager.SaveData.communityData.username;
            CheckStrikes(GameManager.SaveData.communityData.username, GameManager.SaveData.communityData.password);
        }

        GetNews();
        GetPosts();
        GetUserPosts();
    }

    private void OnEnable() {
        categoryDropdown.onValueChanged.AddListener(OnCategoryChanged);
        RegisterUser.OnUpdateProfilePicture += OnUserUpdated;
    }

    private void OnDisable() {
        categoryDropdown.onValueChanged.RemoveAllListeners();
        RegisterUser.OnUpdateProfilePicture -= OnUserUpdated;
    }

    private void OnUserUpdated(int profileColor, int profilePicture, string username) {
        CommunityManager.username = username;
        pageCount = 0;
        userpageCount = 0;

        pageBack.SetActive(false);
        pageNext.SetActive(true);
        page.text = (pageCount + 1).ToString();

        userPageBack.SetActive(false);
        userPageNext.SetActive(true);
        userPage.text = (userpageCount + 1).ToString();

        ClearUserPosts();

        GetPosts();
        GetUserPosts();
    }

    private void OnCategoryChanged(int value) {
        pageCount = 0;
        Debug.Log("Category " + value);
        page.text = (pageCount + 1).ToString();
        pageNext.SetActive(true);
        pageBack.SetActive(false);
        GetPosts();
    }

    public void CheckStrikes(string username, string password) {
        StartCoroutine(CheckStrikesRoutine(username, password));
    }

    private IEnumerator CheckStrikesRoutine(string username, string password) {
        List<IMultipartFormSection> dbForm = new List<IMultipartFormSection>();

        dbForm.Add(new MultipartFormDataSection("username", username));
        dbForm.Add(new MultipartFormDataSection("password", password));

        UnityWebRequest request = UnityWebRequest.Post(get_Strikes_API, dbForm);
        yield return request.SendWebRequest();

        if (request.result != UnityWebRequest.Result.Success) {
            Debug.Log(request.error);
        }
        else {
            Debug.Log("Success");
            Debug.Log("Response: " + request.downloadHandler.text);

            if (string.IsNullOrEmpty(request.downloadHandler.text)) {
                //login did not succeed
                EasyMessageBox.Show("Username or Password is wrong");
                yield break;
            }

            var res = "{\"strikes\": " + request.downloadHandler.text + "}";
            Debug.Log(res);
            Strikes strikes = JsonUtility.FromJson<Strikes>(res);
            Strike strike = strikes.strikes[0];

            //check if the strike is new
            DateTime dbTime;
            DateTime localSaveTime;
            DateTime.TryParse(strike.strikeDate, out dbTime);
            DateTime.TryParse(GameManager.SaveData.SaveTime, out localSaveTime);

            if (DateTime.Compare(dbTime, localSaveTime) > 0) {
                //there was a change in strikes since the last user activity
                //check how many
                if (strike.strikes >= 3) {
                    //user is banned from posting
                    var op = bannedNotice.GetLocalizedStringAsync();
                    yield return op;
                    EasyMessageBox.Show(op.Result);
                    postBtn.SetActive(false);
                }
                else {
                    var op = strikeNotice.GetLocalizedStringAsync();
                    yield return op;
                    EasyMessageBox.Show(string.Format(op.Result, strike.strikes));
                }
            }

            //>3 - user is banned from posting
            postBtn.SetActive(strike.strikes < 3);

            strikeCount.text = string.Format(Values.LOCALIZED_TERMS[Values.LOC_KEY_STRIKES], strike.strikes);
        }
    }

    public void ReturnToMainScene() {
        UnityEngine.Resources.UnloadUnusedAssets();

        backToMainWarning.GetLocalizedStringAsync().Completed += asyncOp => {
            EasyMessageBox.Show(message: asyncOp.Result,
                button1Text: Values.LOCALIZED_TERMS[Values.LOC_KEY_CONFIRM],
            button1Action: delegate { UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(bunLace.Constants.Scenes.MAIN, UnityEngine.SceneManagement.LoadSceneMode.Single); },
            button2Text: Values.LOCALIZED_TERMS[Values.LOC_KEY_CANCEL]
            );
        };
    }

    public void LogOut() {
        //sync save
        //remove previous community data from save file
        GameManager.Instance.SyncSave(GameManager.SaveData.communityData.username, GameManager.SaveData.communityData.password, bunLace.Tools.SynchronisationOptions.CLEAR_LOCAL);
    }

    #region Posts
    private void GetPosts() {
        string url = categoryDropdown.value == 0 ? get_Post_API + pageCount : get_Post_API + pageCount + "&category=" + categoryDropdown.value;

        var request = new RequestHelper {
            Uri = url,
            Method = "GET",
            Timeout = 60,
            Retries = 5,
            RetrySecondsDelay = 2,
            RetryCallback = (err, retries) => { Debug.Log("Repeat error Check version: " + err.Message); }, //See the error before retrying the request
            EnableDebug = true,
        };


        RestClient.Request(request).Then(response => {
            //get list from JSON
            if (string.IsNullOrEmpty(response.Text)) {
                if (categoryDropdown.value > 0) {
                    //no posts found for this category
                    ClearPosts();
                }
                else {
                    //no posts found
                    return;
                }
            }

            var res = "{\"posts\": " + response.Text + "}";
            Debug.Log(res);
            CommunityPosts post = JsonUtility.FromJson<CommunityPosts>(res);
            if (post == null || post.posts.Length == 0) {
                return;
            }

            Post[] posts = post.posts;
            Debug.Log(posts.Length);
            GeneratePosts(posts);

        }).Catch(err => {
            Debug.Log("Error: " + err);
        }).Done();
    }

    private void GetNews() {
        var request = new RequestHelper {
            Uri = get_News_API,
            Method = "GET",
            Timeout = 60,
            Retries = 5,
            RetrySecondsDelay = 2,
            RetryCallback = (err, retries) => { Debug.Log("Repeat error Check version: " + err.Message); }, //See the error before retrying the request
            EnableDebug = true,
        };


        RestClient.Request(request).Then(response => {
            //get list from JSON
            if (string.IsNullOrEmpty(response.Text)) {
                //no posts found
                return;
            }

            var res = "{\"posts\": " + response.Text + "}";
            Debug.Log(res);
            CommunityPosts post = JsonUtility.FromJson<CommunityPosts>(res);
            if (post == null || post.posts.Length == 0) {
                return;
            }

            Post[] posts = post.posts;
            Debug.Log(posts.Length);
            GeneratePosts(posts);

        }).Catch(err => {
            Debug.Log("Error: " + err);
        }).Done();
    }

    private void ClearPosts() {
        UnityEngine.Resources.UnloadUnusedAssets();

        heartPs.transform.SetParent(postContent);
        foreach (Transform child in postContent) {
            if (child.CompareTag(Tags.PLAYER)) continue;
            GameObject.Destroy(child.gameObject);
        }
    }

    public void OnPageStep(int step) {
        pageCount += step;
        if (pageCount <= 0) {
            pageCount = 0;
            pageBack.SetActive(false);
            pageNext.SetActive(true);
        }
        else if (pageCount > 0) {
            pageBack.SetActive(true);
            pageNext.SetActive(true);
        }

        string url = categoryDropdown.value == 0 ? get_Post_API + pageCount : get_Post_API + pageCount + "&category=" + categoryDropdown.value;

        var request = new RequestHelper {
            Uri = url,
            Method = "GET",
            Timeout = 60,
            Retries = 5,
            RetrySecondsDelay = 2,
            RetryCallback = (err, retries) => { Debug.Log("Repeat error Check version: " + err.Message); }, //See the error before retrying the request
            EnableDebug = true,
        };

        RestClient.Request(request).Then(response => {
            //get list from JSON
            if (string.IsNullOrEmpty(response.Text)) {
                //no posts found
                pageCount -= step;
                //disable next btn
                pageNext.SetActive(false);
                if (pageCount <= 0) {
                    pageBack.SetActive(false);
                }
            }
            else {
                GetPosts();
            }

            page.text = (pageCount + 1).ToString();
        }).Catch(err => {
            Debug.Log("Error: " + err);
        }).Done();

    }


    private void GeneratePosts(Post[] posts) {
        if (posts == null) {
            return;
        }
        ClearPosts();

        for (int i = 0; i < posts.Length; i++) {
            if (string.IsNullOrEmpty(posts[i].link)) {
                //regular post
                GeneratePost(posts[i]);
            }
            else {
                //news post
                GenerateNewsPost(posts[i]);
            }
        }

    }

    private void GeneratePost(Post post) {
        postReference.InstantiateAsync(postContent).Completed += asyncOp => {
            GameObject go = asyncOp.Result;
            go.GetComponent<CommunityPost>().Initialize(post);
            pagination.SetAsLastSibling();

        };
    }

    private void GenerateNewsPost(Post post) {
        newsPostReference.InstantiateAsync(newsContent).Completed += asyncOp => {
            GameObject go = asyncOp.Result;
            go.GetComponent<CommunityPost>().Initialize(post);
        };
    }

    public void TriggerHeartParticles(Transform btnLike) {
        heartPs.transform.SetParent(btnLike);
        heartPs.GetComponent<RectTransform>().anchoredPosition = Vector3.zero;
        heartPs.Play();
    }
    #endregion

    #region User Posts
    private void GetUserPosts() {
        if (string.IsNullOrEmpty(username)) return;
        Debug.Log("Get user posts " + username);

        string url = string.Format(get_User_Post_API, userpageCount, username);

        var request = new RequestHelper {
            Uri = url,
            Method = "GET",
            Timeout = 60,
            Retries = 5,
            RetrySecondsDelay = 2,
            RetryCallback = (err, retries) => { Debug.Log("Repeat error Check version: " + err.Message); }, //See the error before retrying the request
            EnableDebug = true,
        };

        RestClient.Request(request).Then(response => {
            //get list from JSON
            if (string.IsNullOrEmpty(response.Text)) {
                if (categoryDropdown.value > 0) {
                    //no posts found for this category
                    ClearPosts();
                }
                else {
                    //no posts found
                    return;
                }
            }

            var res = "{\"posts\": " + response.Text + "}";
            Debug.Log("user posts: " + res);
            CommunityPosts post = JsonUtility.FromJson<CommunityPosts>(res);
            if (post == null || post.posts.Length == 0) {
                return;
            }

            Post[] posts = post.posts;
            GenerateUserPosts(posts);

        }).Catch(err => {
            Debug.Log("Error: " + err);
        }).Done();
    }

    public void OnUserPageStep(int step) {
        if (string.IsNullOrEmpty(username)) return;

        userpageCount += step;
        if (userpageCount <= 0) {
            userpageCount = 0;
            userPageBack.SetActive(false);
            userPageNext.SetActive(true);
        }
        else if (userpageCount > 0) {
            userPageBack.SetActive(true);
            userPageNext.SetActive(true);
        }


        string url = string.Format(get_User_Post_API, userpageCount, username);

        var request = new RequestHelper {
            Uri = url,
            Method = "GET",
            Timeout = 60,
            Retries = 5,
            RetrySecondsDelay = 2,
            RetryCallback = (err, retries) => { Debug.Log("Repeat error Check version: " + err.Message); }, //See the error before retrying the request
            EnableDebug = true,
        };

        RestClient.Request(request).Then(response => {
            //get list from JSON
            if (string.IsNullOrEmpty(response.Text)) {
                //no posts found
                userpageCount -= step;
                //disable next btn
                userPageNext.SetActive(false);
                if (userpageCount <= 0) {
                    userPageBack.SetActive(false);
                }
            }
            else {
                GetUserPosts();
            }

            userPage.text = (userpageCount + 1).ToString();
        }).Catch(err => {
            Debug.Log("Error: " + err);
        }).Done();

    }


    private void GenerateUserPosts(Post[] posts) {
        if (posts == null) {
            Debug.Log("D");

            return;
        }
        ClearUserPosts();

        for (int i = 0; i < posts.Length; i++) {
            if (string.IsNullOrEmpty(posts[i].link)) {
                //regular post
                GenerateUserPost(posts[i]);
            }
        }

    }

    private void GenerateUserPost(Post post) {
        postReference.InstantiateAsync(userContent).Completed += asyncOp => {
            GameObject go = asyncOp.Result;
            go.GetComponent<CommunityPost>().Initialize(post.ID, post.filename, post.description, post.likes);
            userPagination.SetAsLastSibling();

        };
    }

    private void ClearUserPosts() {
        UnityEngine.Resources.UnloadUnusedAssets();

        heartPs.transform.SetParent(postContent);

        foreach (Transform child in userContent) {
            if (child.CompareTag(Tags.PLAYER)) continue;
            GameObject.Destroy(child.gameObject);
        }
    }
    #endregion
}
