using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Proyecto26;
using TMPro;
using bunLace.Localization;
using UnityEngine.Localization.Components;

public class PostCategoryDropdown : MonoBehaviour {
    public TMP_Dropdown dropdown;
    private const string get_API = "https://stempelpass.de/APP/API/getPostCategories.php";

    private int[] categories;

    void Start() {
        if (InternetReachabilityVerifier.Instance.status != InternetReachabilityVerifier.Status.NetVerified) {
            return;
        }

        //fetch current existing categories
        //such as region specific contests
        var request = new RequestHelper {
            Uri = get_API,
            Method = "GET",
            Timeout = 60,
            Retries = 5,
            RetrySecondsDelay = 2,
            RetryCallback = (err, retries) => {
                Debug.Log("Repeat error: " + err.Message);
            }, //See the error before retrying the request
            EnableDebug = true,
        };

        RestClient.Request(request).Then(response => {
            Debug.Log("adding dropdown items...");
            categories = JsonUtil.FromJson<int[]>(response.Text);

            //generate dropdown item
            var options = new List<TMP_Dropdown.OptionData>();
            int selected = 0;
            for (int i = 0; i < categories.Length; i++) {
                options.Add(new TMP_Dropdown.OptionData(LocalizationManager.GetLocalizedDBString(categories[i])));
            }

            dropdown.options = options;

            dropdown.value = selected;
            dropdown.onValueChanged.AddListener(delegate { SelectCategory(); });
            SelectCategory();

        }).Catch(err => {
            Debug.Log("Error: " + err);
        }).Done();

    }

    public delegate void OnCategorySelect(int category);
    public static event OnCategorySelect OnCategorySelected;

    private void SelectCategory() {
        OnCategorySelected?.Invoke(categories[dropdown.value]);
    }
}
