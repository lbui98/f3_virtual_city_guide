using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class LeaderboardEntry : MonoBehaviour {
    public Image color, profile;
    public TextMeshProUGUI username, points, ranking;


    public void FillInformation(string username, int profileImgId, int profileColorId, int points, int ranking) {
        this.username.text = username;
        profile.sprite = CommunityManager.Instance.profilePictures[profileImgId];
        color.color = CommunityManager.Instance.profileColors[profileColorId];
        this.points.text = points.ToString();
        this.ranking.text = ranking.ToString();
    }
}
