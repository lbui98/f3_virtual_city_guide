using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CommunityProfilePicture : MonoBehaviour {
    [SerializeField] private Image profileColor, profilePicture;
    public void ChangeProfilePicture(int colorId, int profileId) {
        profileColor.color = CommunityManager.Instance.profileColors[colorId];
        profilePicture.sprite = CommunityManager.Instance.profilePictures[profileId];
    }
}
