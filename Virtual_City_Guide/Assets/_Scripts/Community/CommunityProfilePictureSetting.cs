using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CommunityProfilePictureSetting : MonoBehaviour {
    [SerializeField] private Image profilePicture;
    [SerializeField] private Image profileColor;

    public int currentProfilePicture = 0;
    public int currentProfileColor = 0;

    public void ChangeProfilePicture(int step) {
        currentProfilePicture += step;
        if (currentProfilePicture >= CommunityManager.Instance.profilePictures.Length) {
            currentProfilePicture = 0;
        }
        else if (currentProfilePicture < 0) {
            currentProfilePicture = CommunityManager.Instance.profilePictures.Length - 1;
        }

        profilePicture.sprite = CommunityManager.Instance.profilePictures[currentProfilePicture];
    }

    public void ChangeProfileColor(int step) {
        currentProfileColor += step;
        if (currentProfileColor >= CommunityManager.Instance.profileColors.Length) {
            currentProfileColor = 0;
        }
        else if (currentProfileColor < 0) {
            currentProfileColor = CommunityManager.Instance.profileColors.Length - 1;
        }

        profileColor.color = CommunityManager.Instance.profileColors[currentProfileColor];
    }

}
