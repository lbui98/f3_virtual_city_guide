using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using UnityEngine.Networking;

public class CommunityUser : MonoBehaviour {
    [SerializeField] private TextMeshProUGUI usernameSettings, usernameProfile;
    [SerializeField] private CommunityProfilePicture[] profilePictures;
    [SerializeField] private CommunityProfilePictureSetting profilePictureSetting;

    private const string change_profile_API = "https://www.stempelpass.de/APP/API/setUser.php";

    private void Start() {
        if (GameManager.SaveData != null && GameManager.SaveData.communityData != null && !string.IsNullOrEmpty(GameManager.SaveData.communityData.username)) {
            UpdateProfilePicture(GameManager.SaveData.communityData.profileColorId, GameManager.SaveData.communityData.profileImgId, GameManager.SaveData.communityData.username);
        }
    }

    private void OnEnable() {
        RegisterUser.OnUpdateProfilePicture += UpdateProfilePicture;
    }

    private void OnDisable() {
        RegisterUser.OnUpdateProfilePicture -= UpdateProfilePicture;
    }

    private void UpdateProfilePicture(int profileColor, int profilePicture, string username) {
        profilePictureSetting.currentProfileColor = profileColor;
        profilePictureSetting.currentProfilePicture = profilePicture;

        usernameSettings.text = username;
        usernameProfile.text = username;
        for (int i = 0; i < profilePictures.Length; i++) {
            profilePictures[i].ChangeProfilePicture(profileColor, profilePicture);
        }
    }

    public void ConfirmProfileChange() {
        UpdateProfilePicture(profilePictureSetting.currentProfileColor, profilePictureSetting.currentProfilePicture, CommunityManager.username);
        StartCoroutine(UpdateUser());
    }

    private IEnumerator UpdateUser() {
        List<IMultipartFormSection> dbForm = new List<IMultipartFormSection>();

        dbForm.Add(new MultipartFormDataSection("username", CommunityManager.username));
        dbForm.Add(new MultipartFormDataSection("password", GameManager.SaveData.communityData.password));

        dbForm.Add(new MultipartFormDataSection("savedata", JsonUtility.ToJson(GameManager.SaveData)));
        dbForm.Add(new MultipartFormDataSection("profileImgID", profilePictureSetting.currentProfilePicture.ToString()));
        dbForm.Add(new MultipartFormDataSection("profileColorID", profilePictureSetting.currentProfileColor.ToString()));

        UnityWebRequest request = UnityWebRequest.Post(change_profile_API, dbForm);
        yield return request.SendWebRequest();

        if (request.result != UnityWebRequest.Result.Success) {
            Debug.Log(request.error);
        }
        else {
            Debug.Log("Success");
            Debug.Log("Response: " + request.downloadHandler.text);
        }

        GameManager.SaveData.communityData.profileImgId = profilePictureSetting.currentProfilePicture;
        GameManager.SaveData.communityData.profileColorId = profilePictureSetting.currentProfileColor;
        GameManager.Instance.Save();
        GameManager.Instance.SyncSave();
    }
}
