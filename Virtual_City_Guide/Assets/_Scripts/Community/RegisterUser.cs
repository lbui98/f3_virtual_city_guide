using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Networking;

public class RegisterUser : MonoBehaviour {
    [SerializeField] private TMP_InputField usernameR, usernameL;
    [SerializeField] private TMP_InputField passwordR, passwordL;
    [SerializeField] private CommunityProfilePictureSetting communityProfilePicture;


    private const string register_API = "https://www.stempelpass.de/APP/API/setUser.php";
    private const string login_API = "https://www.stempelpass.de/APP/API/getUser.php";

    [System.Serializable]
    private class User {
        public int profileImgId;
        public int profileColorId;
    }

    private void Start() {
        if (!string.IsNullOrEmpty(GameManager.SaveData.communityData.username)) {
            gameObject.SetActive(false);
        }
    }


    public void Register() {
        StartCoroutine(RegisterRoutine());
    }

    private IEnumerator RegisterRoutine() {
        List<IMultipartFormSection> dbForm = new List<IMultipartFormSection>();

        dbForm.Add(new MultipartFormDataSection("username", usernameR.text));
        dbForm.Add(new MultipartFormDataSection("password", passwordR.text));
        dbForm.Add(new MultipartFormDataSection("points", GameManager.Instance.CalculateTotalPoints().ToString()));
        dbForm.Add(new MultipartFormDataSection("savedata", JsonUtility.ToJson(GameManager.SaveData)));
        dbForm.Add(new MultipartFormDataSection("profileImgID", communityProfilePicture.currentProfilePicture.ToString()));
        dbForm.Add(new MultipartFormDataSection("profileColorID", communityProfilePicture.currentProfileColor.ToString()));

        UnityWebRequest request = UnityWebRequest.Post(register_API, dbForm);
        yield return request.SendWebRequest();

        if (request.result != UnityWebRequest.Result.Success) {
            Debug.Log(request.error);
            EasyMessageBox.Show("Register Error");
            yield break;
        }
        else {
            Debug.Log("Success");
            Debug.Log("Response: " + request.downloadHandler.text);
        }

        GameManager.SaveData.communityData.username = usernameR.text;
        GameManager.SaveData.communityData.password = passwordR.text;
        GameManager.SaveData.communityData.profileImgId = communityProfilePicture.currentProfilePicture;
        GameManager.SaveData.communityData.profileColorId = communityProfilePicture.currentProfileColor;
        GameManager.Instance.Save();
        GameManager.Instance.SyncSave();
        gameObject.SetActive(false);
    }

    public void Login() {
        StartCoroutine(LoginRoutine());
    }

    public delegate void UpdateProfilePicture(int profileColor, int profilePicture, string username);
    public static event UpdateProfilePicture OnUpdateProfilePicture;

    private IEnumerator LoginRoutine() {
        List<IMultipartFormSection> dbForm = new List<IMultipartFormSection>();

        dbForm.Add(new MultipartFormDataSection("username", usernameL.text));
        dbForm.Add(new MultipartFormDataSection("password", passwordL.text));

        UnityWebRequest request = UnityWebRequest.Post(login_API, dbForm);
        yield return request.SendWebRequest();

        if (request.result != UnityWebRequest.Result.Success) {
            Debug.Log(request.error);
        }
        else {
            Debug.Log("Success");
            Debug.Log("Response: " + request.downloadHandler.text);

            if (string.IsNullOrEmpty(request.downloadHandler.text)) {
                //login did not succeed
                EasyMessageBox.Show("Username or Password is wrong");
                yield break;
            }

            var res = request.downloadHandler.text.TrimStart('[').TrimEnd(']');
            Debug.Log(res);
            User user = JsonUtility.FromJson<User>(res);

            //before changing users, check if already logged in with another account
            //sync save if logged in
            //if (!string.IsNullOrEmpty(GameManager.SaveData.communityData.username)) {
            //    Debug.Log("Sync bc logged in");
            //}
            //else {
            //    Debug.Log("force sync, overwwrite");
            //}
            GameManager.Instance.SyncSave(usernameL.text, passwordL.text, bunLace.Tools.SynchronisationOptions.FORCE_LOCAL_OVERWRITE);
            CommunityManager.username = usernameL.text;
            CommunityManager.Instance.CheckStrikes(usernameL.text, passwordL.text);
            OnUpdateProfilePicture?.Invoke(user.profileColorId, user.profileImgId, usernameL.text);
            gameObject.SetActive(false);
        }

    }
}
