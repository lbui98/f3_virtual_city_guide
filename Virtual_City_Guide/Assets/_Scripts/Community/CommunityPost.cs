using bunLace.Constants;
using Proyecto26;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Localization;
using UnityEngine.Networking;
using UnityEngine.UI;

public class CommunityPost : MonoBehaviour {
    [SerializeField] private RawImage img;
    [SerializeField] private Image profileImg;
    [SerializeField] private Image profileImgColor;
    [SerializeField] private TextMeshProUGUI username;
    [SerializeField] private TextMeshProUGUI description;
    [SerializeField] private TextMeshProUGUI likes;
    [SerializeField] private Button button;
    [SerializeField] private bool isNews = false;
    [SerializeField] private LocalizedString deleteWarning;
    [SerializeField] private GameObject deleteBtn;

    private const string get_Img_API = "https://stempelpass.de/APP/API/getImageUpload.php?file=";
    private const string like_API = "https://stempelpass.de/APP/API/updateLikes.php";
    //private const string delete_API = "http://localhost:81/stempelpass/API/deletePost.php";
    private const string delete_API = "https://stempelpass.de/APP/API/deletePost.php";

    private int likeCount;
    private int postId;

    [SerializeField] private Image likeImg;
    [SerializeField] private Sprite heart, fullHeart;

    public void Initialize(CommunityManager.Post post) {
        DownloadImage(post.filename, img);
        description.text = post.description;
        postId = post.ID;

        if (isNews) {
            button.onClick.AddListener(delegate { Application.OpenURL(post.link); });
        }
        else {
            username.text = post.username;
            button.onClick.AddListener(delegate { OnBtnLike(post.ID); });

            heart = likeImg.sprite;
            if (GameManager.SaveData.communityData != null && GameManager.SaveData.communityData.likedPosts != null && GameManager.SaveData.communityData.likedPosts.Contains(post.ID)) {
                likeImg.sprite = fullHeart;
            }
            else {
                likeImg.sprite = heart;
            }

            profileImgColor.color = CommunityManager.Instance.profileColors[post.profileColorId];
            profileImg.sprite = CommunityManager.Instance.profilePictures[post.profileImgId];

            UpdateLikeCount(post.likes);

            deleteBtn.SetActive(post.username == CommunityManager.username);
        }
    }

    public void Initialize(int postId, string filename, string description, int likes) {
        DownloadImage(filename, img);
        this.description.text = description;
        this.postId = postId;

        username.text = CommunityManager.username;
        button.onClick.AddListener(delegate { OnBtnLike(postId); });

        heart = likeImg.sprite;
        if (GameManager.SaveData.communityData != null && GameManager.SaveData.communityData.likedPosts != null && GameManager.SaveData.communityData.likedPosts.Contains(postId)) {
            likeImg.sprite = fullHeart;
        }
        else {
            likeImg.sprite = heart;
        }

        profileImgColor.color = CommunityManager.Instance.profileColors[GameManager.SaveData.communityData.profileColorId];
        profileImg.sprite = CommunityManager.Instance.profilePictures[GameManager.SaveData.communityData.profileImgId];

        UpdateLikeCount(likes);

        deleteBtn.SetActive(true);
    }

    private void OnDestroy() {
        Texture2D.Destroy(img.texture);
    }

    private void UpdateLikeCount(int newLikes) {
        likeCount = newLikes;
        if (likeCount > 0) {
            likes.text = likeCount.ToString();
            likes.gameObject.SetActive(true);
        }
        else {
            likes.gameObject.SetActive(false);
        }
    }

    private void OnBtnLike(int postId) {
        StartCoroutine(UpdateLike(postId));
    }

    private IEnumerator UpdateLike(int postId) {
        int like = 0;
        if (GameManager.SaveData.communityData.likedPosts == null) GameManager.SaveData.communityData.likedPosts = new List<int>();
        if (GameManager.SaveData.communityData.likedPosts.Contains(postId)) {
            //unlike
            GameManager.SaveData.communityData.likedPosts.Remove(postId);
            likeImg.sprite = heart;
            like = -1;
        }
        else {
            //like
            GameManager.SaveData.communityData.likedPosts.Add(postId);
            CommunityManager.Instance.TriggerHeartParticles(button.transform);
            likeImg.sprite = fullHeart;
            like = 1;
        }
        UpdateLikeCount(likeCount + like);

        List<IMultipartFormSection> dbForm = new List<IMultipartFormSection>();

        dbForm.Add(new MultipartFormDataSection("username", CommunityManager.username));
        dbForm.Add(new MultipartFormDataSection("password", GameManager.SaveData.communityData.password));
        dbForm.Add(new MultipartFormDataSection("like", like.ToString()));
        dbForm.Add(new MultipartFormDataSection("postID", postId.ToString()));

        UnityWebRequest request = UnityWebRequest.Post(like_API, dbForm);
        yield return request.SendWebRequest();

        if (request.result != UnityWebRequest.Result.Success) {
            Debug.Log(request.error);
        }
        else {
            Debug.Log("Success");
            Debug.Log("Response: " + request.downloadHandler.text);
        }
    }

    void DownloadImage(string imgfile, RawImage targetImg) {
        string url = get_Img_API + imgfile;
        var request = new RequestHelper {
            Uri = url,
            Method = "GET",
            Timeout = 60,
            Retries = 5,
            DownloadHandler = new DownloadHandlerTexture(),
            RetrySecondsDelay = 2,
            RetryCallback = (err, retries) => { Debug.Log("Repeat error Check version: " + err.Message); }, //See the error before retrying the request
            EnableDebug = true,
        };


        RestClient.Request(request).Then(response => {
            if (response.Text != "error") { //check if image was found
                Texture2D texture = ((DownloadHandlerTexture)response.Request.downloadHandler).texture;
                if (texture == null) return;

                targetImg.texture = texture;
                var ratio = targetImg.GetComponent<AspectRatioFitter>();
                //ratio.aspectMode = AspectRatioFitter.AspectMode.EnvelopeParent;
                ratio.aspectRatio = (float)texture.width / (float)texture.height;
            }
        }).Catch(err => Debug.Log("Error: " + err)
        ).Done();
    }

    public void ShowDeleteWarning() {
        deleteWarning.GetLocalizedStringAsync().Completed += asyncOp => {
            EasyMessageBox.Show(
                message: asyncOp.Result,
                button1Action: delegate { StartCoroutine(DeletePostRoutine()); },
                button1Text: Values.LOCALIZED_TERMS[Values.LOC_KEY_CONFIRM],
                button2Text: Values.LOCALIZED_TERMS[Values.LOC_KEY_CANCEL]);
        };
    }

    private IEnumerator DeletePostRoutine() {
        List<IMultipartFormSection> dbForm = new List<IMultipartFormSection>();

        dbForm.Add(new MultipartFormDataSection("username", GameManager.SaveData.communityData.username));
        dbForm.Add(new MultipartFormDataSection("password", GameManager.SaveData.communityData.password));
        dbForm.Add(new MultipartFormDataSection("postID", postId.ToString()));

        UnityWebRequest request = UnityWebRequest.Post(delete_API, dbForm);
        yield return request.SendWebRequest();

        if (request.result != UnityWebRequest.Result.Success) {
            Debug.Log(request.error);
        }
        else {
            Debug.Log("Success");
            Debug.Log("Response: " + request.downloadHandler.text);

            Destroy(gameObject);
        }
    }
}
