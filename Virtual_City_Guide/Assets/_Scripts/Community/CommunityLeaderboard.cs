using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Proyecto26;

public class CommunityLeaderboard : MonoBehaviour {
    private const string ranking_API = "https://www.stempelpass.de/APP/API/getRanking.php";
    private const string user_ranking_API = "https://www.stempelpass.de/APP/API/getUserRanking.php?username=";

    private LeaderboardEntry[] leaderboardEntries;
    [SerializeField] private TextMeshProUGUI userPoints;
    [SerializeField] private string locUserPoints;

    [System.Serializable]
    private class RankingEntries {
        public RankingEntry[] rankingEntries;
    }

    [System.Serializable]
    private class RankingEntry {
        public string username;
        public int profileImgId;
        public int profileColorId;
        public int points;
        public int communityPoints;
    }

    private void Start() {
        locUserPoints = userPoints.text;
        leaderboardEntries = GetComponentsInChildren<LeaderboardEntry>(true);
        GetRanking();

        //if (GameManager.SaveData.communityData != null && !string.IsNullOrEmpty(GameManager.SaveData.communityData.username)) {
        //    Debug.Log("Get Points");
        //    SetUserPoints(0, 0, GameManager.SaveData.communityData.username);
        //}
    }

    private void OnEnable() {
        if (GameManager.SaveData.communityData != null && !string.IsNullOrEmpty(GameManager.SaveData.communityData.username)) {
            Debug.Log("Get Points");
            SetUserPoints(0, 0, GameManager.SaveData.communityData.username);
        }
    }

    private void SetUserPoints(int profileColor, int profilePicture, string username) {
        var request = new RequestHelper {
            Uri = user_ranking_API + username,
            Method = "GET",
            Timeout = 60,
            Retries = 5,
            RetrySecondsDelay = 2,
            RetryCallback = (err, retries) => { Debug.Log("Repeat error Check version: " + err.Message); }, //See the error before retrying the request
            EnableDebug = true,
        };


        RestClient.Request(request).Then(response => {
            //get list from JSON
            if (string.IsNullOrEmpty(response.Text)) {
                //no posts found
                return;
            }

            Debug.Log(response.Text);

            userPoints.text = locUserPoints + response.Text;


        }).Catch(err => {
            Debug.Log("Error: " + err);
        }).Done();
    }

    private void GetRanking() {
        var request = new RequestHelper {
            Uri = ranking_API,
            Method = "GET",
            Timeout = 60,
            Retries = 5,
            RetrySecondsDelay = 2,
            RetryCallback = (err, retries) => { Debug.Log("Repeat error Check version: " + err.Message); }, //See the error before retrying the request
            EnableDebug = true,
        };


        RestClient.Request(request).Then(response => {
            //get list from JSON
            if (string.IsNullOrEmpty(response.Text)) {
                //no posts found
                return;
            }

            var res = "{\"rankingEntries\": " + response.Text + "}";
            Debug.Log(res);
            RankingEntries rankings = JsonUtility.FromJson<RankingEntries>(res);
            if (rankings == null) {
                return;
            }

            RankingEntry[] rankingEntries = rankings.rankingEntries;
            Debug.Log(rankingEntries.Length);
            for (int i = 0; i < rankingEntries.Length; i++) {
                leaderboardEntries[i].FillInformation(rankingEntries[i].username, rankingEntries[i].profileImgId, rankingEntries[i].profileColorId, (rankingEntries[i].points + rankingEntries[i].communityPoints), i + 1);
                leaderboardEntries[i].gameObject.SetActive(true);
            }

        }).Catch(err => {
            Debug.Log("Error: " + err);
        }).Done();
    }
}
