using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.Networking;
using Proyecto26;
using Unity.Services.Core;
using System;
using System.Collections.Generic;
using Unity.Services.Analytics;
using UnityEngine.UI;
using bunLace.Tools;
using bunLace.Constants;
using UnityEngine.Purchasing.Security;
using UnityEngine.Localization;
using bunLace.Singleton;

public class IAPManager : PersistentSingleton<IAPManager>, IStoreListener {
    public GameObject buttonPrefab;
    public Transform content;

    private static IStoreController m_StoreController;          // The Unity Purchasing system.
    private static IExtensionProvider m_StoreExtensionProvider; // The store-specific Purchasing subsystems.
    private static UnityEngine.Purchasing.Product test_product = null;

    IGooglePlayStoreExtensions m_GooglePlayStoreExtensions;

    private CrossPlatformValidator purchaseValidator;

    private readonly string catalog_API = "https://stempelpass.de/APP/API/catalog.php";

    Dictionary<int, string> items = new Dictionary<int, string>();
    public TMPro.TextMeshProUGUI myText;

    private Boolean return_complete = true;

    private event Action<int> MyAction;

    [SerializeField]
    private LocalizedString locPurchaseFailed, locPurchaseSuccess;

    [System.Serializable]
    public class PremiumRegion {
        public int regionID;
        public string purchaseID;
    }

    async void Start() {
        try {
            await UnityServices.InitializeAsync();
            List<string> consentIdentifiers = await AnalyticsService.Instance.CheckForRequiredConsents();
        }
        catch (ConsentCheckException e) {
            MyDebug("Consent: :" + e.ToString());  // Something went wrong when checking the GeoIP, check the e.Reason and handle appropriately.
        }

        MyAction += myFunction;

        //Initialize();
    }


    public void Initialize() {
        if (IsInitialized()) {
            return;
        }

        var module = StandardPurchasingModule.Instance();
        module.useFakeStoreUIMode = FakeStoreUIMode.StandardUser;
        //module.useFakeStoreAlways = true;

        var builder = ConfigurationBuilder.Instance(module);
        builder.Configure<IGooglePlayConfiguration>().SetDeferredPurchaseListener(OnDeferredPurchase);
        builder.Configure<IGooglePlayConfiguration>().SetQueryProductDetailsFailedListener(MyAction);

        var request = new RequestHelper {
            Uri = catalog_API,
            Method = "GET",
            Timeout = 60,
            Retries = 5,
            RetrySecondsDelay = 2,
            RetryCallback = (err, retries) => { MyDebug("Repeat error Check catalog items: " + err.Message); }, //See the error before retrying the request
            EnableDebug = true,
        };


        RestClient.Request(request).Then(response => {
            PremiumRegion[] premiumRegions;
            premiumRegions = JsonHelper.FromJson<PremiumRegion>(response.Text);

            foreach (PremiumRegion item in premiumRegions) {
                builder.AddProduct(item.purchaseID, ProductType.NonConsumable);
                MyDebug(item.purchaseID);
                items.Add(item.regionID, item.purchaseID);
            }

            UnityPurchasing.Initialize(this, builder);
        }).Catch(err => MyDebug("Update Check Error: " + err)).Done();

    }


    void myFunction(int myInt) {
        MyDebug("Listener = " + myInt.ToString());
    }

    public bool IsInitialized() {
        return m_StoreController != null && m_StoreExtensionProvider != null;
    }

    void OnDeferredPurchase(UnityEngine.Purchasing.Product product) {
        MyDebug($"Purchase of {product.definition.id} is deferred");
    }

    public delegate void PurchaseSuccess();
    public static event PurchaseSuccess OnPurchaseSuccess;

    public void CompletePurchase() {
        if (test_product == null)
            MyDebug("Cannot complete purchase, product not initialized.");
        else {
            m_StoreController.ConfirmPendingPurchase(test_product);
            MyDebug("Completed purchase with " + test_product.transactionID.ToString());
        }
    }

    public void ToggleComplete() {
        return_complete = !return_complete;
        MyDebug("Complete = " + return_complete.ToString());

    }
    public void RestorePurchases() {
        m_StoreExtensionProvider.GetExtension<IAppleExtensions>().RestoreTransactions(result => {

            if (result) {
                MyDebug("Restore purchases succeeded.");
                Dictionary<string, object> parameters = new Dictionary<string, object>()
                {
                    { "restore_success", true },
                };
                AnalyticsService.Instance.CustomData("myRestore", parameters);
            }
            else {
                MyDebug("Restore purchases failed.");
                Dictionary<string, object> parameters = new Dictionary<string, object>()
                {
                    { "restore_success", false },
                };
                AnalyticsService.Instance.CustomData("myRestore", parameters);
            }

            AnalyticsService.Instance.Flush();
        });

    }

    public void BuyProductID(string productId) {
        if (IsInitialized()) {
            UnityEngine.Purchasing.Product product = m_StoreController.products.WithID(productId);
            if (HasPurchased(product)) return; //cannot buy product twice

            if (product != null && product.availableToPurchase) {
                MyDebug(string.Format("Purchasing product:" + product.definition.id.ToString()));
                m_StoreController.InitiatePurchase(product);
            }
            else {
                MyDebug("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
            }
        }
        else {
            MyDebug("BuyProductID FAIL. Not initialized.");
        }
    }

    public bool IsPurchasable(int id) {
        Debug.Log(items.Count + " in Dict");
        Debug.Log(items.ContainsKey(id));
        return items.ContainsKey(id);
    }

    public bool HasPurchased(string productID) {
        UnityEngine.Purchasing.Product product = m_StoreController.products.WithID(productID);
        return HasPurchased(product);
    }

    public bool HasPurchased(UnityEngine.Purchasing.Product product) {
        return product.hasReceipt && IsPurchaseValid(product);
    }

    public void OnInitialized(IStoreController controller, IExtensionProvider extensions) {
        MyDebug("OnInitialized: PASS");

        m_StoreController = controller;
        m_StoreExtensionProvider = extensions;
        m_GooglePlayStoreExtensions = extensions.GetExtension<IGooglePlayStoreExtensions>();

        //List<string> purchased = CheckPreviousPurchases();

        //foreach (var item in items) {
        //    bool purchasedProduct = purchased.Contains(item.Value);

        //    GameObject btn = Instantiate(buttonPrefab, content);
        //    var purchase = btn.GetComponent<Button>();
        //    btn.FindComponentInChildWithTag<TMPro.TextMeshProUGUI>(Tags.NAME_REPLACE).text = item.Value;

        //    purchase.onClick.AddListener(delegate { BuyProductID(item.Value); });
        //    purchase.interactable = !purchasedProduct;
        //}
    }

    public void OnInitializeFailed(InitializationFailureReason error) {
        // Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
        MyDebug("OnInitializeFailed InitializationFailureReason:" + error);
    }

    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args) {
        test_product = args.purchasedProduct;

        //var validator = new CrossPlatformValidator(GooglePlayTangle.Data(), AppleTangle.Data(), Application.identifier);
        //var result = validator.Validate(args.purchasedProduct.receipt);
        //MyDebug("Validate = " + result.ToString());

        if (m_GooglePlayStoreExtensions.IsPurchasedProductDeferred(test_product)) {
            //The purchase is Deferred.
            //Therefore, we do not unlock the content or complete the transaction.
            //ProcessPurchase will be called again once the purchase is Purchased.
            return PurchaseProcessingResult.Pending;
        }

        if (return_complete) {
            MyDebug(string.Format("ProcessPurchase: Complete. Product:" + args.purchasedProduct.definition.id + " - " + test_product.transactionID.ToString()));
            locPurchaseSuccess.GetLocalizedStringAsync().Completed += asyncOp => {
                EasyMessageBox.Show(asyncOp.Result);
            };
            OnPurchaseSuccess?.Invoke();
            return PurchaseProcessingResult.Complete;
        }
        else {
            MyDebug(string.Format("ProcessPurchase: Pending. Product:" + args.purchasedProduct.definition.id + " - " + test_product.transactionID.ToString()));
            return PurchaseProcessingResult.Pending;
        }

    }

    public void OnPurchaseFailed(UnityEngine.Purchasing.Product product, PurchaseFailureReason failureReason) {
        MyDebug(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
        locPurchaseFailed.GetLocalizedStringAsync().Completed += asyncOp => {
            EasyMessageBox.Show(asyncOp.Result);
        };
    }

    private List<string> CheckPreviousPurchases() {
        List<string> purchased = new List<string>();
        foreach (UnityEngine.Purchasing.Product item in m_StoreController.products.all) {
            if (item.hasReceipt && IsPurchaseValid(item)) {
                MyDebug("In list for  " + item.receipt.ToString());
                purchased.Add(item.definition.id);
            }
        }

        return purchased;
    }


    private bool IsPurchaseValid(UnityEngine.Purchasing.Product product) {
        if (purchaseValidator != null) {
            try {
                purchaseValidator.Validate(product.receipt);
            }
            catch (IAPSecurityException reason) {
                Debug.LogWarning("Invalid IAP receipt: " + reason);
                return false;
            }
        }

        return true;
    }

    public string GetPurchaseID(int regionID) {
        if (items.ContainsKey(regionID)) {
            return items[regionID];
        }
        else {
            Debug.LogWarning("region is not premium | " + regionID);
            return string.Empty;
        }
    }

    private void MyDebug(string debug) {
        Debug.Log(debug);
        //myText.text += "\r\n" + debug;
    }

    public void OnInitializeFailed(InitializationFailureReason error, string message) {
        Debug.Log("IAP initialization failed: " + error + " || " + message);
    }
}