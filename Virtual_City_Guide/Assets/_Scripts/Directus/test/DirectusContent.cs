using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using Directus.Model;
using Proyecto26;
using UnityEngine;
using UnityEngine.Localization;

namespace Directus {
    public class DirectusContent : MonoBehaviour {
        [Header("Last Content Update")]
        public int DirectusActivityLastId;

        [SerializeField]
        private string DirectusActivityLastUpdate;

        [Space]
        [Header("Directus Token")]
        [SerializeField]
        private string currentToken;

        [Space]
        [Header("Directus Collection Length")]

        private List<int> collectionLengths = new List<int>();

        private DirectusDBUpdate _directusDBUpdate;
        public string currentDBVersion;

        [Header("Directus Content")]

        public DirectusAchievements.ContentCollection achievements;
        public DirectusPOI.ContentCollection poi;
        public DirectusRegion.ContentCollection region;
        public DirectusTour.ContentCollection tour;
        public DirectusAdditionalContent.ContentCollection additionalContent;
        public DirectusLocalization.ContentCollection localization;
        public DirectusReward.ContentCollection reward;
        public DirectusPOIRegion.ContentCollection poiRegion;
        public DirectusAdditionalContentPOI.ContentCollection additionalContentPOI;
        public DirectusVersionControl.ContentCollection versionControl;

        [SerializeField] private LocalizedString checkDB, updatingDB, preloadAssets;
        [HideInInspector] public string checkDbText, updatingDbText, preloadAssetsText;

        private async void Start() {
            _directusDBUpdate = GetComponent<DirectusDBUpdate>();
            checkDbText = await checkDB.GetLocalizedStringAsync();
            updatingDbText = await updatingDB.GetLocalizedStringAsync();
            preloadAssetsText = await preloadAssets.GetLocalizedStringAsync();
        }

        private void OnEnable() {
            DirectusManager.OnAuthenticateSuccess += OnAuthenticateSuccess;
        }

        private void OnDisable() {
            DirectusManager.OnAuthenticateSuccess -= OnAuthenticateSuccess;
        }

        /// <summary>
        /// API Call to Auth and get a access token
        /// this token is 20 min valid
        /// </summary>
        public void GetCMSToken() {
            DownloadProgress.Instance.UpdateProgressText(checkDbText);
            DirectusManager.Instance.Authenticate();
        }


        private void OnAuthenticateSuccess(string token) {
            if (string.IsNullOrEmpty(token)) {
                ForceSkip("Authentication failed");
            }
            else {
                currentToken = token;
                CheckDirectusActivity();
            }
        }


        public delegate UniTask NoUpdate();
        public static event NoUpdate OnNoUpdate;

        /// <summary>
        /// API Call to get the latest content activity
        /// we check for create or update collection items
        /// </summary>
        public void CheckDirectusActivity() {
            RestClient.DefaultRequestHeaders["Authorization"] = "Bearer " + currentToken;
            var currentRequest = new RequestHelper {
                Uri = DirectusManager.DirectusURL + DirectusManager.DirectusLastActivity,
                Method = "GET",
                Timeout = 60,
                Retries = 5, //Number of retries
                RetrySecondsDelay = 2, //Seconds of delay to make a retry
                RetryCallback = (err, retries) => {
                    Debug.Log("Repeat error CheckDirectusActivity: " + err.Message);
                }, //See the error before retrying the request
                EnableDebug = true, //See logs of the requests for debug mode
            };

            RestClient.Request(currentRequest).Then(response => {
                //var activity = JsonUtility.FromJson<DirectusActivity.ContentCollection>(response.Text);
                //DirectusActivityLastId = activity.data[0].id;

                //if (!PlayerPrefs.HasKey(DirectusManager.DirectusPlayerPrefs)) {
                //    DownloadProgress.Instance.UpdateProgressText(updatingDbText);
                //    GetCollectionLength();
                //}
                //else {
                //    //check if we have a new content update
                //    if (PlayerPrefs.GetInt(DirectusManager.DirectusPlayerPrefs) < DirectusActivityLastId) {
                //        DownloadProgress.Instance.UpdateProgressText(updatingDbText);
                //        GetCollectionLength();
                //    }
                //    else {
                //        //content is up to date
                //        DebugMe.Log("No DB Update available");
                //        DownloadProgress.Instance.UpdateProgressText(preloadAssetsText);
                //        OnNoUpdate?.Invoke();
                //        //DirectusSignals.DIRECTUS_UPDATES_AVAILABE.Dispatch(false, null);
                //    }
                //}

                var result = JsonUtility.FromJson<DirectusVersionControl.ContentCollection>(response.Text);
                string dbVersion = result.data[0].Version;
                currentDBVersion = dbVersion;

                Debug.Log($"BUNNY: Directus Database is at {dbVersion}");
                if (!PlayerPrefs.HasKey("directusDb")) {
                    //first time getting DB
                    DownloadProgress.Instance.UpdateProgressText(updatingDbText);
                    GetCollectionLength();
                }
                else {
                    //check if we have a new content update
                    if (PlayerPrefs.GetString("directusDb") != dbVersion) {
                        //current db is not up to date
                        DownloadProgress.Instance.UpdateProgressText(updatingDbText);
                        GetCollectionLength();
                    }
                    else {
                        //content is up to date
                        Debug.Log("No DB Update available");
                        DownloadProgress.Instance.UpdateProgressText(preloadAssetsText);
                        OnNoUpdate?.Invoke();
                    }
                }
            })
            .Catch(err => {
                ForceSkip(err.Message);
            })
            .Done();
        }

        /// <summary>
        /// API Call to get the amount of current collection entries
        /// </summary>
        public void GetCollectionLength() {
            // default request headers for all requests
            RestClient.DefaultRequestHeaders["Authorization"] = "Bearer " + currentToken;


            List<RequestHelper> RequestAggregates = new List<RequestHelper>();

            for (int i = 0; i < DirectusManager.Instance.DirectusCollections.Length; i++) {
                RequestAggregates.Add(new RequestHelper {
                    Uri = DirectusManager.DirectusURL + "items/" + DirectusManager.Instance.DirectusCollections[i] + DirectusManager.DirectusCollectionCount,
                    Method = "GET",
                    Timeout = 60,
                    Retries = 5,
                    RetrySecondsDelay = 2,
                    RetryCallback = (err, retries) => { Debug.Log("Repeat error CheckDirectusActivity: " + err.Message); }, //See the error before retrying the request
                    EnableDebug = true,
                });
            }

            //Collection Achievement
            var currentRequest = RequestAggregates[0];

            RestClient.Request<DirectusCollectionCount.ContentCollection>(currentRequest).Then(response => {
                collectionLengths.Add(response.data[0].count.id);
                //Collection Additional Content
                currentRequest = RequestAggregates[1];
                return RestClient.Request<DirectusCollectionCount.ContentCollection>(currentRequest);
            }).Then(response => {
                //Collection POI
                collectionLengths.Add(response.data[0].count.id);
                currentRequest = RequestAggregates[2];
                return RestClient.Request<DirectusCollectionCount.ContentCollection>(currentRequest);
            }).Then(response => {
                //Collection Region
                collectionLengths.Add(response.data[0].count.id);
                currentRequest = RequestAggregates[3];
                return RestClient.Request<DirectusCollectionCount.ContentCollection>(currentRequest);
            }).Then(response => {
                //Collection Tour
                collectionLengths.Add(response.data[0].count.id);
                currentRequest = RequestAggregates[4];
                return RestClient.Request<DirectusCollectionCount.ContentCollection>(currentRequest);
            }).Then(response => {
                collectionLengths.Add(response.data[0].count.id);
                currentRequest = RequestAggregates[5];
                return RestClient.Request<DirectusCollectionCount.ContentCollection>(currentRequest);
            }).Then(response => {
                collectionLengths.Add(response.data[0].count.id);
                currentRequest = RequestAggregates[6];
                return RestClient.Request<DirectusCollectionCount.ContentCollection>(currentRequest);
            }).Then(response => {
                collectionLengths.Add(response.data[0].count.id);
                currentRequest = RequestAggregates[7];
                return RestClient.Request<DirectusCollectionCount.ContentCollection>(currentRequest);
            }).Then(response => {
                collectionLengths.Add(response.data[0].count.id);
                currentRequest = RequestAggregates[8];
                return RestClient.Request<DirectusCollectionCount.ContentCollection>(currentRequest);
            }).Then(response => {
                collectionLengths.Add(response.data[0].count.id);
                currentRequest = RequestAggregates[9];
                return RestClient.Request<DirectusCollectionCount.ContentCollection>(currentRequest);
            }).Then(response => {
                collectionLengths.Add(response.data[0].count.id);
                return RestClient.Request<DirectusCollectionCount.ContentCollection>(currentRequest);
            }).Catch(err => {
                ForceSkip(err.Message);
            })
            .Done(GetCollectionLengthDone);
        }

        /// <summary>
        /// Get Collection Length Done Action
        /// </summary>
        private void GetCollectionLengthDone() {
            GetCollections(0, 1);
        }

        private int _collectionsCount;
        private int _pageCount;

        /// <summary>
        /// Get Collection Content
        /// </summary>
        /// <param name="collections"></param>
        /// <param name="pageCount"></param>
        private void GetCollections(int collections, int pageCount) {
            _pageCount = pageCount;

            RestClient.DefaultRequestHeaders["Authorization"] = "Bearer " + currentToken;
            var currentRequest = new RequestHelper {
                Uri = DirectusManager.DirectusURL + "items/" + DirectusManager.Instance.DirectusCollections[collections] + "?page=" + pageCount,
                Method = "GET",
                Timeout = 60,
                Retries = 5, //Number of retries
                RetrySecondsDelay = 2, //Seconds of delay to make a retry
                RetryCallback = (err, retries) => { Debug.Log("Repeat error CheckDirectusActivity: " + err.Message); }, //See the error before retrying the request
                EnableDebug = true, //See logs of the requests for debug mode
            };
            Debug.Log("Collection Call: " + DirectusManager.DirectusURL + "items/" + DirectusManager.Instance.DirectusCollections[collections] + "?page=" + pageCount);
            int pageAmount = collectionLengths[collections] / 100 + 1;

            Debug.Log($"Current Collection: {collections}; {pageCount} of {pageAmount}");
            DownloadProgress.Instance.OnDownloadProgress((float)pageCount / (float)pageAmount);

            RestClient.Request(currentRequest).Then(response => {
                //TODO Store info into dif collections or DB tables
                switch (collections) {
                    case 0:
                        var achievementsJson = JsonUtility.FromJson<DirectusAchievements.ContentCollection>(response.Text);
                        foreach (var item in achievementsJson.data) {
                            achievements.data.Add(item);
                        }
                        break;
                    case 1:
                        var additionalContentJson = JsonUtility.FromJson<DirectusAdditionalContent.ContentCollection>(response.Text);
                        foreach (var item in additionalContentJson.data) {
                            additionalContent.data.Add(item);
                        }
                        break;
                    case 2:
                        var poiJson = JsonUtility.FromJson<DirectusPOI.ContentCollection>(response.Text);
                        foreach (var item in poiJson.data) {
                            poi.data.Add(item);
                        }
                        break;
                    case 3:
                        var tourJson = JsonUtility.FromJson<DirectusTour.ContentCollection>(response.Text);
                        foreach (var item in tourJson.data) {
                            tour.data.Add(item);
                        }
                        break;
                    case 4:
                        var regionJson = JsonUtility.FromJson<DirectusRegion.ContentCollection>(response.Text);
                        foreach (var item in regionJson.data) {
                            region.data.Add(item);
                        }
                        break;
                    case 5:
                        var localizationJson = JsonUtility.FromJson<DirectusLocalization.ContentCollection>(response.Text);
                        foreach (var item in localizationJson.data) {
                            localization.data.Add(item);
                        }
                        break;
                    case 6:
                        var rewardJson = JsonUtility.FromJson<DirectusReward.ContentCollection>(response.Text);
                        Debug.Log(response.Text);
                        foreach (var item in rewardJson.data) {
                            reward.data.Add(item);
                        }
                        break;
                    case 7:
                        var poiRegionJson = JsonUtility.FromJson<DirectusPOIRegion.ContentCollection>(response.Text);
                        Debug.Log(response.Text);
                        foreach (var item in poiRegionJson.data) {
                            poiRegion.data.Add(item);
                        }
                        break;
                    case 8:
                        var additionalContentPoiJson = JsonUtility.FromJson<DirectusAdditionalContentPOI.ContentCollection>(response.Text);
                        Debug.Log(response.Text);
                        foreach (var item in additionalContentPoiJson.data) {
                            additionalContentPOI.data.Add(item);
                        }
                        break;
                    case 9:
                        var versionJson = JsonUtility.FromJson<DirectusVersionControl.ContentCollection>(response.Text);
                        foreach (var item in versionJson.data) {
                            versionControl.data.Add(item);
                        }
                        break;
                }
            })
            .Catch(err => {
                ForceSkip(err.Message);
            }).Done(ValidateNextCollection);
        }

        /// </summary>
        private void ValidateNextCollection() {
            if (_collectionsCount == DirectusManager.Instance.DirectusCollections.Length - 1) {
                Debug.Log("DONE Collection call");
                //call directus file api
                //_directusFileLibrary.GetFiles(currentToken);

                //update sqlite db
                _directusDBUpdate.DBUpdate();
            }
            else {
                if (_collectionsCount < DirectusManager.Instance.DirectusCollections.Length && _pageCount == collectionLengths[_collectionsCount] / 100 + 1) {
                    _collectionsCount++;
                    _pageCount = 1;
                    GetCollections(_collectionsCount, _pageCount);
                }
                else {
                    if (_pageCount < Mathf.Ceil(collectionLengths[_collectionsCount] / 100 + 1)) {
                        _pageCount++;
                        GetCollections(_collectionsCount, _pageCount);
                    }
                }
            }
        }

        private void ForceSkip(string err) {
            Debug.LogWarning("BUNNY: " + err);
            OnNoUpdate?.Invoke();
        }
    }
}