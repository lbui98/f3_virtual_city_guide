using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using bunLace.Database;
using Directus.Model;
using SimpleSQL;
using UnityEngine;
using Cysharp.Threading.Tasks;

namespace Directus {
    public class DirectusDBUpdate : MonoBehaviour {
        private MapTables _mapTables = new MapTables();
        private int count;

        public void DBUpdate() {
            count = 0;
            Debug.Log("Update DB");
            GameManager.Instance.DatabaseManager.debugTrace = true;
            //sql drop tables
            string dropSqlAchievements = "DROP TABLE IF EXISTS Achievements";
            string dropSqlAdditionalContent = "DROP TABLE IF EXISTS AdditionalContent";
            string dropSqlPoi = "DROP TABLE IF EXISTS POI";
            string dropSqlTour = "DROP TABLE IF EXISTS Tour";
            string dropSqlRegion = "DROP TABLE IF EXISTS Region";
            string dropSqlLocalization = "DROP TABLE IF EXISTS Localization";
            string dropSqlVersionControl = "DROP TABLE IF EXISTS VersionControl";
            string dropSqlReward = "DROP TABLE IF EXISTS Reward";
            string dropSqlPoiRegion = "DROP TABLE IF EXISTS POIRegion";
            string dropSqlAdditionalContentPoi = "DROP TABLE IF EXISTS AdditionalContentPOI";

            //drop tables
            GameManager.Instance.DatabaseManager.BeginTransaction();
            GameManager.Instance.DatabaseManager.Execute(dropSqlAchievements);
            GameManager.Instance.DatabaseManager.Execute(dropSqlAdditionalContent);
            GameManager.Instance.DatabaseManager.Execute(dropSqlPoi);
            GameManager.Instance.DatabaseManager.Execute(dropSqlTour);
            GameManager.Instance.DatabaseManager.Execute(dropSqlRegion);
            GameManager.Instance.DatabaseManager.Execute(dropSqlLocalization);
            GameManager.Instance.DatabaseManager.Execute(dropSqlReward);
            GameManager.Instance.DatabaseManager.Execute(dropSqlVersionControl);
            GameManager.Instance.DatabaseManager.Execute(dropSqlAdditionalContentPoi);
            GameManager.Instance.DatabaseManager.Execute(dropSqlPoiRegion);
            GameManager.Instance.DatabaseManager.Commit();

            //create new tables
            string[] createTableSql =
            {
                "CREATE TABLE IF NOT EXISTS `Achievements` (`ID` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, `RegionID` INTEGER, `NameLocKey` INTEGER, `DescriptionLocKey` INTEGER,`AtlasReference` TEXT, `ImgReference` TEXT, `ConditionsJSON` TEXT, `IsRegional` INTEGER)",
                "CREATE TABLE IF NOT EXISTS `AdditionalContent` (`ID` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, `NameLocKey` INTEGER, `DescriptionLocKey` INTEGER, `AtlasReference` TEXT, `PreviewImgReference` TEXT, `AssetReference` TEXT, `MediaType` INTEGER, `RequiresPOIUnlocked` INTEGER)",
                "CREATE TABLE IF NOT EXISTS `POI` (`ID` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, `NameLocKey` INTEGER, `DescriptionLocKey` INTEGER, `MoreDescriptionLocKey` INTEGER, `ContactInfoLocKey` INTEGER, `Latitude` REAL, `Longitude` REAL, `Perimeter` INTEGER, `AtlasReference` TEXT, `ImgReference` TEXT, `WebsiteURL` TEXT, `Telephone` TEXT, `UnlockPoints` INTEGER, `Order` INTEGER, `SponsorReference` TEXT, `TypeInt` INTEGER, `StampAtlasReference` TEXT, `StampReference` TEXT, `StampColor` TEXT)",
                "CREATE TABLE IF NOT EXISTS `Region` (`ID` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, `Name` TEXT, `DescriptionLocKey` INTEGER, `Latitude` REAL, `Longitude` REAL, `AtlasReference` TEXT, `ImgReference` TEXT, `LogoImgReference` TEXT, `DefaultTour` INTEGER, `HelpURL` TEXT, `ColorBackground` TEXT, `ColorForeground` TEXT, `ColorSelected` TEXT, `ColorUnselected` TEXT, `ColorTextDark` TEXT, `ColorTextLight` TEXT, `IsActive` INTEGER, `Link` TEXT, `Order` INTEGER, `AdReference` TEXT, `PreviewReference` TEXT, `ParentID` INTEGER)",
                "CREATE TABLE IF NOT EXISTS `Tour` (`ID` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, `NameLocKey` INTEGER, `RegionID` INTEGER, `TourType` INTEGER, `AtlasReference` TEXT, `ImgPreviewReference` TEXT, `POIIDsJSON` TEXT)",
                "CREATE TABLE IF NOT EXISTS `Localization` (`ID` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, `Key` TEXT, `DE` TEXT, `EN` TEXT, `FR` TEXT, `ES` TEXT, `CN` TEXT, `JP` TEXT)",
                "CREATE TABLE IF NOT EXISTS `VersionControl` (`ID` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, `Version` TEXT)",
                "CREATE TABLE IF NOT EXISTS `POIRegion` (`ID` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, `POIID` Integer, `RegionID` Integer)",
                "CREATE TABLE IF NOT EXISTS `AdditionalContentPOI` (`ID` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, `POIID` Integer, `AdditionalContentID` Integer)",
                "CREATE TABLE IF NOT EXISTS `Reward` (`ID` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, `RegionID` INTEGER, `Code` TEXT, `RequiredPoints` INTEGER, `RewardMessage` TEXT)"
            };

            for (int i = 0; i < createTableSql.Length; i++) {
                try {
                    Debug.Log("DB create table " + createTableSql[i]);
                    GameManager.Instance.DatabaseManager.Execute(createTableSql[i]);
                }
                catch (SQLiteException ex) {
                    Debug.Log("<color=red> error Log: " + ex.Result + " sql string: " + createTableSql[i] + "</color>");
                }
            }

            MapDbTables();
        }

        private void MapDbTables() {
            DirectusContent directusContent = GetComponent<DirectusContent>();


            List<Achievements> achievementsList = new List<Achievements>();
            foreach (var item in directusContent.achievements.data) {
                achievementsList.Add(new Achievements { ID = item.ID, RegionID = item.RegionID, NameLocKey = item.NameLocKey, DescriptionLocKey = item.DescriptionLocKey, AtlasReference = item.AtlasReference, ImgReference = item.ImgReference, ConditionsJSON = item.ConditionsJSON, IsRegional = item.IsRegional });
            }

            List<AdditionalContent> additionalContentList = new List<AdditionalContent>();
            List<int> additionalContentToIgnore = new List<int>();
            Version appVersion = new Version(Application.version);
            foreach (var item in directusContent.additionalContent.data) {
                if (item.RequiredVersion != Application.version) {
                    Version required = new Version(item.RequiredVersion);
                    if (appVersion.CompareTo(required) < 0) {
                        additionalContentToIgnore.Add(item.ID);
                        continue;
                    }
                }
                additionalContentList.Add(new AdditionalContent() { ID = item.ID, NameLocKey = item.NameLocKey, DescriptionLocKey = item.DescriptionLocKey, AtlasReference = item.AtlasReference, PreviewImgReference = item.PreviewImgReference, AssetReference = item.AssetReference, MediaType = item.MediaType, RequiresPOIUnlocked = item.RequiresPOIUnlocked });
            }

            List<POI> poiList = new List<POI>();
            foreach (var item in directusContent.poi.data) {
                poiList.Add(new POI() { ID = item.ID, NameLocKey = item.NameLocKey, DescriptionLocKey = item.DescriptionLocKey, MoreDescriptionLocKey = item.MoreDescriptionLocKey, ContactInfoLocKey = item.ContactInfoLocKey, Latitude = item.Latitude, Longitude = item.Longitude, Perimeter = item.Perimeter, AtlasReference = item.AtlasReference, ImgReference = item.ImgReference, WebsiteURL = item.WebsiteURL, Telephone = item.Telephone, UnlockPoints = item.UnlockPoints, Order = item.Order, SponsorReference = item.SponsorReference, TypeInt = item.Type, StampAtlasReference = item.StampAtlasReference, StampReference = item.StampReference, StampColor = item.StampColor });
            }

            List<Region> regionList = new List<Region>();
            foreach (var item in directusContent.region.data) {
                regionList.Add(new Region() { ID = item.ID, Name = item.Name, DescriptionLocKey = item.DescriptionLocKey, Latitude = item.Latitude, Longitude = item.Longitude, AtlasReference = item.AtlasReference, ImgReference = item.ImgReference, LogoImgReference = item.LogoImgReference, DefaultTour = item.DefaultTour, HelpURL = item.HelpURL, ColorBackground = item.ColorBackground, ColorForeground = item.ColorForeground, ColorSelected = item.ColorSelected, ColorUnselected = item.ColorUnselected, ColorTextDark = item.ColorTextDark, ColorTextLight = item.ColorTextLight, IsActive = item.IsActive, Link = item.HomepageURL, Order = item.Order, AdReference = item.AdReference, PreviewReference = item.PreviewReference, ParentID = item.ParentID });
            }

            List<Tour> tourList = new List<Tour>();
            foreach (var item in directusContent.tour.data) {
                tourList.Add(new Tour() { ID = item.ID, RegionID = item.RegionID, NameLocKey = item.NameLocKey, AtlasReference = item.AtlasReference, ImgPreviewReference = item.ImgPreviewReference, TourType = item.TourType, POIIDsJSON = item.POIIDsJSON });
            }

            List<Localization> localizationList = new List<Localization>();
            foreach (var item in directusContent.localization.data) {
                localizationList.Add(new Localization() { ID = item.ID, Key = item.Key, DE = item.DE, EN = item.EN, FR = item.FR, ES = item.ES, CN = item.CN, JP = item.JP });
            }

            List<Reward> rewardList = new List<Reward>();
            foreach (var item in directusContent.reward.data) {
                rewardList.Add(new Reward() { ID = item.ID, RegionID = item.RegionID, Code = item.Code, RequiredPoints = item.RequiredPoints, RewardMessage = item.RewardMessage });
            }

            List<POIRegion> poiRegionList = new List<POIRegion>();
            foreach (var item in directusContent.poiRegion.data) {
                poiRegionList.Add(new POIRegion() { ID = item.id, RegionID = item.Region_ID, POIID = item.POI_ID });
            }


            List<AdditionalContentPOI> additionalContentPOIList = new List<AdditionalContentPOI>();
            foreach (var item in directusContent.additionalContentPOI.data) {
                if (additionalContentToIgnore.Contains(item.AdditionalContent2_ID)) continue; //ignore the additional content whose versions did not match earlier
                additionalContentPOIList.Add(new AdditionalContentPOI() { ID = item.id, AdditionalContentID = item.AdditionalContent2_ID, POIID = item.POI_ID });
            }

            List<VersionControl> version = new List<VersionControl>();
            foreach (var item in directusContent.versionControl.data) {
                version.Add(new VersionControl() { ID = item.ID, Version = item.Version });
            }

            _mapTables.achievementsList = achievementsList;
            _mapTables.additionalContentList = additionalContentList;
            _mapTables.poiList = poiList;
            _mapTables.regionList = regionList;
            _mapTables.tourList = tourList;
            _mapTables.localizationList = localizationList;
            _mapTables.rewardList = rewardList;
            _mapTables.poiRegionList = poiRegionList;
            _mapTables.additionalContentPOIList = additionalContentPOIList;
            _mapTables.version = version;

            WriteToDB();
        }

        public delegate UniTask DBWritingDone();
        public static event DBWritingDone OnDBWritingDone;

        private void WriteToDB() {
            DirectusContent directusContent = GetComponent<DirectusContent>();

            if (count < DirectusManager.Instance.DirectusCollections.Length) {
                switch (count) {
                    case 0:
                        StartCoroutine(WriteDatabaseTables(_mapTables.achievementsList));
                        break;
                    case 1:
                        StartCoroutine(WriteDatabaseTables(_mapTables.additionalContentList));
                        break;
                    case 2:
                        StartCoroutine(WriteDatabaseTables(_mapTables.poiList));
                        break;
                    case 3:
                        StartCoroutine(WriteDatabaseTables(_mapTables.regionList));
                        break;
                    case 4:
                        StartCoroutine(WriteDatabaseTables(_mapTables.tourList));
                        break;
                    case 5:
                        StartCoroutine(WriteDatabaseTables(_mapTables.localizationList));
                        break;
                    case 6:
                        StartCoroutine(WriteDatabaseTables(_mapTables.rewardList));
                        break;
                    case 7:
                        StartCoroutine(WriteDatabaseTables(_mapTables.poiRegionList));
                        break;
                    case 8:
                        StartCoroutine(WriteDatabaseTables(_mapTables.additionalContentPOIList));
                        break;
                    case 9:
                        StartCoroutine(WriteDatabaseTables(_mapTables.version));
                        break;
                }
            }
            else {
                Debug.Log("Writing to DB done");
                OnDBWritingDone?.Invoke();
                DownloadProgress.Instance.UpdateProgressText(directusContent.preloadAssetsText);
                //first install of the app

                PlayerPrefs.SetString("directusDb", directusContent.currentDBVersion);
                PlayerPrefs.Save();
            }
        }

        IEnumerator WriteDatabaseTables<T>(List<T> mapTable) {
            yield return new WaitWhile(() => GameManager.Instance.DatabaseManager.IsInTransaction);
            GameManager.Instance.DatabaseManager.InsertAll(mapTable);
            yield return new WaitWhile(() => GameManager.Instance.DatabaseManager.IsInTransaction);
            count++;
            WriteToDB();
        }
    }

    public class MapTables {
        public List<Achievements> achievementsList;
        public List<AdditionalContent> additionalContentList;
        public List<POI> poiList;
        public List<Region> regionList;
        public List<Tour> tourList;
        public List<Localization> localizationList;
        public List<Reward> rewardList;
        public List<POIRegion> poiRegionList;
        public List<AdditionalContentPOI> additionalContentPOIList;
        public List<VersionControl> version;
    }
}