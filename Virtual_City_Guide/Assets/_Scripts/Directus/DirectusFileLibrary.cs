using System.Collections.Generic;
using Directus.Model;
using Proyecto26;
using UnityEngine;

namespace Directus {
    public class DirectusFileLibrary : MonoBehaviour {
        // http://playfit-app.de:8055/files

        // http://playfit-app.de:8055/assets/0781328b-a835-4cb3-8260-139e45d774bc?download&access_token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjJjNjdjMmJjLWNlZWQtNDExNi04YWMzLWFmZThhMTNjYzFjOCIsImlhdCI6MTYzMzQ1NDE0OCwiZXhwIjoxNjMzNDU1MDQ4LCJpc3MiOiJkaXJlY3R1cyJ9.QDoxIlRRrVtIOpx4cQYF87m-0jn7kliF3wVghkQ3kzk

        // http://playfit-app.de:8055/assets/[fileId]?download&access_token=[token]

        private RequestHelper currentRequest;
        public static string DirectusFileEndpoint = "files";
        public static string DirectusDownloadCall = "?download&access_token=";

        [SerializeField]
        private List<string> downloadFileList = new List<string>();

        public void GetFiles(string token) {
            RestClient.DefaultRequestHeaders["Authorization"] = "Bearer " + token;
            currentRequest = new RequestHelper {
                Uri = DirectusManager.DirectusURL + DirectusFileEndpoint,
                Method = "GET",
                Timeout = 60,
                Retries = 5, //Number of retries
                RetrySecondsDelay = 2, //Seconds of delay to make a retry
                RetryCallback = (err, retries) => {
                    Debug.Log("Repeat error CheckDirectusActivity: " + err.Message);
                }, //See the error before retrying the request
                EnableDebug = true, //See logs of the requests for debug mode
            };

            RestClient.Request(currentRequest).Then(response => {
                var fileCollection = JsonUtility.FromJson<DirectusFiles.ContentCollection>(response.Text);
                foreach (var file in fileCollection.data) {
                    downloadFileList.Add(DirectusManager.DirectusURL + "assets/" + file.id + DirectusDownloadCall + token);
                }
            })
            .Catch(err => {
                Debug.Log("Directus error: " + err.Message);
            })
            .Done(GetFilesDone);
        }

        private void GetFilesDone() {
            //DirectusSignals.DIRECTUS_UPDATES_AVAILABE.Dispatch(true, downloadFileList.ToArray());
            Debug.Log("Done Download File Url");
        }
    }
}