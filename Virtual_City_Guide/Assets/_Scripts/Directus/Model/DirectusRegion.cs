using System;
using System.Collections.Generic;
using UnityEngine;

namespace Directus.Model {
    [Serializable]
    public class DirectusRegion {
        [Serializable]
        public class ContentItem {
            public string date_created;
            public string date_updated;
            public int ID;
            public string Name;
            public int DescriptionLocKey;
            public float Latitude;
            public float Longitude;
            public string AtlasReference;
            public string ImgReference;
            public string LogoImgReference;
            public int DefaultTour;
            public string HelpURL;
            public string ColorBackground;
            public string ColorForeground;
            public string ColorSelected;
            public string ColorUnselected;
            public string ColorTextDark;
            public string ColorTextLight;
            public int IsActive;
            public string HomepageURL;
            public int Order;
            public string AdReference;
            public string PreviewReference;
            public int ParentID;
        }

        [Serializable]
        public class ContentCollection {
            public List<ContentItem> data;
        }
    }
}