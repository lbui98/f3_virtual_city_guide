using System;
using System.Collections.Generic;

namespace Directus.Model {
    [Serializable]
    public class DirectusAdditionalContent {
        [Serializable]
        public class ContentItem {
            public string date_created;
            public string date_updated;
            public int ID;
            public int RegionID;
            public int POIID;
            public int NameLocKey;
            public int DescriptionLocKey;
            public string AtlasReference;
            public string PreviewImgReference;
            public string AssetReference;
            public int MediaType;
            public string RequiredVersion;
            public int RequiresPOIUnlocked;
        }

        [Serializable]
        public class ContentCollection {
            public List<ContentItem> data;
        }

    }
}