using System;
using System.Collections.Generic;

namespace Directus.Model
{
    [Serializable]
    public class DirectusFiles
    {
        [Serializable]
        public class ContentItem
        {
            public string charset;
            public string description;
            public string duration;
            public string embed;
            public string filename_disk;
            public string filename_download;
            public string filesize;
            public string folder;
            public string height;
            public string id;
            public string location;
            public string metadata;
            public string modified_by;
            public string modified_on;
            public string storage;
            public string tags;
            public string title;
            public string type;
            public string uploaded_by;
            public string uploaded_on;
            public string width;
        }
        
        [Serializable]
        public class ContentCollection
        {
            public ContentItem[] data;
        }
        
    }
}

