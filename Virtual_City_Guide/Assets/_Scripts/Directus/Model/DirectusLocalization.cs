using System;
using System.Collections.Generic;
using UnityEngine;

namespace Directus.Model {
    [Serializable]
    public class DirectusLocalization {
        [Serializable]
        public class ContentItem {
            public int ID;
            public string date_created;
            public string date_updated;
            public string Key;
            public string DE;
            public string EN;
            public string FR;
            public string CN;
            public string JP;
            public string ES;
        }

        [Serializable]
        public class ContentCollection {
            public List<ContentItem> data;
        }
    }
}