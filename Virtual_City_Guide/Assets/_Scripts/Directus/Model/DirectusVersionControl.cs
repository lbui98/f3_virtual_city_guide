using System;
using System.Collections.Generic;
using UnityEngine;

namespace Directus.Model {
    [Serializable]
    public class DirectusVersionControl {
        [Serializable]
        public class ContentItem {
            public int ID;
            public string date_created;
            public string date_updated;
            public string Version;
        }

        [Serializable]
        public class ContentCollection {
            public List<ContentItem> data;
        }
    }
}