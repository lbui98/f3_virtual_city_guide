using System;
using System.Collections.Generic;

namespace Directus.Model {
    [Serializable]
    public class DirectusAdditionalContentPOI {
        [Serializable]
        public class ContentItem {
            public int id;
            public int AdditionalContent2_ID;
            public int POI_ID;
        }

        [Serializable]
        public class ContentCollection {
            public List<ContentItem> data;
        }
    }
}