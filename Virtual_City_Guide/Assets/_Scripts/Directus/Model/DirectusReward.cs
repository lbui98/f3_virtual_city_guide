using System;
using System.Collections.Generic;

namespace Directus.Model {
    [Serializable]
    public class DirectusReward {
        [Serializable]
        public class ContentItem {
            public string date_created;
            public string date_updated;
            public int ID;
            public int RegionID;
            public string Code;
            public int RequiredPoints;
            public string RewardMessage;
        }

        [Serializable]
        public class ContentCollection {
            public List<ContentItem> data;
        }
    }
}