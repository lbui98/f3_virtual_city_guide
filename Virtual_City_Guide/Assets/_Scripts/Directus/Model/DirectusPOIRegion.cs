using System;
using System.Collections.Generic;

namespace Directus.Model {
    [Serializable]
    public class DirectusPOIRegion {
        [Serializable]
        public class ContentItem {
            public int id;
            public int POI_ID;
            public int Region_ID;
        }

        [Serializable]
        public class ContentCollection {
            public List<ContentItem> data;
        }
    }
}