using System;
using System.Collections.Generic;

namespace Directus.Model
{
    [Serializable]
    public class DirectusActivity
    {
        [Serializable]
        public class ContentItem
        {
            public string action;
            public string collection;
            public string comment;
            public int id;
            public string ip;
            public string item;
            public string timestamp;
            public string user;
            public string user_agent;
            public List<int> revisions;
        }

        [Serializable]
        public class ContentCollection
        {
            public ContentItem[] data;
        }
    }
}


