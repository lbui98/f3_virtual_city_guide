using System;

namespace Directus.Model
{
    [Serializable]
    public class DirectusCollectionCount
    {
        [Serializable]
        public class ContentCount
        {
            public int id;
        }
        
        [Serializable]
        public class ContentItem
        {
            public ContentCount count;
        }

        [Serializable]
        public class ContentCollection
        {
            public ContentItem[] data;
        }
    }
}