using System;
using System.Collections.Generic;

namespace Directus.Model {
    [Serializable]
    public class DirectusAchievements {
        [Serializable]
        public class ContentItem {
            public string date_created;
            public string date_updated;
            public int ID;
            public int RegionID;
            public int NameLocKey;
            public int DescriptionLocKey;
            public string AtlasReference;
            public string ImgReference;
            public string ConditionsJSON;
            public int IsRegional;
        }

        [Serializable]
        public class ContentCollection {
            public List<ContentItem> data;
        }
    }
}