using System;
using System.Collections.Generic;

namespace Directus.Model {
    [Serializable]
    public class DirectusPOI {
        [Serializable]
        public class ContentItem {
            public string date_created;
            public string date_updated;
            public int ID;
            public int RegionID;
            public int NameLocKey;
            public int DescriptionLocKey;
            public int MoreDescriptionLocKey;
            public int ContactInfoLocKey;
            public float Latitude;
            public float Longitude;
            public int Perimeter;
            public string AtlasReference;
            public string ImgReference;
            public string MediaGuideHtmlReference;
            public string WebsiteURL;
            public string SponsorReference;
            public string Telephone;
            public int UnlockPoints;
            public int Order;
            public int Type;
            public string StampAtlasReference;
            public string StampReference;
            public string StampColor;

        }

        [Serializable]
        public class ContentCollection {
            public List<ContentItem> data;
        }
    }
}