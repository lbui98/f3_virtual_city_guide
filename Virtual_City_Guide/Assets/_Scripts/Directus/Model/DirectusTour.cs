using System;
using System.Collections.Generic;

namespace Directus.Model {
    [Serializable]
    public class DirectusTour {
        [Serializable]
        public class ContentItem {
            public string date_created;
            public string date_updated;
            public int ID;
            public int RegionID;
            public int NameLocKey;
            //public string DescriptionLocKey;
            public int TourType;
            public string AtlasReference;
            public string ImgPreviewReference;
            public string POIIDsJSON;
        }

        [Serializable]
        public class ContentCollection {
            public List<ContentItem> data;
        }
    }
}