using System;

namespace Directus.Model
{
    [Serializable]
    public class DirectusToken
    {
        [Serializable]
        public class Data
        {
            public string access_token;
            public int expires;
            public string refresh_token;
        }

        [Serializable]
        public class RootObject
        {
            public Data data;
            public Data expires;
            public Data refresh_token;
        }
    }
}