using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using bunLace.Singleton;
using Proyecto26;
using Directus;
using Directus.Model;

public class DirectusManager : PersistentSingleton<DirectusManager> {
    /// <summary>
    /// Directus API URL
    /// </summary>
    public const string DirectusURL = "http://212.227.71.52:8055/";
    /// <summary>
    /// App User Login
    /// </summary>
    public const string User = "info@stempelpass.de";
    public const string Password = "6vQ$@r44fueZBjFd4!lkZ";
    /// <summary>
    /// Directus API Call - last Activity
    /// </summary>
    public const string DirectusLastActivity = "items/VersionControl";
    //public const string DirectusLastActivity = "activity?filter={\"_and\":[{\"_or\":[{\"action\":{\"_eq\": \"create\"}},{\"action\":{\"_eq\":\"update\"}}]},{\"id\":{\"_gt\":0}}]}&sort=-id&limit=1";
    /// <summary>
    /// Directus API Call - get collection length
    /// </summary>
    public const string DirectusCollectionCount = "?aggregate[count]=id";
    /// <summary>
    /// Player Prefs Directus Last Activity Id
    /// </summary>
    public const string DirectusPlayerPrefs = "directus";

    /// <summary>
    /// Directus Collection Endpoints
    /// </summary>
    public string[] DirectusCollections = { "Achievements", "AdditionalContent", "POI", "Tour", "Region", "Localization", "Reward", "VersionControl" };


    public delegate void AuthenticateSuccess(string token);
    public static event AuthenticateSuccess OnAuthenticateSuccess;


    private string currentToken;

    private class DirectusUser {
        public string email;
        public string password;
    }

    public void Authenticate() {
        var currentRequest = new RequestHelper {
            Uri = DirectusURL + "auth/login",
            Method = "GET",
            Timeout = 60,
            Body = new DirectusUser {
                email = User,
                password = Password
            },
            EnableDebug = true //See logs of the requests for debug mode
        };

        RestClient.Post<DirectusToken.RootObject>(currentRequest)
        .Then(response => {
            //clear the default query string params for all requests
            RestClient.ClearDefaultParams();

            currentToken = response.data.access_token;




            Debug.Log("Directus token: " + currentToken);
            OnAuthenticateSuccess?.Invoke(currentToken);
        })
        .Catch(err => {
            OnAuthenticateSuccess?.Invoke(string.Empty);
        });
    }
}
