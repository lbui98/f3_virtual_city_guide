using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SlidingPuzzleManager2D))]
public class SlidingPuzzleManagerEditor : Editor {
    public override void OnInspectorGUI() {
        base.OnInspectorGUI();

        SlidingPuzzleManager2D manager = (SlidingPuzzleManager2D)target;
        if (GUILayout.Button("Set Original Puzzle Positions")) {
            manager.SetOriginalPositions();
        }
        if (GUILayout.Button("Shuffle)")) {
            manager.Shuffle();
        }
    }
}
